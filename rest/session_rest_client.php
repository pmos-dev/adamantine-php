<?php
namespace Adamantine\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "core.php";
require_once ABSTRACTION_ROOT_PATH . "rest/key_rest_client.php";

use \Abstraction\Data as Data;
use \Abstraction\REST as REST;

/**
 * @internal
 */
class SessionRESTClient_Exception extends REST\KeyRESTClient_Exception {}

class SessionRESTClient extends REST\KeyRESTClient {
	const METHOD_SESSION = "session";
	
	private $session;
	private $key;
	
	public function __construct($root_api_url, $postdata_format, $key) {
		parent::__construct($root_api_url, $postdata_format, REST\KeyRESTClient::TYPE_BASE62, $key);
		
		$this->key = $key;
		$this->session = null;
	}
	
	public function set_session(array $session) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $session["uid"])) throw new SessionRESTClient_Exception("Session UID is not Base62");
		$this->session = $session;
	}
	
	public function get($path, array $params) {
		if ($this->session === null) throw new SessionRESTClient_Exception("No session UID currently set");
		$this->set_authorisation(self::METHOD_SESSION, $this->session["uid"]);

		return parent::get($path, $params);
	}
	
	public function head($path, array $params) {
		if ($this->session === null) throw new SessionRESTClient_Exception("No session UID currently set");
		$this->set_authorisation(self::METHOD_SESSION, $this->session["uid"]);
		
		return parent::head($path, $params);
	}
	
	public function post($path, array $data) {
		if ($this->session === null) throw new SessionRESTClient_Exception("No session UID currently set");
		$this->set_authorisation(self::METHOD_SESSION, $this->session["uid"]);
		
		return parent::post($path, $data);
	}
	
	public function put($path, array $data) {
		if ($this->session === null) throw new SessionRESTClient_Exception("No session UID currently set");
		$this->set_authorisation(self::METHOD_SESSION, $this->session["uid"]);
		
		return parent::put($path, $data);
	}
	
	public function patch($path, array $data) {
		if ($this->session === null) throw new SessionRESTClient_Exception("No session UID currently set");
		$this->set_authorisation(self::METHOD_SESSION, $this->session["uid"]);
		
		return parent::patch($path, $data);
	}
	
	public function delete($path, array $params) {
		if ($this->session === null) throw new SessionRESTClient_Exception("No session UID currently set");
		$this->set_authorisation(self::METHOD_SESSION, $this->session["uid"]);
		
		return parent::delete($path, $params);
	}
	
	public function authenticate(array $data) {
		$this->set_authorisation(self::METHOD_KEY, $this->key);
		
		if (null === ($session = parent::post("/sessions", $data))) throw new SessionRESTClient_Exception("Invalid session returned (not object)");
		if (!is_array($session) || !Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $session["uid"])) throw new SessionRESTClient_Exception("Invalid session returned (no uid)");
		
		$this->session = $session;
		
		return $this->session;
	}
	
	public function validate() {
		try {
			$this->get("/sessions/validate");
			return true;
		} catch(\Exception $e) {
			return false;
		}
	}
	
	public function logoff() {
		try {
			$this->delete("/sessions", array());
		// @phpcs:ignore Generic.CodeAnalysis.EmptyStatement
		} catch(\Exception $e) {}
		
		$this->session = null;
	}
}
