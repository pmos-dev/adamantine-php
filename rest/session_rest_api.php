<?php
namespace Adamantine\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "rest/express.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH is not set");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";

require_once ADAMANTINE_ROOT_PATH . "rest/key_rest_api.php";

use \Abstraction\Data as Data;
use \Abstraction\REST as REST;
use \Abstraction\Database as Database;

use \Adamantine\Models as Models;

abstract class SessionRESTAPI extends KeyRESTAPI {
	const METHOD_SESSION = "session";
	
	private $session_model;
	private $unique_named_helper;
	
	private $authorisation_method = null;
	
	private $session;
	
	public function __construct(
			\Abstraction\Renderer\REST\REST $rest,
			Database\Wrapper $database,
			Models\Repository $repository,
			Models\UniqueNamed $key_model,
			Models\TempSession $session_model
	) {
		parent::__construct($rest, $database, $repository, $key_model, KeyRESTAPI::TYPE_BASE62);

		$this->session_model = $session_model;
		
		$this->unique_named_helper = Models\UniqueNamedHelper::build($database);
		
		$this->setup();
	}
	
	public static function get_authorisation_by_session() {
		$authorisation = RESTAPI::get_authorisation();

		if ($authorisation["method"] === self::METHOD_SESSION) {
			if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $authorisation["value"])) throw new REST\Error_BadRequest("Key is not Base62");
			return array("method" => self::METHOD_SESSION, "uid" => $authorisation["value"]);
		}
		
		throw new REST\Error_Forbidden("Unsupported authorisation method");
	}
	
	public static function get_authorisation_by_key_or_session() {
		$authorisation = RESTAPI::get_authorisation();
		
		if ($authorisation["method"] === self::METHOD_KEY) return KeyRESTAPI::get_authorisation_by_key(self::TYPE_BASE62);
		if ($authorisation["method"] === self::METHOD_SESSION) return self::get_authorisation_by_session();
		
		throw new REST\Error_Forbidden("Unsupported authorisation method");
	}
	
	protected function get_installed_id() {
		$authorisation = self::get_authorisation_by_key_or_session();

		$this->authorisation_method = $authorisation["method"];
		
		if ($authorisation["method"] === self::METHOD_KEY) return parent::get_installed_id();
		
		if (null === ($this->session = $this->unique_named_helper->get_firstclass_by_name($this->session_model, $authorisation["uid"], "uid"))) throw new REST\Error_Forbidden("Access denied for session UID");
		
		return $this->session["installed"];
	}
	
	public final function is_via_key() {
		return $this->authorisation_method === self::METHOD_KEY;
	}
	
	public final function is_via_session() {
		return $this->authorisation_method === self::METHOD_SESSION;
	}
	
	public final function get_session() {
		return $this->session;
	}
	
	protected abstract function authenticate(array $values, array $data);
	protected abstract function resume();
	
	// this has to be protected so that the HybridRESTAPI can access it
	protected function monkey_patch_code($code) {
		return function (array $values, array $queries_or_data, RESTAPI $restthis) use ($code) {
			if (!$this->is_via_session()) throw new REST\Error_BadRequest("Trying to resume a session without session authorisation");
			
			// recheck session still exists in case it was expired during authentication
			if (null === $this->session_model->get_by_firstclass_and_uid($this->get_installed(), $this->session["uid"])) throw new REST\Error_Forbidden("Session has expired");
			
			$this->session_model->touch($this->get_installed(), $this->session);
			
			$this->resume();
			
			return $code($values, $queries_or_data, $restthis);
		};
	}

	// For all these, use RESTAPI:: rather than parent::, so that the key authentication is skipped (i.e. session auth used instead)
	// NB, the RESTAPI:: syntax is NOT a static method, rather a bizarre PHP syntax notation to use a named grand-parent method.
	
	public function head($path, $code) {
		RESTAPI::head($path, SessionRESTAPI::monkey_patch_code($code));
	}
	
	public function get($path, $code) {
		RESTAPI::get($path, SessionRESTAPI::monkey_patch_code($code));
	}
	
	public function post($path, $code) {
		RESTAPI::post($path, SessionRESTAPI::monkey_patch_code($code));
	}
	
	public function put($path, $code) {
		RESTAPI::put($path, SessionRESTAPI::monkey_patch_code($code));
	}
	
	public function patch($path, $code) {
		RESTAPI::patch($path, SessionRESTAPI::monkey_patch_code($code));
	}
	
	public function delete($path, $code) {
		RESTAPI::delete($path, SessionRESTAPI::monkey_patch_code($code));
	}
	
	protected abstract function start_new_session(array $values);
	protected function stop_session() {
		return $this->session_model->stop($this->get_installed(), $this->session);
	}
	
	private function setup() {
		// use parent so that key authentication is used
		// @phpcs:ignore Generic.CodeAnalysis.UnusedFunctionParameter
		parent::post("/sessions", function (array $values, array $data, RESTAPI $ignore) {
			if (!$this->is_via_key()) throw new REST\Error_Forbidden("Trying to authenticate without key authorisation");

			if (!$this->authenticate($values, $data)) throw new REST\Error_Forbidden("Access denied");
			$this->session = $this->start_new_session($values);
			
			return $this->session;
		});

		// use $this so that key authentication is not used
		// @phpcs:ignore Generic.CodeAnalysis.UnusedFunctionParameter
		$this->get("/sessions/validate", function (array $values, array $ignore1, RESTAPI $ignore2) {
			return true;
		});

		// ditto last comment
		// @phpcs:ignore Generic.CodeAnalysis.UnusedFunctionParameter
		$this->delete("/sessions", function (array $values, array $ignore1, RESTAPI $ignore2) {
			return $this->stop_session();
		});
	}
}
