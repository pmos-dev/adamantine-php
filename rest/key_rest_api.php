<?php
namespace Adamantine\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "rest/express.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH is not set");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";

require_once ADAMANTINE_ROOT_PATH . "rest/rest_api.php";

use \Abstraction\Data as Data;
use \Abstraction\REST as REST;
use \Abstraction\Database as Database;

use \Adamantine\Models as Models;

abstract class KeyRESTAPI extends RESTAPI {
	const TYPE_BASE36 = "base36";
	const TYPE_BASE62 = "base62";
	
	const METHOD_KEY = "key";
	
	private $key_model;
	private $unique_named_helper;
	private $type;
	
	private $key;
	
	public function __construct(
			\Abstraction\Renderer\REST\REST $rest,
			Database\Wrapper $database,
			Models\Repository $repository,
			Models\UniqueNamed $key_model,
			$type
	) {
		parent::__construct($rest, $database, $repository);

		$this->key_model = $key_model;

		if (!Data\Data::validate_option(array(self::TYPE_BASE36, self::TYPE_BASE62), $type)) throw new \Adamantine\Exception("Key type is not valid");
		$this->type = $type;
		
		$this->unique_named_helper = Models\UniqueNamedHelper::build($database);
	}
	
	public function get_type() {
		return $this->type;
	}
	
	public final function get_key() {
		return $this->key;
	}
	
	public static function get_authorisation_by_key($type) {
		$authorisation = RESTAPI::get_authorisation();
		
		if ($authorisation["method"] === self::METHOD_KEY) {
			// No need for a default catch as the validate_option in the constructor guarantees the type
			switch ($type) {
				case self::TYPE_BASE36:
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE36_ID, $authorisation["value"])) throw new REST\Error_BadRequest("Key is not Base36");
					break;
				case self::TYPE_BASE62:
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $authorisation["value"])) throw new REST\Error_BadRequest("Key is not Base62");
					break;
			}
			
			return array("method" => self::METHOD_KEY, "key" => $authorisation["value"]);
		}
		
		throw new REST\Error_Forbidden("Unsupported authentication method");
	}
	
	protected function get_key_from_authorisation() {
		$authorisation = self::get_authorisation_by_key($this->type);
		
		return $this->unique_named_helper->get_firstclass_by_name($this->key_model, $authorisation["key"], "key");
	}
	
	protected function get_installed_id() {
		if (null === ($this->key = $this->get_key_from_authorisation())) throw new REST\Error_Forbidden("Access denied for key");
		
		return $this->key["installed"];
	}

	// this has to be protected so that the HybridRESTAPI can access it
	protected function monkey_patch_code($code) {
		return function (array $values, array $queries_or_data, RESTAPI $restthis) use ($code) {
			// check key exists, in case a superclass has allowed init without one
			if (null === ($this->key = $this->get_key_from_authorisation())) throw new REST\Error_Forbidden("Access denied");
			
			return $code($values, $queries_or_data, $restthis);
		};
	}
	
	public function head($path, $code) {
		parent::head($path, KeyRESTAPI::monkey_patch_code($code));
	}
	
	public function get($path, $code) {
		parent::get($path, KeyRESTAPI::monkey_patch_code($code));
	}
	
	public function post($path, $code) {
		parent::post($path, KeyRESTAPI::monkey_patch_code($code));
	}
	
	public function put($path, $code) {
		parent::put($path, KeyRESTAPI::monkey_patch_code($code));
	}
	
	public function patch($path, $code) {
		parent::patch($path, KeyRESTAPI::monkey_patch_code($code));
	}
	
	public function delete($path, $code) {
		parent::delete($path, KeyRESTAPI::monkey_patch_code($code));
	}
	
}
