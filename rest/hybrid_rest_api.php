<?php
namespace Adamantine\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "rest/express.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH is not set");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";

require_once ADAMANTINE_ROOT_PATH . "rest/key_rest_api.php";
require_once ADAMANTINE_ROOT_PATH . "rest/session_rest_api.php";

use \Abstraction\Data as Data;
use \Abstraction\REST as REST;
use \Abstraction\Database as Database;

use \Adamantine\Models as Models;

abstract class HybridRESTAPI extends SessionRESTAPI {
	
	protected function monkey_patch_code($code) {
		// @phpcs:ignore Generic.CodeAnalysis.UnusedFunctionParameter
		return function (array $values, array $queries_or_data, RESTAPI $restthis) use ($code) {
			if ($this->is_via_key()) {
				$patched = KeyRESTAPI::monkey_patch_code($code);
				return $patched($values, $queries_or_data, $this);
			}
			if ($this->is_via_session()) {
				$patched = SessionRESTAPI::monkey_patch_code($code);
				return $patched($values, $queries_or_data, $this);
			}
			
			throw new REST\Error_Forbidden("No authentication supplied");
		};
	}

	// For all these, use RESTAPI:: rather than parent::, so that the key authentication is skipped (i.e. session auth used instead)
	// NB, the RESTAPI:: syntax is NOT a static method, rather a bizarre PHP syntax notation to use a named grand-parent method.
	
	public function head($path, $code) {
		RESTAPI::head($path, HybridRESTAPI::monkey_patch_code($code));
	}
	
	public function get($path, $code) {
		RESTAPI::get($path, HybridRESTAPI::monkey_patch_code($code));
	}
	
	public function post($path, $code) {
		RESTAPI::post($path, HybridRESTAPI::monkey_patch_code($code));
	}
	
	public function put($path, $code) {
		RESTAPI::put($path, HybridRESTAPI::monkey_patch_code($code));
	}
	
	public function patch($path, $code) {
		RESTAPI::patch($path, HybridRESTAPI::monkey_patch_code($code));
	}
	
	public function delete($path, $code) {
		RESTAPI::delete($path, HybridRESTAPI::monkey_patch_code($code));
	}
}
