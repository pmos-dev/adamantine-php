<?php
namespace Adamantine\REST;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "rest/rest_api.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH is not set");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";

use \Abstraction\Data as Data;
use \Abstraction\REST as REST;
use \Abstraction\Database as Database;

use \Adamantine\Models as Models;

abstract class RESTAPI extends REST\RESTAPI {
	private $database;
	protected $repository;
	
	private $instance;
	private $installed;
	
	public function __construct(
			\Abstraction\Renderer\REST\REST $rest,
			Database\Wrapper $database,
			Models\Repository $repository
	) {
		parent::__construct($rest);

		$this->database = $database;
		$this->repository = $repository;
	}
	
	private function get_instance_and_installed_by_installed_id($id) {
		$installeds = $this->database->select_rows_by_conditions($this->repository->get("_core", "Installed")->get_table(), array(
				"instance" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array(
				"id" => new Database\Param($id, new Database\Type_Id(Database\Type::NOT_NULL))
		));
		if (sizeof($installeds) !== 1) throw new REST\Error_InternalServerError("No such installed exists");
		$installed = $installeds[0];
		
		if (null === ($instance = $this->repository->get("_core", "Instance")->get_by_id($installeds[0]["instance"]))) throw new REST\Error_InternalServerError("No such instance exists");
		
		// report instance disabled/locked before installed disabled/locked
		if ($instance["status"] === Models\Installed::DISABLED) throw new REST\Error_ServiceUnavailable("This instance is disabled");
		if ($instance["status"] === Models\Installed::LOCKED) throw new REST\Error_ServiceUnavailable("This instance is locked");
		
		// load the full model data
		$installed = $this->repository->get("_core", "Installed")->get_by_firstclass_and_id($instance, $id);
		
		if ($installed["status"] === Models\Installed::DISABLED) throw new REST\Error_ServiceUnavailable("This area is disabled");
		if ($installed["status"] === Models\Installed::LOCKED) throw new REST\Error_ServiceUnavailable("This area is locked");
		
		return array($instance, $installed);
	}
	
	protected abstract function get_installed_id();
	protected abstract function init();
	
	public final function get_instance() {
		return $this->instance;
	}
	
	public final function get_installed() {
		return $this->installed;
	}
	
	public final function get_repository() {
		return $this->repository;
	}
	
	public final function get_database() {
		return $this->database;
	}
	
	private function monkey_patch_code($code) {
		return function (array $values, array $queries_or_data) use ($code) {
			$installed_id = $this->get_installed_id();
			if (!Data\Data::validate_id($installed_id)) throw new REST\Error_InternalServerError("Unable to obtain initial installed ID");
			
			list($instance, $installed) = $this->get_instance_and_installed_by_installed_id($installed_id);
			$this->instance = $instance;
			$this->installed = $installed;
			
			$this->init();
		
			return $code($values, $queries_or_data, $this);
		};
	}
	
	public function head($path, $code) {
		parent::head($path, $this->monkey_patch_code($code));
	}
	
	public function get($path, $code) {
		parent::get($path, $this->monkey_patch_code($code));
	}
	
	public function post($path, $code) {
		parent::post($path, $this->monkey_patch_code($code));
	}
	
	public function put($path, $code) {
		parent::put($path, $this->monkey_patch_code($code));
	}
	
	public function patch($path, $code) {
		parent::patch($path, $this->monkey_patch_code($code));
	}
	
	public function delete($path, $code) {
		parent::delete($path, $this->monkey_patch_code($code));
	}
}
