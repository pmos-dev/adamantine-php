<?php
/**
 * Adamantine root index page.
 * 
 * Redirects backwards to the APP_ROOT_PATH index page if an instance ID has been declared in the URL.
 * Otherwise, redirects to the select_instance page.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine;

define("SKIP_SESSION_INITIALIZE", true);
define("APP_ROOT_PATH", "./../");
require_once APP_ROOT_PATH . "adamantine/_init.php";

if (!isset($_GET["xsi"])) $_HTTP->redirect(APP_ROOT_PATH . "adamantine/select_instance.php");

$_HTTP->redirect(APP_ROOT_PATH . "?xsi={$_GET["xsi"]}");
