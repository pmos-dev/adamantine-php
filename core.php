<?php
/**
 * Core Exception class definitions.
 *
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine;

if (!defined("APP_ROOT_PATH")) die("APP_ROOT_PATH has not been set.");

use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * Root exception class within the Adamantine framework.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class Exception extends \Exception {
	/**
	 * Constructs a new exception.
	 * 
	 * If the EXCEPTIONS_SHOW_VALUES flag is set, then the contents of the $value parameter will be included in the error message.
	 * 
	 * @param string $message message to report
	 * @param string $value any context relevant value causing the exception, rendered using print_r
	 */
	public function __construct($message = "Unspecified exception", $value = "\t_NO_VALUE") {
		if (Config\EXCEPTIONS_SHOW_VALUES && $value !== "\t_NO_VALUE") {
			ob_start();
			print_r($value);
			$message .= ":" . substr(ob_get_clean(), 0, 4096);
		}

		error($message);
	}
}
