<?php
/**
 * Adamantine select instance page, for use if an instance ID is not specified in the URL.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine;

define("SKIP_SESSION_INITIALIZE", true);
define("APP_ROOT_PATH", "./../");
require_once APP_ROOT_PATH . "adamantine/_init.php";

use \Abstraction\Framework as Framework;
use \Abstraction\Renderer\HTML as HTML;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Select instance to use", Config\DEFAULT_SUBTITLE);

$page = $_UI->get_content();

$page->add(new HTML\Header("Publically visible instances", HTML\Header::LEVEL_3));

$page->add($table = new HTML\Table(null, "unitable"));
$table->add_column("name");
$table->add_column("description");
$table->add_column("status");

foreach ($_INSTANCE->list_visible() as $instance) {
	$row = $table->new_row();
	
	$name = $instance["name"];
	if ($instance["status"] !== Framework\Models\Instance::DISABLED) $name = new HTML\Link($instance["name"], APP_ROOT_PATH . "?xsi={$instance["id"]}", null, "command icon_go");
	$row->set_cell("name", $name);
	$row->set_cell("description", $instance["description"]);
	$row->set_cell("status", $instance["status"]);
}

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "search_instance_do.php", "GET", "search", "uniform simple autosize"));

$form->add_row("name", new HTML\Form_Text("", "name", "validate idname"), "name");
$form->add_submit("Search instance");

$_HTML->complete();
