<?php
/**
 * Adamantine system administration: View database table ID defragmentation
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Models as Models;
use \Abstraction\Renderer\HTML as HTML;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Table defragmentation", Config\DEFAULT_SUBTITLE);

$areas = $_AREA->list_all();
array_unshift($areas, array("name" => "_core"));

$areamodels = array();
foreach ($areas as $area) {
	if ($area["name"] !== "_core") require_once APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";
	if (!$_REPOSITORY->does_area_exist($area["name"])) continue;
	
	$areamodels[$area["name"]] = array();

	foreach ($_REPOSITORY->list_models_by_area($area["name"]) as $model) $areamodels[$area["name"]][$model] = $_REPOSITORY->get($area["name"], $model);
}

$page = $_UI->get_content();

$page->add(new HTML\Header("Fragmentation statistics", HTML\Header::LEVEL_3));

$page->add($table = new HTML\Table("stats", "unitable"));
$table->add_column("area");
$table->add_column("model");
$table->add_column("table");
$table->add_column("rows");
$table->add_column("max ID");
$table->add_column("action", "");

foreach ($areamodels as $area => $models) foreach ($models as $name => $model) {
	$row = $table->new_row();
	$row->set_cell("area", $area);
	$row->set_cell("model", $name);
	$row->set_cell("table", $model->get_table());
	
	$count = $model->count_all();
	$row->set_cell("rows", "{$count}");
	
	$row->set_cell("action", "");
	if (!($model instanceof Models\FirstClass)) {
		$row->set_cell("max ID", "n/a");
	}
	else {
		$row->set_cell("action", new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/tick.svg", null, "Fully compacted"));
		if ($count === 0) $row->set_cell("max ID", "-");
		else {
			$row->set_cell("max ID", $max = $model->get_max_id());
			if ($model instanceof Models\Hierarchy) {
				$row->set_cell("action", new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/database_error.svg", null, "Hierarchies cannot be defragmented."));
			} else {
				if ($count !== $max) $row->set_cell("action", new HTML\Link("Defragment", ADAMANTINE_ROOT_PATH . "sysadmin/defragment_do.php?area={$area}&model={$name}", null, "command icon_defrag"));
			}
		}
	}
}

$_HTML->complete();
