<?php
/**
 * Adamantine system administration: welcome page.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Renderer\HTML as HTML;

use \_APPLICATION_NAMESPACE_\Config as Config;
use \Adamantine as Adamantine;

$_UI->set_titles("System administration", Config\DEFAULT_SUBTITLE);

$page = $_UI->get_content();

$page->add(new HTML\Header("Welcome to the system administration area.", HTML\Header::LEVEL_3));

$_HTML->complete();
