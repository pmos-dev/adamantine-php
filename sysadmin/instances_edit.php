<?php
/**
 * Adamantine system administration: Edit instance
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Edit instance", Config\DEFAULT_SUBTITLE);

$path = ADAMANTINE_ROOT_PATH;
$_HTML->add_code("js", <<<JAVASCRIPT
var ADAMANTINE_ROOT_PATH = "{$path}";
JAVASCRIPT
);

if (!isset($_GET["instance"])) Adamantine\error("No instance ID sent");
if (!Data\Data::validate_id($_GET["instance"])) Adamantine\error("Bad instance ID sent");

if (null === ($instance = $_INSTANCE->get($_GET["instance"]))) Adamantine\error("No such instance exists");

$page = $_UI->get_content();

$page->add(new HTML\Header("Instance details", HTML\Header::LEVEL_3));

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "sysadmin/instances_edit_do.php", "POST", "details", "uniform autosize"));
$form->add_hidden("instance", $instance["id"]);

$form->add_row("Name", new HTML\Form_Text($instance["name"], "name", "validate idname"), "name");
$form->add_row("Description", new HTML\Form_Text($instance["description"], "description", "validate string"), "description");

$form->add_row("Status", $select = new HTML\Form_Select("status"), "status");

$select->add_option("disabled", null, $instance["status"] === "disabled");
$select->add_option("enabled", null, $instance["status"] === "enabled");
$select->add_option("locked", null, $instance["status"] === "locked");

$form->add_row("Visibility", $select = new HTML\Form_Select("visibility"), "visibility");

$select->add_option("visible", null, $instance["visibility"] === "visible");
$select->add_option("hidden", null, $instance["visibility"] === "hidden");

$_SYSTEM->load_installed_matrix($instance);

$form->add(new HTML\Header("Installed area statuses", HTML\Header::LEVEL_4));

$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/moveable.css");
$_HTML->add_include("jquery", ADAMANTINE_ROOT_PATH . "js/moveable.js");

$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/autoscroll.css");
$_HTML->add_include("js", ADAMANTINE_ROOT_PATH . "js/autoscroll.js");

$form->add($table = new HTML\Table("areas", "unitable moveable"));

$table->add_column("Area");
$table->add_column("Status");
$table->add_column("_movement", "", "moveable");

foreach ($_INSTALLED->list_ordered_by_firstclass($instance) as $installed) {
	$area = $_AREA->get_by_id($installed["area"]);

	$installed = $_SYSTEM->get_installed_by_area($area);
	
	$row = $table->new_row();
	
	$row->set_cell("Area", $area["name"]);
	
	if ($installed["status"] === null) $row->set_cell("Status", "-");
	else {
		$row->set_cell("Status", $select = new HTML\Form_Select("status_{$installed["id"]}"));
	
		$select->add_option("disabled", null, $installed["status"] === "disabled");
		$select->add_option("enabled", null, $installed["status"] === "enabled");
		$select->add_option("locked", null, $installed["status"] === "locked");
	}

	$row->set_cell("_movement", $movement = new HTML\MultiElements());

	$icons = array();
	$movement->add($icons[] = new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/bullet_arrow_top.svg", "Move item to top", ADAMANTINE_ROOT_PATH . "sysadmin/installed_move_do.php?direction=top&instance={$instance["id"]}&installed={$installed["id"]}", null, "top"));
	$movement->add($icons[] = new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/bullet_arrow_up.svg", "Move item up", ADAMANTINE_ROOT_PATH . "sysadmin/installed_move_do.php?direction=up&instance={$instance["id"]}&installed={$installed["id"]}", null, "up"));
	$movement->add($icons[] = new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/bullet_arrow_down.svg", "Move item down", ADAMANTINE_ROOT_PATH . "sysadmin/installed_move_do.php?direction=down&instance={$instance["id"]}&installed={$installed["id"]}", null, "down"));
	$movement->add($icons[] = new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/bullet_arrow_bottom.svg", "Move item to bottom", ADAMANTINE_ROOT_PATH . "sysadmin/installed_move_do.php?direction=bottom&instance={$instance["id"]}&installed={$installed["id"]}", null, "bottom"));
	foreach ($icons as $icon) $icon->get_classes()->add("movementicon");
}

$form->add_submit("Save changes");

$page->add($linkbar = new HTML\LinkBar());
$linkbar->add(new HTML\Link("Log in as root", ADAMANTINE_ROOT_PATH . "sysadmin/instances_root_do.php?instance={$instance["id"]}", null, "command icon_root"));
$linkbar->add(new HTML\Link("Become user", ADAMANTINE_ROOT_PATH . "sysadmin/instances_user.php?instance={$instance["id"]}", null, "command icon_user"));
$linkbar->add(new HTML\Link("Export backup", ADAMANTINE_ROOT_PATH . "sysadmin/export.php?instance={$instance["id"]}", null, "command icon_backup"));
$linkbar->add(new HTML\Link("Delete instance", ADAMANTINE_ROOT_PATH . "sysadmin/instances_delete_do.php?instance={$instance["id"]}", null, "command icon_delete"));

$page->add(new HTML\Header("Area installation", HTML\Header::LEVEL_3));

$page->add($table = new HTML\Table("installs", "unitable"));

$table->add_column("Area");
$table->add_column("Installation", "");

foreach ($_AREA->list_all() as $area) {
	$installed = $_SYSTEM->get_installed_by_area($area);
	
	$row = $table->new_row();
	
	$row->set_cell("Area", $area["name"]);
	$row->set_cell("Installation", $installed === null
		? new HTML\Link("Install", ADAMANTINE_ROOT_PATH . "sysadmin/instances_install_do.php?instance={$instance["id"]}&area={$area["id"]}", null, "command icon_install")
		: new HTML\Link("Uninstall", ADAMANTINE_ROOT_PATH . "sysadmin/instances_uninstall_do.php?instance={$instance["id"]}&installed={$installed["id"]}", null, "command icon_uninstall")
	);
}

$_HTML->complete();
