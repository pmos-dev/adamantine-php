<?php
/**
 * Adamantine system administration: Unbuild area from the system
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Display SQL for additional table", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["area"])) Adamantine\error("No area ID sent");
if (!Data\Data::validate_id($_GET["area"])) Adamantine\error("Bad area ID sent");

if (null === ($area = $_AREA->get($_GET["area"]))) Adamantine\error("No such area exists");

require APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";

$api = \Adamantine\Areas\Area_API::get_api_by_area_name($area["name"]);

$models = $_REPOSITORY->list_models_by_area($area["name"]);

$page = $_UI->get_content();

if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $_GET["model"])) {
	$page->add(new HTML\Header("Available models for {$area["name"]}", HTML\Header::LEVEL_3));
	
	$page->add($list = new HTML\ItemList(HTML\ItemList::UNORDERED, null, "unilist"));
	foreach ($models as $model) {
		$list->new_item(new HTML\Link($model, ADAMANTINE_ROOT_PATH . "sysadmin/areas_table.php?area={$area["id"]}&model={$model}", null, "command inline icon_go"));
	}
	
	$_HTML->complete();
}

if (null === ($model = $_REPOSITORY->get($area["name"], $_GET["model"]))) Adamantine\error("No such model exists");
$structure = $model->get_structure();

$_HTML->add_code("css", <<<CSS

pre {
	border: 1px solid #88b4dc;
	white-space: pre-wrap;
}

CSS
);


$page->add(new HTML\Header("SQL modification code", HTML\Header::LEVEL_3));

$page->add(new HTML\Pre($multi = new HTML\MultiElements()));

list($create, $foreign, $unique, $views) = $model->show_create_table_sql();

$multi->add(Data\Data::deindent($create) . "\r\n");
foreach ($foreign as $sql) $multi->add(Data\Data::deindent($sql) . "\r\n");
foreach ($unique as $sql) $multi->add(Data\Data::deindent($sql) . "\r\n");
foreach ($views as $sql) $multi->add(Data\Data::deindent($sql) . "\r\n");

$_HTML->complete();
