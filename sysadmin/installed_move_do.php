<?php
/**
 * Adamantine system administration: Re-order installed instance area
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Move installed area", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["instance"])) Adamantine\error("No instance ID sent");
if (!Data\Data::validate_id($_GET["instance"])) Adamantine\error("Bad instance ID sent");
if (null === ($instance = $_INSTANCE->get($_GET["instance"]))) Adamantine\error("No such instance exists");

if (!isset($_GET["installed"])) Adamantine\error("No installed ID sent");
if (!Data\Data::validate_id($_GET["installed"])) Adamantine\error("Bad installed ID sent");
if (null === ($installed = $_INSTALLED->get_by_firstclass_and_id($instance, $_GET["installed"]))) Adamantine\error("No such installed exists");

if (!isset($_GET["direction"])) Adamantine\error("No movement direction specified");
switch ($_GET["direction"]) {
	case "up":
	case "down":
	case "top":
	case "bottom":
		$_INSTALLED->move($installed, $_GET["direction"]);
		break;
	default:
		Adamantine\error("Invalid movement direction");
}

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/instances_edit.php?instance={$instance["id"]}");
