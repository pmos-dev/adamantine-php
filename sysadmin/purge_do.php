<?php
/**
 * Adamantine system administration: View database table ID defragmentation
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Models as Models;
use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Purge dirty area", Config\DEFAULT_SUBTITLE);

$areas = array();
foreach ($_DATABASE->query_params("SHOW TABLES") as $t) {
	list($table) = array_values($t);
	if (!preg_match("`^" . Config\DATABASE_PREFIX . "(.+)$`", $table, $array)) continue;
	
	list(, $table) = $array;
	
	if (preg_match("`^(areas|group_installed|groups|installeds|instances|logs|settings|user_group|user_installed|users|variables)($|__)`", $table)) continue;

	preg_match("`^([a-z0-9]{1,64})_(.+)$`", $table, $array);
	list(, $area, $table) = $array;

	if (!array_key_exists($area, $areas)) $areas[$area] = array();
	
	$view = null;
	if (preg_match("`^(.+?)__([A-Z0-9]{1,64}(?:_[A-Z0-9]{1,64})*)$`", $table, $array)) list(, $table, $view) = $array;
	
	if (!array_key_exists($table, $areas[$area]))	$areas[$area][$table] = array();
	if ($view !== null) $areas[$area][$table][] = $view;
}

foreach ($_AREA->list_all() as $area) {
	if (array_key_exists($area["name"], $areas)) unset($areas[$area["name"]]);
}

if (!Data\Data::validate_regex("`^[a-z0-9]{3,32}$`", $_GET["area"])) \Adamantine\error("Invalid area");
if (!array_key_exists($_GET["area"], $areas)) \Adamantine\error("No such area can be purged");

$area = $areas[$_GET["area"]];

$_UI->assert_confirmed("Purge the remains of this area? This can be very dangerous! Tables and view will be forcibly dropped without unbuilding!");

$named_tables = array();
$named_views = array();
foreach ($area as $table => $views) {
	$named_tables[] = Config\DATABASE_PREFIX . "{$_GET["area"]}_{$table}";
	foreach ($views as $view) {
		$named_views[] = Config\DATABASE_PREFIX . "{$_GET["area"]}_{$table}__{$view}";
	}
}

$sql = array();

// drop views prior to tables
for ($i = sizeof($named_views); $i-- > 0;) {
	$sql[] = "DROP VIEW IF EXISTS `{$named_views[$i]}`";
}

// iterate through each table twice in order to cope with not being able to know which foreign keys depend on each other.
for ($i = sizeof($named_tables); $i-- > 0;) for ($j = sizeof($named_tables); $j-- > 0;) {
	$sql[] = "DROP TABLE IF EXISTS `{$named_tables[$j]}`";
}

foreach ($sql as $query) {
	try {
		$_DATABASE->query_params($query);
	// @phpcs:ignore Generic.CodeAnalysis.EmptyStatement
	} catch(\Exception $e) {}
}

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/purge.php");
