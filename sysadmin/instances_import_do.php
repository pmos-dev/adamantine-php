<?php
/**
 * Adamantine system administration: Create new instance
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Framework as Framework;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

require_once APP_ROOT_PATH . "adamantine/areas/area_importexport.php";

use \Adamantine\Areas\ImportExport;

$_UI->set_titles("Add new instance", Config\DEFAULT_SUBTITLE);

set_time_limit(0);
ini_set("memory_limit", "256M");

$areas = Data\Data::build_firstclass_lookup($_AREA->list_all());

$_IMPORTEXPORT = array();
foreach (array_values($areas) as $area) {
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";
	
	if (!file_exists(APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php")) continue;
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php";
	
	$area_lookup[$area["name"]] = $area;
}

if (isset($_FILES["file"]) && ($_FILES["file"]["name"] != "") && ($_FILES["file"]["size"] > 0)) {
	$_POST["code"] = file_get_contents($_FILES["file"]["tmp_name"]);
}

$lines = explode("\n", $_POST["code"]);

// start a transaction so if any failures occur the entire import is aborted
$_DATABASE->transaction_begin();

$xsi = null;

foreach ($lines as $line) {
	if ("" === ($line = trim($line))) continue;
	if (substr($line, 0, 1) === "#") continue;

	// reset timeout for each legitimate line
	set_time_limit(10);
	
	if (!preg_match("`^([-a-z0-9_.]{1,63})\s`iD", $line, $array)) Adamantine\error("Syntax error: {$line}");
	list(, $area) = $array;
	$area = strtolower($area);

	if ($area === "sysadmin") {
		if (!preg_match("`^SYSADMIN (.+)$`i", $line, $array)) Adamantine\error("Syntax error: {$line}");
		list(, $line) = $array;
		
		$line .= " ";	// this helps with simplifying regular expression matching
		
		if (preg_match("`^CREATE INSTANCE`i", $line)) {
			if (null === ($name = ImportExport::extract_field("NAME", $line))) Adamantine\error("No instance name supplied");
			if (!Data\Data::validate_regex("`^[a-zA-Z0-9]+$`D", $name)) Adamantine\error("Bad instance name");
			if (null !== $_INSTANCE->get_by_name($name)) Adamantine\error("An instance with that name already exists");
			
			if (null === ($description = ImportExport::extract_field("DESCRIPTION", $line))) Adamantine\error("No instance description supplied");
			if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $description)) Adamantine\error("Bad instance description");

			if (null === ($visibility = ImportExport::extract_field("VISIBILITY", $line))) Adamantine\error("No instance visibility supplied");
			if (!Data\Data::validate_option(array(Framework\Models\Instance::VISIBLE, Framework\Models\Instance::HIDDEN), $visibility)) Adamantine\error("Invalid visibility supplied");
				
			if (null === ($status = ImportExport::extract_field("STATUS", $line))) Adamantine\error("No instance status supplied");
			if (!Data\Data::validate_option(array(Framework\Models\Instance::DISABLED, Framework\Models\Instance::ENABLED, Framework\Models\Instance::LOCKED), $status)) Adamantine\error("Invalid status supplied");
			
			$instance = $_SYSTEM->create_instance(array(
				"name" => $name,
				"description" => $description
			));

			$instance["status"] = $status;
			$instance["visibility"] = $visibility;
			
			$_INSTANCE->update($instance);
			
			$_ACCESS->setup_instance($instance, array(
				"name" => "system"
			), array(
				"name" => "everyone"
			), array(
				"name" => "guest",
				"fullname" => "(guest user)"
			), array(
				"name" => "root",
				"fullname" => "(root user)"
			));
		}

		if (preg_match("`^INSTALL AREA`i", $line)) {
			if (null === ($name = ImportExport::extract_field("INSTANCE", $line))) Adamantine\error("No instance name supplied");
			if (!Data\Data::validate_regex("`^[a-zA-Z0-9]+$`D", $name)) Adamantine\error("Bad instance name");
			if (null === ($instance = $_INSTANCE->get_by_name($name))) Adamantine\error("No such instance with that name exists");

			if (null === ($area = ImportExport::extract_field("AREA", $line))) Adamantine\error("No area name supplied");
			if (!Data\Data::validate_regex("`^[a-zA-Z0-9]+$`D", $area)) Adamantine\error("Bad area name");
			if (null === ($area = $_AREA->get_by_name($area))) Adamantine\error("No such area with that name exists");
			
			if (null === ($status = ImportExport::extract_field("STATUS", $line))) Adamantine\error("No instance status supplied");
			if (!Data\Data::validate_option(array(Framework\Models\Installed::DISABLED, Framework\Models\Installed::ENABLED, Framework\Models\Installed::LOCKED), $status)) Adamantine\error("Invalid instance area status supplied");
			
			$_SYSTEM->install($instance, $area);
			
			$_SYSTEM->load_installed_matrix($instance);
				
			if (null === ($installed = $_SYSTEM->get_installed_by_area($area))) Adamantine\error("The instance was not installed properly for some reason");

			$installed = $_INSTALLED->get_by_firstclass_and_id($instance, $installed["id"]);	// list_by_instance doesn't return proper INSTALLED model objects
			$installed["status"] = $status;
			$_INSTALLED->update($installed);
		}
		
		if (preg_match("`^USE INSTANCE`i", $line)) {
			if (null === ($name = ImportExport::extract_field("INSTANCE", $line))) Adamantine\error("No instance name supplied");
			if (!Data\Data::validate_regex("`^[a-zA-Z0-9]+$`D", $name)) Adamantine\error("Bad instance name");
			if (null === ($xsi = $_INSTANCE->get_by_name($name))) Adamantine\error("No such instance with that name exists");

			$_SESSIONMANAGER->initialize($xsi);
			$_SETTINGS->initialize($xsi);
			
			$xau = $_SESSIONMANAGER->get_active_user();
				
			if (null === ($root = $_USER->get_root($instance))) Adamantine\error("Unable to identify the root user for this instance");
			
			$md5 = $_USER->generate_one_time_pad_md5($root);
			$root["one_time_pad_md5"] = $md5;
			
			if (!$_SESSIONMANAGER->login_by_one_time_pad($root, $md5)) Adamantine\error("Unable to become root user for this instance");
			
			$user = $_SESSIONMANAGER->get_active_user();
			$user = $_USER->get_by_firstclass_and_id($xsi, $user["id"]);	// this is needed to reload the new one-time-pad expiry
		}
		
		continue;
	}
	
	if ($xsi === null) Adamantine\error("No USE INSTANCE statement has been encountered yet.");
	if (!array_key_exists($area, $area_lookup)) Adamantine\error("No such area is installed", $area);

	$line .= " ";	// this helps with simplifying regular expression matching
	try {
		if (!$_IMPORTEXPORT[$area_lookup[$area]["name"]]->import($line, false)) throw new Database\Exception("Inline exception");
	} catch(Database\Exception $exception) {
		Adamantine\error("There was an error processing this line: {$line}");
	}
}

$_DATABASE->transaction_commit();

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/instances_edit.php?instance={$instance["id"]}");
