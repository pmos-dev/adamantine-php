<?php
/**
 * Adamantine system administration: Create new instance
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Add new instance", Config\DEFAULT_SUBTITLE);

if (!isset($_POST["name"])) Adamantine\error("No instance name sent");
if (!Data\Data::validate_regex("`^[a-zA-Z0-9]+$`D", $_POST["name"])) Adamantine\error("Bad instance name");

if (null !== $_INSTANCE->get_by_name($_POST["name"])) Adamantine\error("An instance with that name already exists");

$instance = $_SYSTEM->create_instance(array(
	"name" => $_POST["name"],
	"description" => $_POST["name"]
));

$_ACCESS->setup_instance($instance, array(
	"name" => "system"
), array(
	"name" => "everyone"
), array(
	"name" => "guest",
	"fullname" => "(guest user)"
), array(
	"name" => "root",
	"fullname" => "(root user)"
));

$page = $_UI->get_content();

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/instances_edit.php?instance={$instance["id"]}");
