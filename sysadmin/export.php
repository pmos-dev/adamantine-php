<?php
/**
 * Adamantine system administration: Generate sysadmin password hash
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Full export", Config\DEFAULT_SUBTITLE);

$page = $_UI->get_content();

$page->add(new HTML\Header("Performing a full export may take a very long time and/or produce more data than the script is capable of handling!", HTML\Header::LEVEL_4, null, "warning"));

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "sysadmin/export_do.php", "post", "export", "uniform autosize"));

$row = $form->add_row("Full export", new HTML\Form_Checkbox(false, "full"), "full");
$row = $form->add_row("Download instead of display", new HTML\Form_Checkbox(true, "download"), "download");

$_HTML->add_code("jquery", <<<JQUERY

$("#full").change(function() {
	$(".export_item").prop("disabled", $(this).prop("checked"));
	if (!$(this).prop("checked")) return;
	$(".export_item").prop("checked", true);
});

JQUERY
);

$preselect_instance = array("id" => null);
$preselect_areas = array();
if (Data\Data::validate_id($_GET["instance"])) {
	if (null === ($preselect_instance = $_INSTANCE->get_by_id($_GET["instance"]))) Adamantine\error("No such instance exists");
	foreach ($_INSTALLED->list_by_firstclass($preselect_instance) as $installed) {
		$preselect_areas[] = $installed["area"];
	}
}

$instances = $_INSTANCE->list_all();
$areas = $_AREA->list_all();

$form->add(new HTML\Header("Areas", HTML\Header::LEVEL_3));
foreach ($areas as $area) {
	if (!file_exists(APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php")) continue;
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php";

	$row = $form->add_row($area["name"], new HTML\Form_Checkbox(in_array($area["id"], $preselect_areas), "area_{$area["id"]}", "export_item"), "area_{$area["id"]}");
	$row->get_classes()->add("optional");
}

$form->add(new HTML\Header("Instances", HTML\Header::LEVEL_3));
foreach ($instances as $instance) {
	$row = $form->add_row($instance["name"], new HTML\Form_Checkbox($preselect_instance["id"] === $instance["id"], "instance_{$instance["id"]}", "export_item"), "instance_{$instance["id"]}");
	$row->get_classes()->add("optional");
}

$form->add_submit("Export");

$_HTML->complete();
