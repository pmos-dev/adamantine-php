<?php
/**
 * Adamantine system administration: Install new area into instance
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Install area into instance", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["instance"])) Adamantine\error("No instance ID sent");
if (!Data\Data::validate_id($_GET["instance"])) Adamantine\error("Bad instance ID sent");
if (null === ($instance = $_INSTANCE->get($_GET["instance"]))) Adamantine\error("No such instance exists");

if (!isset($_GET["area"])) Adamantine\error("No area ID sent");
if (!Data\Data::validate_id($_GET["area"])) Adamantine\error("Bad area ID sent");
if (null === ($area = $_AREA->get($_GET["area"]))) Adamantine\error("No such area exists");

$_SYSTEM->install($instance, $area);

require APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";
require APP_ROOT_PATH . "areas/{$area["name"]}/_install.php";

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/instances_edit.php?instance={$instance["id"]}");
