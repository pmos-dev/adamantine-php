<?php
/**
 * Adamantine system administration: Save changes to instance
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Framework\Models as Models;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Edit instance", Config\DEFAULT_SUBTITLE);

if (!isset($_POST["instance"])) Adamantine\error("No instance ID sent");
if (!Data\Data::validate_id($_POST["instance"])) Adamantine\error("Bad instance ID sent");

if (null === ($instance = $_INSTANCE->get($_POST["instance"]))) Adamantine\error("No such instance exists");

if (!Data\Data::validate_regex("`^[a-zA-Z0-9]+$`D", $_POST["name"])) Adamantine\error("Instance name is invalid");
if (!Data\Data::validate_string($_POST["description"])) Adamantine\error("Instance description is invalid");

if (!Data\Data::validate_option(array(Models\Instance::ENABLED, Models\Instance::DISABLED, Models\Instance::LOCKED), $_POST["status"])) Adamantine\error("Instance status is invalid");
if (!Data\Data::validate_option(array(Models\Instance::VISIBLE, Models\Instance::HIDDEN), $_POST["visibility"])) Adamantine\error("Instance visibility is invalid");

$instance["name"] = $_POST["name"];
$instance["description"] = $_POST["description"];
$instance["status"] = $_POST["status"];
$instance["visibility"] = $_POST["visibility"];

$_INSTANCE->update($instance);

$_SYSTEM->load_installed_matrix($instance);

$areas = $_AREA->list_all();
foreach ($areas as $area) {
	$installed = $_SYSTEM->get_installed_by_area($area);
	
	if ($installed === null) continue;	// skip not installed areas

	if (!isset($_POST["status_{$installed["id"]}"])) Adamantine\error("Installed area status not sent");
	switch ($_POST["status_{$installed["id"]}"]) {
		case Models\Installed::ENABLED:
		case Models\Installed::DISABLED:
		case Models\Installed::LOCKED:
			break;
		default:
			Adamantine\error("Installed area status is invalid");
	}

	$installed = $_INSTALLED->get_by_firstclass_and_id($instance, $installed["id"]);	// list_by_instance doesn't return proper INSTALLED model objects
	$installed["status"] = $_POST["status_{$installed["id"]}"];
	$_INSTALLED->update($installed);
}

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/instances.php");
