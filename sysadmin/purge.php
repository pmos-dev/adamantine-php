<?php
/**
 * Adamantine system administration: View database table ID defragmentation
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Models as Models;
use \Abstraction\Renderer\HTML as HTML;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Purge dirty area", Config\DEFAULT_SUBTITLE);

$areas = array();
foreach ($_DATABASE->query_params("SHOW TABLES") as $t) {
	list($table) = array_values($t);
	if (!preg_match("`^" . Config\DATABASE_PREFIX . "(.+)$`", $table, $array)) continue;
	
	list(, $table) = $array;
	
	if (preg_match("`^(areas|group_installed|groups|installeds|instances|logs|settings|user_group|user_installed|users|variables)($|__)`", $table)) continue;

	preg_match("`^([a-z0-9]{1,64})_(.+)$`", $table, $array);
	list(, $area, $table) = $array;

	if (!array_key_exists($area, $areas)) $areas[$area] = array();
	
	$view = null;
	if (preg_match("`^(.+?)__([A-Z0-9]{1,64}(?:_[A-Z0-9]{1,64})*)$`", $table, $array)) list(, $table, $view) = $array;
	
	if (!array_key_exists($table, $areas[$area]))	$areas[$area][$table] = array();
	if ($view !== null) $areas[$area][$table][] = $view;
}

foreach ($_AREA->list_all() as $area) {
	if (array_key_exists($area["name"], $areas)) unset($areas[$area["name"]]);
}

$page = $_UI->get_content();

$page->add(new HTML\Header("Potentially dirty areas", HTML\Header::LEVEL_3));

$page->add($table = new HTML\Table("stats", "unitable"));
$table->add_column("area");
$table->add_column("tables");
$table->add_column("views");
$table->add_column("action", "");

foreach ($areas as $area => $tables) {
	$row = $table->new_row();
	$row->set_cell("area", $area);
	$row->set_cell("tables", sizeof($tables));
	
	$total = 0;
	foreach (array_values($tables) as $views) $total += sizeof($views);

	$row->set_cell("views", $total);
	
	$row->set_cell("action", new HTML\Link("Purge", ADAMANTINE_ROOT_PATH . "sysadmin/purge_do.php?area={$area}", null, "command icon_delete"));
}

$_HTML->complete();
