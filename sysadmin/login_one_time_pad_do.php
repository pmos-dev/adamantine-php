<?php
/**
 * Adamantine system administration: Log in as root to instance via one-time-pad
 * 
 * Part 2: load instance and log in using supplied pad.
 * This has to be split into two parts due to the need to effectively have two sessions at once (one for sysadmin and one for the instance).
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/_init.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Login", Config\DEFAULT_SUBTITLE);

if (!Data\Data::validate_id($_GET["user"])) Adamantine\error("Invalid user ID supplied");
if (null === ($user = $_USER->get($xsi, $_GET["user"]))) Adamantine\error("No such user exists");

if (!isset($_GET["md5"])) Adamantine\error("No MD5 one time pad sent");
if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_MD5, $_GET["md5"])) Adamantine\error("Invalid MD5 one time pad sent");

if (!$_SESSIONMANAGER->login_by_one_time_pad($user, $_GET["md5"])) Adamantine\error("Unable to log in with that one time pad.");

if (!Data\Data::checkbox_boolean($_GET["sysadmin"])) $_SESSIONMANAGER->log_core("Login via one time pad");

$_HTTP->redirect(APP_ROOT_PATH . "index.php?xsi={$xsi["id"]}");
