<?php
/**
 * Adamantine system administration: Delete instance
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Delete instance", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["instance"])) Adamantine\error("No instance ID sent");
if (!Data\Data::validate_id($_GET["instance"])) Adamantine\error("Bad instance ID sent");

if (null === ($instance = $_INSTANCE->get($_GET["instance"]))) Adamantine\error("No such instance exists");

$_UI->assert_confirmed("Deleting this instance will destroy all data associated within it and cannot be undone!");

$_INSTANCE->delete($instance);

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/instances.php");
