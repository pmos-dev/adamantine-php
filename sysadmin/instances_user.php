<?php
/**
 * Adamantine system administration: Generate sysadmin password hash
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Become user", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["instance"])) Adamantine\error("No instance ID sent");
if (!Data\Data::validate_id($_GET["instance"])) Adamantine\error("Bad instance ID sent");

if (null === ($instance = $_INSTANCE->get($_GET["instance"]))) Adamantine\error("No such instance exists");

$page = $_UI->get_content();

$page->add(new HTML\Header("Available users for this instance", HTML\Header::LEVEL_3));

$page->add($table = new HTML\Table(null, "unitable"));
$table->add_column("name");
$table->add_column("fullname");

foreach ($_USER->list_normal_by_instance($instance) as $user) {
	$row = $table->new_row();
	
	$row->set_cell("name", new HTML\Link($user["name"], ADAMANTINE_ROOT_PATH . "sysadmin/instances_user_do.php?instance=${_GET["instance"]}&user=${user["id"]}", null, "command inline icon_user"));
	$row->set_cell("fullname", $user["fullname"]);
}

$_HTML->complete();
