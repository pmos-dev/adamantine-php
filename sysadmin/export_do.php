<?php
/**
 * Adamantine system administration: Generate sysadmin password hash
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;

require_once APP_ROOT_PATH . "adamantine/areas/area_importexport.php";

use \Adamantine\Areas\ImportExport;

use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Full export", Config\DEFAULT_SUBTITLE);

$page = $_UI->get_content();

$_HTML->add_code("css", <<<CSS

pre {
	border: 1px solid #88b4dc;
	white-space: pre-wrap;
}
		
CSS
);

$_DOWNLOAD = Data\Data::checkbox_boolean($_POST["download"]);
$_FULL = Data\Data::checkbox_boolean($_POST["full"]);

if ($_DOWNLOAD) $_HTTP->download_headers("application/download", "adamantine_export_" . date("Y-m-d_H-i-s") . ".txt");
else ob_start();

ImportExport::comment("Adamantine export at " . date("Y-m-d H:i:s"));
ImportExport::gap();

set_time_limit(0);
ini_set("memory_limit", "256M");

$instances = $_INSTANCE->list_all();
$areas = Data\Data::build_firstclass_lookup($_AREA->list_all());

$_IMPORTEXPORT = array();
foreach (array_values($areas) as $area) {
	if (!$_FULL && !Data\Data::checkbox_boolean($_POST["area_{$area["id"]}"])) continue;
	
	if (!file_exists(APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php")) continue;
	
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php";
}

foreach ($instances as $instance) {
	if (!$_FULL && !Data\Data::checkbox_boolean($_POST["instance_{$instance["id"]}"])) continue;
	
	$_SESSIONMANAGER->set_session_instance_for_full_export($instance);
	$_SETTINGS->initialize($instance);

	ImportExport::line();
	
	ImportExport::write_line("SYSADMIN CREATE INSTANCE", array(
		"NAME" => array(ImportExport::SIMPLE, $instance["name"]),
		"DESCRIPTION" => array(ImportExport::SIMPLE, $instance["description"]),
		"VISIBILITY" => array(ImportExport::SIMPLE, $instance["visibility"]),
		"STATUS" => array(ImportExport::SIMPLE, $instance["status"]),
	));

	foreach ($_INSTALLED->list_by_firstclass($instance) as $installed) {
		$area = $areas[$installed["area"]];
		ImportExport::write_line("SYSADMIN INSTALL AREA", array(
			"INSTANCE" => array(ImportExport::SIMPLE, $instance["name"]),
			"AREA" => array(ImportExport::SIMPLE, $area["name"]),
			"STATUS" => array(ImportExport::SIMPLE, $installed["status"]),
		));
	}
	
	ImportExport::write_line("SYSADMIN USE", array(
		"INSTANCE" => array(ImportExport::SIMPLE, $instance["name"])
	));
	ImportExport::gap();
	
	foreach ($_INSTALLED->list_by_firstclass($instance) as $installed) {
		$area = $areas[$installed["area"]];
		if (!$_FULL && !Data\Data::checkbox_boolean($_POST["area_{$area["id"]}"])) continue;
		
		// reset timeout for each legitimate line
		set_time_limit(0);

		ImportExport::line();
		ImportExport::comment("Exports for {$area["name"]}");
		ImportExport::gap();
		
		$exports = $_IMPORTEXPORT[$area["name"]]->list_exports();
		if (sizeof($_VARIABLE->list_by_area($area)) > 0) {
			$exports["config"] = "Configuration settings";
		}
	
		foreach ($exports as $keyword => $caption) {
			$_IMPORTEXPORT[$area["name"]]->export($keyword);
			ImportExport::gap();
		}
	}
}

if ($_DOWNLOAD) {
	flush();
	die();
}

$page = $_UI->get_content();

$page->add(new HTML\Header("Export at " . date("Y-m-d H:i:s"), HTML\Header::LEVEL_3));

$page->add(new HTML\Pre($multi = new HTML\MultiElements()));
$multi->add(ob_get_clean());

$_HTML->complete();
