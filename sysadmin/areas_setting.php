<?php
/**
 * Adamantine system administration: Unbuild area from the system
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Display SQL for additional setting", Config\DEFAULT_SUBTITLE);

if (!isset($_REQUEST["area"])) Adamantine\error("No area ID sent");
if (!Data\Data::validate_id($_REQUEST["area"])) Adamantine\error("Bad area ID sent");

if (null === ($area = $_AREA->get($_REQUEST["area"]))) Adamantine\error("No such area exists");

require APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";

$api = \Adamantine\Areas\Area_API::get_api_by_area_name($area["name"]);

$page = $_UI->get_content();

if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $_POST["name"])) {
	$page->add(new HTML\Header("New field details", HTML\Header::LEVEL_3));
	
	$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "sysadmin/areas_setting.php", HTML\Form::SUBMIT_POST, null, "uniform autosize"));
	$form->add_hidden("area", $_GET["area"]);
	$form->add_row("Name", new HTML\Form_Text("", "name", "validate idname"), "name");
	$form->add_row("Description", new HTML\Form_Text("", "description", "validate string"), "description");
	$form->add_row("Type", $select = new HTML\Form_Select("type"), "type");
	$select->add_option(Adamantine\Models\Variable::INTEGER);
	$select->add_option(Adamantine\Models\Variable::UNSIGNED);
	$select->add_option(Adamantine\Models\Variable::BOOLEAN);
	$select->add_option(Adamantine\Models\Variable::STRING);
	$select->add_option(Adamantine\Models\Variable::SELECT);
	$form->add(new HTML\Header("Options are only for select type. Use the syntax: <val>:<desc>,<val>:<desc>,...", HTML\Header::LEVEL_4));
	$form->add_row("Options", new HTML\Form_Text("", "options", "validate string optional"), "options");
	$form->add_row("Default value", new HTML\Form_Text("", "default", "validate string optional"), "default");
	
	$form->add_submit("Generate SQL");
	
	$_HTML->complete();
}

$_POST["name"] = trim(strtoupper(str_replace(" ", "_", $_POST["name"])));

if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_TEXT_255, $_POST["description"])) Adamantine\error("Invalid description supplied");

if (!Data\Data::validate_option("integer,unsigned,boolean,string,select", $_POST["type"])) Adamantine\error("Invalid type supplied");
$json = $options = null;
if ($_POST["type"] === "select") {
	if (!Data\Data::validate_regex("`^(([a-z0-9]+):([ A-Za-z0-9-]+))(,(([a-z0-9]+):([ A-Za-z0-9-]+)))*$`", $_POST["options"])) Adamantine\error("Invalid option list specified");
	$options = array();
	foreach (explode(",", $_POST["options"]) as $option) {
		list($val, $desc) = explode(":", $option);
		$options[$val] = $desc;
	}
	
	$json = json_encode($options);
}

switch ($_POST["type"]) {
	case "integer":
		if (!Data\Data::validate_string($_POST["default"])) Adamantine\error("No default value specified");
		if (!Data\Data::validate_int($_POST["default"])) Adamantine\error("Default value is not an integer");
		$_POST["default"] = strval($_POST["default"]);
		break;
	case "unsigned":
		if (!Data\Data::validate_string($_POST["default"])) Adamantine\error("No default value specified");
		if (!Data\Data::validate_int($_POST["default"], 0)) Adamantine\error("Default value is not an unsigned integer");
		$_POST["default"] = strval($_POST["default"]);
		break;
	case "boolean":
		$_POST["default"] = Data\Data::checkbox_boolean($_POST["default"]);
		break;
	case "string":
		$_POST["default"] = Data\Data::trim_text_or_empty_string($_POST["default"]);
		break;
	case "select":
		if (!Data\Data::validate_option(array_keys($options), $_POST["default"])) Adamantine\error("Default value is not in the options list");
		break;
}

$encryptor = new Database\Type_Encrypted(Config\ENCRYPTION_PASSPHRASE, true, null);
$default = json_encode($_POST["default"]);
$encryptor->encrypt($default);
$default = bin2hex($default);

$_HTML->add_code("css", <<<CSS

pre {
	border: 1px solid #88b4dc;
	white-space: pre-wrap;
}

CSS
);

$page->add(new HTML\Header("SQL modification code", HTML\Header::LEVEL_3));

$page->add(new HTML\Pre($multi = new HTML\MultiElements()));

$multi->add(new HTML\Header("MySQL", HTML\Header::LEVEL_4));
$multi->add("\r\nINSERT INTO " . $_DATABASE->delimit($_VARIABLE->get_table()) . " (`name`, `description`, `type`, `options`, `default`, `area`) VALUES (" . <<<SQL
"{$_POST["name"]}", "{$_POST["description"]}", "{$_POST["type"]}", 
SQL
. ($json === null ? "NULL" : "\"" . addslashes($json) . "\"") . ", UNHEX(\"{$default}\"), {$area["id"]});\r\n");

$multi->add(new HTML\Header("Postgres", HTML\Header::LEVEL_4));
$multi->add("\r\nINSERT INTO " . $_DATABASE->delimit($_VARIABLE->get_table()) . " (\"name\", \"description\", \"type\", \"options\", \"default\", \"area\") VALUES (" . <<<SQL
'{$_POST["name"]}', '{$_POST["description"]}', '{$_POST["type"]}', 
SQL
. ($json === null ? "NULL" : "'" . addslashes($json) . "'") . ", decode('{$default}', 'hex'), {$area["id"]});\r\n");

$_HTML->complete();
