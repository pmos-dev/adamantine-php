<?php
/**
 * Adamantine system administration: Defragment database table IDs
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Defragment table", Config\DEFAULT_SUBTITLE);

if (!Data\Data::validate_regex("`^[a-z0-9_]{1,32}$`iD", $_GET["area"])) Adamantine\error("You did not supply a valid area");

if ($_GET["area"] !== "_core") {
	if (null === ($area = $_AREA->get_by_name($_GET["area"]))) Adamantine\error("No such area is installed in the system");
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";
}

if (!$_REPOSITORY->does_area_exist($_GET["area"])) Adamantine\error("The supplied area does not exist, or maybe you do not have access. This can happen if you have been logged out.");

if (!Data\Data::validate_regex("`^[a-z0-9_]{1,32}$`iD", $_GET["model"])) Adamantine\error("You did not supply a valid model");
if (!$_REPOSITORY->does_model_exist($_GET["area"], $_GET["model"])) Adamantine\error("The supplied model does not exist");

$_MODEL = $_REPOSITORY->get($_GET["area"], $_GET["model"]);

if (!($_MODEL instanceof Models\FirstClass)) Adamantine\error("This model cannot be defragmented");
if ($_MODEL instanceof Models\Hierarchy) Adamantine\error("This model cannot be defragmented");

$_UI->assert_confirmed("Defragment this table? This is an instance wide operation which can potentially corrupt the whole system if it fails!");

$_MODEL->defragment();

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/defragment.php");
