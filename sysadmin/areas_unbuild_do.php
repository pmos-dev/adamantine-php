<?php
/**
 * Adamantine system administration: Unbuild area from the system
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Unbuild area", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["area"])) Adamantine\error("No area ID sent");
if (!Data\Data::validate_id($_GET["area"])) Adamantine\error("Bad area ID sent");

if (null === ($area = $_AREA->get($_GET["area"]))) Adamantine\error("No such area exists");

if (sizeof($_INSTALLED->list_by_area($area)) !== 0) Adamantine\error("This area is still installed into some instances");

$_UI->assert_confirmed("Unbuilding the area '{$area["name"]}' will remove all elements of it from the system. This is usually safe since no instances should have it installed.");

if (!is_dir(APP_ROOT_PATH . "areas/{$area["name"]}")) Adamantine\error("No such area exists");
if (!file_exists(APP_ROOT_PATH . "areas/{$area["name"]}/_unbuild.php")) Adamantine\error("No such area exists");

$sqllog = array();
if (Data\Data::checkbox_boolean($_GET["debug"])) $_DATABASE->set_log_create_delete_alter($sqllog);

require APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";
require APP_ROOT_PATH . "areas/{$area["name"]}/_unbuild.php";

if (Data\Data::checkbox_boolean($_GET["debug"])) {
	$_UI->set_titles("SQL log", Config\DEFAULT_SUBTITLE);

	$page = $_UI->get_content();

	foreach ($sqllog as $sql) {
		$page->add($pre = new HTML\Pre($sql));
	}

	$_HTML->complete();
}

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/areas.php");
