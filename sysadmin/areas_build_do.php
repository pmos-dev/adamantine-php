<?php
/**
 * Adamantine system administration: Build area into the system
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Build area", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["area"])) Adamantine\error("No area sent");
if (!Data\Data::validate_regex("`^[a-z0-9]+$`D", $_GET["area"])) Adamantine\error("Bad area");
if (!is_dir(APP_ROOT_PATH . "areas/{$_GET["area"]}")) Adamantine\error("No such area exists");
if (!file_exists(APP_ROOT_PATH . "areas/{$_GET["area"]}/_build.php")) Adamantine\error("No such area exists");

$_UI->assert_confirmed("Build the area '{$_GET["area"]}' into the system?");

$sqllog = array();
if (Data\Data::checkbox_boolean($_GET["debug"])) $_DATABASE->set_log_create_delete_alter($sqllog);

require APP_ROOT_PATH . "areas/{$_GET["area"]}/_load.php";
require APP_ROOT_PATH . "areas/{$_GET["area"]}/_build.php";

if (Data\Data::checkbox_boolean($_GET["debug"])) {
	$_UI->set_titles("SQL log", Config\DEFAULT_SUBTITLE);
	
	$page = $_UI->get_content();
	
	foreach ($sqllog as $sql) {
		$page->add($pre = new HTML\Pre($sql));
	}
	
	$_HTML->complete();
}

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/areas.php");
