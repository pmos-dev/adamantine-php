<?php
/**
 * Adamantine system administration: View details of an area built into the system
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Area management", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["area"])) Adamantine\error("No area ID sent");
if (!Data\Data::validate_id($_GET["area"])) Adamantine\error("Bad area ID sent");

if (null === ($area = $_AREA->get($_GET["area"]))) Adamantine\error("No such area exists");

$page = $_UI->get_content();

$page->add(new HTML\Header("Area: {$area["name"]}", HTML\Header::LEVEL_3));
$page->add(new HTML\Header("Instances using this area", HTML\Header::LEVEL_4));

$page->add($table = new HTML\Table("instances", "unitable"));

$table->add_column("Instance");
$table->add_column("Status");

$installeds = $_INSTALLED->list_by_area($area);

if (sizeof($installeds) == 0) {
	$table->add_title_row("(none)");

	$page->add($linkbar = new HTML\LinkBar());
	
	$linkbar->add(new HTML\Link("Unbuild this area from the system", ADAMANTINE_ROOT_PATH . "sysadmin/areas_unbuild_do.php?area={$area["id"]}", null, "command icon_delete"));
	$linkbar->add(new HTML\Link("Unbuild with debug", ADAMANTINE_ROOT_PATH . "sysadmin/areas_unbuild_do.php?area={$area["id"]}&debug=1", null, "command icon_delete"));
}
else {
	foreach ($installeds as $installed) {
		$instance = $_INSTANCE->get($installed["instance"]);
		$row = $table->new_row();
		$row->set_cell("Instance", new HTML\Link($instance["name"], ADAMANTINE_ROOT_PATH . "sysadmin/instances_edit.php?instance={$instance["id"]}", null, "command icon_go"));
		$row->set_cell("Status", $installed["status"]);
	}
}

$page->add(new HTML\Header("Live existing database modification", HTML\Header::LEVEL_4));
$page->add($linkbar = new HTML\LinkBar());

$linkbar->add(new HTML\Link("New table SQL", ADAMANTINE_ROOT_PATH . "sysadmin/areas_table.php?area={$area["id"]}", null, "command icon_views"));

// For some reason, SQL views for the admin area don't seem to work. Not sure why. Not really that important at this time.
if ($area["name"] !== "admin") {
	$linkbar->add(new HTML\Link("Display SQL views", ADAMANTINE_ROOT_PATH . "sysadmin/areas_views.php?area={$area["id"]}", null, "command icon_views"));
	$linkbar->add(new HTML\Link("New field SQL", ADAMANTINE_ROOT_PATH . "sysadmin/areas_field.php?area={$area["id"]}", null, "command icon_views"));
	$linkbar->add(new HTML\Link("Remove field SQL", ADAMANTINE_ROOT_PATH . "sysadmin/areas_remove_field.php?area={$area["id"]}", null, "command icon_views"));
	$linkbar->add(new HTML\Link("New enum value SQL", ADAMANTINE_ROOT_PATH . "sysadmin/areas_enum.php?area={$area["id"]}", null, "command icon_views"));
}

$linkbar->add(new HTML\Link("New setting SQL", ADAMANTINE_ROOT_PATH . "sysadmin/areas_setting.php?area={$area["id"]}", null, "command icon_views"));

$_HTML->complete();
