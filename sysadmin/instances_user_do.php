<?php
/**
 * Adamantine system administration: Log in as root to instance via one-time-pad
 * 
 * Part 1: generate pad and store.
 * This has to be split into two parts due to the need to effectively have two sessions at once (one for sysadmin and one for the instance).
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Become user", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["instance"])) Adamantine\error("No instance ID sent");
if (!Data\Data::validate_id($_GET["instance"])) Adamantine\error("Bad instance ID sent");

if (null === ($instance = $_INSTANCE->get($_GET["instance"]))) Adamantine\error("No such instance exists");

if (!isset($_GET["user"])) Adamantine\error("No user ID sent");
if (!Data\Data::validate_id($_GET["user"])) Adamantine\error("Bad user ID sent");

if (null === ($user = $_USER->get_by_firstclass_and_id($instance, $_GET["user"]))) Adamantine\error("No such user exists");
$md5 = $_USER->generate_one_time_pad_md5($user);

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "sysadmin/login_one_time_pad_do.php?xsi={$instance["id"]}&user={$user["id"]}&md5={$md5}&sysadmin=1");
