<?php
/**
 * Adamantine system administration: Area management
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Area management", Config\DEFAULT_SUBTITLE);

$page = $_UI->get_content();

$page->add(new HTML\Header("The following areas are currently installed", HTML\Header::LEVEL_3));

$page->add($list = new HTML\ItemList(HTML\ItemList::UNORDERED, "installed", "unilist"));

$areas = $_AREA->list_all();
foreach ($areas as $area) $list->new_item(new HTML\Link($area["name"], ADAMANTINE_ROOT_PATH . "sysadmin/areas_view.php?area={$area["id"]}", null, "command icon_area"));

$page->add(new HTML\Header("The following areas are available to be built into the system", HTML\Header::LEVEL_3));

$page->add($link = new HTML\LinkBar());
if (!Data\Data::checkbox_boolean($_GET["debug"])) {
	$link->add(new HTML\Link("Enable SQL logging", ADAMANTINE_ROOT_PATH . "sysadmin/areas.php?debug=1", null, "command icon_enable"));
} else {
	$link->add(new HTML\Link("Disable SQL logging", ADAMANTINE_ROOT_PATH . "sysadmin/areas.php", null, "command icon_disable"));
}

$existing = array();
foreach ($areas as $area) $existing[] = $area["name"];

$page->add($list = new HTML\ItemList(HTML\ItemList::UNORDERED, "available", "unilist"));

$dh = opendir(APP_ROOT_PATH . "areas/");
while ($file = readdir($dh)) {
	if ($file !== "." && $file !== "..") {
		if (!is_dir(APP_ROOT_PATH . "areas/{$file}")) continue;
		if (in_array($file, $existing)) continue;
		if (!file_exists(APP_ROOT_PATH . "areas/{$file}/_build.php")) continue;
		if (!Data\Data::validate_regex("`^[a-z0-9]+$`D", $file)) continue;
		
		$list->new_item(new HTML\Link($file, ADAMANTINE_ROOT_PATH . "sysadmin/areas_build_do.php?area={$file}" . (Data\Data::checkbox_boolean($_GET["debug"]) ? "&debug=1" : ""), null, "command icon_install"));
	}
}

$_HTML->complete();
