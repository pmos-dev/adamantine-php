<?php
/**
 * Adamantine system administration: Instance management
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Renderer\HTMLJQuery\UI as HTMLJQueryUI;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Instance management", Config\DEFAULT_SUBTITLE);

$page = $_UI->get_content();

$page->add(new HTML\Header("Existing instances", HTML\Header::LEVEL_3));

$instances = $_INSTANCE->list_all();
$areas = $_AREA->list_all();

$page->add($table = new HTML\Table("instances", "unitable"));

$table->add_column("Instance");
$table->add_column("Description");
$table->add_column("Visibility");
$table->add_column("Status");

if (sizeof($instances) == 0) $table->add_title_row("(none)");
else {
	foreach ($instances as $instance) {
		$row = $table->new_row();
		$row->set_cell("Instance", new HTML\Link($instance["name"], ADAMANTINE_ROOT_PATH . "sysadmin/instances_edit.php?instance={$instance["id"]}", null, "command icon_go"));
		$row->set_cell("Description", $instance["description"]);
		$row->set_cell("Visibility", $instance["visibility"]);
		$row->set_cell("Status", $instance["status"]);
	}
}

$page->add(new HTML\Header("Add new instance", HTML\Header::LEVEL_3));

$page->add($tabs = new HTMLJQueryUI\Tabs($_HTML, "new"));

$tabs->add_tab("Manual", $form = new HTML\Form(ADAMANTINE_ROOT_PATH . "sysadmin/instances_add_do.php", "post", "addnew", "uniform simple autosize"), "manual");
$form->add_row("name", new HTML\Form_Text("", "name", "validate idname"), "name");
$form->add_submit("Create");

$tabs->add_tab("Import", $form = new HTML\Form(ADAMANTINE_ROOT_PATH . "sysadmin/instances_import_do.php", "filepost", "import", "uniform simple autosize"), "import");
$form->add_hidden("MAX_FILE_SIZE", (1024 * 1024) * 20);
$form->add_row("Script code", new HTML\Form_TextArea("", "code", "huge"), "code");
$form->add_row("File code", new HTML\Form_File("", "file"), "file");
$form->add_submit("Import");

$_HTML->complete();
