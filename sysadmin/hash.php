<?php
/**
 * Adamantine system administration: Generate sysadmin password hash
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Generate password hash", Config\DEFAULT_SUBTITLE);

$page = $_UI->get_content();

$page->add(new HTML\Header("This will allow you to create a password hash suitable for use in the config.php file. This controls the password used to access the sysadmin area. For security reasons, this password hash is stored directly in the config.php file rather than in the database.", HTML\Header::LEVEL_4, null, "info"));

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "sysadmin/hash_do.php", "POST", "hashform", "uniform simple autosize"));

$form->add_row("Password", new HTML\Form_Password("", "password", "validate string"), "password");

$form->add_submit("Generate hash");

$_HTML->complete();
