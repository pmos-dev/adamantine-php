<?php
/**
 * Adamantine system administration: Unbuild area from the system
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Display area SQL views", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["area"])) Adamantine\error("No area ID sent");
if (!Data\Data::validate_id($_GET["area"])) Adamantine\error("Bad area ID sent");

if (null === ($area = $_AREA->get($_GET["area"]))) Adamantine\error("No such area exists");

require APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";

$api = \Adamantine\Areas\Area_API::get_api_by_area_name($area["name"]);

$models = $_REPOSITORY->list_models_by_area($area["name"]);

$_HTML->add_code("css", <<<CSS

pre {
	border: 1px solid #88b4dc;
	white-space: pre-wrap;
}

CSS
);

$page = $_UI->get_content();

$page->add(new HTML\Header("Defined views for {$area["name"]}", HTML\Header::LEVEL_3));

$page->add(new HTML\Pre($multi = new HTML\MultiElements()));

foreach ($models as $name) {
	$multi->add(new HTML\Header($name, HTML\Header::LEVEL_4));
	
	$model = $_REPOSITORY->get($area["name"], $name);
	$views = $model->list_views();
	foreach ($views as $name => $sql) {
		$multi->add(new HTML\Header($name, HTML\Header::LEVEL_4));
		$multi->add(Data\Data::deindent($sql));
	}
}

$_HTML->complete();
