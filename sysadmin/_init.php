<?php
/**
 * Adamantine system administration: (initialisation script)
 * 
 * Sets up a basic application framework in a non-instance context using the SYSADMIN_SESSION_NAME session instead.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../");
if (!defined("SKIP_SESSION_INITIALIZE")) define("SKIP_SESSION_INITIALIZE", true);

if (defined("ADAMANTINE_ROOT_PATH")) require_once ADAMANTINE_ROOT_PATH . "_init.php";
else require_once APP_ROOT_PATH . "adamantine/_init.php";

use \_APPLICATION_NAMESPACE_\Config as Config;

session_name(Config\SYSADMIN_SESSION_NAME);
session_start();

if (!isset($_SESSION["allow_sysadmin_access"]) || $_SESSION["allow_sysadmin_access"] !== true) {
	unset($_SESSION["allow_sysadmin_access"]);
	session_destroy();
	
	$_HTTP->redirect(APP_ROOT_PATH);
}

$_UI->add_menu("Admin", array(
	"Area management" => ADAMANTINE_ROOT_PATH . "sysadmin/areas.php",
	"Instance management" => ADAMANTINE_ROOT_PATH . "sysadmin/instances.php",
	"Defragment" => ADAMANTINE_ROOT_PATH . "sysadmin/defragment.php",
	"Purge dirty area" => ADAMANTINE_ROOT_PATH . "sysadmin/purge.php",
	"Unbuild system" => ADAMANTINE_ROOT_PATH . "build/unbuild.php",
	"Full export" => ADAMANTINE_ROOT_PATH . "sysadmin/export.php",
	"Generate password hash" => ADAMANTINE_ROOT_PATH . "sysadmin/hash.php",
	"Return to system" => APP_ROOT_PATH
));
