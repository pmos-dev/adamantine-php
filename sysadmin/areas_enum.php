<?php
/**
 * Adamantine system administration: Unbuild area from the system
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Display SQL for additional field enum value", Config\DEFAULT_SUBTITLE);

if (!isset($_GET["area"])) Adamantine\error("No area ID sent");
if (!Data\Data::validate_id($_GET["area"])) Adamantine\error("Bad area ID sent");

if (null === ($area = $_AREA->get($_GET["area"]))) Adamantine\error("No such area exists");

require APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";

$api = \Adamantine\Areas\Area_API::get_api_by_area_name($area["name"]);

$models = $_REPOSITORY->list_models_by_area($area["name"]);

$page = $_UI->get_content();

if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $_GET["model"])) {
	$page->add(new HTML\Header("Available models for {$area["name"]}", HTML\Header::LEVEL_3));
	
	$page->add($list = new HTML\ItemList(HTML\ItemList::UNORDERED, null, "unilist"));
	foreach ($models as $model) {
		$list->new_item(new HTML\Link($model, ADAMANTINE_ROOT_PATH . "sysadmin/areas_enum.php?area={$area["id"]}&model={$model}", null, "command inline icon_go"));
	}
	
	$_HTML->complete();
}

if (null === ($model = $_REPOSITORY->get($area["name"], $_GET["model"]))) Adamantine\error("No such model exists");
$structure = $model->get_structure();

if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $_GET["field"])) {
	$page->add(new HTML\Header("Available fields for {$area["name"]}:{$model->get_table()}", HTML\Header::LEVEL_3));
	
	$page->add($list = new HTML\ItemList(HTML\ItemList::UNORDERED, null, "unilist"));
	foreach ($structure as $name => $def) {
		$list->new_item(new HTML\Link($name, ADAMANTINE_ROOT_PATH . "sysadmin/areas_enum.php?area={$area["id"]}&model={$_GET["model"]}&field={$name}", null, "command inline icon_go"));
	}
	
	$_HTML->complete();
}

$field = null;
foreach ($structure as $name => $definition) {
	if ($_GET["field"] === $name) {
		$field = $definition;
		break;
	}
}
if ($field === null) Adamantine\error("No such field exists");

$_HTML->add_code("css", <<<CSS

pre {
	border: 1px solid #88b4dc;
	white-space: pre-wrap;
}

CSS
);


$page->add(new HTML\Header("SQL modification code", HTML\Header::LEVEL_3));

$page->add(new HTML\Header("M2MLinkTables and similar which are related to this table will NOT be updated by this SQL code, and will need to be changed manually!", HTML\Header::LEVEL_4, null, "warning"));

$page->add(new HTML\Pre($multi = new HTML\MultiElements()));

$fields = array();
foreach ($structure as $name => $type) {
	if ($_GET["field"] !== $name) continue;
	
	$check = "";
	if ($type instanceof Database\Type_Enum) if (null !== ($create_enum = $type->get_create_enum($_DATABASE->get_db_type()))) {
		$page->add("\r\n{$create_enum}\r\n");
	}
	if ($type instanceof Database\Type_Numeric) $check = $type->get_check($name, $_DATABASE->get_db_type());
		
	$multi->add("\r\nALTER TABLE " . $_DATABASE->delimit($model->get_table()) . " CHANGE " . $_DATABASE->delimit($name) . " " . $_DATABASE->delimit($name) . " " . $type->render($_DATABASE->get_db_type()) . $check . "\r\n");
}

$_HTML->complete();
