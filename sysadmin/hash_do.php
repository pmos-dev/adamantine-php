<?php
/**
 * Adamantine system administration: Generate sysadmin password hash
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\SysAdmin;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/sysadmin/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Generate password hash", Config\DEFAULT_SUBTITLE);

if (!Data\Data::validate_string($_POST["password"])) Adamantine\error("You did not enter a valid password");

$page = $_UI->get_content();

$page->add(new HTML\Header("Generated hash value for password", HTML\Header::LEVEL_3));
$page->add(new HTML\Paragraph(Data\Data::hexchunk_encode(hash("sha512", $_POST["password"], true))));

$_HTML->complete();
