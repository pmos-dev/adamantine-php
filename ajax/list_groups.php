<?php
/**
 * AJAX functionality: list all available groups.
 * 
 * Only 'normal' groups are returned (i.e. not "everyone" and "system") for the currently loaded instance session.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\AJAX;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Data as Data;

$_ERROR_RENDERER_OVERRIDE = $_JSON;

if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $_GET["area"])) $_JSON->error("Bad area");
if (null === ($area = $_AREA->get_by_name($_GET["area"]))) $_JSON->error("No such area exists");

if (!$_SESSIONMANAGER->has_access_by_name($area["name"], "read")) $_JSON->error("Access denied");

$names = array();
foreach ($_GROUP->list_normal_by_instance($xsi) as $group) $names[$group["id"]] = $group["name"];

$_JSON->success($names);
