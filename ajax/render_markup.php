<?php
/**
 * AJAX functionality: render Markup as HTML.
 * 
 * Used for the MarkupEditor preview functionality.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\AJAX;

define("APP_ROOT_PATH", "./../../");

define("SKIP_SESSION_INITIALIZE", true);
require_once APP_ROOT_PATH . "adamantine/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Renderer\JSON as JSON;

use \Adamantine as Adamantine;

$_ERROR_RENDERER_OVERRIDE = $_JSON;
$_JSON->enable_origin();

if (Data\Data::validate_string($_GET["callback"])) {
	require_once ABSTRACTION_ROOT_PATH . "renderer/json/jsonp.php";
	$_ERROR_RENDERER_OVERRIDE = $_JSON = new JSON\JSONP($_GET["callback"]);
}

if (!array_key_exists("markup", $_GET)) Adamantine\error("No markup supplied");

$renderer = new HTML\Markup($_GET["markup"]);

ob_start();
$renderer->render();
$html = ob_get_clean();

$_JSON->success($html);
