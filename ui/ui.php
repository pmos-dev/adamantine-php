<?php
/**
 * User interface abstraction for HTML output.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\UI;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/renderer.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/html.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/user.php";

use \Abstraction\Data as Data;
use \Abstraction\Renderer as Renderer;
use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Renderer\HTMLJQuery as HTMLJQuery;
use \Abstraction\Renderer\HTML\Semantics as Semantics;
use \Adamantine\Models as Models;
use \Adamantine as Adamantine;

/**
 * @internal
 */
class UI_Exception extends Adamantine\Exception {}

/**
 * Provides an anonymous class implementation of the Renderable interface in the form of a callback.
 * 
 * This is primarily used for building the menu system, for which it is desirable to construct the list after higher level processing such as ACLs has been considered.
 * 
 * @internal
 * @author Pete Morris
 * @version 1.2.0
 */
class CallbackRenderer implements Renderer\Renderable {
	private $callback;
	
	/**
	 * Constructs a new instance of this anonymous callback.
	 * 
	 * @param callable $callback
	 */
	public function __construct($callback) {
		$this->callback = $callback;
	}

	/**
	 * Calls the stored callback to render to stdout.
	 * 
	 * @return void
	 */
	function render() {
		$callback = $this->callback;
		$callback();
	}
}

/**
 * Root skeleton user interface framework for Adamantine implementations.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class UI {
	protected $html;
	
	private $header, $nav, $body, $content, $footer;
	protected $components;
	private $poweredby;
	
	private $menus;
	
	/**
	 * Constructs a new instance of this user interface.
	 * 
	 * Subclasses must make sure they call parent::__construct(html) as this constructor sets up most of the page.
	 * 
	 * @param HTML\HTML $html
	 */
	public function __construct(HTML\HTML $html) {
		$this->html = $html;
		$this->html->get_page()->add(new Semantics\Header($this->header = new HTML\MultiElements()));
		$this->html->get_page()->add(new Semantics\Nav($this->nav = new HTML\MultiElements()));
		$this->html->get_page()->add(new Semantics\Main($main = new HTML\MultiElements()));
		$this->html->get_page()->add(new Semantics\Footer($this->footer = new HTML\MultiElements()));
		
		$main->add(new HTML\Div($this->body = new HTML\MultiElements(), null, "container"));
		$this->body->add(new HTML\Div(new HTML\Div($this->content = new HTML\MultiElements(), null, "padding"), "content", "twelve columns"));
		
		$this->components = array();
		
		$this->menus = array("_system" => null);
		
		$this->build_header();
		$this->build_footer();
	}
	
	const JQUERY_VERSION = "3.6.4";
	const JQUERYUI_VERSION = "1.13.2";
			
	/**
	 * Adds the basic jQuery and CSS files needed by the skeleton framework.
	 * 
	 * @param string $js_root the URL path to the JavaScript folder containing the files
	 * @param string $css_root the URL path to the CSS folder containing the files
	 * @return void
	 */
	public function add_includes($js_root, $css_root) {
		$this->html->add_include("js", "{$js_root}jquery-" . self::JQUERY_VERSION . ".min.js");

		$this->html->add_include("js", "{$js_root}jquery-ui-" . self::JQUERYUI_VERSION . ".custom.min.js");
		$this->html->add_include("css", "{$css_root}adamantine-theme/jquery-ui-" . self::JQUERYUI_VERSION . ".custom.min.css");
		
		$this->html->add_include("js", "{$js_root}jquery-ui-slideraccess-addon.js");
		
		$this->html->add_include("js", "{$js_root}jquery-ui-timepicker-addon.js");
		$this->html->add_include("css", "{$css_root}jquery-ui-timepicker-addon.css");
		
		$this->html->add_include("js", "{$js_root}jquery.colorpicker.js");
		$this->html->add_include("css", "{$css_root}jquery.colorpicker.css");
		
		$this->html->add_include("css", "{$css_root}skeleton.css");
		
		$this->html->add_include("js", "{$js_root}screen.js");
		$this->html->add_include("css", "{$css_root}screen.css");
		$this->html->add_include("css", "{$css_root}icons.css");
		
		$this->html->add_include("css", "{$css_root}uniform.css");
		$this->html->add_include("js", "{$js_root}uniform.js");
		$this->html->add_include("css", "{$css_root}unitable.css");
		$this->html->add_include("js", "{$js_root}unitable.js");
		$this->html->add_include("css", "{$css_root}unilist.css");
		$this->html->add_include("js", "{$js_root}unilist.js");

		$this->html->add_include("css", "{$css_root}validation.css");
		$this->html->add_include("js", "{$js_root}validation.js");
	}
	
	// this has to be public for PHP 5.3 $closurethis to work
	/**
	 * Recursively traverses the specified menu, to build a menu of unordered lists and hyperlinks representing the structure.
	 * 
	 * Note, this method has to be public for PHP 5.3 closure support in CallbackRenderer to work, however it is essentially a private method. PHP 5.4 upwards may not need this method to be public.
	 * 
	 * @internal
	 * @see CallbackRenderer
	 * @param array $menu a tree structure representing the menu to build
	 * @param HTML\ItemList $element the root list element being built into
	 * @return void
	 */
	public static function _recurse_menu($menu, $element) {
		foreach ($menu as $name => $content) {
			$element->new_item($multi = new HTML\MultiElements());
			if (is_array($content)) {
				$multi->add($name);
				$multi->add($sub = new HTML\ItemList(HTML\ItemList::UNORDERED));
				
				self::_recurse_menu($content, $sub);
			} else {
				$multi->add(new HTML\Link($name, $content));
			}
		}
	}

	/**
	 * Builds the header for the user interface, including the application menu bar.
	 * 
	 * @internal
	 * @return void
	 */
	protected function build_header() {
		$this->header->add(new HTML\Header($this->components["title"] = new HTML\MultiElements(), HTML\Header::LEVEL_1));
		$this->components["title"]->add("Untitled page");
		
		$this->header->add(new HTML\Header($this->components["subtitle"] = new HTML\MultiElements(), HTML\Header::LEVEL_2));
		$this->components["subtitle"]->add("Untitled subtitle");
		
		$this->nav->add(new HTML\Div(new HTML\Div($multi = new HTML\MultiElements(), null, "twelve columns"), null, "container"));
		$multi->add(new HTML\Div(new HTML\Div("", "burger")));

		$multi->add(new HTML\Div($this->components["user"] = new HTML\MultiElements(), "userbar"));
		$this->set_user(null);

		// this is needed for PHP 5.3 support which can't use $this within anonymous functions
		$closurethis = $this;
		
		$multi->add(new CallbackRenderer(function() use ($closurethis) {
			$menubar = new HTML\ItemList(HTML\ItemList::UNORDERED, "menubar");
			foreach ($closurethis->get_menus() as $name => $menu) if ($menu !== null) $closurethis->_recurse_menu(array(($name === "_system" ? "System" : $name) => $menu), $menubar);
			
			$menubar->render();
		}));
	}
	
	/**
	 * Sets the title and sub-title of the page.
	 * 
	 * @param string $title the page title, often the functionalility area e.g. Create new user
	 * @param string $subtitle the page subtitle, often the application name
	 * @return void
	 */
	public function set_titles($title, $subtitle) {
		$this->components["title"]->clear();
		$this->components["title"]->add(new HTML\Inline_Text($title));
		$this->components["subtitle"]->clear();
		$this->components["subtitle"]->add(new HTML\Inline_Text($subtitle));
				
		$this->html->set_variable("page_title", "{$title} - {$subtitle}");
	}

	/**
	 * Sets the current session user for the UI
	 * 
	 * @param mixed[]|NULL $user the current session user, or null for none
	 * @return void
	 */
	public function set_user(array $user = null) {
		$this->components["user"]->clear();
		if ($user === null || $user["type"] === Models\User::GUEST) {
			$this->components["user"]->add("Not logged in");
		} else {
			$this->components["user"]->add("Logged in as: ");
			$this->components["user"]->add(new HTML\Span($user["fullname"]));
		}
	}

	/**
	 * Adds a menu to the menu bar
	 * 
	 * @param string $name the name of the menu (area)
	 * @param array $content an hierarchical array representing the menu
	 * @return void
	 */
	public function add_menu($name, $content) {
		$this->menus[$name] = $content;
	}

	/**
	 * Replaces the default system menu with the specified new menu.
	 * 
	 * @param array $content an hierarchical array representing the menu
	 * @return void
	 */
	public function set_system_menu($content) {
		$this->menus["_system"] = $content;
	}
	
	/**
	 * Returns the current associative array representing the menu bar.
	 * 
	 * @return array
	 */
	public function get_menus() {
		return $this->menus;
	}

	/**
	 * Removes all non-system menus, and purges down the system menu into only welcome and login/logoff
	 * 
	 * @return void
	 */
	public function purge_functionality_menus() {
		$system = array();
		foreach ($this->menus["_system"] as $item => $url) {
			switch ($item) {
				case "Welcome page":
				case "Login":
				case "Logoff":
					$system[$item] = $url;
			}
		}
		
		$this->menus = array(
			"_system" => $system
		);
	}
	
	/**
	 * Builds the footer for the user interface, including the cookies, FireFox, powered by and date information.
	 * 
	 * @internal
	 * @return void
	 */
	protected function build_footer() {
		$this->footer->add($linkbar = new HTML\Linkbar());
		$linkbar->add($link = new HTML\Link("Cookies", ADAMANTINE_ROOT_PATH . "about/cookies.php", "adamantine_cookies", "command icon_cookies"));
		$link->add_param("target", "_blank");
		$linkbar->add($link = new HTML\Link($this->poweredby = new HTML\MultiElements(array("Powered by Adamantine")), ADAMANTINE_ROOT_PATH . "about/", null, "command icon_software"));
		$link->add_param("target", "_blank");
		$linkbar->add($link = new HTML\Link("Designed for Firefox", "http://www.mozilla.org/firefox/", null, "command icon_firefox"));
		$link->add_param("target", "_blank");
		$this->footer->add(new HTML\Paragraph(new HTML\MultiElements(array("Information correct as at", new HTML\Span(date("D d M Y H:i:s"), null, "timestamp")))));
	}
	
	/**
	 * Returns the header region of the page.
	 * 
	 * @return HTML\MultiElements
	 */
	public function get_header() {
		return $this->header;
	}

	/**
	 * Returns the main body region of the page, which initially has the contents as a 16 column div.
	 *
	 * @return HTML\MultiElements
	 */
	public function get_body() {
		return $this->body;
	}
	
	/**
	 * Returns the main contents region of the page.
	 * 
	 * This is generally what all pages will use to attach the various page elements to.
	 * 
	 * @return HTML\MultiElements
	 */
	public function get_content() {
		return $this->content;
	}

	/**
	 * Returns the footer region of the page.
	 * 
	 * @return HTML\MultiElements
	 */
	public function get_footer() {
		return $this->footer;
	}

	/**
	 * Sets a custom 'powered by' message in the footer.
	 * 
	 * @param string $message the message to display.
	 * @return void
	 */
	public function set_poweredby($message) {
		$this->poweredby->clear();
		$this->poweredby->add($message);
	}
	
	/**
	 * Wipes any existing UI page content and replaces it with a standardised error message, then terminates.
	 * 
	 * Note, this method is terminal, so the error is rendered to stdout, and then the script is terminated.
	 * This makes it possible to use it as a single failure method, e.g. if (error condition) $_UI->error("There was an error!");
	 * 
	 * @param string|Renderer\Renderable $message the error message to be displayed.
	 * @param string|Renderer\Renderable|NULL $append any custom content to be appended to the error message 
	 * @return void
	 */
	public function error($message = "(unspecified error)", $append = null) {
		$this->content->clear();
		$this->content->add(new HTML\Header("Oops, an error has occurred!", HTML\Header::LEVEL_3, null, "error"));
		$this->content->add(new HTML\Header($message, HTML\Header::LEVEL_4, null, "error"));
		
		if ($append !== null) $this->content->add($append);

		$this->html->complete();
	}

	const CONFIRM_METHOD_GET = "get";
	const CONFIRM_METHOD_POST = "post";
	const CONFIRM_METHOD_CODE = "code";
	
	const REGEX_PATTERN_CONFIRM_CODE = "`^[0-9a-f]{4}$`";
	
	/**
	 * Checks for the existance and value of a URL parameter confirmation.
	 * 
	 * @param string $method the method to consider, either CONFIRM_METHOD_GET or CONFIRM_METHOD_POST
	 * @param string $param an optional override to the default parameter key of "confirm"
	 * @throws Adamantine\Exception
	 * @return boolean
	 */
	public static function is_confirmed($method = self::CONFIRM_METHOD_GET, $param = "confirm") {
		switch ($method) {
			case self::CONFIRM_METHOD_GET:
				global $_GET;
				return Data\Data::checkbox_boolean($_GET[$param]);
			case self::CONFIRM_METHOD_POST:
				global $_POST;
				return Data\Data::checkbox_boolean($_POST[$param]);
			case self::CONFIRM_METHOD_CODE:
				global $_POST;
				if (!Data\Data::validate_regex(self::REGEX_PATTERN_CONFIRM_CODE, $_POST["{$param}_code"])) return false;
				if (!Data\Data::validate_regex(self::REGEX_PATTERN_CONFIRM_CODE, $_POST["{$param}_text"])) return false;
				return $_POST["{$param}_code"] === $_POST["{$param}_text"];
			default:
				throw new Adamantine\Exception("Unknown confirmation method: only get and post are supported.");
		}
	}
	
	/**
	 * Wipes any existing UI page content and replaces it with a standardised confirmation message, then terminates.
	 * 
	 * Note, this method is terminal, so the confirmation page is rendered to stdout, and then the script is terminated.
	 * The link is automatically built as a callback to the generating page URL, with ?confirm=1 or &confirm=1 appended on the end.
	 * This makes it possible to use it as a single failure method, e.g. if (!isset($_GET["confirm"]) || !$_GET["confirm"]) $_UI->confirm("Delete this user: are you sure?");
	 * 
	 * @param string|Renderer\Renderable $message the confirmation message to be displayed.
	 * @return void
	 */
	public function confirm($message, $method = self::CONFIRM_METHOD_GET, $param = "confirm") {
		$this->content->clear();
		$this->content->add(new HTML\Header("Confirmation required", HTML\Header::LEVEL_3, null, "confirm"));
		$this->content->add(new HTML\Header($message, HTML\Header::LEVEL_4, null, "warning"));

		switch ($method) {
			case self::CONFIRM_METHOD_GET:
				$url = $_SERVER["REQUEST_URI"] . (preg_match("`\?`", $_SERVER["REQUEST_URI"]) ? "&" : "?") . "{$param}=1";
				$this->content->add(HTML\LinkBar::singular(new HTML\Link("Yes, do this", $url, null, "command icon_confirm")));
				break;
			case self::CONFIRM_METHOD_POST:
				throw new Adamantine\Exception("POST confirmation is not currently supported. Consider using CODE.");
			case self::CONFIRM_METHOD_CODE:
				$url = $_SERVER["REQUEST_URI"];
				if (preg_match("`^(.+)\?`", $url, $array)) $url = $array[1];
				
				$this->content->add(new HTML\Header("High-risk operation confirmation.", HTML\Header::LEVEL_4));
				
				$this->content->add($form = new HTML\Form($url, HTML\Form::SUBMIT_POST, "confirm"));
				foreach ($_REQUEST as $key => $val) $form->add_hidden($key, $val);
				
				$form->add(new HTML\Paragraph("Please type in the following code to confirm you wish to proceed with this."));
				
				$this->html->add_code(HTML\HTML::TYPE_CSS, <<<CSS
				
div#confirm_code {
	font-family: courier;
	font-size: 2em;
	background: rgba(0, 0, 0, 0.04);
	width: 4em;
	text-align: center;
	margin-bottom: 15px;
}
				
input#confirm_text {
	width: 4em;
	margin-bottom: 1em;
}
				
CSS
		);
				
				$code = substr(md5(rand(0, 100) . time()), 0, 4);
				$form->add(new HTML\Div($code, "confirm_code"));
				$form->add_hidden("confirm_code", $code);

				$form->add(new HTML\Label("Confirmation"));
				$form->add(new HTML\Form_Text("", "confirm_text", "validate idname"));

				$form->add_submit("Yes, do this");
				break;
			default:
				throw new Adamantine\Exception("Unknown confirmation method: only get and post are supported.");
		}
				
		$this->html->complete();
	}

	/**
	 * Combines the is_confirmed() check with the confirm() method to produce a single confirmation assertation.
	 * 
	 * Note, if the confirm flag is not present, this method terminates script execution after displaying the confirmation.
	 * 
	 * @see UI::is_confirmed()
	 * @see UI::confirm()
	 * @param string|Renderer\Renderable $message the confirmation message to be displayed.
	 * @param string $method the method to consider, either CONFIRM_METHOD_GET or CONFIRM_METHOD_POST
	 * @param string $param an optional override to the default parameter key of "confirm"
	 * @return void
	 */
	public function assert_confirmed($message, $method = self::CONFIRM_METHOD_GET, $param = "confirm") {
		if (!self::is_confirmed($method, $param)) $this->confirm($message, $method, $param);
	}
	
	/**
	 * Wipes any existing UI page content and replaces it with a standardised success message, then terminates.
	 * 
	 * Note, this method is terminal, so the success page is rendered to stdout, and then the script is terminated.
	 * 
	 * @param string|Renderer\Renderable $title the title of the success message
	 * @param string|Renderer\Renderable $message any optional additional success details
	 * @return void
	 */
	public function success($title, $message = "") {
		$this->content->clear();
		$this->content->add(new HTML\Header($title, HTML\Header::LEVEL_3));
		if ($message !== "") $this->content->add(new HTML\Paragraph($message));

		$this->html->complete();
	}
	
	public function simple_datechange($title, $date, $url, $datevar, array $xsi, array $params = array(), array $jumps = array()) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_YMD, $date)) throw new Adamantine\Exception("Invalid date supplied");
		
		$elements = new HTML\MultiElements();
		
		$elements->add(new HTML\Header("{$title} for " . Data\Data::pretty_date($date), HTML\Header::LEVEL_3));
		
		$elements->add($form = new HTML\Form($url, "GET", null, "simpledatechange uniform simple autosize"));
		$form->add_hidden("xsi", $xsi["id"]);
		foreach ($params as $var => $value) $form->add_hidden($var, $value);
		
		$form->add_row("Date", new HTML\Form_Text(Data\Data::date_ymd_to_dmy($date), $datevar, "validate date"), $datevar);
		
		$form->add_submit("Apply");
		
		$jumpparams = array("xsi={$xsi["id"]}");
		foreach ($params as $var => $value) $jumpparams[] = "{$var}=" . rawurlencode($value);
		$jumpparams = implode("&", $jumpparams);
		
		$elements->add($linkbar = new HTML\Linkbar());
		$linkbar->add(new HTML\Link("Change date", "#", null, "simplechangedate command icon_expand"));
		foreach ($jumps as $description => $jump) $linkbar->add(new HTML\Link($description, "{$url}?{$datevar}=" . date("d/m/Y", strtotime($jump)) . "&{$jumpparams}", null, "command icon_date"));
		
		$this->html->add_include("js", ADAMANTINE_ROOT_PATH . "js/datechange.js");
		
		return $elements;
	}
	
	public function range_datechange($title, $start, $end, $url, $startvar, $endvar, array $xsi, array $params = array(), array $jumps = array()) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_YMD, $start)) throw new Adamantine\Exception("Invalid start date supplied");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_YMD, $end)) throw new Adamantine\Exception("Invalid end date supplied");
		
		Data\Data::auto_order_date($start, $end);
		
		$elements = new HTML\MultiElements();
		
		if ($start === $end) $elements->add(new HTML\Header("{$title} for " . Data\Data::pretty_date($start), HTML\Header::LEVEL_3));
		else $elements->add(new HTML\Header("{$title} from " . Data\Data::pretty_date($start) . " to " . Data\Data::pretty_date($end), HTML\Header::LEVEL_3));
		
		$elements->add($form = new HTML\Form($url, "GET", null, "rangedatechange uniform simple autosize"));
		$form->add_hidden("xsi", $xsi["id"]);
		foreach ($params as $var => $value) $form->add_hidden($var, $value);
		
		$form->add_row("Start", new HTML\Form_Text(Data\Data::date_ymd_to_dmy($start), $startvar, "validate date"), $startvar);
		$form->add_row("End", new HTML\Form_Text(Data\Data::date_ymd_to_dmy($end), $endvar, "validate date"), $endvar);
		
		$form->add_submit("Apply");
		$form->add_button("Cancel", HTML\Form::BUTTON_OTHER, null, "cancel");
		
		$elements->add($linkbar = new HTML\Linkbar());
		
		$linkbar->add(new HTML\Link("Change date", "#", null, "rangechangedate command icon_expand"));
		
		$jumpparams = array("xsi={$xsi["id"]}");
		foreach ($params as $var => $value) $jumpparams[] = "{$var}=" . rawurlencode($value);
		$jumpparams = implode("&", $jumpparams);
		
		foreach ($jumps as $description => $jump) {
			list($jump_start, $jump_end) = explode(",", $jump);
			$linkbar->add(new HTML\Link($description, "{$url}?{$startvar}=" . date("d/m/Y", strtotime($jump_start)) . "&{$endvar}=" . date("d/m/Y", strtotime($jump_end)) . "&{$jumpparams}", null, "command icon_date"));
		}
		
		$this->html->add_include("js", ADAMANTINE_ROOT_PATH . "js/datechange.js");
		
		return $elements;
	}
	
	public static function limitlength($string, $max_length, $use_hellip = true, $hard_limit = true) {
		if (strlen($string) <= $max_length) return $string;
		
		$multi = new HTML\MultiElements();
		
		if ($hard_limit || ($max_length < 10)) {
			$multi->add(substr($string, 0, $max_length));
		} else {
			if (!preg_match("`^(.{" . ($max_length - 8) . ",{$max_length}})\W`", $string, $array)) $multi->add(substr($string, 0, $max_length));
			else {
				$multi->add($array[1]);
			}
		}
		
		if ($use_hellip) $multi->add(new HTML\Raw("&hellip;"));
		
		return $multi;
	}
	
	public function standardfield_form(HTML\MultiElements $multi, array $field, $value, $formpreface = "field", $listset_model = null, array $listset_firstclass = null) {
		if (!Data\Data::validate_id_object($field)) throw new UI_Exception("Supplied field is not valid");
		
		if ($field["type"] === Models\StandardField::LISTSET) {
			if (!($listset_model instanceof Models\StandardListSet)) throw new UI_Exception("Supplied model is not a StandardListSet child");
			if (!Data\Data::validate_id_object($listset_firstclass)) throw new UI_Exception("Supplied listset_firstclass parent is not valid");
			
			if ($field["listset"] === null) $multi->add(new HTML\Div("No list set defined"));
			else {
				$listset = $listset_model->get_by_firstclass_and_id($listset_firstclass, $field["listset"]);
		
				if ($value === null) $ids = array();
				else {
					if (!is_array($value)) throw new UI_Exception("Supplied value is not an array");
					$ids = Data\Data::extract_ids_from_id_object_array($value);
				}
		
				$multi->add($select = new HTML\Form_Select("{$formpreface}_{$field["id"]}" . ($field["allow_multiple"] ? "[]" : ""), $field["optional"] ? "optional" : ""));
				if ($field["allow_multiple"]) $select->add_param("multiple", "multiple");
				else if ($field["optional"]) $select->add_option(null, "");
					
				foreach ($listset_model->get_listitem_model()->list_ordered_by_firstclass($listset) as $listitem) {
					$select->add_option($listitem["id"], $listitem["name"], in_array($listitem["id"], $ids));
				}
			}
		} else {
			$optional = $field["optional"] ? "optional" : "";
		
			switch ($field["type"]) {
				case Models\StandardField::SINGLE:
					// strip out any 'word' characters
					if ($value !== null) $value = Data\Data::sanitise_word_characters($value);
		
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate string {$optional}"));
					if ($field["size"] !== null) $element->add_param("maxlength", $field["size"]);
					break;
				case Models\StandardField::MULTI:
					// strip out any 'word' characters
					if ($value !== null) $value = Data\Data::sanitise_word_characters($value);
		
					$multi->add($element = new HTML\Form_TextArea($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate text {$optional}"));
					if ($field["size"] !== null) $element->add_param("maxlength", $field["size"]);
					break;
				case Models\StandardField::BOOLEAN:
					if ($value === null) $value = false;
					else $value = Data\Data::checkbox_boolean($value);
		
					$multi->add($element = new HTML\Form_CheckBox($value, "{$formpreface}_{$field["id"]}"));
					break;
				case Models\StandardField::INTEGER:
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate int {$optional}"));
					break;
				case Models\StandardField::FLOAT:
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate float {$optional}"));
					break;
				case Models\StandardField::DATE:
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate date {$optional}"));
					$this->html->add_include(HTMLJQuery\HTML::TYPE_JQUERY, ADAMANTINE_ROOT_PATH . "js/standardfield.js");
					break;
				case Models\StandardField::TIME:
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate time {$optional}"));
					$this->html->add_include(HTMLJQuery\HTML::TYPE_JQUERY, ADAMANTINE_ROOT_PATH . "js/standardfield.js");
					break;
				case Models\StandardField::DATETIME:
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate datetime {$optional}"));
					$this->html->add_include(HTMLJQuery\HTML::TYPE_JQUERY, ADAMANTINE_ROOT_PATH . "js/standardfield.js");
					break;
				case Models\StandardField::EMAIL:
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate email {$optional}"));
					if ($field["size"] !== null) $element->add_param("maxlength", $field["size"]);
					break;
				case Models\StandardField::PHONE:
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate phone {$optional}"));
					if ($field["size"] !== null) $element->add_param("maxlength", $field["size"]);
					break;
				case Models\StandardField::URL:
					$multi->add($element = new HTML\Form_Text($value === null ? "" : $value, "{$formpreface}_{$field["id"]}", "validate url {$optional}"));
					if ($field["size"] !== null) $element->add_param("maxlength", $field["size"]);
					break;
				default: {
					throw new UI_Exception("No such field type defined: {$field["type"]}");
				}
			}
		}		
	}
	
	public static function standardfield_html(HTML\MultiElements $multi, array $field, $value, $listset_model = null, array $listset_firstclass = null) {
		if (!Data\Data::validate_id_object($field)) throw new UI_Exception("Supplied field is not valid");
		
		if ($field["type"] === Models\StandardField::LISTSET) {
			if (!($listset_model instanceof Models\StandardListSet)) throw new UI_Exception("Supplied model is not a StandardListSet child");
			if (!Data\Data::validate_id_object($listset_firstclass)) throw new UI_Exception("Supplied listset_firstclass parent is not valid");
			
			if ($field["listset"] === null) $multi->add(new HTML\Div("No list set defined"));
			else {
				$listset = $listset_model->get_by_firstclass_and_id($listset_firstclass, $field["listset"]);
		
				if ($value === null) $items = array();
				else {
					if (!is_array($value)) throw new UI_Exception("StandardField listset item value is not an array");
					$items = $value;
					foreach ($items as $item) if (!Data\Data::validate_id_object($item)) throw new UI_Exception("StandardField listset item value does not contain an array of ID objects");
				}
				
				if ($field["allow_multiple"]) {
					$multi->add($list = new HTML\ItemList(HTML\ItemList_Item::UNORDERED, null, "unilist"));
					foreach ($items as $item) $list->new_item($item["name"]);
				}
				else if (sizeof($items) > 0) $multi->add($items[0]["name"]);
				else $multi->add(new HTML\Span("none", null, "none"));
			}
		} else {
			switch ($field["type"]) {
				case Models\StandardField::SINGLE:
				case Models\StandardField::INTEGER:
				case Models\StandardField::EMAIL:
				case Models\StandardField::PHONE:
				case Models\StandardField::URL:
				case Models\StandardField::TIME:
				case Models\StandardField::MULTI:
				case Models\StandardField::FLOAT:
					if ($value !== null) $multi->add("{$value}");
					else $multi->add(new HTML\Span("none", null, "none"));
					break;
				case Models\StandardField::DATE:
					if ($value !== null) $multi->add(Data\Data::pretty_date($value));
					else $multi->add(new HTML\Span("none", null, "none"));
					break;
				case Models\StandardField::DATETIME:
					if ($value !== null) $multi->add(Data\Data::pretty_datetime($value));
					else $multi->add(new HTML\Span("none", null, "none"));
					break;
				case Models\StandardField::BOOLEAN:
					if ($value !== null) $multi->add(Data\Data::checkbox_boolean($value) ? new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/tick.svg") : new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/cross.svg"));
					else $multi->add(new HTML\Span("none", null, "none"));
					break;
				default: {
					throw new UI_Exception("No such field type defined: {$field["type"]}");
				}
			}
		}
		
	}
}
