<?php
/**
 * Standard ListItems for ListSets.
 * 
 * This is part of the StandardListSet class, and is not intended for being used directly.
 * 
 * @copyright 2015 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/managedorientatedordered.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class StandardListItem_Exception extends Models\SecondClass_Exception {}

/**
 * Defines a root model for ListItems.
 * 
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
class StandardListItem extends ManagedOrientatedOrdered {
	protected $listset;
	
	public function __construct(Database\Wrapper $database, StandardListSet $listset, $_ADD_ALIAS_SUPPORT = false) {
		$this->listset = $listset;

		$table = $listset->get_table() . "__listitems";
		
		parent::__construct($database, $listset, $table, array(), array(), array(), $_ADD_ALIAS_SUPPORT, "listset");
	}

	//-------------------------------------------------------------------------
	
	public function get_access_required_to_manage() {
		return $this->listset->get_access_required_to_manage();
	}
}
