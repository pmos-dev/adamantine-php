<?php
/**
 * Standard Field within the Adamantine framework.
 * This is a helper model built from other structures rather than corresponding to a specific structure in its own right.
 * 
 * @copyright 2015 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/orientatedordered.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/managedorientatedordered.php";
require_once ADAMANTINE_ROOT_PATH . "models/standardlistitem.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class StandardField_Exception extends ManagedOrientatedOrdered_Exception {}

/**
 * Defines a root model for 'managed' orientated-ordered structured models.
 * 
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
abstract class StandardField extends ManagedOrientatedOrdered {
	// Native types supported. These can be extended in subclasses, or reduced using the $types array in the constructor.
	const SINGLE = "single";
	const MULTI = "multi";
	const BOOLEAN = "boolean";
	const INTEGER = "integer";
	const FLOAT = "float";
	const DATE = "date";
	const TIME = "time";
	const DATETIME = "datetime";
	const PHONE = "phone";
	const EMAIL = "email";
	const URL = "url";
	const LISTSET = "listset";
	
	protected $firstclass, $firstclass_field;
	private $listset;
	
	/**
	 * Constructs a new instance of this standard model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Models\FirstClass $firstclass the FirstClass model to associate with
	 * @param string $table the table name within the database
	 * @param string[] $types the field types permitted
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param StandardListSet $listset the StandardListSet model to associate with, or null for no listset support
	 * @param string $firstclass_field the name to use for the firstclass field, which tends to correspond to the firstclass model name, e.g. "installed"
	 * @throws StandardField_Exception
	 * @throws ManagedOrientatedOrdered_Exception
	 */
	public function __construct(Database\Wrapper $database, Models\FirstClass $firstclass, $table, array $types = array(
			self::SINGLE,
			self::MULTI,
			self::BOOLEAN,
			self::INTEGER,
			self::FLOAT,
			self::DATE,
			self::TIME,
			self::DATETIME,
			self::PHONE,
			self::EMAIL,
			self::URL,
			self::LISTSET
	), array $structure = array(), StandardListSet $listset = null, $firstclass_field = "installed") {
		$this->firstclass = $firstclass;
		$this->firstclass_field = $firstclass_field;
		$this->listset = $listset;
		
		foreach ($types as $type) self::assert_regex(Data\Data::REGEX_PATTERN_ID_NAME, $type);

		$foreign_keys = array();
		
		if (array_key_exists("type", $structure)) throw new StandardField_Exception("The type field is implicit for StandardField models and should not be explicitly stated in the structure");
		$structure["type"] = new Database\Type_Enum(
				$types,
				Database\Type::NOT_NULL,
				null,
				"type__${table}__type"
		);

		if (array_key_exists("optional", $structure)) throw new StandardField_Exception("The optional field is implicit for StandardField models and should not be explicitly stated in the structure");
		$structure["optional"] = new Database\Type_Bool(Database\Type::NOT_NULL, true);

		if (array_key_exists("size", $structure)) throw new StandardField_Exception("The size field is implicit for StandardField models and should not be explicitly stated in the structure");
		$structure["size"] = new Database\Type_SmallInt(Database\Type::ALLOW_NULL);
		
		if (in_array(self::LISTSET, $types) && $listset === null) throw new StandardField_Exception("ListSet support is enabled in the StandardField types, but no StandardListSet model has been supplied.");
		if (!in_array(self::LISTSET, $types) && $listset !== null) throw new StandardField_Exception("ListSet support is disabled in the StandardField types, but a StandardListSet model has been supplied. This is not technically impossible, but is very likely a mistake.");
		
		if ($listset !== null) {
			if (array_key_exists("listset", $structure)) throw new StandardField_Exception("The listset field is implicit for StandardField models and should not be explicitly stated in the structure");
			$structure["listset"] = new Database\Type_Id(Database\Type::ALLOW_NULL, null);

			if (array_key_exists("allow_multiple", $structure)) throw new StandardField_Exception("The allow_multiple field is implicit for StandardField models and should not be explicitly stated in the structure");
			$structure["allow_multiple"] = new Database\Type_Bool(Database\Type::NOT_NULL, false);
			
			$foreign_keys["listset"] = new \Abstraction\Models\ForeignKey($listset, "SET NULL", "CASCADE", $table . "__fk_listset");
		}
		
		parent::__construct($database, $firstclass, $table, $structure, $foreign_keys, array(), false, $firstclass_field);
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns the ListSet model, if any, associated with this StandardField.
	 * 
	 * @return StandardListSet|null
	 */
	public function get_listset_model() {
		return $this->listset;
	}
		
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	public function get_view_additional_file() {
		return ADAMANTINE_ROOT_PATH . "management/_fields_additional.php";
	}
	
	public function get_manageable_fields() {
		$fields = array();
		foreach (parent::get_manageable_fields() as $field) if (!in_array($field, array("listset", "allow_multiple"))) $fields[] = $field;
	
		return $fields;
	}
	
	/**
	 * Called before changing the StandardField listset by management. Allows existing data to be wiped and tidied up.
	 * 
	 * @param array $field the StandardField object that is about to change listset.
	 * @return boolean true for success; false to abort the operation.
	 */
	public function pre_change_listset_wipe_data(array $field) {
		return true;
	}
	
	//-------------------------------------------------------------------------
	
	public function validate(array $field, &$data, array $listset_firstclass_context = null, $string_listset_values = false) {
		if (!isset($data)) $data = null;
	
		switch ($field["type"]) {
			case self::LISTSET:
				if ($this->listset === null) throw new StandardField_Exception("Attempted to validate a ListSet field against a StandardField with no listset support");

				if ($field["listset"] === null) break;
				
				if ($listset_firstclass_context === null) throw new StandardField_Exception("ListSet firstclass context for StandardField has not been supplied");
	
				$listset = $this->listset->get_by_firstclass_and_id($listset_firstclass_context, $field["listset"]);
				$items = array();
	
				if ($field["allow_multiple"]) {
					if (!isset($data)) $data = array();
					if (!is_array($data)) $data = array($data);
					foreach ($data as $item) {
						if ($string_listset_values) {
							if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $item)) return false;
							if (null === ($item = $this->listset->get_listitem_model()->get_by_name($listset, $item))) return false;
							
							$items[] = $item;
						} else {
							if (!Data\Data::validate_id($item)) return false;
							$items[] = $this->listset->get_listitem_model()->get_by_firstclass_and_id($listset, $item);
						}
					}
				} else {
					if ($string_listset_values) {
						if (Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $data)) {
							if (null === ($listitem = $this->listset->get_listitem_model()->get_by_name($listset, $data))) return false;
							$items[] = $listitem;
						}
					} else {
						if (Data\Data::validate_id($data)) {
							if (null === ($listitem = $this->listset->get_listitem_model()->get_by_firstclass_and_id($listset, $data))) return false;
							$items[] = $listitem;
						}
					}
				}
	
				if (sizeof($items) < 1 && !$field["optional"]) return false;
	
				$data = $items;
				break;
			case self::SINGLE:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_string($data)) return false;
				if ($field["size"] !== null && strlen($data) > $field["size"]) return false;
				break;
			case self::MULTI:
				$data = Data\Data::trim_text_or_null($data);
				if ($data === null && $field["optional"]) break;
				if ($field["size"] !== null && strlen($data) > $field["size"]) return false;
				break;
			case self::BOOLEAN:
				$data = Data\Data::checkbox_boolean($data) ? "true" : "false";
				break;
			case self::INTEGER:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_int($data)) return false;
				break;
			case self::FLOAT:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_numeric($data)) return false;
				break;
			case self::DATE:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_DMY, $data)) return false;
				break;
			case self::TIME:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_TIME_HI, $data)) return false;
				break;
			case self::DATETIME:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_DMYHI, $data)) return false;
				break;
			case self::PHONE:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_PHONE, $data)) return false;
				if ($field["size"] !== null && strlen($data) > $field["size"]) return false;
				break;
			case self::EMAIL:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_EMAIL, $data)) return false;
				if ($field["size"] !== null && strlen($data) > $field["size"]) return false;
				break;
			case self::URL:
				if (($data === null || $data === "") && $field["optional"]) break;
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_URL, $data)) return false;
				if ($field["size"] !== null && strlen($data) > $field["size"]) return false;
				break;
			default:
				return false;
		}
	
		return true;
	}
}
