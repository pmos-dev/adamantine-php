<?php
/**
 * Instance variable setting modelling within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/models.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/area.php";
require_once ADAMANTINE_ROOT_PATH . "models/instance.php";
require_once ADAMANTINE_ROOT_PATH . "models/installed.php";
require_once ADAMANTINE_ROOT_PATH . "models/variable.php";

if (!defined("APP_ROOT_PATH")) die("APP_ROOT_PATH has not been set.");

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * @internal
 */
class NoSettingExistsYetException extends \Exception {}

/**
 * Defines the model for instance variable settings.
 *
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Setting extends Models\Models {
	protected $instance, $area, $installed, $variable;

	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Instance $instance the Instance model to associate with
	 * @param Area $area the Area model to associate with
	 * @param Installed $installed the Installed model to associate with
	 * @param Variable $variable the Variable model to associate with
	 */
	public function __construct(Database\Wrapper $database, Instance $instance, Area $area, Installed $installed, Variable $variable) {
		$this->instance = $instance;
		$this->area = $area;
		$this->installed = $installed;
		$this->variable = $variable;

		parent::__construct($database, Config\DATABASE_PREFIX . "settings", array(
			"installed" => new Database\Type_Id(Database\Type::NOT_NULL),
			"variable" => new Database\Type_Id(Database\Type::NOT_NULL),
				
			"value" => new Database\Type_Encrypted(Config\ENCRYPTION_PASSPHRASE, Database\Type::ALLOW_NULL)
		), array(
			"installed" => new Models\ForeignKey($installed, "CASCADE", "CASCADE", Config\DATABASE_PREFIX . "_setting__fk_installed"),
			"variable" => new Models\ForeignKey($variable, "CASCADE", "CASCADE", Config\DATABASE_PREFIX . "_setting__fk_variable")
		), array(
			new Models\UniqueKey(array("installed", "variable"), Config\DATABASE_PREFIX . "_setting__uk_iv")
		));
	}

	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function define_views() {
		parent::define_views();

		$this->define_view("DERIVED_VARIABLES", "
			SELECT " . $this->modelfield($this->instance, "id") . " AS " . $this->tempfield("instance") . ",
				" . $this->modelfield($this->area, "name") . " AS " . $this->tempfield("area") . ",
				" . $this->modelfield($this->variable, "id") . ",
				" . $this->modelfield($this->variable, "name") . ",
				" . $this->modelfield($this->variable, "type") . ",
				" . $this->modelfield($this->variable, "default") . "
			FROM " . $this->model($this->variable) . "
			INNER JOIN " . $this->model($this->area) . "
				ON " . $this->modelfield($this->variable, "area") . "=" . $this->modelfield($this->area, $this->area->get_id_field()) . "
			INNER JOIN " . $this->model($this->installed) . "
				ON " . $this->modelfield($this->installed, "area") . "=" . $this->modelfield($this->area, $this->area->get_id_field()) . "
			INNER JOIN " . $this->model($this->instance) . "
				ON " . $this->modelfield($this->installed, "instance") . "=" . $this->modelfield($this->instance, $this->instance->get_id_field()) . "
		");

		$this->define_view("DERIVED_SETTINGS", "
			SELECT " . $this->modelfield($this->instance, "id") . " AS " . $this->tempfield("instance") . ",
				" . $this->modelfield($this, "variable") . ",
				" . $this->modelfield($this, "value") . "
			FROM " . $this->model($this) . "
			INNER JOIN " . $this->model($this->installed) . "
				ON " . $this->modelfield($this, "installed") . "=" . $this->modelfield($this->installed, "id") . "
			INNER JOIN " . $this->model($this->instance) . "
				ON " . $this->modelfield($this->installed, "instance") . "=" . $this->modelfield($this->instance, "id") . "
		");

		$this->define_view("LOAD_ALL", "
			SELECT " . $this->derivedfield("derived_variables", "instance") . ",
				" . $this->derivedfield("derived_variables", "area") . ",
				" . $this->derivedfield("derived_variables", "name") . ",
				" . $this->derivedfield("derived_variables", "type") . ",
				(CASE WHEN (" . $this->derivedfield("derived_settings", "value") . " IS NULL)
					THEN " . $this->derivedfield("derived_variables", "default") . "
					ELSE " . $this->derivedfield("derived_settings", "value") . "
					END
				) AS " . $this->tempfield("value") . "
			FROM " . $this->modelview($this, "DERIVED_VARIABLES") . " AS " . $this->derived("derived_variables") . "
			LEFT JOIN " . $this->modelview($this, "DERIVED_SETTINGS") . " AS " . $this->derived("derived_settings") . "
				ON " . $this->derivedfield("derived_settings", "variable") . "=" . $this->derivedfield("derived_variables", "id") . "
			AND " . $this->derivedfield("derived_settings", "instance") . "=" . $this->derivedfield("derived_variables", "instance") . "
		");
	}

	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$this->database->preprepare("ADAMANTINE__Setting__{$this->table}__GET", "
			SELECT " . $this->modelfield($this, "value") . "
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, "installed") . "=:installed
			AND " . $this->modelfield($this, "variable") . "=:variable
		", array(
			"installed" => new Database\Type_Id(Database\Type::NOT_NULL),
			"variable" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array("value" => $this->structure["value"]));

		$variable_structure = $this->variable->get_structure();
		$this->database->preprepare("ADAMANTINE__Setting__{$this->table}__LOAD_ALL", "
			SELECT " . $this->modelviewfield($this, "LOAD_ALL", "area") . ",
				" . $this->modelviewfield($this, "LOAD_ALL", "name") . ",
				" . $this->modelviewfield($this, "LOAD_ALL", "type") . ",
				" . $this->modelviewfield($this, "LOAD_ALL", "value") . "
			FROM " . $this->modelview($this, "LOAD_ALL") . "
			WHERE " . $this->modelviewfield($this, "LOAD_ALL", "instance") . "=:instance
		", array(
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array(
			"area" => new Database\Type_IdName(Database\Type::NOT_NULL),
			"name" => new Database\Type_IdName(Database\Type::NOT_NULL),
			"type" => $variable_structure["type"],
			"value" => $this->structure["value"]
		));
	}

	//-------------------------------------------------------------------------

	/**
	 * Fetches the setting result stored for the specified installed area and variable combination.
	 * 
	 * @param mixed[] $installed an existing row within the Installed model
	 * @param mixed[] $variable an existing row within the Variable model
	 * @param string $allow_none true to return null for no matches, or false to throw an exception
	 * @throws NoSettingExistsYetException
	 * @return mixed[]|NULL
	 */
	public function get(array $installed, array $variable, $allow_none = true) {
		$value = $this->database->execute_params_value("ADAMANTINE__Setting__{$this->table}__GET", array(
			"installed" => $installed["id"],
			"variable" => $variable["id"]
		), true);
		if ($value === null) {
			if ($allow_none) return null;
			else throw new NoSettingExistsYetException();
		}

		return json_decode($value);
	}

	/**
	 * Returns an array of variables and their corresponding setting for the specified installed area.
	 * 
	 * @param mixed[] $instance an existing row within the Instance model
	 * @return array
	 */
	public function list_all_by_instance(array $instance) {
		$all = array();
		foreach ($this->database->execute_params("ADAMANTINE__Setting__{$this->table}__LOAD_ALL", array(
			"instance" => $instance[$this->instance->get_id_field()]
		)) as $row) {
			$row["value"] = json_decode($row["value"]);
			$all[] = $row;
		}

		return $all;
	}

	/**
	 * Stores a value in the setting Model table for the specified installed area and variable combination.
	 * 
	 * @param mixed[] $installed an existing row within the Installed model
	 * @param mixed[] $variable an existing row within the Variable model
	 * @param mixed the value to store
	 * @throws Models\Exception
	 * @return true always returns true; failure will throw an exception
	 */
	public function set(array $installed, array $variable, $value) {
		switch ($this->database->get_db_type()) {
			case Database\Wrapper::DATABASE_MYSQL:
				$this->database->replace_row($this->table, array(
					"installed" => new Database\Param($installed["id"], $this->structure["installed"]),
					"variable" => new Database\Param($variable["id"], $this->structure["variable"]),
					"value" => new Database\Param(json_encode($value), $this->structure["value"])
				));
				break;
			case Database\Wrapper::DATABASE_POSTGRESQL:
				$_TRANSACTION_OWNER = $this->database->transaction_claim();
				
				if ($this->database->does_row_exist_by_conditions($this->table, array(
					"installed" => new Database\Param($installed["id"], $this->structure["installed"]),
					"variable" => new Database\Param($variable["id"], $this->structure["variable"])
				))) $this->database->delete_rows_by_conditions($this->table, array(
					"installed" => new Database\Param($installed["id"], $this->structure["installed"]),
					"variable" => new Database\Param($variable["id"], $this->structure["variable"])
				), true);
				$this->database->insert_row($this->table, array(
					"installed" => new Database\Param($installed["id"], $this->structure["installed"]),
					"variable" => new Database\Param($variable["id"], $this->structure["variable"]),
					"value" => new Database\Param(json_encode($value), $this->structure["value"])
				));
				
				if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
				break;
			default:
				throw new Models\Exception("Unsupported for this database type");
		}

		return true;
	}
}
