<?php
/**
 * Temporary session for REST API.
 * This is a helper model built from other structures rather than corresponding to a specific structure in its own right.
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

use \Adamantine as Adamantine;

/**
 * @internal
 */
class TempSession_Exception extends Models\SecondClass_Exception {}

/**
 * Defines a root model for 'managed' orientated-ordered structured models.
 * 
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
abstract class TempSession extends Models\SecondClass implements Adamantine\Models\UniqueNamed {
	protected $firstclass, $firstclass_field;
	protected $uid_field;
	
	/**
	 * Constructs a new instance of this standard model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Models\FirstClass $firstclass the FirstClass model to associate with
	 * @param string $table the table name within the database
	 * @param string[] $types the field types permitted
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param StandardListSet $listset the StandardListSet model to associate with, or null for no listset support
	 * @param string $firstclass_field the name to use for the firstclass field, which tends to correspond to the firstclass model name, e.g. "installed"
	 * @throws TempSession_Exception
	 * @throws ManagedOrientatedOrdered_Exception
	 */
	public function __construct(
				Database\Wrapper $database, 
				Models\FirstClass $firstclass, 
				$table, 
				array $structure, 
				array $foreign_keys = array(), 
				array $unique_keys = array(), 
				$firstclass_field = "installed",
				$uid_field = "uid"
		) {
		$this->firstclass = $firstclass;
		$this->firstclass_field = $firstclass_field;
		$this->uid_field = $uid_field;
		
		if (array_key_exists($this->uid_field, $structure)) throw new TempSession_Exception("The uid field is implicit for TempSession models and should not be explicitly stated in the structure");
		$structure[$this->uid_field] = new Database\Type_IdName(Database\Type::NOT_NULL);
		
		if (array_key_exists("timestamp", $structure)) throw new TempSession_Exception("The timestamp field is implicit for TempSession models and should not be explicitly stated in the structure");
		$structure["timestamp"] = new Database\Type_DateTime(Database\Type::NOT_NULL);
		
		$unique_keys[] = new Models\UniqueKey(array($firstclass_field, $this->uid_field), "{$table}_uk_in");
		
		parent::__construct(
				$database, 
				$table, 
				$structure, 
				$firstclass, 
				$this->firstclass_field, 
				"id", 
				$foreign_keys, 
				$unique_keys
		);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();
		
		$this->database->preprepare("ADAMANTINE__TEMPSESSION__{$this->table}__DELETE_EXIPIRED", "
			DELETE FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, "timestamp") . " < :threshold
		", array(
				"threshold" => new Database\Type_DateTime(Database\Type::NOT_NULL)
		));
		
		Adamantine\Models\UniqueNamedHelper::build($this->database)->build_preprepare_for_firstclass($this, $this->uid_field);
		Adamantine\Models\UniqueNamedHelper::build($this->database)->build_preprepare_for_secondclass($this, $this->uid_field);
	}

	//-------------------------------------------------------------------------

	public function get_by_firstclass_and_uid(array $firstclass, $uid) {
		self::assert_id_object($firstclass);
		self::assert_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $uid);
		
		return Adamantine\Models\UniqueNamedHelper::build($this->database)->get_secondclass_by_firstclass_and_name($this, $firstclass, $uid, $this->uid_field);
	}
	
	public function get_by_uid($uid) {
		self::assert_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $uid);
		
		return Adamantine\Models\UniqueNamedHelper::build($this->database)->get_firstclass_by_name($this, $uid, $this->uid_field);
	}
	
	public function start(array $firstclass, $values = null) {
		self::assert_id_object($firstclass, $this->get_firstclass()->get_id_field());
		
		if ($values !== null) {
			self::assert_array($values);
			if (array_key_exists($this->uid_field, $values)) throw new TempSession_Exception("The UID was specified in the values. This is not allowed.");
		}
		else $values = array();

		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		$uid = null;
		for ($i = 1000; $i-- > 0;) {
			$attempt = Database\Type_Base62BigId::generate_random_base62_id();
			if (null === $this->get_by_uid($attempt)) {
				$uid = $attempt;
				break;
			}
		}
		
		if ($uid === null) {
			if ($_TRANSACTION_OWNER) $this->database->transaction_rollback();
			throw new TempSession_Exception("Unable to generate a unique Base62 uid in over 1000 attempts. This is highly unlikely.");
		}
		
		$values[$this->uid_field] = $uid;
		$values["timestamp"] = date("Y-m-d H:i:s");
		
		$this->insert_for_firstclass($firstclass, $values);
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();

		return $this->get_by_firstclass_and_uid($firstclass, $uid);
	}
	
	public function touch(array $firstclass, array $tempsession) {
		self::assert_id_object($firstclass, $this->get_firstclass()->get_id_field());
		self::assert_id_object($tempsession, $this->get_id_field());
		
		$tempsession["timestamp"] = date("Y-m-d H:i:s");
		$this->update_for_firstclass($firstclass, $tempsession);
		
		return true;
	}
		
	public function stop(array $firstclass, array $tempsession) {
		self::assert_id_object($firstclass, $this->get_firstclass()->get_id_field());
		self::assert_id_object($tempsession, $this->get_id_field());

		return $this->delete_for_firstclass($firstclass, $tempsession);
	}
	
	public function delete_expired($threshold) {
		self::assert_regex(Data\Data::REGEX_PATTERN_DATETIME_YMDHIS, $threshold);

		return $this->database->execute_params("ADAMANTINE__TEMPSESSION__{$this->table}__DELETE_EXIPIRED", array(
				"threshold" => $threshold
		));
	}
}
