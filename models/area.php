<?php
/**
 * Area modelling within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/area.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";

if (!defined("APP_ROOT_PATH")) die("APP_ROOT_PATH has not been set.");

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Framework as Framework;
use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * Defines the model for areas.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Area extends Framework\Models\Area implements UniqueNamed {
	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 */
	public function __construct(Database\Wrapper $database) {
		parent::__construct($database, Config\DATABASE_PREFIX . "areas", array(
			"name" => new Database\Type_IdName(Database\Type::NOT_NULL)
		), array(), array(
			new Models\UniqueKey(array("name"), Config\DATABASE_PREFIX . "_areas__uk_n")
		));
	}

	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();
		
		UniqueNamedHelper::build($this->database)->build_preprepare_for_firstclass($this);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the model row with the specified name.
	 * 
	 * @param string $name the unique name of the area
	 * @return mixed[]|NULL
	 */
	public function get_by_name($name) {
		return UniqueNamedHelper::build($this->database)->get_firstclass_by_name($this, $name);
	}
}
