<?php
/**
 * Area variable modelling within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/area.php";

if (!defined("APP_ROOT_PATH")) die("APP_ROOT_PATH has not been set.");

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * Defines the model for area variables.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Variable extends Models\SecondClass implements UniqueNamed {
	const INTEGER = "integer";
	const UNSIGNED = "unsigned";
	const BOOLEAN = "boolean";
	const STRING = "string";
	const SELECT = "select";
	
	/**
	 * Constructs a new instance of this model.
	 * 
* @param Database\Wrapper $database the database interface to build the model upon
	 * @param Area $area the Area model to associate with
	 */
	public function __construct(Database\Wrapper $database, Area $area) {
		parent::__construct($database, Config\DATABASE_PREFIX . "variables", array(
			"name" => new Database\Type_IdName(Database\Type::NOT_NULL),
			"description" => new Database\Type_String(255, Database\Type::NOT_NULL),
			
			"type" => new Database\Type_Enum(array(
				self::INTEGER,
				self::UNSIGNED,
				self::BOOLEAN,
				self::STRING,
				self::SELECT
			), Database\Type::NOT_NULL, self::STRING, "type__" . Config\DATABASE_PREFIX . "variables__type"),
			
			"options" => new Database\Type_JSON(Database\Type::ALLOW_NULL),
			"default" => new Database\Type_Encrypted(Config\ENCRYPTION_PASSPHRASE, Database\Type::ALLOW_NULL)
		), $area, "area", "id", array(), array(
			new Models\UniqueKey(array("area", "name"), Config\DATABASE_PREFIX . "_variable__uk_an")
		));
	}

	//-------------------------------------------------------------------------
	
	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		UniqueNamedHelper::build($this->database)->build_preprepare_for_secondclass($this);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns an array of all the variable rows associated with the specified area.
	 * 
	 * @param mixed[] $area the area to fetch the variables for
	 * @return array
	 */
	public function list_by_area(array $area) {
		return UniqueNamedHelper::build($this->database)->list_secondclass_by_firstclass_order_by_name($this, $area);
	}

	/**
	 * Fetches the variable row with the specified name for the specified area, if it exists.
	 * 
	 * @param mixed[] $area the area to fetch the variables for
	 * @param string $name the unique name of the variable
	 * @return mixed[]|NULL
	 */
	public function get_by_name(array $area, $name) {
		return UniqueNamedHelper::build($this->database)->get_secondclass_by_firstclass_and_name($this, $area, $name);
	}
}
