<?php
/**
 * User modelling within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/user.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";
require_once ADAMANTINE_ROOT_PATH . "models/manageable.php";
require_once ADAMANTINE_ROOT_PATH . "models/instance.php";

if (!defined("APP_ROOT_PATH")) die("APP_ROOT_PATH has not been set.");

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Framework as Framework;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * Defines the model for instance users.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class User extends Framework\Models\User implements Manageable {
	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Instance $instance the Instance model to associate with
	 * @param Database\Type[] $additional_structure an associated array to define any further content within the model structure, taking the format field name => field type object
	 * @param Database\ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param Database\UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @throws Adamantine\Exception
	 */
	public function __construct(Database\Wrapper $database, Instance $instance, array $additional_structure = array(), array $foreign_keys = array(), array $unique_keys = array()) {
		// this method is necessary to ensure that name always comes first in the order
		
		$structure = array();
		if (array_key_exists("name", $additional_structure)) throw new Adamantine\Exception("The name field is implicit for user models and should not be explicitly stated in the structure");
		$structure["name"] = new Database\Type_IdName(Database\Type::NOT_NULL);

		if (array_key_exists("fullname", $additional_structure)) throw new Adamantine\Exception("The fullname field is implicit for user models and should not be explicitly stated in the structure");
		$structure["fullname"] = new Database\Type_String(128, Database\Type::NOT_NULL);
		
		if (array_key_exists("one_time_pad_md5", $additional_structure)) throw new Adamantine\Exception("The one_time_pad_md5 field is implicit for user models and should not be explicitly stated in the structure");
		$structure["one_time_pad_md5"] = new Database\Type_MD5(Database\Type::ALLOW_NULL, null);
		
		if (array_key_exists("last_logged_in", $additional_structure)) throw new Adamantine\Exception("The last_logged_in field is implicit for user models and should not be explicitly stated in the structure");
		$structure["last_logged_in"] = new Database\Type_DateTime(Database\Type::ALLOW_NULL, null);

		foreach ($additional_structure as $var => $type) $structure[$var] = $type;
		
		$unique_keys[] = new Models\UniqueKey(array("instance", "name"), Config\DATABASE_PREFIX . "_users__uk_in");
		
		parent::__construct($database, Config\DATABASE_PREFIX . "users", $instance, $structure, $foreign_keys, $unique_keys);
	}

	//-------------------------------------------------------------------------

	/**
	 * @internal
	 * @see Manageable::is_model_manageable()
	 */
	public function is_model_manageable() { 
		return true; 
	}

	/**
	 * Returns the repository name automatically by parsing the class name out of the namespace.
	 * 
	 * Subclasses who's repository name does not match the class name should override this method to change this behaviour.
	 * 
	 * @internal
	 * @see Manageable::get_repository_name()
	 * @throws Adamantine\Exception
	 * @return string
	 */
	public function get_repository_name() {
		if (!preg_match("`\\\([-a-z0-9_]+)$`iD", get_class($this), $array)) throw new Adamantine\Exception("Unable to automatically parse classname from model namespace");
		return $array[1];
	}

	/**
	 * @internal
	 * @see Manageable::is_aliasing_supported();
	 */
	public function is_aliasing_supported() { 
		return false; 
	}
	
	/**
	 * @internal
	 * @see Manageable::get_manageable_fields()
	 */
	public function get_manageable_fields() {
		$fields = array();
		foreach (array_keys($this->structure) as $field) if (!in_array($field, array("id", "instance", "type", "one_time_pad_md5", "last_logged_in"))) $fields[] = $field;
		
		return $fields;
	}

	/**
	 * @internal
	 * @see Manageable::get_access_required_to_manage()
	 */
	public function get_access_required_to_manage() {
		return "editor";
	}

	/**
	 * @internal
	 * @see Manageable::is_object_viewable()
	 */
	public function is_object_viewable(array $user) {
		return true;
	}

	/**
	 * @internal
	 * @see Manageable::is_object_editable()
	 */
	public function is_object_editable(array $user) {
		return $user["type"] === Framework\Models\User::NORMAL;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_field_description()
	 */
	public function get_field_description($field) {
		if ($field === "fullname") return "full name";
		
		return $field;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_field_suffix()
	 */
	public function get_field_suffix($field) {
		return null;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_view_additional_file()
	 */
	public function get_view_additional_file() {
		return ADAMANTINE_ROOT_PATH . "areas/admin/_users_additional.php";
	}
			
	//-------------------------------------------------------------------------
	
	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();
		
		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);

		$this->database->reprepare("MODELS_SECONDCLASS__{$this->table}__LIST_BY_FIRSTCLASS", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
			ORDER BY " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "type") . "=" . $this->database->quote(Framework\Models\User::NORMAL) . " ASC,
				" . $this->modelviewfield($this, "SECONDCLASS_JOIN", "name") . " ASC
		", array(
			"firstclass" => $this->structure[$this->firstclass_field]
		), $this->structure);
		
		$this->database->preprepare("ADAMANTINE__User__LIST_NORMAL_INSTANCE", "
			SELECT {$this->fields}
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, "instance") . "=:instance
			AND " . $this->modelfield($this, "type") . "=" . $this->database->quote(self::NORMAL) . "
			ORDER BY " . $this->modelfield($this, "name") . " ASC
		", array(
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL)
		), $this->structure);
		
		UniqueNamedHelper::build($this->database)->build_preprepare_for_secondclass($this);
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Returns an array of all the users associated with the specified instance.
	 * 
	 * @param mixed[] $instance the instance to fetch the users for
	 * @return array
	 */
	public function list_by_instance(array $instance) {
		return UniqueNamedHelper::build($this->database)->list_secondclass_by_firstclass_order_by_name($this, $instance);
	}

	/**
	 * Returns an array of all the 'normal' users associated with the specified instance, i.e. not root and guest user.
	 * 
	 * @param mixed[] $instance the instance to fetch the users for
	 * @return array
	 */
	public function list_normal_by_instance(array $instance) {
		self::assert_id_object($instance);
		
		return $this->database->execute_params("ADAMANTINE__User__LIST_NORMAL_INSTANCE", array(
			"instance" => $instance["id"]
		));
	}

	//-------------------------------------------------------------------------
	
	/**
	 * Fetches the user row with the specified user name for the specified instance, if it exists.
	 * 
	 * @param mixed[] $instance the instance associated with the user
	 * @param string $name the unique name of the user
	 * @return mixed[]|NULL
	 */
	public function get_by_name(array $instance, $name) {
		return UniqueNamedHelper::build($this->database)->get_secondclass_by_firstclass_and_name($this, $instance, $name);
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Generates a one-time-pad for the specified user and stores it in the row.
	 * 
	 * Returns an generated MD5 one-time-pad as a string for further use.
	 * 
	 * @param mixed[] $user the user for generate the one-time-pad for
	 * @return string
	 */
	public function generate_one_time_pad_md5(array $user) {
		self::assert_id_object($user, $this->get_id_field());
		
		$user["one_time_pad_md5"] = $this->structure["one_time_pad_md5"]->hash(Data\Crypt::generate_random_password());
		$this->update($user);

		return $user["one_time_pad_md5"];
	}
	
	/**
	 * Attempts to use a MD5 one-time-pad for the specified user.
	 * 
	 * The one-time-pad for that user is expired regardless of success for security reasons.
	 * 
	 * @param mixed[] $user the user for authenticate the one-time-pad against
	 * @param string $md5 the one-time-pad in MD5 notation
	 * @throws Adamantine\Exception
	 * @return boolean
	 */
	public function use_one_time_pad_md5(array $user, $md5) {
		self::assert_id_object($user, $this->get_id_field());
		self::assert_regex(Data\Data::REGEX_PATTERN_MD5, $md5);
		
		$existing = $user["one_time_pad_md5"];
		$user["one_time_pad_md5"] = null;
		if (!$this->update($user)) throw new Adamantine\Exception("Unable to expire one time pad");
		
		return $existing === $md5;
	}
	
	/**
	 * Sets the last_logged_in for a user.
	 * 
	 * @param mixed[] $user the user for generate the one-time-pad for
	 * @return void
	 */
	public function set_last_logged_in(array $user) {
		self::assert_id_object($user, $this->get_id_field());
		
		$user["last_logged_in"] = date("Y-m-d H:i:s");
		$this->update($user);
	}
}
