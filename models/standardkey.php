<?php
/**
 * Standard Key within the Adamantine framework.
 * This is a helper model built from other structures rather than corresponding to a specific structure in its own right.
 * 
 * @copyright 2018 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");

require_once ADAMANTINE_ROOT_PATH . "models/managedorientatedordered.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

use \Adamantine as Adamantine;

/**
 * @internal
 */
class StandardKey_Exception extends ManagedOrientatedOrdered_Exception {}

/**
 * Defines a root model for 'standard' key structured models, e.g. external access
 * 
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
abstract class StandardKey extends ManagedOrientatedOrdered implements Adamantine\Models\UniqueNamed {
	const TYPE_BASE36 = "base36";
	const TYPE_BASE62 = "base62";
	
	private $type;
	
	protected $key_field;
	
	/**
	 * Constructs a new instance of this standard model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Models\FirstClass $firstclass the FirstClass model to associate with
	 * @param string $table the table name within the database
	 * @param string[] $types the field types permitted
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param array $foreign_keys
	 * @param array $unique_keys
	 * @param string $firstclass_field the name to use for the firstclass field, which tends to correspond to the firstclass model name, e.g. "installed"
	 * @param string $key_field
	 * @param string $type the key type to use, e.g. TYPE_BASE62
	 * @throws StandardKey_Exception
	 */
	public function __construct(
				Database\Wrapper $database, 
				Models\FirstClass $firstclass, 
				$table, 
				array $structure, 
				array $foreign_keys = array(), 
				array $unique_keys = array(), 
				$firstclass_field = "installed",
				$key_field = "key",
				$type = self::TYPE_BASE62
		) {
		if (!Data\Data::validate_option(array(self::TYPE_BASE36, self::TYPE_BASE62), $type)) throw new StandardKey_Exception("Key type is not valid");
		$this->type = $type;
		
		$this->key_field = $key_field;
		
		if (array_key_exists($this->key_field, $structure)) throw new StandardKey_Exception("The key field is implicit for StandardKey models and should not be explicitly stated in the structure");
		switch ($this->type) {
			case self::TYPE_BASE36:
				$structure[$this->key_field] = new Database\Type_Base36BigId(Database\Type::ALLOW_NULL);
				break;
			case self::TYPE_BASE62:
				$structure[$this->key_field] = new Database\Type_Base62BigId(Database\Type::ALLOW_NULL);
				break;
		}
		
		$unique_keys[] = new Models\UniqueKey(array($this->key_field), "{$table}_uk_k");

		parent::__construct(
				$database, 
				$firstclass,
				$table, 
				$structure, 
				$foreign_keys,
				$unique_keys,
				false,
				$firstclass_field
		);
	}
	
	public function get_type() {
		return $this->type;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$case_sensitive = $this->type === self::TYPE_BASE62;
		
		Adamantine\Models\UniqueNamedHelper::build($this->database)->build_preprepare_for_firstclass($this, $this->key_field, $case_sensitive);
		Adamantine\Models\UniqueNamedHelper::build($this->database)->build_preprepare_for_secondclass($this, $this->key_field, $case_sensitive);
	}

	//-------------------------------------------------------------------------

	public function get_manageable_fields() {
		$fields = array();
		foreach (parent::get_manageable_fields() as $field) if ($field !== $this->key_field) $fields[] = $field;
		
		return $fields;
	}
	
	//-------------------------------------------------------------------------
	
	public function get_by_firstclass_and_key(array $firstclass, $key) {
		self::assert_id_object($firstclass);
		
		switch ($this->type) {
			case self::TYPE_BASE36:
				self::assert_regex(Data\Data::REGEX_PATTERN_BASE36_ID, $key);
				break;
			case self::TYPE_BASE62:
				self::assert_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $key);
				break;
		}
		
		return Adamantine\Models\UniqueNamedHelper::build($this->database)->get_secondclass_by_firstclass_and_name($this, $firstclass, $key, $this->key_field);
	}
	
	public function get_by_key($key) {
		switch ($this->type) {
			case self::TYPE_BASE36:
				self::assert_regex(Data\Data::REGEX_PATTERN_BASE36_ID, $key);
				break;
			case self::TYPE_BASE62:
				self::assert_regex(Data\Data::REGEX_PATTERN_BASE62_ID, $key);
				break;
		}
		
		return Adamantine\Models\UniqueNamedHelper::build($this->database)->get_firstclass_by_name($this, $key, $this->key_field);
	}
	
	//-------------------------------------------------------------------------
	
	public function regenerate_key(array $object) {
		self::assert_id_object($object, $this->id_field);
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		try {
			$key = Adamantine\Models\UniqueNamedHelper::build($this->database)->generate_uid($this, $this->type, $this->key_field);
			
			$object[$this->key_field] = $key;
			
			$this->update($object);
			
			if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
			
			return true;
		} catch(\Exception $e) {
			if ($_TRANSACTION_OWNER) $this->database->transaction_rollback();
			throw $e;
		}
	}
	
	public function clear_key(array $object) {
		self::assert_id_object($object, $this->id_field);
		
		$object[$this->key_field] = null;
		
		return $this->update($object);
	}
}

/**
 * Defines a root model for 'standard' Base36 key models.
 *
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
class StandardKey_Base36 extends StandardKey {
	public function __construct(
			Database\Wrapper $database,
			Models\FirstClass $firstclass,
			$table,
			array $structure,
			array $foreign_keys = array(),
			array $unique_keys = array(),
			$firstclass_field = "installed",
			$key_field = "key"
	) {
		parent::__construct(
				$database,
				$firstclass,
				$table,
				$structure,
				$foreign_keys,
				$unique_keys,
				$firstclass_field,
				$key_field,
				self::TYPE_BASE36
		);
	}
}

/**
 * Defines a root model for 'standard' Base62 key models.
 *
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
class StandardKey_Base62 extends StandardKey {
	public function __construct(
			Database\Wrapper $database,
			Models\FirstClass $firstclass,
			$table,
			array $structure,
			array $foreign_keys = array(),
			array $unique_keys = array(),
			$firstclass_field = "installed",
			$key_field = "key"
	) {
		parent::__construct(
				$database,
				$firstclass,
				$table,
				$structure,
				$foreign_keys,
				$unique_keys,
				$firstclass_field,
				$key_field,
				self::TYPE_BASE62
		);
	}
}
