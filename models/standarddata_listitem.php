<?php
/**
 * Standard FirstClass/Field/ListSet data within the Adamantine framework.
 * This is a helper model built from other structures rather than corresponding to a specific structure in its own right.
 * It joins the StandardField to another FirstClass (or SecondClass/orientated etc.) to store ListSet ListItem entries for the combination.
 * 
 * @copyright 2015 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/m2mlinktable.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/standardfield.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class StandardData_ListItem_Exception extends Models\Exception {}

/**
 * Defines a root model for 'standard' data storage of ListItems between StandardField and a FirstClass.
 * 
 * @api
 * @author Pete Morris
 * @version 1.0.0
 */
abstract class StandardData_ListItem extends Models\Models {
	protected $owner, $owner_field;
	protected $standardfield, $standardfield_field;
	protected $listset;
	
	/**
	 * Constructs a new instance of this standard model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Models\FirstClass $owner the FirstClass model to associate with
	 * @param StandardField $standardfield the StandardField model to associate with
	 * @param StandardListSet $listset the StandardListSet model to associate with
	 * @param string $table the table name within the database
	 * @param string $owner_field the name to use for the firstclass field, which tends to correspond to the owning firstclass model name, e.g. "assignment"
	 * @param string $standardfield_field the name to use for the standardfield field, which tends to correspond to the owning field model name, e.g. "field"
	 * @throws StandardField_Exception
	 */
	public function __construct(Database\Wrapper $database, Models\FirstClass $owner, StandardField $standardfield, StandardListSet $listset, $table, $owner_field = "owner", $standardfield_field = "field") {
		$this->owner = $owner;
		$this->owner_field = $owner_field;
		$this->standardfield = $standardfield;
		$this->standardfield_field = $standardfield_field;
		$this->listset = $listset;

		parent::__construct($database, $table, array(
			$owner_field => new Database\Type_Id(Database\Type::NOT_NULL),
			$standardfield_field => new Database\Type_Id(Database\Type::NOT_NULL),
			"listitem" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array(
			$standardfield_field => new Models\ForeignKey($standardfield, "CASCADE", "CASCADE", "{$table}__fk_field"),
			"listitem" => new Models\ForeignKey($listset->get_listitem_model(), "CASCADE", "CASCADE", "{$table}__fk_listitem")
		), array(
			new Models\UniqueKey(array($owner_field, $standardfield_field, "listitem"), "{$table}__uk")
		));
	}
	
	//-------------------------------------------------------------------------
	
	protected function preprepare() {
		parent::preprepare();
		
		$this->database->preprepare("ADAMANTINE__StandardDataListItem__{$this->table}__LIST_BY_OWNER_AND_STANDARDFIELD", "
			SELECT " . $this->modelfield($this->listset->get_listitem_model(), $this->listset->get_listitem_model()->get_id_field()) . ",
				" . $this->modelfield($this->listset->get_listitem_model(), "name") . "
			FROM " . $this->model($this) . "
			INNER JOIN " . $this->model($this->owner) . "
				ON " . $this->modelfield($this, $this->owner_field) . "=" . $this->modelfield($this->owner, $this->owner->get_id_field()) . "
			INNER JOIN " . $this->model($this->standardfield) . "
				ON " . $this->modelfield($this, $this->standardfield_field) . "=" . $this->modelfield($this->standardfield, $this->standardfield->get_id_field()) . "
			INNER JOIN " . $this->model($this->listset) . "
				ON " . $this->modelfield($this->standardfield, "listset") . "=" . $this->modelfield($this->listset, $this->listset->get_id_field()) . "
			INNER JOIN " . $this->model($this->listset->get_listitem_model()) . "
				ON " . $this->modelfield($this->listset->get_listitem_model(), "listset") . "=" . $this->modelfield($this->listset, $this->listset->get_id_field()) . "
				AND " . $this->modelfield($this, "listitem") . "=" . $this->modelfield($this->listset->get_listitem_model(), $this->listset->get_listitem_model()->get_id_field()) . "
			WHERE " . $this->modelfield($this->owner, $this->owner->get_id_field()) . "=:owner
			AND " . $this->modelfield($this->standardfield, $this->standardfield->get_id_field()) . "=:standardfield
		", array(
			"owner" => new Database\Type_Id(Database\Type::NOT_NULL),
			"standardfield" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array(
			$this->listset->get_listitem_model()->get_id_field() => new Database\Type_Id(Database\Type::NOT_NULL),
			"name" => new Database\Type_IdName(Database\Type::NOT_NULL)
		));
	}
	
	//-------------------------------------------------------------------------
	
	public function list_by_owner_and_standardfield(array $owner, array $standardfield) {
		self::assert_id_object($owner, $this->owner->get_id_field());
		self::assert_id_object($standardfield, $this->standardfield->get_id_field());
		
		return $this->database->execute_params("ADAMANTINE__StandardDataListItem__{$this->table}__LIST_BY_OWNER_AND_STANDARDFIELD", array(
			"owner" => $owner["id"],
			"standardfield" => $standardfield["id"]
		), true);
	}
	
	public function set_for_owner_and_standardfield(array $owner, array $standardfield, array $items) {
		self::assert_id_object($owner, $this->owner->get_id_field());
		self::assert_id_object($standardfield, $this->standardfield->get_id_field());
		foreach ($items as $item) self::assert_id_object($item, $this->listset->get_listitem_model()->get_id_field());
		
		// automatically switch to just the first for singulars
		if (!$standardfield["allow_multiple"] && sizeof($items) > 1) $items = array($items[0]);
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		if (!$this->database->delete_rows_by_conditions($this->table, array(
			$this->owner_field => new Database\Param($owner[$this->owner->get_id_field()], $this->structure[$this->owner_field]),
			$this->standardfield_field => new Database\Param($standardfield[$this->standardfield->get_id_field()], $this->structure[$this->standardfield_field])
		))) throw new StandardData_ListItem_Exception("Unable to pre-delete existing items before setting new ones", $this->table);
		
		$data = array(
			$this->owner_field => $owner[$this->owner->get_id_field()],
			$this->standardfield_field => $standardfield[$this->standardfield->get_id_field()],
			"listitem" => null
		);
		foreach ($items as $item) {
			$data["listitem"] = $item[$this->listset->get_listitem_model()->get_id_field()];
			
			if (false === $this->insert($data)) {
				$this->database->transaction_rollback();
				throw new StandardData_ListItem_Exception("Unable to create owner in detail listitem table");
			}
		}
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}
	
	public function add_listitem_for_owner_and_standardfield(array $owner, array $standardfield, array $listitem) {
		self::assert_id_object($owner, $this->owner->get_id_field());
		self::assert_id_object($standardfield, $this->standardfield->get_id_field());
		self::assert_id_object($listitem, $this->listset->get_listitem_model()->get_id_field());
		
		$data = array(
			$this->owner_field => $owner[$this->owner->get_id_field()],
			$this->standardfield_field => $standardfield[$this->standardfield->get_id_field()],
			"listitem" => $listitem[$this->listset->get_listitem_model()->get_id_field()]
		);
		
		if (false === $this->insert($data)) {
			$this->database->transaction_rollback();
			throw new StandardData_ListItem_Exception("Unable to create owner in detail listitem table");
		}
		
		return true;
	}
	
	public function delete_all_for_standardfield(array $standardfield) {
		self::assert_id_object($standardfield, $this->standardfield->get_id_field());
		
		if (!$this->database->delete_rows_by_conditions($this->table, array(
			$this->standardfield_field => new Database\Param($standardfield[$this->standardfield->get_id_field()], $this->structure[$this->standardfield_field])
		))) throw new StandardData_ListItem_Exception("Unable to delete existing field data", $this->table);
		
		return true;
	}
	
}
