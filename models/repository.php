<?php
/**
 * Provides support for a central repository for all models used within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "core.php";

use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

/**
 * @internal
 */
class Repository_Exception extends Adamantine\Exception {}

/**
 * Defines a central repository for storage of area models.
 * 
 * This makes auto-management significantly easier, and prevents global variables or naming conventions having to be used to store each model.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class Repository {
	private $models;
	
	/**
	 * Constructs a new instance.
	 */
	public function __construct() {
		$this->models = array();
	}

	/**
	 * Returns whether or not the specified area exists within this repository.
	 * 
	 * @param string $area the name of the area to check for
	 * @return boolean
	 */
	public function does_area_exist($area) {
		return array_key_exists($area, $this->models);
	}

	/**
	 * Adds a new area to the repository.
	 * 
	 * @param string $area the name of the new area to add
	 * @throws Repository_Exception
	 * @return void
	 */
	public function add_area($area) {
		if ($this->does_area_exist($area)) throw new Repository_Exception("Area already exists in repository", $area);
		$this->models[$area] = array();
	}

	/**
	 * Returns an array of strings corresponding to the names of all models within the specified area.
	 * 
	 * @param string $area the name of the area
	 * @throws Repository_Exception
	 * @return string[]
	 */
	public function list_models_by_area($area) {
		if (!$this->does_area_exist($area)) throw new Repository_Exception("Area does not exist in repository", $area);
		return array_keys($this->models[$area]);
	}
	
	/**
	 * Returns whether or not the specified model exists within this repository.
	 * 
	 * @param string $area the name of the area containing the model
	 * @param string $name the name of the model to check for
	 * @throws Repository_Exception
	 * @return boolean
	 */
	public function does_model_exist($area, $name) {
		if (!$this->does_area_exist($area)) throw new Repository_Exception("Area does not exist in repository", $area);
		return array_key_exists($name, $this->models[$area]);
	}
	
	/**
	 * Adds a new model to the repository
	 * 
	 * @param string $area the name of the area that will contain the model
	 * @param string $name the name of the new model
	 * @param Models\Models $model the model to add
	 * @throws Repository_Exception
	 * @return void
	 */
	public function add($area, $name, Models\Models $model) {
		if ($this->does_model_exist($area, $name)) throw new Repository_Exception("Model already exists in repository", $name);
		if (!is_object($model) || !($model instanceof Models\Models)) throw new Repository_Exception("This is not a model you are trying to add to the repository");
		
		$this->models[$area][$name] = $model;
	}

	/**
	 * Fetches a model from the repository using the specified area and model name.
	 * 
	 * @param string $area the name of the area containing the model
	 * @param string $name the name of the model
	 * @throws Repository_Exception
	 * @return Models\Models
	 */
	public function get($area, $name) {
		if (!$this->does_model_exist($area, $name)) throw new Repository_Exception("Model does not exist in repository", $name);
		return $this->models[$area][$name];
	}
}
