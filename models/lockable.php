<?php
/**
 * Support for models that can be locked between page transactions.
 * 
 * Note this form of locking is about identification and protection, not spin-waiting.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "models/models.php";
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class Lockable_Exception extends Models\Exception {}

/**
 * Internal interface to identify a model as supporting the Lockable constructs.
 * 
 * @internal
 */
interface Lockable {}

/**
 * Various unique naming helper methods that models can make use of.
 * 
 * Since multiple inheritence is not possible for child models, this is a helper class.
 * The build() method creates a new instance of this helper collection, allowing models to apply the methods for themselves.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class LockableHelper extends Models\ModelHelperMethods {
	
	/**
	 * Returns a newly constructed instance around the given database wrapper.
	 * 
	 * @param Database\Wrapper $database the database interface to use
	 * @return LockableHelper
	 */
	public static function build(Database\Wrapper $database) {
		return new LockableHelper($database);
	}

	/**
	 * Appends the specified model structure and foreign key arrays with the necessary components for a lockable model.
	 * 
	 * @param array $structure the existing model structure
	 * @param array $foreign_keys the existing model foreign keys
	 * @param User $user the corresponding User table for the lock to build the foreign key upon
	 * @param string $foreign_key_name a name for the foreign key index
	 * @param string $hashfield the field name that will hold the lock hash
	 * @param string $userfield the field name that will hold the lock user
	 * @param string $timestampfield the field name that will hold the lock timestamp
	 * @param string $modifiedfield the field name that will hold the last modified timestamp
	 * @return void
	 */
	public static function append_lockable(array &$structure, array &$foreign_keys, User $user, $foreign_key_name, $hashfield = "lock_hash", $userfield = "lock_user", $timestampfield = "lock_timestamp", $modifiedfield = "last_modified") {
		$structure[$hashfield] = new Database\Type_MD5(Database\Type::ALLOW_NULL);
		$structure[$userfield] = new Database\Type_Id(Database\Type::ALLOW_NULL);
		$structure[$timestampfield] = new Database\Type_DateTime(Database\Type::ALLOW_NULL);
		$structure[$modifiedfield] = new Database\Type_DateTime(Database\Type::NOT_NULL);
		
		$foreign_keys[$userfield] = new \Abstraction\Models\ForeignKey($user, "SET NULL", "CASCADE", $foreign_key_name);
	}
	
	/**
	 * Helper method to preprepare various useful SQL calls for lockable FirstClass models.
	 * 
	 * @param Models\FirstClass $firstclass the FirstClass Lockable model
	 * @param User $user the corresponding User table for the lock
	 * @param string $hashfield the field name that will hold the lock hash
	 * @param string $userfield the field name that will hold the lock user
	 * @param string $timestampfield the field name that will hold the lock timestamp
	 * @param string $modifiedfield the field name that will hold the last modified timestamp
	 * @return void
	 */
	// @phpcs:ignore Generic.CodeAnalysis.UnusedFunctionParameter
	public function build_preprepare_for_firstclass(Models\FirstClass $firstclass, User $user, $hashfield = "lock_hash", $userfield = "lock_user", $timestampfield = "lock_timestamp", $modifiedfield = "last_modified") {
		if (!($firstclass instanceof Lockable)) throw new Lockable_Exception("The specified class does not implement the Lockable interface");
		
		$this->database->preprepare("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__GET_LOCK", "
			SELECT " . $this->modelfield($firstclass, $hashfield) . " AS " . $this->tempfield("lock_hash") . ",
				" . $this->modelfield($firstclass, $userfield) . " AS " . $this->tempfield("lock_user") . ",
				" . $this->modelfield($firstclass, $timestampfield) . " AS " . $this->tempfield("lock_timestamp") . ",
				" . $this->modelfield($firstclass, $modifiedfield) . " AS " . $this->tempfield("last_modified") . "
			FROM " . $this->model($firstclass) . "
			WHERE " . $this->modelfield($firstclass, $firstclass->get_id_field()) . "=:id
			LIMIT 1
			FOR UPDATE
		", array(
			"id" => new Database\Type_Id(Database\Type::NOT_NULL)
		), array(
			"lock_hash" => new Database\Type_MD5(Database\Type::ALLOW_NULL),
			"lock_user" => new Database\Type_Id(Database\Type::ALLOW_NULL),
			"lock_timestamp" => new Database\Type_DateTime(Database\Type::ALLOW_NULL),
			"last_modified" => new Database\Type_DateTime(Database\Type::ALLOW_NULL)
		));

		switch ($this->database->get_db_type()) {
			case Database\Wrapper::DATABASE_MYSQL:
				$this->database->preprepare("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__SET_LOCK", "
					UPDATE " . $this->model($firstclass) . "
					SET " . $this->modelfield($firstclass, $hashfield) . " = :hash,
						" . $this->modelfield($firstclass, $timestampfield) . " = :timestamp,
						" . $this->modelfield($firstclass, $userfield) . " = :user
					WHERE " . $this->modelfield($firstclass, $firstclass->get_id_field()) . " = :id
				", array(
					"hash" => new Database\Type_MD5(Database\Type::ALLOW_NULL),
					"timestamp" => new Database\Type_DateTime(Database\Type::ALLOW_NULL),
					"user" => new Database\Type_Id(Database\Type::ALLOW_NULL),
					"id" => new Database\Type_Id(Database\Type::NOT_NULL)
				));
				$this->database->preprepare("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__SET_MODIFIED", "
					UPDATE " . $this->model($firstclass) . "
					SET " . $this->modelfield($firstclass, $modifiedfield) . " = :modified
					WHERE " . $this->modelfield($firstclass, $firstclass->get_id_field()) . " = :id
				", array(
					"modified" => new Database\Type_DateTime(Database\Type::NOT_NULL),
					"id" => new Database\Type_Id(Database\Type::NOT_NULL)
				));
				$this->database->preprepare("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__CLEAN_EXPIRED_LOCKS", "
					UPDATE " . $this->model($firstclass) . "
					SET " . $this->modelfield($firstclass, $hashfield) . " = null,
						" . $this->modelfield($firstclass, $timestampfield) . " = null,
						" . $this->modelfield($firstclass, $userfield) . " = null
					WHERE " . $this->modelfield($firstclass, $timestampfield) . " < :timestamp
				", array(
					"timestamp" => new Database\Type_DateTime(Database\Type::NOT_NULL)
				));
				break;
			case Database\Wrapper::DATABASE_POSTGRESQL:
				$this->database->preprepare("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__SET_LOCK", "
					UPDATE " . $this->model($firstclass) . "
					SET " . $this->database->delimit($hashfield) . " = :hash,
						" . $this->database->delimit($timestampfield) . " = :timestamp,
						" . $this->database->delimit($userfield) . " = :user
					WHERE " . $this->modelfield($firstclass, $firstclass->get_id_field()) . " = :id
				", array(
					"hash" => new Database\Type_MD5(Database\Type::ALLOW_NULL),
					"timestamp" => new Database\Type_DateTime(Database\Type::ALLOW_NULL),
					"user" => new Database\Type_Id(Database\Type::ALLOW_NULL),
					"id" => new Database\Type_Id(Database\Type::NOT_NULL)
				));
				$this->database->preprepare("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__SET_MODIFIED", "
					UPDATE " . $this->model($firstclass) . "
					SET " . $this->database->delimit($modifiedfield) . " = :modified
					WHERE " . $this->modelfield($firstclass, $firstclass->get_id_field()) . " = :id
				", array(
					"modified" => new Database\Type_DateTime(Database\Type::NOT_NULL),
					"id" => new Database\Type_Id(Database\Type::NOT_NULL)
				));
				$this->database->preprepare("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__CLEAN_EXPIRED_LOCKS", "
					UPDATE " . $this->model($firstclass) . "
					SET " . $this->database->delimit($hashfield) . " = null,
						" . $this->database->delimit($timestampfield) . " = null,
						" . $this->database->delimit($userfield) . " = null
					WHERE " . $this->modelfield($firstclass, $timestampfield) . " < :timestamp
				", array(
					"timestamp" => new Database\Type_DateTime(Database\Type::NOT_NULL)
				));
				break;
			default:
				throw new Database\Exception("Database type is not supported");
		}
	}

	//-------------------------------------------------------------------------
	
	/**
	 * Cleans any expired locks on objects
	 * 
	 * @param Models\FirstClass $firstclass the FirstClass Lockable model
	 * @param int $seconds the maximum number of seconds an existing lock is allowed to exist for
	 * @throws Lockable_Exception
	 * @return void
	 */
	public function clean_expired_locks(Models\FirstClass $firstclass, $seconds) {
		$this->database->execute_params("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__CLEAN_EXPIRED_LOCKS", array(
			"timestamp" => date("Y-m-d H:i:s", time() - $seconds)
		));
	}
	
	/**
	 * Attempts to claim the lock for a specified lockable object.
	 * 
	 * @param Models\FirstClass $firstclass the FirstClass Lockable model
	 * @param array $lockable the lockable object
	 * @param User $user the corresponding User table for the lock
	 * @param array $locker the user who is claiming the lock
	 * @param int $seconds the maximum number of seconds an existing lock is allowed to exist for
	 * @throws Lockable_Exception
	 * @return boolean|string returns the new hash on the lock; TRUE if the object is already locked to this user; FALSE is the object is locked to another user
	 */
	public function claim_lock(Models\FirstClass $firstclass, array $lockable, User $user, array $locker, $seconds) {
		if (!($firstclass instanceof Lockable)) throw new Lockable_Exception("The specified class does not implement the Lockable interface");
		
		self::assert_id_object($lockable, $firstclass->get_id_field());
		self::assert_id_object($locker, $user->get_id_field());
	
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
	
		$this->database->execute_params("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__CLEAN_EXPIRED_LOCKS", array(
			"timestamp" => date("Y-m-d H:i:s", time() - $seconds)
		));
		
		$lock = $this->database->execute_params_single("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__GET_LOCK", array(
			"id" => $lockable[$firstclass->get_id_field()]
		));
		
		if ($lock === null) {
			$this->database->transaction_rollback();
			throw new Lockable_Exception("Attempt to claim event row lock returned null -- this should not occur");
		}
	
		if ($lock["lock_user"] !== null) {
			if ($lock["lock_timestamp"] === null) {
				$this->database->transaction_rollback();
				throw new Lockable_Exception("Hashed lock timestamp is null -- this should not be possible");
			}
			$delta = time() - strtotime($lock["lock_timestamp"]);
				
			if ($delta < $seconds) {
				if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
				return $lock["lock_user"] === $locker[$user->get_id_field()];	// true for this user, false for another
			}
		}

		$hash = md5(time() . rand(0, 65536));
		
		$this->database->execute_params("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__SET_LOCK", array(
			"id" => $lockable[$firstclass->get_id_field()],
			"hash" => $hash,
			"timestamp" => date("Y-m-d H:i:s"),
			"user" => $locker[$user->get_id_field()]
		));
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
	
		return $hash;
	}
	
	/**
	 * Checks whether the specified lock hash matches that currently assigned to a locked object.
	 * @param Models\FirstClass $firstclass the FirstClass Lockable model
	 * @param array $lockable the lockable object
	 * @param string $hash the hash value to check
	 * @param string $hashfield the field name containing the hash within the object
	 * @throws Lockable_Exception
	 * @return boolean TRUE if the hash matches; FALSE if it does not
	 */
	public function validate_lock(Models\FirstClass $firstclass, array $lockable, $hash, $hashfield = "lock_hash") {
		if (!($firstclass instanceof Lockable)) throw new Lockable_Exception("The specified class does not implement the Lockable interface");
		
		self::assert_id_object($lockable, $firstclass->get_id_field());
	
		return $lockable[$hashfield] === $hash;
	}

	/**
	 * Attempts to release a locked object back for other users to lock.
	 * @param Models\FirstClass $firstclass the FirstClass Lockable model
	 * @param array $lockable the currently locked object
	 * @param string $hash the expected current hash value that is being released
	 * @param int $seconds the maximum number of seconds an existing lock is allowed to exist for
	 * @throws Lockable_Exception
	 * @return void
	 */
	public function release_lock(Models\FirstClass $firstclass, array $lockable, $hash, $seconds) {
		if (!($firstclass instanceof Lockable)) throw new Lockable_Exception("The specified class does not implement the Lockable interface");
		
		self::assert_id_object($lockable, $firstclass->get_id_field());
	
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
	
		$this->database->execute_params("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__CLEAN_EXPIRED_LOCKS", array(
			"timestamp" => date("Y-m-d H:i:s", time() - $seconds)
		));
		
		$lock = $this->database->execute_params_single("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__GET_LOCK", array(
			"id" => $lockable[$firstclass->get_id_field()]
		));
	
		if ($lock === null) {
			$this->database->transaction_rollback();
			throw new Lockable_Exception("Attempt to claim event row lock returned null -- this should not occur");
		}
	
		if ($lock["lock_user"] === null) {
			// lock has probably been purged
			if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
			return;
		}
	
		if ($lock["lock_hash"] !== $hash) {
			$this->database->transaction_rollback();
			throw new Lockable_Exception("Hashed event lock does not match expected value -- this should not occur");
		}
	
		$this->database->execute_params("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__SET_LOCK", array(
			"id" => $lockable[$firstclass->get_id_field()],
			"hash" => null,
			"timestamp" => null,
			"user" => null
		));
			
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
	}
	
	/**
	 * Returns the current timestamp.
	 * 
	 * This is simply a class method for the time() function.
	 * 
	 * @return int the result of time()
	 */
	public function get_current_timestamp() {
		return time();
	}

	/**
	 * Updates the last modified field to the current timestamp.
	 * 
	 * @param Models\FirstClass $firstclass the FirstClass Lockable model
	 * @param array $lockable the lockable object
	 * @throws Lockable_Exception
	 * @return void
	 */
	public function touch_modified(Models\FirstClass $firstclass, array $lockable) {
		if (!($firstclass instanceof Lockable)) throw new Lockable_Exception("The specified class does not implement the Lockable interface");
		
		self::assert_id_object($lockable, $firstclass->get_id_field());
	
		$this->database->execute_params("ADAMANTINE__LOCKABLE_FIRSTCLASS__" . $firstclass->get_table() . "__SET_MODIFIED", array(
			"id" => $lockable[$firstclass->get_id_field()],
			"modified" => date("Y-m-d H:i:s")
		));
	}
	
	/**
	 * Returns whether the lockable object has been touch_modified since the current timestamp was fetched.
	 * 
	 * @param array $lockable the lockable object
	 * @param int $timestamp the expected timestamp
	 * @param string $modifiedfield the field name that will hold the last modified timestamp
	 * @return boolean TRUE if the object has been modified since; FALSE if it has not
	 */
	public function is_modified_since(array $lockable, $timestamp, $modifiedfield = "last_modified") {
		self::assert_int($timestamp, 0);
		
		$set = strtotime($lockable[$modifiedfield]);
		return $set > $timestamp;
	}
}
