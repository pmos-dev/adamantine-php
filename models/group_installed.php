<?php
/**
 * Group/installed area access modelling within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/m2m_group_installed.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/group.php";
require_once ADAMANTINE_ROOT_PATH . "models/installed.php";

if (!defined("APP_ROOT_PATH")) die("APP_ROOT_PATH has not been set.");

use \Abstraction\Database as Database;
use \Abstraction\Framework as Framework;
use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * Defines the model for group/installed area access.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Group_Installed extends Framework\Models\M2M_Group_Installed {
	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Group $group the Group model to associate with
	 * @param Installed $installed the Installed model to associate with
	 */
	public function __construct(Database\Wrapper $database, Group $group, Installed $installed) {
		parent::__construct($database, Config\DATABASE_PREFIX . "group_installed", $group, $installed);
	}
}
