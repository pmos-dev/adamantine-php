<?php
/**
 * Standard FirstClass/Field data within the Adamantine framework.
 * This is a helper model built from other structures rather than corresponding to a specific structure in its own right.
 * It joins the StandardField to another FirstClass (or SecondClass/orientated etc.) to store data for the combination.
 * 
 * @copyright 2015 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/m2mlinktable.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/standardfield.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * @internal
 */
class StandardData_Exception extends Models\M2MLinkTable_Exception {}

/**
 * Defines a root model for 'standard' data storage between StandardField and a FirstClass.
 * 
 * @api
 * @author Pete Morris
 * @version 1.0.0
 */
abstract class StandardData extends Models\M2MLinkTable {
	
	protected $owner, $owner_field;
	protected $standardfield, $standardfield_field;
	
	/**
	 * Constructs a new instance of this standard model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Models\FirstClass $owner the FirstClass model to associate with
	 * @param StandardField $standardfield the StandardField model to associate with
	 * @param string $table the table name within the database
	 * @param string $owner_field the name to use for the firstclass field, which tends to correspond to the owning firstclass model name, e.g. "assignment"
	 * @param string $standardfield_field the name to use for the standardfield field, which tends to correspond to the owning field model name, e.g. "field"
	 * @throws StandardField_Exception
	 */
	public function __construct(Database\Wrapper $database, Models\FirstClass $owner, StandardField $standardfield, $table, $owner_field = "owner", $standardfield_field = "field") {
		$this->owner = $owner;
		$this->owner_field = $owner_field;
		$this->standardfield = $standardfield;
		$this->standardfield_field = $standardfield_field;

		parent::__construct($database, $table, $owner, $standardfield, $owner_field, $standardfield_field, array(
			"data" => new Database\Type_Encrypted(Config\ENCRYPTION_PASSPHRASE, Database\Type::NOT_NULL)
		));
	}

	//-------------------------------------------------------------------------

	public function get_by_owner_and_standardfield(array $owner, array $standardfield) {
		if (!$this->is_linked($owner, $standardfield)) return null;
		
		$row = $this->get_link_row($owner, $standardfield);
		return $row["data"];
	}
	
	public function set_for_owner_and_standardfield(array $owner, array $standardfield, $data = null) {
		self::assert_id_object($owner, $this->owner->get_id_field());
		self::assert_id_object($standardfield, $this->standardfield->get_id_field());
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		if ($data === null) {
			if ($this->is_linked($owner, $standardfield)) $this->unlink($owner, $standardfield);
		} else {
			if ($this->is_linked($owner, $standardfield)) {
				$existing = $this->get_link_row($owner, $standardfield);
				$existing["data"] = $data;
				$this->update_link_row($existing);
			} else {
				$this->link($owner, $standardfield, array(
					"data" => $data
				));
			}
		}
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}
	
	public function delete_all_for_owner(array $owner) {
		self::assert_id_object($owner, $this->owner->get_id_field());
		
		return $this->delete_all_for_a($owner);
	}
	
	public function delete_all_for_standardfield(array $standardfield) {
		self::assert_id_object($standardfield, $this->standardfield->get_id_field());
		
		return $this->delete_all_for_a($standardfield);
	}
}
