<?php
/**
 * Managed hierarchy model support within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/hierarchy.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";
require_once ADAMANTINE_ROOT_PATH . "models/manageable.php";
require_once ADAMANTINE_ROOT_PATH . "models/alias.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class ManagedHierarchy_Exception extends Models\Hierarchy_Exception {}

/**
 * Defines a root model for 'managed' orientated-ordered structured models.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class ManagedHierarchy extends Models\Hierarchy implements Manageable {
	protected $firstclass, $firstclass_field;
	protected $alias_model = null;
	
	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Models\FirstClass $firstclass the FirstClass parent to associate with
	 * @param string $table the table name within the database
	 * @param Database\Type[] $structure an associated array to define the model structure, taking the format field name => field type object
	 * @param Models\ForeignKey[] $foreign_keys an associated array to define any foreign keys present, taking the format field name => foreign key object
	 * @param Models\UniqueKey[] $unique_keys any unique keys to apply to the model
	 * @param string $firstclass_field the name to use for the firstclass field, which tends to correspond to the firstclass model name, e.g. "installed"
	 * @throws ManagedHierarchy_Exception
	 */
	public function __construct(Database\Wrapper $database, Models\FirstClass $firstclass, $table, array $structure, array $foreign_keys = array(), array $unique_keys = array(), $_ADD_ALIAS_SUPPORT = false, $firstclass_field = "installed") {
		$this->firstclass = $firstclass;
		$this->firstclass_field = $firstclass_field;
		
		if (array_key_exists("name", $structure)) throw new ManagedHierarchy_Exception("The name field is implicit for managed models and should not be explicitly stated in the structure");
		$structure["name"] = new Database\Type_IdName(Database\Type::NOT_NULL);
		
		$unique_keys[] = new Models\UniqueKey(array($firstclass_field, "name"), "{$table}_uk_in");
		
		parent::__construct($database, $table, $structure, $firstclass, $firstclass_field, "ordered", "parent", "id", $foreign_keys, $unique_keys);
		
		if ($_ADD_ALIAS_SUPPORT) $this->alias_model = new Alias($database, $this);
	}

	//-------------------------------------------------------------------------
	
	/**
	 * @internal
	 */
	public function create_table() {
		parent::create_table();
	
		if ($this->alias_model !== null) $this->alias_model->create_table();
	}
	
	/**
	 * @internal
	 */
	public function drop_table() {
		if ($this->alias_model !== null) $this->alias_model->drop_table();
	
		parent::drop_table();
	}
	
	//-------------------------------------------------------------------------

	/**
	 * @internal
	 * @see Manageable::is_model_manageable()
	 */
	public function is_model_manageable() { 
		return true; 
	}
	
	/**
	 * Returns the repository name automatically by parsing the class name out of the namespace.
	 * 
	 * Subclasses who's repository name does not match the class name should override this method to change this behaviour.
	 * 
	 * @internal
	 * @see Manageable::get_repository_name()
	 * @throws Models\Exception
	 * @return string
	 */
	public function get_repository_name() {
		if (!preg_match("`\\\([-a-z0-9_]+)$`iD", get_class($this), $array)) throw new Models\Exception("Unable to automatically parse classname from model namespace");
		return $array[1];
	}

	/**
	 * @internal
	 * @see Manageable::is_aliasing_supported()
	 */
	public function is_aliasing_supported() {
		return $this->alias_model !== null;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_manageable_fields()
	 */
	public function get_manageable_fields() {
		$fields = array("name");
		foreach (array_keys($this->structure) as $field) if (!in_array($field, array("name", "id", $this->firstclass_field, "ordered", "parent"))) $fields[] = $field;
		
		return $fields;
	}
	
	/**
	 * Returns an array of fields that are applicable to be exported for this model.
	 * 
	 * @return string[]
	 */
	public function get_importexportable_fields() {
		return $this->get_manageable_fields();
	}
	
	/**
	 * @internal
	 * @see Manageable::get_access_required_to_manage()
	 */
	public function get_access_required_to_manage() {
		return "full";
	}

	/**
	 * @internal
	 * @see Manageable::is_object_viewable()
	 */
	public function is_object_viewable(array $object) {
		return true;
	}

	/**
	 * @internal
	 * @see Manageable::is_object_editable()
	 */
	public function is_object_editable(array $object) {
		return true;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_field_description()
	 */
	public function get_field_description($field) {
		return $field;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_field_suffix()
	 */
	public function get_field_suffix($field) {
		return null;
	}
		
	/**
	 * @internal
	 * @see Manageable::get_view_additional_file()
	 */
	public function get_view_additional_file() {
		return null;
	}
			
	//-------------------------------------------------------------------------
	
	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();
		
		UniqueNamedHelper::build($this->database)->build_preprepare_for_secondclass($this);
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Fetches the model row associated with the specified unique name for the specified instance, if it exists.
	 * 
	 * Note, the search is performed on the tree as a flat list, so all child nodes are available to be returned regardless of depth.
	 * 
	 * @param mixed[] $firstclass the firstclass parent associated with the model
	 * @param string $name the unique name associated with this row
	 * @return mixed[]|NULL
	 */
	public function get_by_name(array $firstclass, $name) {
		return UniqueNamedHelper::build($this->database)->get_secondclass_by_firstclass_and_name($this, $firstclass, $name);
	}

	/**
	 * Returns an array of any model rows that match via their aliases rather than the name itself.
	 * 
	 * @param mixed[] $firstclass the firstclass parent associated with the model
	 * @param string $name the alias to look for
	 * @return array
	 */
	public function list_by_alias(array $firstclass, $name) {
		if (!$this->is_aliasing_supported()) throw new ManagedHierarchy_Exception("Aliasing is not supported for this managed model");
		
		return $this->alias_model->list_by_grandparent_and_name($firstclass, $name);
	}
	
	/**
	 * Searches this model for any matching names for the specified firstclass parent context. Returns a non-associative array of associative row results.
	 * 
	 * Aliases can also be searched too if desired and available.
	 * Preference of order is given to matches in the following order:
	 * 1) model nodes which directly match the name
	 * 2) aliases which directly match the name
	 * 3) model nodes which indirectly match the name
	 * 4) aliases which indirectly match the name
	 * 
	 * @param mixed[] $firstclass the firstclass parent associated with the model
	 * @param string $name the unique name associated with this row
	 * @param boolean $_ALLOW_ALIASES whether to also search aliases (if available)
	 * @return array
	 */
	public function search_by_name_or_alias(array $firstclass, $name) {
		if (!$this->is_aliasing_supported()) throw new ManagedHierarchy_Exception("Aliasing is not supported for this managed model");
		
		$results = array();
		
		if (null !== ($match = $this->get_by_name($firstclass, $name))) $results[] = $match;
		
		foreach ($this->alias_model->list_by_grandparent_and_name($firstclass, $name) as $match) $results[] = $match;

		foreach ($this->search_fields_by_firstclass($firstclass, $name, array("name")) as $match) $results[] = $match;

		foreach ($this->alias_model->search_by_grandparent_and_name($firstclass, $name) as $match) $results[] = $match;

		$prune = array(); 
		$ids = array();
		foreach ($results as $result) {
			if (in_array($result["id"], $ids)) continue;
			$prune[] = $result;
			$ids[] = $result["id"];
		}
		
		return $prune;
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns an array of aliases that are set for the specified row.
	 * 
	 * @param mixed[] $row the row in this model to fetch aliases for
	 * @throws ManagedHierarchy_Exception
	 * @return string[]
	 */
	public function list_aliases_by_row(array $row) {
		if (!$this->is_aliasing_supported()) throw new ManagedHierarchy_Exception("Aliasing is not supported for this managed model");
		
		self::assert_id_object($row, $this->get_id_field());

		$aliases = array();
		foreach ($this->alias_model->list_by_firstclass($row) as $alias) $aliases[] = $alias["name"];
		
		return $aliases;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Adds an alias for the specified row.
	 * 
	 * @param mixed[] $row the row in this model to add the alias to
	 * @param string $name the alias
	 * @throws ManagedHierarchy_Exception
	 */
	public function add_alias(array $row, $name) {
		if (!$this->is_aliasing_supported()) throw new ManagedHierarchy_Exception("Aliasing is not supported for this managed model");
		
		self::assert_id_object($row, $this->get_id_field());
		self::assert_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name);
	
		return $this->alias_model->insert_for_firstclass($row, array(
			"name" => $name
		));
	}

	/**
	 * Removes an alias from the specified row.
	 * 
	 * @param mixed[] $row the row in this model to remove the alias from
	 * @param string $name the alias
	 * @throws ManagedHierarchy_Exception
	 * @return boolean always returns true; failure will throw an exception
	 */
	public function remove_alias(array $row, $name) {
		if (!$this->is_aliasing_supported()) throw new ManagedHierarchy_Exception("Aliasing is not supported for this managed model");
		
		self::assert_id_object($row, $this->get_id_field());
		self::assert_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name);
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		if (null === ($alias = $this->alias_model->get_by_name($row, $name))) throw new ManagedHierarchy_Exception("No such alias by that name exists for this row");

		$this->alias_model->delete_for_firstclass($row, $name);

		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}

	/**
	 * Replaces all existing aliases for a model row with the specified new set.
	 * 
	 * @param mixed[] $row the row in this model to replace the aliases for
	 * @param string[] $names the new aliases
	 * @throws ManagedHierarchy_Exception
	 * @return boolean always returns true; failure will throw an exception
	 */
	public function replace_aliases(array $row, array $names) {
		if (!$this->is_aliasing_supported()) throw new ManagedHierarchy_Exception("Aliasing is not supported for this managed model");
		
		self::assert_id_object($row, $this->get_id_field());
				
		$_TRANSACTION_OWNER = $this->database->transaction_claim();

		$this->alias_model->delete_all_for_firstclass($row);
		
		foreach ($names as $name) $this->add_alias($row, $name);

		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
		
		return true;
	}
}
