<?php
/**
 * Group modelling within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "framework/models/group.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";
require_once ADAMANTINE_ROOT_PATH . "models/manageable.php";
require_once ADAMANTINE_ROOT_PATH . "models/instance.php";

if (!defined("APP_ROOT_PATH")) die("APP_ROOT_PATH has not been set.");

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Framework as Framework;
use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * Defines the model for instance groups.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Group extends Framework\Models\Group implements Manageable {
	/**
	 * Constructs a new instance of this model.
	 * 
* @param Database\Wrapper $database the database interface to build the model upon
	 * @param Instance $instance the Instance model to associate with
	 */
	public function __construct(Database\Wrapper $database, Instance $instance) {
		parent::__construct($database, Config\DATABASE_PREFIX . "groups", $instance, array(
			"name" => new Database\Type_IdName(Database\Type::NOT_NULL)
		), array(), array(
			new Models\UniqueKey(array("instance", "name"), Config\DATABASE_PREFIX . "_groups__uk_in")
		));
	}

	//-------------------------------------------------------------------------
	
	/**
	 * @internal
	 * @see Manageable::is_model_manageable()
	 */
	public function is_model_manageable() {
		return true;
	}
	
	/**
	 * Returns the repository name automatically by parsing the class name out of the namespace.
	 * 
	 * Subclasses who's repository name does not match the class name should override this method to change this behaviour.
	 * 
	 * @internal
	 * @see Manageable::get_repository_name()
	 * @throws Models\Exception
	 * @return string
	 */
	public function get_repository_name() {
		if (!preg_match("`\\\([-a-z0-9_]+)$`iD", get_class($this), $array)) throw new Models\Exception("Unable to automatically parse classname from model namespace");
		return $array[1];
	}

	/**
	 * @internal
	 * @see Manageable::is_aliasing_supported();
	 */
	public function is_aliasing_supported() {
		return false;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_manageable_fields()
	 */
	public function get_manageable_fields() {
		$fields = array();
		foreach (array_keys($this->structure) as $field) if (!in_array($field, array("id", "instance", "type"))) $fields[] = $field;
		
		return $fields;
	}

	/**
	 * @internal
	 * @see Manageable::get_access_required_to_manage()
	 */
	public function get_access_required_to_manage() {
		return "editor";
	}

	/**
	 * @internal
	 * @see Manageable::is_object_viewable()
	 */
	public function is_object_viewable(array $group) {
		return true;
	}
	
	/**
	 * @internal
	 * @see Manageable::is_object_editable()
	 */
	public function is_object_editable(array $group) {
		return $group["type"] === Framework\Models\Group::NORMAL;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_field_description()
	 */
	public function get_field_description($field) {
		return $field;
	}
	
	/**
	 * @internal
	 * @see Manageable::get_field_suffix()
	 */
	public function get_field_suffix($field) {
		return null;
	}

	/**
	 * @internal
	 * @see Manageable::get_view_additional_file()
	 */
	public function get_view_additional_file() {
		return ADAMANTINE_ROOT_PATH . "areas/admin/_groups_additional.php";
	}
			
	//-------------------------------------------------------------------------
	
	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();
	
		$fields = array();
		foreach (array_keys($this->structure) as $field) $fields[] = $this->modelviewfield($this, "SECONDCLASS_JOIN", $field);
		$fields = implode(",", $fields);
	
		$this->database->reprepare("MODELS_SECONDCLASS__{$this->table}__LIST_BY_FIRSTCLASS", "
			SELECT {$fields}
			FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=:firstclass
			ORDER BY " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "type") . "=" . $this->database->quote(Framework\Models\Group::NORMAL) . " ASC,
				" . $this->modelviewfield($this, "SECONDCLASS_JOIN", "name") . " ASC
		", array(
			"firstclass" => $this->structure[$this->firstclass_field]
		), $this->structure);
		
		$this->database->preprepare("ADAMANTINE__Group__LIST_NORMAL_INSTANCE", "
			SELECT {$this->fields}
			FROM " . $this->model($this) . "
			WHERE " . $this->modelfield($this, "instance") . "=:instance
			AND " . $this->modelfield($this, "type") . "=" . $this->database->quote(self::NORMAL) . "
			ORDER BY " . $this->modelfield($this, "name") . " ASC
		", array(
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL)
		), $this->structure);
		
		UniqueNamedHelper::build($this->database)->build_preprepare_for_secondclass($this);
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Returns an array of all the groups associated with the specified instance.
	 * 
	 * @param mixed[] $instance the instance to fetch the groups for
	 * @return array
	 */
	public function list_by_instance(array $instance) {
		return UniqueNamedHelper::build($this->database)->list_secondclass_by_firstclass_order_by_name($this, $instance);
	}

	/**
	 * Returns an array of all the 'normal' groups associated with the specified instance, i.e. not the everyone or system groups.
	 * 
	 * @param mixed[] $instance the instance to fetch the groups for
	 * @return array
	 */
	public function list_normal_by_instance(array $instance) {
		self::assert_id_object($instance);
		
		return $this->database->execute_params("ADAMANTINE__Group__LIST_NORMAL_INSTANCE", array(
			"instance" => $instance["id"]
		));
	}

	//-------------------------------------------------------------------------
	
	/**
	 * Fetches the group row with the specified group name for the specified instance, if it exists.
	 * 
	 * @param mixed[] $instance the instance associated with the group
	 * @param string $name the unique name of the group
	 * @return mixed[]|NULL
	 */
	public function get_by_name(array $instance, $name) {
		return UniqueNamedHelper::build($this->database)->get_secondclass_by_firstclass_and_name($this, $instance, $name);
	}
}
