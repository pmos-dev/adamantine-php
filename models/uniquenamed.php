<?php
/**
 * Support for models with a single per-instance unique name.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "models/models.php";
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/multiparentsecondclass.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Data as Data;

/**
 * @internal
 */
class UniqueNamed_Exception extends Models\Exception {}

/**
 * Internal interface to identify a model as supporting the UniqueNamed constructs.
 * 
 * @internal
 */
interface UniqueNamed {}

/**
 * Various unique naming helper methods that models can make use of.
 * 
 * Since multiple inheritence is not possible for child models, this is a helper class.
 * The build() method creates a new instance of this helper collection, allowing models to apply the methods for themselves.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class UniqueNamedHelper extends Models\ModelHelperMethods {
	const TYPE_BASE36 = "base36";
	const TYPE_BASE62 = "base62";
	
	/**
	 * Returns a newly constructed instance around the given database wrapper.
	 * 
	 * @param Database\Wrapper $database the database interface to use
	 * @return UniqueNamedHelper
	 */
	public static function build(Database\Wrapper $database) {
		return new UniqueNamedHelper($database);
	}

	/**
	 * Helper method to preprepare various useful SQL calls for unique named FirstClass models.
	 * 
	 * @param Models\FirstClass $firstclass the FirstClass model
	 * @param string $namefield the field name that will hold the unique naming
	 * @return void
	 */
	public function build_preprepare_for_firstclass(Models\FirstClass $firstclass, $namefield = "name", $case_sensitive = false) {
		if (!($firstclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");
		
		$where_clause = $this->modelfield($firstclass, $namefield) . " = :name";
		
		if ($this->database->get_db_type() === Database\Wrapper::DATABASE_MYSQL && $case_sensitive) {
			$where_clause = $this->modelfield($firstclass, $namefield) . " = BINARY :name";
		}
		if ($this->database->get_db_type() === Database\Wrapper::DATABASE_POSTGRESQL && !$case_sensitive) {
			$where_clause = "LOWER(" . $this->modelfield($firstclass, $namefield) . ") = LOWER(:name)";
		}
		
		$this->database->preprepare("ADAMANTINE__UNIQUENAMED_FIRSTCLASS__" . $firstclass->get_table() . "__GET_BY_NAME__{$namefield}", "
			SELECT " . $this->modelfield_csv($firstclass) . "
			FROM " . $this->model($firstclass) . "
			WHERE ${where_clause}
		", array(
			"name" => new Database\Type_IdName(Database\Type::NOT_NULL)
		), $firstclass->get_structure());

		$this->database->preprepare("ADAMANTINE__UNIQUENAMED_FIRSTCLASS__" . $firstclass->get_table() . "__LIST_ORDER_BY_NAME__{$namefield}", "
			SELECT " . $this->modelfield_csv($firstclass) . "
			FROM " . $this->model($firstclass) . "
			ORDER BY " . $this->modelfield($firstclass, $namefield) . " ASC
		", null, $firstclass->get_structure());
	}
	
	/**
	 * Provides a generic get_by_name method for FirstClass models.
	 * 
	 * @param Models\FirstClass $firstclass the FirstClass model
	 * @param string $name the unique name to search for
	 * @param string $namefield the field name that will hold the unique naming
	 * @throws UniqueNamed_Exception
	 * @return mixed[]|NULL
	 */
	public function get_firstclass_by_name(Models\FirstClass $firstclass, $name, $namefield = "name") {
		if (!($firstclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");
		
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name)) throw new UniqueNamed_Exception("Invalid value for name", $name);
		
		return $this->database->execute_params_single("ADAMANTINE__UNIQUENAMED_FIRSTCLASS__" . $firstclass->get_table() . "__GET_BY_NAME__{$namefield}", array(
			"name" => $name
		));
	}

	/**
	 * Returns the rows of the specified FirstClass model ordered by the unique name field.
	 * 
	 * @param Models\FirstClass $firstclass the FirstClass model
	 * @param string $namefield the field name that will hold the unique naming
	 * @return array
	 */
	public function list_firstclass_order_by_name(Models\FirstClass $firstclass, $namefield = "name") {
		if (!($firstclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");
		
		return $this->database->execute_params("ADAMANTINE__UNIQUENAMED_FIRSTCLASS__" . $firstclass->get_table() . "__LIST_ORDER_BY_NAME__{$namefield}");
	}

	/**
	 * Helper method to preprepare various useful SQL calls for unique named SecondClass models.
	 * 
	 * @param Models\SecondClass $secondclass the SecondClass model
	 * @param string $namefield the field name that will hold the unique naming
	 * @return void
	 */
	public function build_preprepare_for_secondclass(Models\SecondClass $secondclass, $namefield = "name", $case_sensitive = false) {
		if (!($secondclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");
		
		$where_clause = $this->modelfield($secondclass, $namefield) . " = :name";
		
		if ($this->database->get_db_type() === Database\Wrapper::DATABASE_MYSQL && $case_sensitive) {
			$where_clause = $this->modelfield($secondclass, $namefield) . " = BINARY :name";
		}
		if ($this->database->get_db_type() === Database\Wrapper::DATABASE_POSTGRESQL && !$case_sensitive) {
			$where_clause = "LOWER(" . $this->modelfield($secondclass, $namefield) . ") = LOWER(:name)";
		}
		
		$this->database->preprepare("ADAMANTINE__UNIQUENAMED_SECONDCLASS__" . $secondclass->get_table() . "__GET_BY_NAME__{$namefield}", "
			SELECT " . $this->modelfield_csv($secondclass) . "
			FROM " . $this->model($secondclass) . "
			INNER JOIN " . $this->model($secondclass->get_firstclass()) . "
				ON " . $this->modelfield($secondclass, $secondclass->get_firstclass_field()) . "=" . $this->modelfield($secondclass->get_firstclass(), $secondclass->get_firstclass()->get_id_field()) . "
			WHERE " . $this->modelfield($secondclass->get_firstclass(), $secondclass->get_firstclass()->get_id_field()) . "=:firstclass
			AND ${where_clause}
		", array(
			"firstclass" => new Database\Type_Id(Database\Type::NOT_NULL),
			"name" => new Database\Type_IdName(Database\Type::NOT_NULL)
		), $secondclass->get_structure());

		$this->database->preprepare("ADAMANTINE__UNIQUENAMED_SECONDCLASS__" . $secondclass->get_table() . "__LIST_ORDER_BY_NAME__{$namefield}", "
			SELECT " . $this->modelfield_csv($secondclass) . "
			FROM " . $this->model($secondclass) . "
			INNER JOIN " . $this->model($secondclass->get_firstclass()) . "
				ON " . $this->modelfield($secondclass, $secondclass->get_firstclass_field()) . "=" . $this->modelfield($secondclass->get_firstclass(), $secondclass->get_firstclass()->get_id_field()) . "
			WHERE " . $this->modelfield($secondclass->get_firstclass(), $secondclass->get_firstclass()->get_id_field()) . "=:firstclass
			ORDER BY " . $this->modelfield($secondclass, $namefield) . " ASC
		", array(
			"firstclass" => new Database\Type_Id(Database\Type::NOT_NULL)
		), $secondclass->get_structure());
	}
	
	/**
	 * Helper method to preprepare various useful SQL calls for unique named SecondClass models.
	 *
	 * @param Models\SecondClass $secondclass the SecondClass model
	 * @param string $namefield the field name that will hold the unique naming
	 * @return void
	 */
	public function build_preprepare_for_multiparentsecondclass(Models\MultiParentSecondClass $multiparentsecondclass, $namefield = "name", $case_sensitive = false) {
		if (!($multiparentsecondclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");

		$where_clause = $this->modelviewfield($multiparentsecondclass, "MULTIPARENTSECONDCLASS_JOIN", $namefield) . " = :name";
		
		if ($this->database->get_db_type() === Database\Wrapper::DATABASE_MYSQL && $case_sensitive) {
			$where_clause = $this->modelviewfield($multiparentsecondclass, "MULTIPARENTSECONDCLASS_JOIN", $namefield) . " = BINARY :name";
		}
		if ($this->database->get_db_type() === Database\Wrapper::DATABASE_POSTGRESQL && !$case_sensitive) {
			$where_clause = "LOWER(" . $this->modelviewfield($multiparentsecondclass, "MULTIPARENTSECONDCLASS_JOIN", $namefield) . ") = LOWER(:name)";
		}
		
		$parents = $multiparentsecondclass->get_firstclass_fields();
		$structure = $multiparentsecondclass->get_structure();
		
		$clauses = array();
		$params = array("name" => $structure[$namefield]);
		foreach ($parents as $parent) {
			$clauses[] = $this->modelviewfield($multiparentsecondclass, "MULTIPARENTSECONDCLASS_JOIN", "firstclass_{$parent}") . "=:firstclass_{$parent}";
			$params["firstclass_{$parent}"] = $structure[$parent];
		}
		
		$this->database->preprepare("ADAMANTINE__UNIQUENAMED_MULTIPARENTSECONDCLASS__" . $multiparentsecondclass->get_table() . "__GET_BY_NAME__{$namefield}", "
			SELECT " . $this->modelviewfield_csv($multiparentsecondclass, "MULTIPARENTSECONDCLASS_JOIN") . "
			FROM " . $this->modelview($multiparentsecondclass, "MULTIPARENTSECONDCLASS_JOIN") . "
			WHERE ${where_clause}
			AND " . implode(" AND ", $clauses) . "
		", $params, $multiparentsecondclass->get_structure());
	}
	
	/**
	 * Provides a generic get_by_name method for SecondClass models.
	 * 
	 * @param Models\SecondClass $secondclass the SecondClass model
	 * @param mixed[] $firstclass the associated FirstClass model row context to search within.
	 * @param string $name the unique name to search for
	 * @param string $namefield the field name that will hold the unique naming
	 * @throws UniqueNamed_Exception
	 * @return mixed[]|NULL
	 */
	public function get_secondclass_by_firstclass_and_name(Models\SecondClass $secondclass, array $firstclass, $name, $namefield = "name") {
		if (!($secondclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");
		
		if (!Data\Data::validate_id_object($firstclass, $secondclass->get_firstclass()->get_id_field())) throw new UniqueNamed_Exception("Invalid firstclass object", $firstclass);
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name)) throw new UniqueNamed_Exception("Invalid value for name", $name);
	
		return $this->database->execute_params_single("ADAMANTINE__UNIQUENAMED_SECONDCLASS__" . $secondclass->get_table() . "__GET_BY_NAME__{$namefield}", array(
			"firstclass" => $firstclass[$secondclass->get_firstclass()->get_id_field()],
			"name" => $name
		));
	}

	/**
	 * Returns the associated rows of the specified SecondClass model ordered by the unique name field.
	 * 
	 * @param Models\SecondClass $secondclass the SecondClass model
	 * @param mixed[] $firstclass the associated FirstClass model row context to search within.
	 * @param string $namefield the field name that will hold the unique naming
	 * @throws UniqueNamed_Exception
	 * @return array
	 */
	public function list_secondclass_by_firstclass_order_by_name(Models\SecondClass $secondclass, array $firstclass, $namefield = "name") {
		if (!($secondclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");
		
		if (!Data\Data::validate_id_object($firstclass, $secondclass->get_firstclass()->get_id_field())) throw new UniqueNamed_Exception("Invalid firstclass object", $firstclass);
		
		return $this->database->execute_params("ADAMANTINE__UNIQUENAMED_SECONDCLASS__" . $secondclass->get_table() . "__LIST_ORDER_BY_NAME__{$namefield}", array(
			"firstclass" => $firstclass[$secondclass->get_firstclass()->get_id_field()]
		));
	}
	
	/**
	 * Provides a generic get_by_name method for MultiParentSecondClass models.
	 *
	 * @param Models\MultiParentSecondClass $secondclass the MultiParentSecondClass model
	 * @param mixed[][] $firstclasses the associated FirstClass models row contexts to search within.
	 * @param string $name the unique name to search for
	 * @param string $namefield the field name that will hold the unique naming
	 * @throws UniqueNamed_Exception
	 * @return mixed[]|NULL
	 */
	public function get_multiparentsecondclass_by_firstclass_and_name(Models\MultiParentSecondClass $multiparentsecondclass, array $firstclasses, $name, $namefield = "name") {
		if (!($multiparentsecondclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");

		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name)) throw new UniqueNamed_Exception("Invalid value for name", $name);
		
		$parents = $multiparentsecondclass->get_firstclass_fields();
		$values = array("name" => $name);
		foreach ($parents as $parent) {
			$firstclass = $multiparentsecondclass->get_firstclass($parent);
			if (!Data\Data::validate_id_object($firstclasses[$parent], $firstclass->get_id_field())) throw new UniqueNamed_Exception("Invalid firstclass object", $firstclasses[$parent]);
			$values["firstclass_{$parent}"] = $firstclasses[$parent][$firstclass->get_id_field()];
		}
		
		return $this->database->execute_params_single("ADAMANTINE__UNIQUENAMED_MULTIPARENTSECONDCLASS__" . $multiparentsecondclass->get_table() . "__GET_BY_NAME__{$namefield}", $values);
	}
	
	/**
	 * Provides a generic get_by_name method for MultiParentSecondClass models.
	 *
	 * @param Models\MultiParentSecondClass $secondclass the MultiParentSecondClass model
	 * @param mixed[][] $firstclasses the associated FirstClass models row contexts to search within.
	 * @param string $name the unique name to search for
	 * @param string $namefield the field name that will hold the unique naming
	 * @throws UniqueNamed_Exception
	 * @return mixed[]|NULL
	 */
	/**
	 * Generates a unique firstclass UID for a model.
	 * 
	 * NB, transactions are NOT used, so must be implemented by the calling method.
	 * NB, for Base62, the build_ by_firstclass needs to have been already called with case_sensitive=true
	 * 
	 * @param Models\FirstClass $firstclass
	 * @param string $type TYPE_BASE36 or TYPE_BASE62
	 * @param string $uid_field the name of the unique identifier field
	 * @throws UniqueNamed_Exception if a unique UID cannot be generated
	 * @return string the generated unique UID
	 */
	public function generate_uid(Models\FirstClass $firstclass, $type, $uid_field = "uid") {
		if (!($firstclass instanceof UniqueNamed)) throw new UniqueNamed_Exception("The specified model does not implement the UniqueNamed interface");

		switch ($type) {
			case self::TYPE_BASE36:
			case self::TYPE_BASE62:
				break;
			default:
				throw new UniqueNamed_Exception("Unknown key type: ${type}");
		}
		
		$key = null;
		for ($i = 1000; $i-- > 0;) {
			$attempt = null;
			switch ($type) {
				case self::TYPE_BASE36:
					$attempt = Database\Type_Base36BigId::generate_random_base36_id();
					break;
				case self::TYPE_BASE62:
					$attempt = Database\Type_Base62BigId::generate_random_base62_id();
					break;
			}
		
			if (null === $this->get_firstclass_by_name($firstclass, $attempt, $uid_field)) {
				$key = $attempt;
				break;
			}
		}
		
		if ($key === null) throw new UniqueNamed_Exception("Unable to generate a unique ${type} key in over 1000 attempts. This is highly unlikely.");
		
		return $key;
	}
}
