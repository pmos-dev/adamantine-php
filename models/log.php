<?php
/**
 * Audit log modelling within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "models/models.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/area.php";
require_once ADAMANTINE_ROOT_PATH . "models/instance.php";
require_once ADAMANTINE_ROOT_PATH . "models/installed.php";
require_once ADAMANTINE_ROOT_PATH . "models/user.php";

if (!defined("APP_ROOT_PATH")) die("APP_ROOT_PATH has not been set.");

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Data as Data;
use \_APPLICATION_NAMESPACE_\Config as Config;

/**
 * Defines the model for audit log entries.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Log extends Models\Models {
	private $installed, $user;
	
	/**
	 * Constructs a new instance of this model.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Area $area the Area model to associate with
	 * @param Instance $instance the Instance model to associate with
	 * @param Installed $installed the Installed model to associate with
	 * @param User $user the User model to associate with
	 */
	public function __construct(Database\Wrapper $database, Area $area, Instance $instance, Installed $installed, User $user) {
		$this->area = $area;
		$this->instance = $instance;
		$this->installed = $installed;
		$this->user = $user;
		
		parent::__construct($database, Config\DATABASE_PREFIX . "logs", array(
			"id" => new Database\Type_Base36BigId(Database\Type::NOT_NULL),
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL),
			"installed" => new Database\Type_Id(Database\Type::ALLOW_NULL),
				
			"timestamp" => new Database\Type_DateTime(Database\Type::NOT_NULL),
			"ipaddr" => new Database\Type_InetAddr(Database\Type::NOT_NULL),
			
			"user" => new Database\Type_Id(Database\Type::ALLOW_NULL),
			"user_name" => new Database\Type_String(255, Database\Type::NOT_NULL, ""),
			
			"action" => new Database\Type_String(255, Database\Type::NOT_NULL),
			"data" => new Database\Type_Text(Database\Type::ALLOW_NULL)
		), array(
			"instance" => new Models\ForeignKey($instance, "CASCADE", "CASCADE", Config\DATABASE_PREFIX . "_variable__fk_instance"),
			"installed" => new Models\ForeignKey($installed, "CASCADE", "CASCADE", Config\DATABASE_PREFIX . "_variable__fk_installed"),
			"user" => new Models\ForeignKey($user, "SET NULL", "CASCADE", Config\DATABASE_PREFIX . "_variable__fk_user")
		), array(), array(
			new Models\UniqueKey(array("id"), Config\DATABASE_PREFIX . "_logs__uk_i")
		));
	}

	//-------------------------------------------------------------------------
	
	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		$results = $this->structure;
		$results["join_user_name"] = new Database\Type_IdName(Database\Type::ALLOW_NULL);
		$results["join_area_name"] = new Database\Type_IdName(Database\Type::ALLOW_NULL);
		
		$this->database->preprepare("ADAMANTINE__Log__LIST_BY_INSTANCE_AND_DATE", "
			SELECT {$this->fields},
				" . $this->modelfield($this->user, "name") . " AS " . $this->tempfield("join_user_name") . ",
				" . $this->modelfield($this->area, "name") . " AS " . $this->tempfield("join_area_name") . "
			FROM " . $this->model($this) . "
			LEFT JOIN " . $this->model($this->installed) . "
				ON " . $this->modelfield($this, "installed") . "=" . $this->modelfield($this->installed, $this->installed->get_id_field()) . "
			LEFT JOIN " . $this->model($this->area) . "
				ON " . $this->modelfield($this->installed, "area") . "=" . $this->modelfield($this->area, $this->area->get_id_field()) . "
			LEFT JOIN " . $this->model($this->user) . "
				ON " . $this->modelfield($this, "user") . "=" . $this->modelfield($this->user, $this->user->get_id_field()) . "
			WHERE " . $this->modelfield($this, "instance") . "=:instance
			AND " . $this->modelfield($this, "timestamp") . ">=:date
			ORDER BY " . $this->modelfield($this, "timestamp") . " DESC
		", array(
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL),
			"date" => new Database\Type_Date(Database\Type::NOT_NULL)
		), $results);
		
		$this->database->preprepare("ADAMANTINE__Log__GET_BY_ID_AND_INSTANCE", "
			SELECT {$this->fields},
				" . $this->modelfield($this->user, "name") . " AS " . $this->tempfield("join_user_name") . ",
				" . $this->modelfield($this->area, "name") . " AS " . $this->tempfield("join_area_name") . "
			FROM " . $this->model($this) . "
			LEFT JOIN " . $this->model($this->installed) . "
				ON " . $this->modelfield($this, "installed") . "=" . $this->modelfield($this->installed, $this->installed->get_id_field()) . "
			LEFT JOIN " . $this->model($this->area) . "
				ON " . $this->modelfield($this->installed, "area") . "=" . $this->modelfield($this->area, $this->area->get_id_field()) . "
			LEFT JOIN " . $this->model($this->user) . "
				ON " . $this->modelfield($this, "user") . "=" . $this->modelfield($this->user, $this->user->get_id_field()) . "
			WHERE " . $this->modelfield($this, "instance") . "=:instance
			AND " . $this->modelfield($this, "id") . "=:id
		", array(
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL),
			"id" => new Database\Type_Base36BigId(Database\Type::NOT_NULL)
		), $results);
		
		unset($results["join_user_name"]);
		
		$this->database->preprepare("ADAMANTINE__Log__LIST_BY_INSTANCE_AND_USER", "
			SELECT {$this->fields},
				" . $this->modelfield($this->area, "name") . " AS " . $this->tempfield("join_area_name") . "
			FROM " . $this->model($this) . "
			INNER JOIN " . $this->model($this->user) . "
				ON " . $this->modelfield($this, "user") . "=" . $this->modelfield($this->user, $this->user->get_id_field()) . "
			LEFT JOIN " . $this->model($this->installed) . "
				ON " . $this->modelfield($this, "installed") . "=" . $this->modelfield($this->installed, $this->installed->get_id_field()) . "
			LEFT JOIN " . $this->model($this->area) . "
				ON " . $this->modelfield($this->installed, "area") . "=" . $this->modelfield($this->area, $this->area->get_id_field()) . "
			WHERE " . $this->modelfield($this, "instance") . "=:instance
			AND " . $this->modelfield($this->user, $this->user->get_id_field()) . "=:user
			ORDER BY " . $this->modelfield($this, "timestamp") . " DESC
		", array(
			"instance" => new Database\Type_Id(Database\Type::NOT_NULL),
			"user" => new Database\Type_Id(Database\Type::NOT_NULL)
		), $results);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Inserts an entry into the audit log directly
	 * 
	 * @internal
	 * @param mixed[] $instance the instance to log activity for
	 * @param mixed[]|NULL $installed the installed area to log activity for, or null for systemwide
	 * @param mixed[] $user the user who has performed the activity being logged
	 * @param string $action a brief description of the activity that has taken place
	 * @param string|NULL $data any further details for the audit log
	 * @throws Models\Exception
	 * @return void
	 */
	private function log_raw(array $instance, $installed, array $user, $action, $data = null) {
		self::assert_id_object($instance, $this->instance->get_id_field());		
		self::assert_id_object($user, $this->user->get_id_field()); 
		self::assert_id_or_null($installed);
		
		$_TRANSACTION_OWNER = $this->database->transaction_claim();
		
		while (true) {
			$id = Database\Type_Base36BigId::generate_random_base36_id();
			if (0 === ($this->database->count_rows_by_conditions(Config\DATABASE_PREFIX . "logs", array(
				"id" => new Database\Param($id, new Database\Type_Base36BigId(Database\Type::NOT_NULL))
			)))) break;
		}
		
		$ip = $_SERVER["REMOTE_ADDR"];
		if (in_array($ip, explode(",", "localhost,::1"))) $ip = "127.0.0.1";
		
		if (false === $this->insert(array(
			"id" => $id,
			"instance" => $instance[$this->instance->get_id_field()],
			"installed" => $installed,
			"timestamp" => date("Y-m-d H:i:s"),
			"ipaddr" => $ip,
			"user" => $user[$this->user->get_id_field()],
			"user_name" => "{$user["name"]}, {$user["fullname"]}",
			"action" => $action,
			"data" => $data
		))) {
			$this->database->transaction_rollback();
			throw new Models\Exception("Error inserting log entry");
		}
		
		if ($_TRANSACTION_OWNER) $this->database->transaction_commit();
	}
	
	/**
	 * Inserts an entry into the audit log about non-installed area activity that has been performed by a user.
	 * 
	 * @param mixed[] $instance the instance to log activity for
	 * @param mixed[] $user the user who has performed the activity being logged
	 * @param string $action a brief description of the activity that has taken place
	 * @param string|NULL $data any further details for the audit log
	 * @return void
	 */
	public function log_core(array $instance, array $user, $action, $data = null) {
		$this->log_raw($instance, null, $user, $action, $data);
	}

	/**
	 * Inserts an entry into the audit log about installed area activity that has been performed by a user.
	 * 
	 * @param mixed[] $instance the instance to log activity for
	 * @param mixed[] $installed the installed area to log activity for
	 * @param mixed[] $user the user who has performed the activity being logged
	 * @param string $action a brief description of the activity that has taken place
	 * @param string|NULL $data any further details for the audit log
	 * @return void
	 */
	public function log_area(array $instance, array $installed, array $user, $action, $data = null) {
		self::assert_id_object($installed, $this->installed->get_id_field()); 
		$this->log_raw($instance, $installed[$this->installed->get_id_field()], $user, $action, $data);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns a log entry for the specified instance and audit log ID.
	 * 
	 * @param mixed[] $instance the instance to fetch entries for
	 * @param string $id the row ID in base36 big integer format
	 * @return mixed[]|NULL
	 */
	public function get_log_by_instance_and_id(array $instance, $id) {		
		self::assert_id_object($instance, $this->instance->get_id_field());
		self::assert_regex(Data\Data::REGEX_PATTERN_BASE36_ID, $id);
		
		return $this->database->execute_params_single("ADAMANTINE__Log__GET_BY_ID_AND_INSTANCE", array(
			"instance" => $instance[$this->instance->get_id_field()],
			"id" => $id
		));
	}
	
	/**
	 * Returns an array of log entries for the specified instance in the given timeframe.
	 * 
	 * @param mixed[] $instance the instance to fetch entries for
	 * @param int the number of days to look backwards into the log
	 * @return array
	 */
	public function get_log_by_instance_and_last_days(array $instance, $days) {
		self::assert_id_object($instance, $this->instance->get_id_field());
		self::assert_int($days, 0);
	
		$d = date("d"); 
		$m = date("m"); 
		$y = date("Y");
		$d -= $days;
	
		$date = date("Y-m-d", mktime(0, 0, 1, $m, $d, $y));
	
		return $this->database->execute_params("ADAMANTINE__Log__LIST_BY_INSTANCE_AND_DATE", array(
			"instance" => $instance[$this->instance->get_id_field()],
			"date" => $date
		));
	}

	/**
	 * Returns an array of log entries for the specified instance and user.
	 * 
	 * @param mixed[] $instance the instance to fetch entries for
	 * @param mixed[] $user the user to fetch entries for
	 * @return array
	 */
	public function get_log_by_instance_and_user(array $instance, array $user) {
		self::assert_id_object($instance, $this->instance->get_id_field());
		self::assert_id_object($user, $this->user->get_id_field());
		
		return $this->database->execute_params("ADAMANTINE__Log__LIST_BY_INSTANCE_AND_USER", array(
			"instance" => $instance[$this->instance->get_id_field()],
			"user" => $user[$this->user->get_id_field()]
		));
	}
}
