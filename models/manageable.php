<?php
/**
 * Interface for 'manageable' models within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";

/**
 * Defines an implementing class as being 'manageable'.
 * 
 * Manageable models have predetermined qualities which allow their underlying data to be manipulated using generic model management scripts.
 * 
 * @author Pete Morris
 * @version 1.2.0
*/
interface Manageable extends UniqueNamed {
	/**
	 * Returns whether or not the model is indeed manageable.
	 * 
	 * This generally should always return true, but allows sub-classes of implementing models to override model management.
	 * 
	 * @return boolean
	 */
	public function is_model_manageable();

	/**
	 * Returns the corresponding name used in the repository for this model.
	 * 
	 * This helps considerably when doing automatic model chain traversal.
	 * 
	 * @return string
	 */
	public function get_repository_name();

	/**
	 * Returns whether or not the model supports aliases natively.
	 * 
	 * @return boolean
	 */
	public function is_aliasing_supported();
	
	/**
	 * Returns an array of which field names within the model should be manageable.
	 * 
	 * Other fields are not made visible through the generic Adamantine model management scripts.
	 * 
	 * @return string[]
	 */
	public function get_manageable_fields();

	/**
	 * Returns the user access level required to manage this model's data.
	 * 
	 * Possible values are "read", "user", "editor" or "full", and are passed to the Session\has_access function to determine whether the current user can manage this model.
	 * 
	 * @see \Abstraction\Framework\Session
	 * @return string
	 */
	public function get_access_required_to_manage();
	
	/**
	 * Returns whether the specified data row of this model is viewable by default.
	 * 
	 * @param mixed[] $object the model row
	 * @return boolean
	 */
	public function is_object_viewable(array $object);

	/**
	 * Returns whether the specified data row of this model is editable or not.
	 * 
	 * @param mixed[] $object the model row
	 * @return boolean
	 */
	public function is_object_editable(array $object);
	
	/**
	 * Returns a description for the specified model field.
	 * 
	 * By default, the field name should be returned if no other description is defined.
	 *  
	 * @param string $field the field name
	 * @return string
	 */
	public function get_field_description($field);

	/**
	 * Returns an optional suffix for the specified model field.
	 * 
	 * @param string $field the field name
	 * @return string|NULL
	 */
	public function get_field_suffix($field);
	
	/**
	 * Allows managed models to specify a custom script to be appended to the model management row view script.
	 * 
	 * @return string
	 */
	public function get_view_additional_file();
}
