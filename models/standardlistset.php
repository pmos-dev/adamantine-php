<?php
/**
 * Standard ListSet within the Adamantine framework.
 * This is a helper model built from other structures rather than corresponding to a specific structure in its own right.
 * 
 * @copyright 2015 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/firstclass.php";
require_once ABSTRACTION_ROOT_PATH . "models/orientatedordered.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/managedorientatedordered.php";
require_once ADAMANTINE_ROOT_PATH . "models/standardlistitem.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;

/**
 * @internal
 */
class StandardListSet_Exception extends ManagedOrientatedOrdered_Exception {}

/**
 * Defines a root model for 'managed' orientated-ordered structured models.
 * 
 * @api
 * @author Pete Morris
 * @version 2.0.0
 */
abstract class StandardListSet extends ManagedOrientatedOrdered {
	protected $firstclass, $firstclass_field;
	private $listitem;
	
	/**
	 * Constructs a new instance of this standard model.
	 * 
	 * Note that an associated StandardListItem model (and table etc.) will be created automatically as part of the standard process.
	 * 
	 * @param Database\Wrapper $database the database interface to build the model upon
	 * @param Models\FirstClass $firstclass the FirstClass model to associate with
	 * @param string $table the table name within the database
	 * @param boolean $_ADD_ALIAS_SUPPORT whether to extend support for aliases to this model's unique name field
	 * @param string $firstclass_field the name to use for the firstclass field, which tends to correspond to the firstclass model name, e.g. "installed"
	 * @throws ManagedOrientatedOrdered_Exception
	 */
	public function __construct(Database\Wrapper $database, Models\FirstClass $firstclass, $table, $_ADD_ALIAS_SUPPORT = false, $firstclass_field = "installed") {
		$this->firstclass = $firstclass;
		$this->firstclass_field = $firstclass_field;

		parent::__construct($database, $firstclass, $table, array(), array(), array(), false, $firstclass_field);
		
		$this->listitem = new StandardListItem($database, $this, $_ADD_ALIAS_SUPPORT);
	}

	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	public function get_view_additional_file() {
		return ADAMANTINE_ROOT_PATH . "management/_listsets_additional.php";
	}
	
	/**
	 * @internal
	 */
	public function create_table() {
		parent::create_table();
		
		$this->listitem->create_table();
	}
	
	/**
	 * @internal
	 */
	public function drop_table() {
		$this->listitem->drop_table();

		parent::drop_table();
	}
	
	//-------------------------------------------------------------------------
	
	abstract function get_listitem_model_repository_name();
	
	public function get_listitem_model() {
		return $this->listitem;
	}
	
	public function register_listitem_in_repository($repository, $area) {
		$repository->add($area, $this->get_listitem_model_repository_name(), $this->get_listitem_model());
	}
}
