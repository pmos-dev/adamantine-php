<?php
/**
 * Alias support for UniqueNamed models.
 * 
 * This is useful for assisting with things like tags which may have multiple names for the same tag.
 * Generally, it shouldn't be used directly, but instead the $_ADD_ALIAS_SUPPORT flag for the various Managed* classes should be set.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Models;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "models/installed.php";
require_once ADAMANTINE_ROOT_PATH . "models/uniquenamed.php";

use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Data as Data;

/**
 * @internal
 */
class Alias_Exception extends Models\SecondClass_Exception {}

/**
 * Defines a root model for aliases.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Alias extends Models\SecondClass implements UniqueNamed {
	protected $uniquenamed;
	
	public function __construct(Database\Wrapper $database, UniqueNamed $uniquenamed) {
		if (!($uniquenamed instanceof Models\FirstClass)) throw new Alias_Exception("Parent models for aliasing must be FirstClass as a minimum");
		
		$this->uniquenamed = $uniquenamed;

		$table = $uniquenamed->get_table() . "__alias";
		
		$structure = array("name" => new Database\Type_IdName(Database\Type::NOT_NULL));

		$unique_keys = array();
		$unique_keys[] = new Models\UniqueKey(array("uniquenamed", "name"), "{$table}_uk_un");
		
		parent::__construct($database, $table, $structure, $uniquenamed, "uniquenamed", "id", array(), $unique_keys);
	}

	//-------------------------------------------------------------------------

	/**
	 * @internal
	 */
	protected function define_views() {
		parent::define_views();

		if ($this->uniquenamed instanceof Models\SecondClass) {
			$fields = array();
			foreach (array_keys($this->uniquenamed->get_structure()) as $field) $fields[] = $this->modelviewfield($this->uniquenamed, "SECONDCLASS_JOIN", $field);
			$fields = implode(",", $fields);
	
			$this->define_view("ALIAS_JOIN", "
				SELECT {$fields},
					" . $this->modelviewfield($this, "SECONDCLASS_JOIN", "name") . " AS " . $this->tempfield("alias") . ",
					" . $this->modelviewfield($this->uniquenamed, "SECONDCLASS_JOIN", "firstclass_id") . " AS " . $this->tempfield("grandparent_id") . "
				FROM " . $this->modelview($this, "SECONDCLASS_JOIN") . "
				INNER JOIN" . $this->modelview($this->uniquenamed, "SECONDCLASS_JOIN") . "
					ON " . $this->modelviewfield($this, "SECONDCLASS_JOIN", "firstclass_id") . "=" . $this->modelviewfield($this->uniquenamed, "SECONDCLASS_JOIN", $this->uniquenamed->get_id_field()) . "
			");
		}
	}
	
	/**
	 * @internal
	 */
	protected function preprepare() {
		parent::preprepare();

		if ($this->uniquenamed instanceof Models\SecondClass) {
			$fields = array();
			foreach (array_keys($this->uniquenamed->get_structure()) as $field) $fields[] = $this->modelviewfield($this, "ALIAS_JOIN", $field);
			$fields = implode(",", $fields);
			
			$results = $this->uniquenamed->get_structure();
			$results["alias"] = new Database\Type_IdName(Database\Type::NOT_NULL);
			
			$this->database->preprepare("MODELS_ALIAS__{$this->table}__LIST_BY_GRANDPARENT_AND_NAME", "
				SELECT {$fields}, " . $this->modelviewfield($this, "ALIAS_JOIN", "alias") . "
				FROM " . $this->modelview($this, "ALIAS_JOIN") . "
				WHERE " . $this->modelviewfield($this, "ALIAS_JOIN", "grandparent_id") . "=:grandparent
					AND " . $this->modelviewfield($this, "ALIAS_JOIN", "alias") . "=:name
			", array(
				"grandparent" => new Database\Type_Id(Database\Type::NOT_NULL),
				"name" => new Database\Type_IdName(Database\Type::NOT_NULL),
			), $results);
		} else {
			$fields = array();
			foreach (array_keys($this->uniquenamed->get_structure()) as $field) $fields[] = $this->modelfield($this->uniquenamed, $field);
			$fields = implode(",", $fields);
				
			$results = $this->uniquenamed->get_structure();
			$results["alias"] = new Database\Type_IdName(Database\Type::NOT_NULL);
				
			$this->database->preprepare("MODELS_ALIAS__{$this->table}__LIST_BY_NAME", "
				SELECT {$fields}, " . $this->modelfield($this, "name") . " AS " . $this->tempfield("alias") . "
				FROM " . $this->model($this) . "
				INNER JOIN " . $this->model($this->uniquenamed) . "
					ON " . $this->modelfield($this, "uniquenamed") . "=" . $this->modelfield($this->uniquenamed, $this->uniquenamed->get_id_field()) . " 
				WHERE " . $this->modelfield($this, "name") . "=:name
			", array(
				"name" => new Database\Type_IdName(Database\Type::NOT_NULL),
			), $results);
		}
		
		UniqueNamedHelper::build($this->database)->build_preprepare_for_secondclass($this);
	}
	
	//-------------------------------------------------------------------------
	
	/**
	 * Fetches the alias row associated with the specified unique alias for the given owning row, if it exists.
	 * 
	 * @param mixed[] $uniquenamed the uniquenamed row which will contain the alias
	 * @param string $name the unique name associated with this row
	 * @return mixed[]|NULL
	 */
	public function get_by_name(array $uniquenamed, $name) {
		return UniqueNamedHelper::build($this->database)->get_secondclass_by_firstclass_and_name($this, $uniquenamed, $name);
	}

	/**
	 * Returns a list of all rows that have the specified alias.
	 *  
	 * @param mixed[] $grandparent the context of the 2-level up grandparent (i.e. the uniquenamed's own firstclass)
	 * @param string $name the alias name to query for
	 * @return array
	 */
	public function list_by_name($name) {
		if (!($this->uniquenamed instanceof Models\FirstClass)) throw new Alias_Exception("Call to list_by_name on a SecondClass model. This cannot be done. Use list_by_grandparent_and_name instead.");
		
		self::assert_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name);
		
		return $this->database->execute_params("MODELS_ALIAS__{$this->table}__LIST_BY_NAME", array(
			"name" => $name
		));
	}

	/**
	 * Returns a list of all rows that have the specified alias.
	 *  
	 * @param mixed[] $grandparent the context of the 2-level up grandparent (i.e. the uniquenamed's own firstclass)
	 * @param string $name the alias name to query for
	 * @return array
	 */
	public function list_by_grandparent_and_name(array $grandparent, $name) {
		if (!($this->uniquenamed instanceof Models\SecondClass)) throw new Alias_Exception("Call to list_by_grandparent_and_name on a FirstClass model. This cannot be done. Use list_by_name instead.");
		
		self::assert_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name);
		self::assert_id_object($grandparent, $this->uniquenamed->get_firstclass()->get_id_field());
		
		return $this->database->execute_params("MODELS_ALIAS__{$this->table}__LIST_BY_GRANDPARENT_AND_NAME", array(
			"grandparent" => $grandparent[$this->uniquenamed->get_firstclass()->get_id_field()],
			"name" => $name
		));
	}

	/**
	 * Returns a list of all rows that match the specified alias in whole or part.
	 *  
	 * @param string $name the alias name to search for
	 * @return array
	 */
	public function search_by_name($name) {
		if (!($this->uniquenamed instanceof Models\FirstClass)) throw new Alias_Exception("Call to search_by_name on a SecondClass model. This cannot be done. Use search_by_grandparent_and_name instead.");
		
		self::assert_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name);

		$fields = array();
		foreach (array_keys($this->uniquenamed->get_structure()) as $field) $fields[] = $this->modelfield($this->uniquenamed, $field);
		$fields = implode(",", $fields);
		
		$results = $this->uniquenamed->get_structure();
		$results["alias"] = new Database\Type_IdName(Database\Type::NOT_NULL);
		
		return $this->database->query_params("
			SELECT {$fields}, " . $this->modelfield($this, "name") . " AS " . $this->tempfield("alias") . "
			FROM " . $this->model($this) . "
			INNER JOIN " . $this->model($this->uniquenamed) . "
				ON " . $this->modelfield($this, "uniquenamed") . "=" . $this->modelfield($this->uniquenamed, $this->uniquenamed->get_id_field()) . " 
			WHERE (
				" . $this->modelfield($this, "name") . " LIKE :valuea
				OR
				" . $this->modelfield($this, "name") . " LIKE :valueb
				OR
				" . $this->modelfield($this, "name") . " LIKE :valuec
				OR
				" . $this->modelfield($this, "name") . " LIKE :valued
			)
		", array(
			"valuea" => new Database\Param($name, new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valueb" => new Database\Param("{$name}%", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valuec" => new Database\Param("%{$name}", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valued" => new Database\Param("%{$name}%", new Database\Type_String(255, Database\Type::NOT_NULL))
		), $results);
	}

	/**
	 * Returns a list of all rows that match the specified alias in whole or part.
	 *  
	 * @param mixed[] $grandparent the context of the 2-level up grandparent (i.e. the uniquenamed's own firstclass)
	 * @param string $name the alias name to search for
	 * @return array
	 */
	public function search_by_grandparent_and_name(array $grandparent, $name) {
		if (!($this->uniquenamed instanceof Models\SecondClass)) throw new Alias_Exception("Call to search_by_grandparent_and_name on a FirstClass model. This cannot be done. Use search_by_name instead.");
		
		self::assert_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name);
		self::assert_id_object($grandparent, $this->uniquenamed->get_firstclass()->get_id_field());

		$fields = array();
		foreach (array_keys($this->uniquenamed->get_structure()) as $field) $fields[] = $this->modelviewfield($this, "ALIAS_JOIN", $field);
		$fields = implode(",", $fields);
		
		$results = $this->uniquenamed->get_structure();
		$results["alias"] = new Database\Type_IdName(Database\Type::NOT_NULL);
		
		return $this->database->query_params("
			SELECT {$fields}, " . $this->modelviewfield($this, "ALIAS_JOIN", "alias") . "
			FROM " . $this->modelview($this, "ALIAS_JOIN") . "
			WHERE " . $this->modelviewfield($this, "ALIAS_JOIN", "grandparent_id") . "=:grandparent
				AND (
					" . $this->modelviewfield($this, "ALIAS_JOIN", "alias") . " LIKE :valuea
					OR
					" . $this->modelviewfield($this, "ALIAS_JOIN", "alias") . " LIKE :valueb
					OR
					" . $this->modelviewfield($this, "ALIAS_JOIN", "alias") . " LIKE :valuec
					OR
					" . $this->modelviewfield($this, "ALIAS_JOIN", "alias") . " LIKE :valued
				)
		", array(
			"grandparent" => new Database\Param($grandparent[$this->uniquenamed->get_firstclass()->get_id_field()], new Database\Type_Id(Database\Type::NOT_NULL)),
			"valuea" => new Database\Param($name, new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valueb" => new Database\Param("{$name}%", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valuec" => new Database\Param("%{$name}", new Database\Type_String(255, Database\Type::NOT_NULL)),
			"valued" => new Database\Param("%{$name}%", new Database\Type_String(255, Database\Type::NOT_NULL))
		), $results);
	}
}
