<?php
/**
 * Adamantine search instance by tag.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine;

define("SKIP_SESSION_INITIALIZE", true);
define("APP_ROOT_PATH", "./../");
require_once APP_ROOT_PATH . "adamantine/_init.php";

use \Abstraction\Data as Data;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Search instance", Config\DEFAULT_SUBTITLE);

if (!Data\Data::validate_regex("`^[a-zA-Z0-9]+$`D", $_GET["name"])) \Adamantine\error("Instance name is invalid");
if (null === ($instance = $_INSTANCE->get_by_name($_GET["name"]))) \Adamantine\error("Unable to find an instance with that name");

$_HTTP->redirect(APP_ROOT_PATH . "?xsi={$instance["id"]}");
