<?php
/**
 * Adamantine core unbuild script.
 * Implementing applications should make sure their own unbuild script lastly calls:
 * require_once APP_ROOT_PATH . "adamantine/build/unbuild.php";
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Build;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../");
if (!defined("SKIP_SESSION_INITIALIZE")) define("SKIP_SESSION_INITIALIZE", true);

if (defined("ADAMANTINE_ROOT_PATH")) require_once ADAMANTINE_ROOT_PATH . "_init.php";
else require_once APP_ROOT_PATH . "adamantine/_init.php";

use \Abstraction\Database as Database;
use \Adamantine as Adamantine;
use \_APPLICATION_NAMESPACE_\Config as Config;

$_UI->set_titles("Unbuild system", Config\DEFAULT_SUBTITLE);

try {
	$_DATABASE->query_params("
		SELECT *
		FROM " . $_DATABASE->delimit(Config\DATABASE_PREFIX . "installeds") . "
		LIMIT 1
	");
} catch(Database\Exception $e) {
	Adamantine\error("System does not appear to have been built yet.");
}

if (sizeof($_INSTANCE->list_all()) > 0) Adamantine\error("There are still instances within this system. Please remove all of these first");
if (sizeof($_AREA->list_all()) > 0) Adamantine\error("There are still areas built into this system. Please remove all of these first");

$_UI->assert_confirmed("Unbuilding will remove all data from the system. This is permanent and cannot be undone.");

$_LOG->drop_table();

$_SETTING->drop_table();
$_VARIABLE->drop_table();

$_ACCESS->drop_tables();
$_SYSTEM->drop_tables();

$_UI->success("System has now been unbuilt.");
