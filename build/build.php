<?php
/**
 * Adamantine core build script.
 * Implementing applications should make sure their own build script initially calls:
 * require_once APP_ROOT_PATH . "adamantine/build/build.php";
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Build;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../");
define("SKIP_BUILT_TEST", true);
define("SKIP_SESSION_INITIALIZE", true);

if (defined("ADAMANTINE_ROOT_PATH")) require_once ADAMANTINE_ROOT_PATH . "_init.php";
else require_once APP_ROOT_PATH . "adamantine/_init.php";

use \Abstraction\Database as Database;
use \_APPLICATION_NAMESPACE_\Config as Config;

try {
	$_DATABASE->query_params("
		SELECT *
		FROM " . $_DATABASE->delimit(Config\DATABASE_PREFIX . "installeds") . "
		LIMIT 1
	");
	$_UI->set_titles("System error", Config\DEFAULT_SUBTITLE);
	Adamantine\error("System is already built.");
// @phpcs:ignore Generic.CodeAnalysis.EmptyStatement
} catch(\Exception $e) {}

$_UI->set_titles("Build system", Config\DEFAULT_SUBTITLE);

$_UI->assert_confirmed("Build system?");

// start a transaction so if any failures occur the entire build is aborted
$_DATABASE->transaction_begin();

$_SYSTEM->create_tables();
$_ACCESS->create_tables();

$_VARIABLE->create_table();
$_SETTING->create_table();

$_LOG->create_table();

$_DATABASE->transaction_commit();

if (!defined("SKIP_BUILD_SUCCESS_MESSAGE") || !SKIP_BUILD_SUCCESS_MESSAGE) $_UI->success("System built", "The system has been successfully built and populated.");
