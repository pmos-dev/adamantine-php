<?php
/**
 * Adamantine Admin area: adjust user group membership.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;

$_UI->set_titles("User group membership", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "editor")) Adamantine\error("You do not have permission to access this area");

if (!isset($_GET["return"])) Adamantine\error("No return destination set");
switch ($_GET["return"]) {
	case "User":
	case "Group":
		break;
	default:
		Adamantine\error("Bad return destination set");
}

if (!Data\Data::validate_id($_GET["user"])) Adamantine\error("Invalid user ID supplied");
if (null === ($user = $_USER->get($xsi, $_GET["user"]))) Adamantine\error("No such user exists");

if (!Data\Data::validate_id($_GET["group"])) Adamantine\error("Invalid group ID supplied");
if (null === ($group = $_GROUP->get($xsi, $_GET["group"]))) Adamantine\error("No such group exists");

switch ($_GET["action"]) {
	case "join":
		$_ACCESS->add_user_to_group($user, $group);
		break;
	case "leave":
		$_ACCESS->remove_user_from_group($user, $group);
		break;
	default:
		Adamantine\error("Unknown action");
}

$_SESSIONMANAGER->log_by_name("admin", $_GET["action"] === "join" ? "Add user to group" : "Remove user from group", "{$user["name"]} : {$group["name"]}");

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "management/view.php?area=_core&model={$_GET["return"]}&id=" . ($_GET["return"] === "User" ? $user["id"] : $group["id"]) . "&xsi={$xsi["id"]}");
