<?php
/**
 * Adamantine Admin area: menu definitions.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) die();	// check the script isn't being called directly

$menu = array();
if ($_SESSIONMANAGER->has_access_by_name("admin", "editor")) $menu["User management"] = ADAMANTINE_ROOT_PATH . "management/list.php?area=_core&model=User&xsi={$xsi["id"]}";
if ($_SESSIONMANAGER->has_access_by_name("admin", "editor")) $menu["Group management"] = ADAMANTINE_ROOT_PATH . "management/list.php?area=_core&model=Group&xsi={$xsi["id"]}";
if ($_SESSIONMANAGER->has_access_by_name("admin", "full")) {
	$menu["Import data"] = ADAMANTINE_ROOT_PATH . "areas/admin/import.php?xsi={$xsi["id"]}";
	$menu["Export data"] = ADAMANTINE_ROOT_PATH . "areas/admin/export.php?xsi={$xsi["id"]}";
}
if ($_SESSIONMANAGER->has_access_by_name("admin", "editor")) $menu["System log"] = ADAMANTINE_ROOT_PATH . "areas/admin/log.php?xsi={$xsi["id"]}";
if ($_SESSIONMANAGER->has_access_by_name("admin", "full")) $menu["Configuration"] = ADAMANTINE_ROOT_PATH . "areas/admin/configuration.php?xsi={$xsi["id"]}";

if (sizeof($menu) > 0) $_UI->add_menu("Admin", $menu);
