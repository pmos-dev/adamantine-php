<?php
/**
 * Adamantine Admin area: additional group view functionality for the managed model.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("ADAMANTINE_ROOT_PATH")) die();	// check the script isn't being called directly

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Framework as Framework;

$page->add(new HTML\Header("Group membership", HTML\Header::LEVEL_3));

$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/autoscroll.css");
$_HTML->add_include("js", ADAMANTINE_ROOT_PATH . "js/autoscroll.js");

$page->add($table = new HTML\Table("groups", "unitable autoscroll"));
$table->add_column("Name");
$table->add_column("Member?");
$table->add_column("Actions");

$membership = array();
foreach ($_USER_GROUP->list_as_by_b($data) as $user) $membership[] = $user["id"];

$users = $_USER->list_by_firstclass($xsi);
foreach ($users as $user) {
	$row = $table->new_row();
	$row->set_cell("Name", new HTML\Link($user["name"], ADAMANTINE_ROOT_PATH . "management/view.php?area=_core&model=User&id={$user["id"]}&xsi={$xsi["id"]}", null, "command icon_go"));
	$row->set_cell("Member?", in_array($user["id"], $membership) ? new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/tick.svg") : new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/cross.svg"));
	$row->set_cell("Actions", "");
	
	if ($data["type"] === Framework\Models\Group::NORMAL) $row->set_cell("Actions", in_array($user["id"], $membership)
			? new HTML\Link("Leave this group", ADAMANTINE_ROOT_PATH . "areas/admin/users_groups_do.php?action=leave&user={$user["id"]}&group={$data["id"]}&xsi={$xsi["id"]}&return=Group", null, "command icon_leave")
			: new HTML\Link("Join this group", ADAMANTINE_ROOT_PATH . "areas/admin/users_groups_do.php?action=join&user={$user["id"]}&group={$data["id"]}&xsi={$xsi["id"]}&return=Group", null, "command icon_join"));
}

$page->add(new HTML\Header("Area access", HTML\Header::LEVEL_3));

if ($data["type"] === Framework\Models\Group::SYSTEM) $page->add(new HTML\Header("The system group is not designed for assigning access permissions", HTML\Header::LEVEL_4, null, "info"));
else {
	$page->add($table = new HTML\Table("access", "unitable"));
	$table->add_column("Area");
	$table->add_column("Access");
	
	$areas = $_SYSTEM->list_installed_areas();
	foreach ($areas as $area) {
		$row = $table->new_row();
		$row->set_cell("Area", $area["name"]);
		
		$installed = $_SYSTEM->get_installed_by_area($area);
		
		switch ($_GROUP_INSTALLED->get_access($data, $installed)) {
			case null:
				$row->set_cell("Access", "-");
				break;
			case Framework\Access::NONE:
				$row->set_cell("Access", "none");
				break;
			case Framework\Access::READ:
				$row->set_cell("Access", "read");
				break;
			case Framework\Access::USER:
				$row->set_cell("Access", "user");
				break;
			case Framework\Access::EDITOR:
				$row->set_cell("Access", "editor");
				break;
			case Framework\Access::FULL:
				$row->set_cell("Access", "full");
				break;
		}
	}
	
	$page->add(HTML\LinkBar::singular(new HTML\Link("Edit access permissions", ADAMANTINE_ROOT_PATH . "areas/admin/groups_direct.php?group={$data["id"]}&xsi={$xsi["id"]}", null, "command icon_access")));
}
