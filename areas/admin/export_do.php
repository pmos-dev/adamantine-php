<?php
/**
 * Adamantine Admin area: perform export of data.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Data as Data;

use \Adamantine as Adamantine;

require_once APP_ROOT_PATH . "adamantine/areas/area_importexport.php";

use \Adamantine\Areas\ImportExport;

$_UI->set_titles("Bulk export data", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "full")) Adamantine\error("You do not have permission to access this area");

$_HTML->add_code("css", <<<CSS

pre {
	border: 1px solid #88b4dc;
	white-space: pre-wrap;
	word-wrap: break-word;
}
		
CSS
);

$_DOWNLOAD = Data\Data::checkbox_boolean($_POST["download"]);
$_FULL = Data\Data::checkbox_boolean($_POST["full"]);

if ($_DOWNLOAD) $_HTTP->download_headers("application/download", "adamantine_export_" . date("Y-m-d_H-i-s") . ".txt");
else ob_start();

ImportExport::comment("Adamantine export at " . date("Y-m-d H:i:s"));
ImportExport::gap();

// reset timeout for each legitimate line
set_time_limit(0);

$_IMPORTEXPORT = array();
foreach ($_SYSTEM->list_installed_areas() as $area) {
	if ($area["status"] === \Abstraction\Framework\Models\Installed::DISABLED) continue;
	if (!file_exists(APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php")) continue;
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php";

	ImportExport::line();
	ImportExport::comment("Exports for {$area["name"]}");
	ImportExport::gap();

	$exports = $_IMPORTEXPORT[$area["name"]]->list_exports();
	if (sizeof($_VARIABLE->list_by_area($area)) > 0) {
		$exports["config"] = "Configuration settings";
	}
	
	foreach ($exports as $keyword => $caption) {
		if ($_FULL || Data\Data::checkbox_boolean($_POST["export_{$area["name"]}_{$keyword}"])) {
			$_IMPORTEXPORT[$area["name"]]->export($keyword);
			ImportExport::gap();
		}
	}
}

if ($_DOWNLOAD) {
	flush();
	die();
}

$page = $_UI->get_content();

$page->add(new HTML\Header("Export at " . date("Y-m-d H:i:s"), HTML\Header::LEVEL_3));

$page->add(new HTML\Pre($multi = new HTML\MultiElements()));
$multi->add(ob_get_clean());

$_HTML->complete();
