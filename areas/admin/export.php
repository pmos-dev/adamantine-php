<?php
/**
 * Adamantine Admin area: export data form.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

$_UI->set_titles("Bulk export data", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "full")) Adamantine\error("You do not have permission to access this area");

$_HTML->add_code("jquery", <<<JQUERY

$("#full").change(function() {
	$(".export_item").prop("disabled", $(this).prop("checked"));
	if (!$(this).prop("checked")) return;
	$(".export_item").prop("checked", true);
});

JQUERY
);

$page = $_UI->get_content();

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "areas/admin/export_do.php", "post", "export", "uniform autosize"));
$form->add_hidden("xsi", $xsi["id"]);

$row = $form->add_row("Full export", new HTML\Form_Checkbox(false, "full"), "full");
$row = $form->add_row("Download instead of display", new HTML\Form_Checkbox(false, "download"), "download");

$_IMPORTEXPORT = array();
foreach ($_SYSTEM->list_installed_areas() as $area) {
	if ($area["status"] === \Abstraction\Framework\Models\Installed::DISABLED) continue;
	if (!file_exists(APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php")) continue;
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php";

	$exports = $_IMPORTEXPORT[$area["name"]]->list_exports();
	
	if (sizeof($_VARIABLE->list_by_area($area)) > 0) {
		$exports = array_merge(array("config" => "Configuration settings"), $exports);
	}
	
	if (sizeof($exports) == 0) continue;

	$_IMPORTEXPORT[$area["name"]]->add_export_jquery_code($_HTML);

	$form->add(new HTML\Header("{$area["name"]} data", HTML\Header::LEVEL_4));
	foreach ($exports as $keyword => $caption) {
		$row = $form->add_row($caption, new HTML\Form_Checkbox(false, "export_{$area["name"]}_{$keyword}", "export_item"), "export_{$area["name"]}_{$keyword}");
		$row->get_classes()->add("optional");
	}
}

$form->add_submit("Export");

$_HTML->complete();
