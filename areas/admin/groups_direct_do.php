<?php
/**
 * Adamantine Admin area: save direct group ACL changes.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Framework as Framework;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;

$_UI->set_titles("Set group access permissions", $xsi["description"]);

if (!Data\Data::validate_id($_POST["group"])) Adamantine\error("Invalid group ID supplied");
if (null === ($group = $_GROUP->get($xsi, $_POST["group"]))) Adamantine\error("No such group exists");

$areas = $_SYSTEM->list_installed_areas();
foreach ($areas as $area) {
	if (!isset($_POST["area_{$area["id"]}"])) $_POST["area_{$area["id"]}"] = null;
	else if ($_POST["area_{$area["id"]}"] === "") $_POST["area_{$area["id"]}"] = null;
	
	$installed = $_SYSTEM->get_installed_by_area($area);
		
	if ($_POST["area_{$area["id"]}"] === null) $_GROUP_INSTALLED->revoke_access($group, $installed);
	else switch ($_POST["area_{$area["id"]}"]) {
		case Framework\Access::NONE:
			throw new Exception("NONE access is not supported for groups. Set to null to revoke instead.");
		case Framework\Access::READ:
		case Framework\Access::USER:
		case Framework\Access::EDITOR:
		case Framework\Access::FULL:
			$_GROUP_INSTALLED->set_access($group, $installed, intval($_POST["area_{$area["id"]}"]));
			break;
		default:
			throw new Exception("Unknown access level supplied", $_POST["area_{$area["id"]}"]);
	}
}

$_SESSIONMANAGER->log_by_name("admin", "Set group access permissions");

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "management/view.php?area=_core&model=Group&id={$group["id"]}&xsi={$xsi["id"]}");
