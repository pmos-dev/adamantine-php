<?php
/**
 * Adamantine Admin area: generic admin area API for Adamantine applications; can be subclassed to extend.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH is not set");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "framework/system.php";
require_once ABSTRACTION_ROOT_PATH . "framework/access.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH is not set");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/area.php";
require_once ADAMANTINE_ROOT_PATH . "models/variable.php";
require_once ADAMANTINE_ROOT_PATH . "areas/area_api.php";

use \Adamantine\Areas as Areas;

/**
 * @internal
 */
class Exception extends Areas\Exception {}

/**
 * Provides core admin functionality for an Adamantine application.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class API extends Areas\Area_API {

	//-------------------------------------------------------------------------

	/**
	 * Returns the area name this API represents: "admin"
	 * 
	 * @return string
	 */
	public function get_area_name() {
		return "admin";
	}
}
