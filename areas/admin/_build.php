<?php
/**
 * Adamantine Admin area: build the admin area into an application system.
 * Subareas should make sure they have a corresponding file that calls:
 * require_once ADAMANTINE_ROOT_PATH . "areas/admin/_build.php";
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) die();	// check the script isn't being called directly

use \Adamantine\Models as Models;

$_DATABASE->transaction_begin();

$_API_ADMIN->build_area();

$_API_ADMIN->create_variable("LOG_DAYS", "Number of days to log activity", Models\Variable::UNSIGNED, 30);

$_DATABASE->transaction_commit();
