<?php
/**
 * Adamantine Admin area: view system log.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;

$_UI->set_titles("System log", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "editor")) Adamantine\error("You do not have permission to access this area");

$_IS_FULL = $_SESSIONMANAGER->has_access_by_name("admin", "full");

$days = $_SETTINGS->get_by_names("admin", "LOG_DAYS");

$page = $_UI->get_content();

$page->add(new HTML\Header("Viewing log entries for the past {$days} days", HTML\Header::LEVEL_3));

$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/autoscroll.css");
$_HTML->add_include("js", ADAMANTINE_ROOT_PATH . "js/autoscroll.js");

$page->add($table = new HTML\Table("log", "unitable autoscroll"));
$table->add_column("Timestamp");
if ($_IS_FULL) $table->add_column("IP");
$table->add_column("User");
$table->add_column("Area");
$table->add_column("Action");

$log = $_LOG->get_log_by_instance_and_last_days($xsi, intval($days));

foreach ($log as $entry) {
	$row = $table->new_row();

	$user = $entry["user_name"];
	if ($entry["user"] !== null) {
		$user = new HTML\Link($entry["join_user_name"], ADAMANTINE_ROOT_PATH . "areas/admin/log_by_user.php?xsi={$xsi["id"]}&user={$entry["user"]}", null, "command icon_go");
	} else $user = $entry["user_name"];

	$row->set_cell("Timestamp", Data\Data::pretty_datetime($entry["timestamp"]));
	if ($_IS_FULL) $row->set_cell("IP", $entry["ipaddr"]);
	$row->set_cell("User", $user);
	$row->set_cell("Area", $entry["join_area_name"]);
	$row->set_cell("Action", new HTML\Link($entry["action"], ADAMANTINE_ROOT_PATH . "areas/admin/log_detail.php?id={$entry["id"]}&xsi={$xsi["id"]}", null, "command icon_go"));
}

$_HTML->complete();
