<?php
/**
 * Adamantine Admin area: view system log by user.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;

$_UI->set_titles("System log for user", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "editor")) Adamantine\error("You do not have permission to access this area");

$_IS_FULL = $_SESSIONMANAGER->has_access_by_name("admin", "full");

$days = $_SETTINGS->get_by_names("admin", "LOG_DAYS");

$page = $_UI->get_content();

if (isset($_GET["user"])) {
	if (!Data\Data::validate_id($_GET["user"])) Adamantine\error("Invalid user ID supplied");
	if (null === ($user = $_USER->get($xsi, $_GET["user"]))) Adamantine\error("No such user exists");
} elseif (isset($_GET["username"])) {
	if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $_GET["username"])) Adamantine\error("Bad username search term supplied");
	if (null === ($user = $_USER->get_by_name($xsi, $_GET["username"]))) Adamantine\error("No such user exists");
} else {
	$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "areas/admin/log_by_user.php", "get", "search", "uniform simple autosize"));
	$form->add_hidden("xsi", $xsi["id"]);
	$form->add_row("User name", new HTML\Form_Text("", "username", "validate idname"), "username");
	$form->add_submit("Search");

	$_HTML->complete();
}

$page->add(new HTML\Header("Viewing log entries for user: {$user["name"]}", HTML\Header::LEVEL_3));

$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/autoscroll.css");
$_HTML->add_include("js", ADAMANTINE_ROOT_PATH . "js/autoscroll.js");

$page->add($table = new HTML\Table("log", "unitable autoscroll"));
$table->add_column("Timestamp");
if ($_IS_FULL) $table->add_column("IP");
$table->add_column("Area");
$table->add_column("Action");

$log = $_LOG->get_log_by_instance_and_user($xsi, $user);

foreach ($log as $entry) {
	$row = $table->new_row();

	$row->set_cell("Timestamp", Data\Data::pretty_datetime($entry["timestamp"]));
	if ($_IS_FULL) $row->set_cell("IP", $entry["ipaddr"]);
	$row->set_cell("Area", $entry["join_area_name"]);
	$row->set_cell("Action", new HTML\Link($entry["action"], ADAMANTINE_ROOT_PATH . "areas/admin/log_detail.php?id={$entry["id"]}&xsi={$xsi["id"]}", null, "command icon_go"));
}

$_HTML->complete();
