<?php
/**
 * Adamantine Admin area: edit direct user ACLs.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Framework as Framework;
use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;

$_UI->set_titles("Set user access permissions", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "editor")) Adamantine\error("You do not have permission to access this area");
if (!Data\Data::validate_id($_GET["user"])) Adamantine\error("Invalid user ID supplied");
if (null === ($user = $_USER->get($xsi, $_GET["user"]))) Adamantine\error("No such user exists");

if ($user["type"] === Framework\Models\User::ROOT) Adamantine\error("The root user is not designed for assigning access permissions");

$_HTML->add_code("jquery", <<<JQUERY

$("input#cancel").click(function() {
	window.location = ADAMANTINE_ROOT_PATH + "management/view.php?area=_core&model=User&id={$user["id"]}&xsi={$xsi["id"]}";
});

JQUERY
);

$page = $_UI->get_content();

$page->add(new HTML\Header("User: {$user["name"]}", HTML\Header::LEVEL_3));

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "areas/admin/users_direct_do.php", "POST", "access", "uniform autosize"));
$form->add_hidden("xsi", $xsi["id"]);
$form->add_hidden("user", $user["id"]);

$areas = $_SYSTEM->list_installed_areas();
foreach ($areas as $area) {
	$form->add_row($area["name"], $select = new HTML\Form_Select("area_{$area["id"]}"), "area_{$area["id"]}");
	
	$installed = $_SYSTEM->get_installed_by_area($area);
	$existing = $_USER_INSTALLED->get_access($user, $installed);

	$select->add_option("", "-", $existing === null);
	$select->add_option(Framework\Access::NONE, "none", $existing === Framework\Access::NONE);
	$select->add_option(Framework\Access::READ, "read", $existing === Framework\Access::READ);
	$select->add_option(Framework\Access::USER, "user", $existing === Framework\Access::USER);
	$select->add_option(Framework\Access::EDITOR, "editor", $existing === Framework\Access::EDITOR);
	$select->add_option(Framework\Access::FULL, "full", $existing === Framework\Access::FULL);
}

$form->add_submit("Save changes");
$form->add_button("Cancel", HTML\Form::BUTTON_OTHER, "cancel");

$_HTML->complete();
