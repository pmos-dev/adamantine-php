<?php
/**
 * Adamantine Admin area: save installed instance area configuration changes.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \Adamantine\Models as Models;

$_UI->set_titles("System configuration", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "full")) Adamantine\error("You do not have permission to access this area");

$areas = $_SYSTEM->list_installed_areas();

foreach ($areas as $area) {
	$variables = $_VARIABLE->list_by_area($area);
	if (sizeof($variables) == 0) continue;

	$installed = $_SYSTEM->get_installed_by_area($area);
	
	foreach ($variables as $variable) {
		switch ($variable["type"]) {
			case Models\Variable::INTEGER:
				if (!Data\Data::validate_int($_POST["variable_{$area["id"]}_{$variable["id"]}"])) Adamantine\error("Bad value sent", $_POST["variable_{$area["id"]}_{$variable["id"]}"]);
				break;
			case Models\Variable::UNSIGNED:
				if (!Data\Data::validate_int($_POST["variable_{$area["id"]}_{$variable["id"]}"], 0)) Adamantine\error("Bad value sent", $_POST["variable_{$area["id"]}_{$variable["id"]}"]);
				break;
			case Models\Variable::STRING:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_TEXT_255, $_POST["variable_{$area["id"]}_{$variable["id"]}"])) Adamantine\error("Bad value sent", $_POST["variable_{$area["id"]}_{$variable["id"]}"]);
				break;
			case Models\Variable::BOOLEAN:
				$_POST["variable_{$area["id"]}_{$variable["id"]}"] = Data\Data::checkbox_boolean($_POST["variable_{$area["id"]}_{$variable["id"]}"]);
				break;
			case Models\Variable::SELECT:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_TEXT_255, $_POST["variable_{$area["id"]}_{$variable["id"]}"])) Adamantine\error("Bad value sent", $_POST["variable_{$area["id"]}_{$variable["id"]}"]);
				if (!in_array($_POST["variable_{$area["id"]}_{$variable["id"]}"], array_keys($variable["options"]))) Adamantine\error("Bad value sent", $_POST["variable_{$area["id"]}_{$variable["id"]}"]);
				break;
			default: Adamantine\error("Unknown variable type", $variable["type"]);
		}
		$_SETTINGS->set($installed, $variable, $_POST["variable_{$area["id"]}_{$variable["id"]}"]);
	}
}

$_SESSIONMANAGER->log_by_name("admin", "Change system configuration");

$_UI->success("System configuration", "Changes saved successfully");
