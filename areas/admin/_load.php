<?php
/**
 * Adamantine Admin area: load the admin area.
 * Instantiates the API and would load any potential models if they existed.
 * Subareas should make sure they have a corresponding file that calls:
 * require_once ADAMANTINE_ROOT_PATH . "areas/admin/_load.php";
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) die();	// check the script isn't being called directly

require_once APP_ROOT_PATH . "areas/admin/_api.php";
$_API_ADMIN = new API($_DATABASE, $_SYSTEM, $_ACCESS, $_AREA, $_VARIABLE, $_REPOSITORY);
