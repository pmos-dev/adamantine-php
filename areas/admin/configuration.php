<?php
/**
 * Adamantine Admin area: edit installed instance area configurations.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Renderer\HTMLJQuery\UI as HTMLJQueryUI;
use \Adamantine as Adamantine;
use \Adamantine\Models as Models;

$_UI->set_titles("System configuration", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "full")) Adamantine\error("You do not have permission to access this area");

$page = $_UI->get_content();

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "areas/admin/configuration_do.php", "POST", "configuration", "uniform autosize"));
$form->add_hidden("xsi", $xsi["id"]);

$form->add($tabs = new HTMLJQueryUI\Tabs($_HTML, "configuration"));

foreach ($_SYSTEM->list_installed_areas() as $area) {
	$variables = $_VARIABLE->list_by_area($area);
	if (sizeof($variables) == 0) continue;

	$installed = $_SYSTEM->get_installed_by_area($area);

	$tabs->add_tab("{$area["name"]} settings", $tab = new HTML\Layout());
	foreach ($variables as $variable) {
		$value = $_SETTINGS->get($installed, $variable);

		switch ($variable["type"]) {
			case Models\Variable::INTEGER:
				$field = new HTML\Form_Text($value, "variable_{$area["id"]}_{$variable["id"]}", "numeric validate int");
				break;
			case Models\Variable::UNSIGNED:
				$field = new HTML\Form_Text($value, "variable_{$area["id"]}_{$variable["id"]}", "numeric validate int unsigned");
				break;
			case Models\Variable::STRING:
				$field = new HTML\Form_Text($value, "variable_{$area["id"]}_{$variable["id"]}", "validate string");
				break;
			case Models\Variable::BOOLEAN:
				$field = new HTML\Form_Checkbox($value == 1, "variable_{$area["id"]}_{$variable["id"]}");
				break;
			case Models\Variable::SELECT:
				$field = new HTML\Form_Select("variable_{$area["id"]}_{$variable["id"]}");
				foreach ($variable["options"] as $key => $v) $field->add_option($key, $v, $value === $key);
				break;
			default: $_UI->error("System error: Unknown variable type: {$variable["type"]}");
		}

		$row = $tab->add_labelled_row($variable["description"], $field, "variable_{$area["id"]}_{$variable["id"]}");
		$row->get_classes()->add("form_row");
	}
}

$form->add_submit("Save changes");

$_HTML->complete();
