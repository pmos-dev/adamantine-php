<?php
/**
 * Adamantine Admin area: view system log entry details.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;

$_UI->set_titles("System log detail", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "editor")) Adamantine\error("You do not have permission to access this area");

$_IS_FULL = $_SESSIONMANAGER->has_access_by_name("admin", "full");

if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_BASE36_ID, $_GET["id"])) Adamantine\error("Invalid log ID supplied");
if (null === ($entry = $_LOG->get_log_by_instance_and_id($xsi, $_GET["id"]))) Adamantine\error("No such log entry exists");

$page = $_UI->get_content();

$page->add(new HTML\Header("Viewing log entry", HTML\Header::LEVEL_3));

$page->add(new HTML\Div($layout = new HTML\Layout(), "details", "unilayout"));

if ($entry["user"] !== null) $layout->add_labelled_row("User", $entry["join_user_name"]);
else $layout->add_labelled_row("User", "{$entry["user_name"]} (deleted user)");

$layout->add_labelled_row("Timestamp", Data\Data::pretty_datetime($entry["timestamp"]));
if ($_IS_FULL) $layout->add_labelled_row("IP address", $entry["ipaddr"]);

$layout->add_labelled_row("Area", $entry["join_area_name"] === null ? "(system)" : $entry["join_area_name"]);
$layout->add_labelled_row("Action", $entry["action"]);

if ($entry["data"] === null || $entry["data"] === "") $layout->add_labelled_row("Details", "(no further data)");
else $layout->add_labelled_row("Data", $entry["data"]);

$_HTML->complete();
