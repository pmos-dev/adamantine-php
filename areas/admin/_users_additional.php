<?php
/**
 * Adamantine Admin area: additional user view functionality for the managed model.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("ADAMANTINE_ROOT_PATH")) die();	// check the script isn't being called directly

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Framework as Framework;

$page->add(new HTML\Header("Group membership", HTML\Header::LEVEL_3));

$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/autoscroll.css");
$_HTML->add_include("js", ADAMANTINE_ROOT_PATH . "js/autoscroll.js");

$page->add($table = new HTML\Table("groups", "unitable autoscroll"));
$table->add_column("Name");
$table->add_column("Member?");
$table->add_column("Actions");

$membership = array();
foreach ($_USER_GROUP->list_bs_by_a($data) as $group) $membership[] = $group["id"];

$groups = $_GROUP->list_by_firstclass($xsi);
foreach ($groups as $group) {
	$row = $table->new_row();
	$row->set_cell("Name", new HTML\Link($group["name"], ADAMANTINE_ROOT_PATH . "management/view.php?area=_core&model=Group&id={$group["id"]}&xsi={$xsi["id"]}", null, "command icon_go"));
	$row->set_cell("Member?", in_array($group["id"], $membership) ? new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/tick.svg") : new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/cross.svg"));
	$row->set_cell("Actions", "");
	
	if ($group["type"] === Framework\Models\Group::NORMAL) $row->set_cell("Actions", in_array($group["id"], $membership)
			? new HTML\Link("Leave this group", ADAMANTINE_ROOT_PATH . "areas/admin/users_groups_do.php?action=leave&user={$data["id"]}&group={$group["id"]}&xsi={$xsi["id"]}&return=User", null, "command icon_leave")
			: new HTML\Link("Join this group", ADAMANTINE_ROOT_PATH . "areas/admin/users_groups_do.php?action=join&user={$data["id"]}&group={$group["id"]}&xsi={$xsi["id"]}&return=User", null, "command icon_join"));
}

$page->add(new HTML\Header("Area access", HTML\Header::LEVEL_3));

if ($data["type"] === Framework\Models\User::ROOT) $page->add(new HTML\Header("Root user has implicit full access to all areas", HTML\Header::LEVEL_4, null, "info"));
else {
	$page->add($table = new HTML\Table("access", "unitable"));
	$table->add_column("Area");
	$table->add_column("Direct access");
	$table->add_column("Overall access");

	// temporarily need to reload the access matrix
	$_ACCESS->load_user_access_matrix($data);

	$areas = $_SYSTEM->list_installed_areas();
	foreach ($areas as $area) {
		$row = $table->new_row();
		$row->set_cell("Area", $area["name"]);

		switch ($_ACCESS->get_access($data, $_SYSTEM->get_installed_by_area($area), true)) {
			case null:
				$row->set_cell("Direct access", "-");
				break;
			case Framework\Access::NONE:
				$row->set_cell("Direct access", "none");
				break;
			case Framework\Access::READ:
				$row->set_cell("Direct access", "read");
				break;
			case Framework\Access::USER:
				$row->set_cell("Direct access", "user");
				break;
			case Framework\Access::EDITOR:
				$row->set_cell("Direct access", "editor");
				break;
			case Framework\Access::FULL:
				$row->set_cell("Direct access", "full");
				break;
		}
			
		switch ($_ACCESS->get_access($data, $_SYSTEM->get_installed_by_area($area), false)) {
			case null:
				$row->set_cell("Overall access", "(implicit none)");
				break;
			case Framework\Access::NONE:
				$row->set_cell("Overall access", "none");
				break;
			case Framework\Access::READ:
				$row->set_cell("Overall access", "read");
				break;
			case Framework\Access::USER:
				$row->set_cell("Overall access", "user");
				break;
			case Framework\Access::EDITOR:
				$row->set_cell("Overall access", "editor");
				break;
			case Framework\Access::FULL:
				$row->set_cell("Overall access", "full");
				break;
		}
	}

	// restore the user access matrix
	$_ACCESS->load_user_access_matrix($xau);

	$page->add(HTML\LinkBar::singular(new HTML\Link("Edit direct access permissions", ADAMANTINE_ROOT_PATH . "areas/admin/users_direct.php?user={$data["id"]}&xsi={$xsi["id"]}", null, "command icon_access")));
}
