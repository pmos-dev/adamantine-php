<?php
/**
 * Adamantine Admin area: perform import of data into instance.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Database as Database;
use \Adamantine as Adamantine;

$_UI->set_titles("Bulk import data", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "full")) Adamantine\error("You do not have permission to access this area");

$_IMPORTEXPORT = array(); 
$area_lookup = array();
foreach ($_SYSTEM->list_installed_areas() as $area) {
	if (!file_exists(APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php")) continue;
	require_once APP_ROOT_PATH . "areas/{$area["name"]}/_importexport.php";
	
	$area_lookup[$area["name"]] = $area;
}

if (isset($_FILES["file"]) && ($_FILES["file"]["name"] != "") && ($_FILES["file"]["size"] > 0)) {
	$_POST["code"] = file_get_contents($_FILES["file"]["tmp_name"]);
}

$lines = explode("\n", $_POST["code"]);

// start a transaction so if any failures occur the entire import is aborted
$_DATABASE->transaction_begin();

foreach ($lines as $line) {
	if ("" === ($line = trim($line))) continue;
	if (substr($line, 0, 1) === "#") continue;

	// reset timeout for each legitimate line
	set_time_limit(10);
	
	if (!preg_match("`^([-a-z0-9_.]{1,63})\s`iD", $line, $array)) $_UI->error("Syntax error: {$line}");
	list(, $area) = $array;
	$area = strtolower($area);

	if (!array_key_exists($area, $area_lookup)) $_UI->error("No such area is installed", $area);
	
	$line .= " ";	// this helps with simplifying regular expression matching
	try {
		if (!$_IMPORTEXPORT[$area_lookup[$area]["name"]]->import($line, false)) throw new Database\Exception("Inline exception");
	} catch(Database\Exception $exception) {
		$_UI->error("There was an error processing this line: {$line}");
	}
}

$_DATABASE->transaction_commit();

$_SESSIONMANAGER->log_by_name("admin", "Import data");

$_UI->success("Data successfully imported.");
