<?php
/**
 * Adamantine Admin area: import and export definitions for admin areas.
 * Subareas should make sure they have a corresponding file that calls:
 * require_once ADAMANTINE_ROOT_PATH . "areas/admin/_importexport.php";
 * and then calls:
 * unset($_IMPORTEXPORT["admin"]);
 * before defining a new subclass and instantiating it back into the $_IMPORTEXPORT array.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH is not set");
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "framework/access.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/html.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH is not set");
require_once ADAMANTINE_ROOT_PATH . "areas/area_importexport.php";
require_once ADAMANTINE_ROOT_PATH . "framework/system.php";
require_once ADAMANTINE_ROOT_PATH . "framework/session.php";
require_once ADAMANTINE_ROOT_PATH . "framework/settings.php";
require_once ADAMANTINE_ROOT_PATH . "models/user.php";
require_once ADAMANTINE_ROOT_PATH . "models/repository.php";

use \Abstraction\Framework as Framework;
use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \Adamantine\Models as Models;
use \Adamantine\Areas as Areas;

/**
 * Provides basic import and export for admin areas.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
class ImportExport extends Adamantine\Areas\ImportExport {

	//-------------------------------------------------------------------------

	/**
	 * Returns the area name this class represents: admin
	 * 
	 * @internal
	 * @return string
	 */
	public function get_area_name() {
		return "admin";
	}

	/**
	 * Returns an associative array of data components that can be exported for this area, in the format name => description
	 * 
	 * @internal
	 * @return string[]
	 */
	public function list_exports() {
		return array(
			"users" => "Users",
			"groups" => "Groups",
			"members" => "User and group membership",
			"acls" => "User and group access permissions"
		);
	}
	
	/**
	 * Adds any necessary jQuery code to the HTML model to facilitate dependencies.
	 * 
	 * @internal
	 * @see Adamantine\Areas\ImportExport::add_export_jquery_code()
	 * @param HTML\HTML $html the HTML model to manipulate
	 * @return void
	 */
	public function add_export_jquery_code(HTML\HTML &$html) {
		$html->add_code("jquery", self::build_export_prerequisites(array(
			"admin_members" => array("admin_users", "admin_groups"),
			"admin_acls" => array("admin_users", "admin_groups")
		)));
	}
	
	//---------------------------------------------------------------------
	
	/**
	 * Exports the corresponding area data for the specified keyword.
	 * 
	 * @internal
	 * @see Adamantine\Areas\ImportExport::export()
	 * @param string $keyword the keyword of the area data to be exported
	 * @return void
	 */
	public function export($keyword) {
		parent::export($keyword);
		
		$instance = $this->session->get_session_instance();
		
		switch($keyword)  {
			case "users":
				$this->export_users($instance);
				break;
			case "groups":
				$this->export_groups($instance);
				break;
			case "members":
				$this->export_members($instance);
				break;
			case "acls":
				$this->export_acls($instance);
				break;
			default:
		}
	}
	
	/**
	 * Exports all normal users for the specified instance.
	 * 
	 * @param mixed[] $instance the instance to export
	 * @return void
	 */
	protected function export_users(array $instance) {
		foreach ($this->repository->get("_core", "User")->list_normal_by_instance($instance) as $user) {
			self::write_line("ADMIN CREATE USER", array(
				"NAME" => array(self::SIMPLE, $user["name"]),
				"FULLNAME" => array(self::RAWURL, $user["fullname"]),
				"VISIBILITY" => array(self::SIMPLE, $user["visibility"]),
				"STATUS" => array(self::SIMPLE, $user["status"])
			));
		}
	}

	/**
	 * Exports all normal groups for the specified instance.
	 * 
	 * @param mixed[] $instance the instance to export
	 * @return void
	 */
	protected function export_groups(array $instance) {
		foreach ($this->repository->get("_core", "Group")->list_normal_by_instance($instance) as $group) {
			self::write_line("ADMIN CREATE GROUP", array(
				"NAME" => array(self::SIMPLE, $group["name"])
			));
		}
	}

	/**
	 * Exports user/group membership for the specified instance.
	 * 
	 * @param mixed[] $instance the instance to export
	 * @return void
	 */
	protected function export_members(array $instance) {
		foreach ($this->repository->get("_core", "Group")->list_normal_by_instance($instance) as $group) {
			foreach ($this->repository->get("_core", "User_Group")->list_as_by_b($group) as $user) {
				self::write_line("ADMIN ADD MEMBER", array(
					"GROUP" => array(self::SIMPLE, $group["name"]),
					"USER" => array(self::SIMPLE, $user["name"])
				));
			}
		}
	}

	/**
	 * Exports all user ACLs for the specified instance.
	 * 
	 * @param mixed[] $instance the instance to export
	 * @throws Areas\ImportExportException
	 * @return void
	 */
	protected function export_acls(array $instance) {
		$groups = $this->repository->get("_core", "Group")->list_normal_by_instance($instance);
		array_unshift($groups, $this->repository->get("_core", "Group")->get_everyone($instance));
		foreach ($groups as $group) {
			foreach ($this->system->list_installed_areas() as $area) {
				$installed = $this->system->get_installed_by_area($area);
				
				// continue doesn't work properly in a switch, so have to do it externally
				if (null === ($a = $this->repository->get("_core", "Group_Installed")->get_access($group, $installed))) continue;
				switch	($a) {
					case Framework\Access::NONE:
						$access = "none";
						break;
					case Framework\Access::READ:
						$access = "read";
						break;
					case Framework\Access::USER:
						$access = "user";
						break;
					case Framework\Access::EDITOR:
						$access = "editor";
						break;
					case Framework\Access::FULL:
						$access = "full";
						break;
					default:
						throw new Areas\ImportExportException("Unknown access level");
				}
				
				if ($access === null) continue;
				self::write_line("ADMIN SET GROUP ACL", array(
					"GROUP" => array(self::SIMPLE, $group["name"]),
					"AREA" => array(self::SIMPLE, $area["name"]),
					"ACCESS" => array(self::SIMPLE, $access)
				));
			}
		}

		$users = $this->repository->get("_core", "User")->list_normal_by_instance($instance);
		array_unshift($users, $this->repository->get("_core", "User")->get_guest($instance));
		foreach ($users as $user) {
			foreach ($this->system->list_installed_areas() as $area) {
				$installed = $this->system->get_installed_by_area($area);
				
				// continue doesn't work properly in a switch, so have to do it externally
				if (null === ($a = $this->repository->get("_core", "User_Installed")->get_access($user, $installed))) continue;
				switch	($a) {
					case Framework\Access::NONE:
						$access = "none";
						break;
					case Framework\Access::READ:
						$access = "read";
						break;
					case Framework\Access::USER:
						$access = "user";
						break;
					case Framework\Access::EDITOR:
						$access = "editor";
						break;
					case Framework\Access::FULL:
						$access = "full";
						break;
					default:
						throw new Areas\ImportExportException("Unknown access level");
				}
				
				if ($access === null) continue;
				self::write_line("ADMIN SET USER ACL", array(
					"USER" => array(self::SIMPLE, $user["name"]),
					"AREA" => array(self::SIMPLE, $area["name"]),
					"ACCESS" => array(self::SIMPLE, $access)
				));
			}
		}
	}
	
	//---------------------------------------------------------------------

	/**
	 * Evaluates the specified line for anything that can be imported by this area.
	 * 
	 * @see Adamantine\Areas\ImportExport::import()
	 * @param string $line the string to consider
	 * @return boolean true if an import was performed, false if nothing importable was found
	 */
	public function import($line) {
		if (parent::import($line)) return true;
		
		$instance = $this->session->get_session_instance();
				
		if (preg_match("`^ADMIN CREATE USER`iD", $line)) return $this->import_user($instance, $line);
		if (preg_match("`^ADMIN CREATE GROUP`iD", $line)) return $this->import_group($instance, $line);
		if (preg_match("`^ADMIN ADD MEMBER`iD", $line)) return $this->import_join($instance, $line);
		if (preg_match("`^ADMIN SET USER ACL`iD", $line)) return $this->import_user_acl($instance, $line);
		if (preg_match("`^ADMIN SET GROUP ACL`iD", $line)) return $this->import_group_acl($instance, $line);
		
		return false;
	}

	/**
	 * Attempts to import a user based upon the data in the specified import line.
	 * 
	 * @param mixed[] $instance the instance to import into
	 * @param string $line the string to consider
	 * @return boolean
	 */
	protected function import_user(array $instance, $line) {
		if (null === ($name = self::extract_field("NAME", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name)) return false;

		if (null !== $this->repository->get("_core", "User")->get_by_name($instance, $name)) Adamantine\error("Sorry, a user with that name already exists");
		
		if (null === ($fullname = self::extract_field("FULLNAME", $line, true))) return false;
		if (!Data\Data::validate_string($fullname)) return false;
		
		if (null === ($status = self::extract_field("STATUS", $line))) return false;
		switch ($status) {
			case Models\User::ENABLED:
			case Models\User::DISABLED:
				break;
			default:
				return false;
		}
		
		if (null === ($visibility = self::extract_field("VISIBILITY", $line))) return false;
		switch ($visibility) {
			case Models\User::VISIBLE:
			case Models\User::HIDDEN:
				break;
			default:
				return false;
		}

		$user = $this->repository->get("_core", "User")->create($instance, "normal", $visibility, $status, array(
			"name" => $name,
			"fullname" => $fullname
		));
		
		$everyone = $this->repository->get("_core", "Group")->get_everyone($instance);
		
		$this->repository->get("_core", "User_Group")->link($user, $everyone);
				
		return true;
	}

	/**
	 * Attempts to import a group based upon the data in the specified import line.
	 * 
	 * @param mixed[] $instance the instance to import into
	 * @param string $line the string to consider
	 * @return boolean
	 */
	protected function import_group(array $instance, $line) {
		if (null === ($name = self::extract_field("NAME", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name)) return false;

		if (null !== $this->repository->get("_core", "Group")->get_by_name($instance, $name)) Adamantine\error("Sorry, a group with that name already exists");
		
		$this->repository->get("_core", "Group")->create($instance, "normal", array(
			"name" => $name
		));
		
		return true;
	}
	
	/**
	 * Attempts to import a user/group membership based upon the data in the specified import line.
	 * 
	 * @param mixed[] $instance the instance to import into
	 * @param string $line the string to consider
	 * @return boolean
	 */
	protected function import_join(array $instance, $line) {
		if (null === ($user = self::extract_field("USER", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $user)) return false;
		if (null === ($user = $this->repository->get("_core", "User")->get_by_name($instance, $user))) Adamantine\error("Sorry, no such user exists");

		if (null === ($group = self::extract_field("GROUP", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $group)) return false;
		if (null === ($group = $this->repository->get("_core", "Group")->get_by_name($instance, $group))) Adamantine\error("Sorry, no such group exists");

		$this->repository->get("_core", "User_Group")->link($user, $group);
		
		return true;
	}
	
	/**
	 * Attempts to import a group ACL based upon the data in the specified import line.
	 * 
	 * @param mixed[] $instance the instance to import into
	 * @param string $line the string to consider
	 * @return boolean
	 */
	protected function import_group_acl(array $instance, $line) {
		if (null === ($group = self::extract_field("GROUP", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $group)) return false;
		if (null === ($group = $this->repository->get("_core", "Group")->get_by_name($instance, $group))) Adamantine\error("Sorry, no such group exists");

		if (null === ($area = self::extract_field("AREA", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $area)) return false;
		if (null === ($area = $this->system->get_area_by_name($area))) Adamantine\error("Sorry, no such area exists");

		if (null === ($installed = $this->system->get_installed_by_area($area))) Adamantine\error("Sorry, no such area is installed");
		
		if (null === ($access = self::extract_field("ACCESS", $line))) return false;
		switch (strtolower($access)) {
			case "read":
				$access = Framework\Access::READ;
				break;
			case "user":
				$access = Framework\Access::USER;
				break;
			case "editor":
				$access = Framework\Access::EDITOR;
				break;
			case "full":
				$access = Framework\Access::FULL;
				break;
			default:
				return false;
		}

		$this->repository->get("_core", "Group_Installed")->set_access($group, $installed, $access);
				
		return true;
	}
	
	/**
	 * Attempts to import a user ACL upon the data in the specified import line.
	 * 
	 * @param mixed[] $instance the instance to import into
	 * @param string $line the string to consider
	 * @return boolean
	 */
	protected function import_user_acl(array $instance, $line) {
		if (null === ($bak = $user = self::extract_field("USER", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $user)) return false;
		if (null === ($user = $this->repository->get("_core", "User")->get_by_name($instance, $user))) Adamantine\error("Sorry, no such user exists", $bak);
	
		if (null === ($area = self::extract_field("AREA", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $area)) return false;
		if (null === ($area = $this->system->get_area_by_name($area))) Adamantine\error("Sorry, no such area exists");
	
		if (null === ($installed = $this->system->get_installed_by_area($area))) Adamantine\error("Sorry, no such area is installed");
	
		if (null === ($access = self::extract_field("ACCESS", $line))) return false;
		switch (strtolower($access)) {
			case "none":
				$access = Framework\Access::NONE;
				break;
			case "read":
				$access = Framework\Access::READ;
				break;
			case "user":
				$access = Framework\Access::USER;
				break;
			case "editor":
				$access = Framework\Access::EDITOR;
				break;
			case "full":
				$access = Framework\Access::FULL;
				break;
			default:
				return false;
		}
	
		$this->repository->get("_core", "User_Installed")->set_access($user, $installed, $access);
	
		return true;
	}
		
	//-------------------------------------------------------------------------
}

$_IMPORTEXPORT["admin"] = new ImportExport($_DATABASE, $_SYSTEM, $_SESSIONMANAGER, $_SETTINGS, $_REPOSITORY);
