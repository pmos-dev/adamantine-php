<?php
/**
 * Adamantine Admin area: save direct user ACLs changes.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Framework as Framework;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;

$_UI->set_titles("Set user access permissions", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "editor")) Adamantine\error("You do not have permission to access this area");
if (!Data\Data::validate_id($_POST["user"])) Adamantine\error("Invalid user ID supplied");
if (null === ($user = $_USER->get($xsi, $_POST["user"]))) Adamantine\error("No such user exists");

$areas = $_SYSTEM->list_installed_areas();
foreach ($areas as $area) {
	if (!isset($_POST["area_{$area["id"]}"])) $_POST["area_{$area["id"]}"] = null;
	else if ($_POST["area_{$area["id"]}"] === "") $_POST["area_{$area["id"]}"] = null;
	
	$installed = $_SYSTEM->get_installed_by_area($area);
		
	if ($_POST["area_{$area["id"]}"] === null) $_USER_INSTALLED->revoke_access($user, $installed);
	else switch ($_POST["area_{$area["id"]}"]) {
		case Framework\Access::NONE:
		case Framework\Access::READ:
		case Framework\Access::USER:
		case Framework\Access::EDITOR:
		case Framework\Access::FULL:
			$_USER_INSTALLED->set_access($user, $installed, intval($_POST["area_{$area["id"]}"]));
			break;
		default:
			throw new Exception("Unknown access level supplied", $_POST["area_{$area["id"]}"]);
	}
}

$_SESSIONMANAGER->log_by_name("admin", "Set user access permissions");

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "management/view.php?area=_core&model=User&id={$user["id"]}&xsi={$xsi["id"]}");
