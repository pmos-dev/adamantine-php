<?php
/**
 * Adamantine Admin area: unbuild the admin area from an application system.
 * Subareas should make sure they have a corresponding file that calls:
 * require_once ADAMANTINE_ROOT_PATH . "areas/admin/_unbuild.php";
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) die();	// check the script isn't being called directly

$_DATABASE->transaction_begin();

$_API_ADMIN->unbuild_area();

$_DATABASE->transaction_commit();
