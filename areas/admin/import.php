<?php
/**
 * Adamantine Admin area: import data form.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas\Admin;

if (!defined("APP_ROOT_PATH")) define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

$_UI->set_titles("Bulk import data", $xsi["description"]);

if (!$_SESSIONMANAGER->has_access_by_name("admin", "full")) Adamantine\error("You do not have permission to access this area");

$page = $_UI->get_content();

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "areas/admin/import_do.php", "filepost", "import", "uniform autosize"));
$form->add_hidden("xsi", $xsi["id"]);
$form->add_hidden("MAX_FILE_SIZE", (1024 * 1024) * 50);

$form->add_row("Script code", new HTML\Form_TextArea("", "code", "huge"), "code");
$form->add_row("File code", new HTML\Form_File("", "file"), "file");

$form->add_submit("Import");

$_HTML->complete();
