<?php
/**
 * Root class for area APIs within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH is not set");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/models.php";
require_once ABSTRACTION_ROOT_PATH . "framework/system.php";
require_once ABSTRACTION_ROOT_PATH . "framework/access.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/multielements.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH is not set");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/area.php";
require_once ADAMANTINE_ROOT_PATH . "models/variable.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Framework as Framework;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

/**
 * @internal
 */
class Exception extends Adamantine\Exception {}

interface Welcomable {
	function append_to_page(HTML\MultiElements $page, array $xsi, array $installed, array $xau);
}

/**
 * Provides a basic skeleton framework for area APIs to extend.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class Area_API extends Models\ModelHelperMethods {
	protected $system, $access;
	protected $area, $variable;
	protected $repository;
	protected static $area_apis = array();
	
	/**
	 * Constructs a new instance of this API.
	 * 
	 * @param Database\Wrapper $database the database interface for the API
	 * @param Framework\System $system the current System framework object
	 * @param Framework\Access $access the current Access framework object
	 * @param Adamantine\Models\Area $area the current Area model
	 * @param Adamantine\Models\Variable $variable the current Variable model
	 */
	public function __construct(Database\Wrapper $database, Framework\System $system, Framework\Access $access, Adamantine\Models\Area $area, Adamantine\Models\Variable $variable, Adamantine\Models\Repository $repository) {
		parent::__construct($database);
		
		$this->system = $system;
		$this->access = $access;
		
		$this->area = $area;
		$this->variable = $variable;

		$this->repository = $repository;
		
		self::$area_apis[$this->get_area_name()] = $this;
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns the appropriate API for the specified area name.
	 * 
	 * @param string $name the name of the area to locate the API for
	 * @throws Exception
	 * @return object
	 */
	public static function get_api_by_area_name($name) {
		if (!array_key_exists($name, self::$area_apis)) throw new Exception("No such area by that name is installed", $name);
		return self::$area_apis[$name];
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the area name this API represents.
	 * 
	 * @internal
	 * @return string
	 */
	abstract public function get_area_name();

	/**
	 * Returns the Area model row for the area corresponding with this API.
	 * 
	 * @return mixed[]
	 */
	public function get_area_by_name() {
		return $this->area->get_by_name($this->get_area_name());
	}
	
	/**
	 * Builds the area into the system.
	 * 
	 * This will involve creating tables and views in the database.
	 * 
	 * @return void
	 */
	public function build_area() {
		$this->system->create_area(array("name" => $this->get_area_name()));
	}
	
	/**
	 * Unbuilds the area from the system.
	 * 
	 * This will involve dropping tables and views in the database.
	 * 
	 * @return void
	 */
	public function unbuild_area() {
		$this->system->delete_area($this->get_area_by_name());
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Registers a new Variable in the database for this area.
	 * 
	 * @see Adamantine\Models\Variable
	 * @param string $name the variable key name
	 * @param string $description a simple description of what the variable does
	 * @param string $type one of the values from the Variable model
	 * @param mixed|NULL $default any default value for the variable to use if an instance installed area setting does not exist
	 * @param mixed[]|NULL $options an optional array of values if the type is SELECT
	 * @throws Exception
	 * @return void
	 */
	public function create_variable($name, $description, $type, $default = null, $options = null) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name)) throw new Exception("Invalid variable name", $name);
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_TEXT_255, $description)) throw new Exception("Invalid variable description", $description);
		
		switch ($type) {
			case Adamantine\Models\Variable::INTEGER:
				if ($default !== null) if (!Data\Data::validate_int($default)) throw new Exception("Invalid default value", $default);
				if ($options !== null) throw new Exception("Do not set options for this type", $default);
				break;
			case Adamantine\Models\Variable::UNSIGNED:
				if ($default !== null) if (!Data\Data::validate_int($default, 0)) throw new Exception("Invalid default value", $default);
				if ($options !== null) throw new Exception("Do not set options for this type", $default);
				break;
			case Adamantine\Models\Variable::BOOLEAN:
				if ($default !== null) $default = $default ? true : false;
				if ($options !== null) throw new Exception("Do not set options for this type", $default);
				break;
			case Adamantine\Models\Variable::STRING:
				if ($default !== null) if (!Data\Data::validate_string($default)) throw new Exception("Invalid default value", $default);
				if ($options !== null) throw new Exception("Do not set options for this type", $default);
				break;
			case Adamantine\Models\Variable::SELECT:
				if ($default !== null) if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $default)) throw new Exception("Invalid default value", $default);
				if (!is_array($options)) throw new Exception("Invalid options");
				foreach ($options as $var => $val) {
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $var)) throw new Exception("Invalid option var", $name);
					if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $val)) throw new Exception("Invalid option val", $name);
				}
				if (!array_key_exists($default, $options)) throw new Exception("Default value is not one of the SELECT options", $default);
				break;
			default:
				throw new Exception("Unknown variable type", $type);
		}
		
		if (null === ($area = $this->area->get_by_name($this->get_area_name()))) throw new Exception("Unable to resolve area from name");
		
		return $this->variable->insert_for_firstclass($area, array(
			"name" => $name,
			"description" => $description,
			"type" => $type,
			"default" => json_encode($default),
			"options" => $options
		));
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the computed 'root' URL of the application installation by locating either the area name or Adamantine management URL component.
	 * 
	 * @return string|NULL the base URL of the application installation
	 */
	public function get_root_url() {
		$protocol = "http" . (isset($_SERVER["HTTPS"]) ? "s" : "");

		$area = $this->get_area_name();
		
		if (preg_match("`^(.*)/areas/{$area}/[^?#&]+\.php(\?|$)`D", $_SERVER["REQUEST_URI"], $array)) return "{$protocol}://{$_SERVER["HTTP_HOST"]}{$array[1]}/";
		if (preg_match("`^(.*)/areas/{$area}/process/(\?|$)`D", $_SERVER["REQUEST_URI"], $array)) return "{$protocol}://{$_SERVER["HTTP_HOST"]}{$array[1]}/";
		if (preg_match("`^(.*)/areas/{$area}/(live|ajax)/([-_a-z0-9]+/)*[^?#&]+/(\?|$)`D", $_SERVER["REQUEST_URI"], $array)) return "{$protocol}://{$_SERVER["HTTP_HOST"]}{$array[1]}/";
		if (preg_match("`^(.*)/areas/{$area}/(live|ajax)/([-_a-z0-9]+/)*[^?#&]+\.php(\?|$)`D", $_SERVER["REQUEST_URI"], $array)) return "{$protocol}://{$_SERVER["HTTP_HOST"]}{$array[1]}/";
		if (preg_match("`^(.*)/adamantine/management/[^?]+\.php(\?|$)`D", $_SERVER["REQUEST_URI"], $array)) return "{$protocol}://{$_SERVER["HTTP_HOST"]}{$array[1]}/";
		
		return null;
	}
	
	/**
	 * Returns the computed 'base' URL of the area within the application installation by locating either the area name or Adamantine management URL component.
	 * 
	 * @return string|NULL the base URL of the application installation
	 */
	public function get_base_area_url() {
		if (null === ($root = $this->get_root_url())) return null;
		
		$area = $this->get_area_name();
		
		return "{$root}areas/{$area}/";
	}
	
	/**
	 * Returns the computed URL for any live AJAX API interface that exists for the area.
	 * 
	 * Note, this method assumes that the location will be <area>/live/ajax/<method>, and does not actually check if such a location exists.
	 * 
	 * @param string $method the API method name, which is generally the PHP filename without the .php suffix
	 * @param array $params any parameters to supply to the API
	 * @param string|NULL $key an optional access key for the API
	 * @return string the computed URL string for this method and parameters
	 */
	public function get_live_ajax_url($method, array $params = array(), $key = null) {
		if ($key !== null) $params["key"] = $key;
		
		return $this->get_base_area_url() . "live/ajax/{$method}.php?" . http_build_query($params);
	}
	
	/**
	 * Returns the computed URL for any REST API interface that exists for the area.
	 *
	 * Note, this method assumes that the location will be <area>/rest/<path>, and does not actually check if such a location exists.
	 *
	 * @param string $path the API controller path
	 * @param array $params any parameters to supply to the API
	 * @param string|NULL $key an optional access key for the API
	 * @return string the computed URL string for this method and parameters
	 */
	public function get_rest_url($method, array $params = array(), $key = null) {
		if ($key !== null) $params["key"] = $key;
		
		$query = http_build_query($params);
		if ($query !== "") $query = "?{$query}";
		
		return $this->get_base_area_url() . "rest/{$method}" . $query;
	}
}
