<?php
/**
 * Root class for area import and export code within the Adamantine framework.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Areas;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH is not set");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "database/database.php";
require_once ABSTRACTION_ROOT_PATH . "models/models.php";
require_once ABSTRACTION_ROOT_PATH . "models/secondclass.php";
require_once ABSTRACTION_ROOT_PATH . "renderer/html/html.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH is not set");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/repository.php";
require_once ADAMANTINE_ROOT_PATH . "models/managedsecondclass.php";
require_once ADAMANTINE_ROOT_PATH . "models/managedorientatedordered.php";
require_once ADAMANTINE_ROOT_PATH . "models/variable.php";
require_once ADAMANTINE_ROOT_PATH . "framework/system.php";
require_once ADAMANTINE_ROOT_PATH . "framework/session.php";

use \Abstraction as Abstraction;
use \Abstraction\Database as Database;
use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \Adamantine\Framework as Framework;
use \Adamantine\Models as Models;

/**
 * @internal
 */
class ImportExportException extends Adamantine\Exception {}

/**
 * @internal
 */
class ValidateFieldException extends ImportExportException {}

/**
 * Provides a basic skeleton framework for area import/export classes to extend.
 * 
 * @author Pete Morris
 * @version 1.2.0
 */
abstract class ImportExport extends Abstraction\Models\ModelHelperMethods {
	protected $system, $session;
	protected $settings, $repository;
	
	const SIMPLE = "simple";
	const RAWURL = "rawurl";
	const HEX = "hex";
	const BOOL = "bool";
	const INT = "int";
	const FLOAT = "float"; 
	const DATE = "date";
	const TIME = "time";
	const DATETIME = "datetime";
	
	const BRANCH_IN = "in";
	const BRANCH_OUT = "out";
	const BRANCH_ROOT = "root";
	
	/**
	 * Constructs a new instance of this class.
	 * 
	 * @param Database\Wrapper $database the database interface for the API
	 * @param Framework\System $system the current System framework object
	 * @param Framework\Session $session the current Session framework object
	 * @param Framework\Settings $settings the current Settings framework object
	 * @param Models\Repository $repository the current model repository
	 */
	public function __construct(Database\Wrapper $database, Framework\System $system, Framework\Session $session, Framework\Settings $settings, Models\Repository $repository) {
		parent::__construct($database);
		
		$this->system = $system;
		$this->session = $session;
		$this->settings = $settings;
		$this->repository = $repository;
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns the area name this class represents.
	 * 
	 * @internal
	 * @return string
	 */
	abstract public function get_area_name();
	
	/**
	 * Returns an associative array of data components that can be exported for this area, in the format name => description
	 * 
	 * @return string[]
	 */
	abstract public function list_exports();
	
	/**
	 * Adds any necessary jQuery code to the HTML model to facilitate dependencies.
	 * 
	 * Some area data cannot be meaningfully exported and imported without other data also being considered too.
	 * This routine should call the add_code() method on the supplied HTML model, using this class's static build_export_prerequisites() method.
	 * 
	 * @see HTML\HTML
	 * @see self::build_export_prerequisites()
	 * @param HTML\HTML $html the HTML model to manipulate
	 * @return void
	 */
	abstract public function add_export_jquery_code(HTML\HTML &$html);
	
	//-------------------------------------------------------------------------

	/**
	 * Returns jQuery code suitable for adding to a HTML model in order to facilitate prerequisite dependencies.
	 * 
	 * @param array $prerequisites an associative array of dependencies in the format exportname => array(dependency exportnames...)
	 * @return string
	 */
	protected static function build_export_prerequisites(array $prerequisites) {
		$code = "";
		
		foreach ($prerequisites as $src => $dest) {
			if (is_string($dest)) $dest = array($dest);

			$dest_code = array();
			foreach ($dest as $d) $dest_code[] = <<<JQUERY
	$("#export_{$d}").prop("checked", true);
JQUERY;
			$dest_code = implode("\r\n", $dest_code);

			$code .= <<<JQUERY

$("#export_{$src}").change(function() {
	if (!$(this).prop("checked")) return;
{$dest_code}
});

JQUERY;

		}
		return $code;
	}

	//-------------------------------------------------------------------------

	/**
	 * Helper method to check if a string contains a keyword.
	 * 
	 * @param string $attrib the keyword to search for
	 * @param string $line the string to search within
	 * @return boolean
	 */
	public static function has_attribute($attrib, $line) {
		$line = preg_replace("`\"[^\"]*\"`D", "\"\"", $line);	// strip out values to avoid false matches
		return preg_match("`\s{$attrib}($|\s)`iD", $line) ? true : false;
	}

	/**
	 * Helper method to extract a value from a [key]=[type]:"[value]" formatted string.
	 * 
	 * The value is automatically decoded from whatever 'type' format it is represented it, and older export syntax is supported transparently.
	 * 
	 * @param string $attrib the variable key to search for
	 * @param string $line the string to search within
	 * @throws ImportExportException
	 * @return mixed|NULL
	 */
	public static function extract_field($attrib, $line) {
		if (!preg_match("`\s{$attrib}=([a-z]{1,16})?:\"([^\"]*)\"`iD", $line, $array)) {
			// ct3 support
			if (preg_match("`\s{$attrib}=\"([^\"]*)\"`iD", $line, $temp)) {
				$array = array(0 => "", 1 => self::RAWURL, 2 => $temp[1]);
			} elseif (preg_match("`\s{$attrib}=([0-9]+)($|\s)`iD", $line, $temp)) {
				$array = array(0 => "", 1 => self::SIMPLE, 2 => $temp[1]);
			} else return null;
		}
		list($temp, $type, $value) = $array;
		
		switch ($type) {
			case self::SIMPLE:
				return $value;
			case self::RAWURL:
				return rawurldecode($value);
			case self::HEX:
				return Data\Data::hexchunk_decode($value);
			case self::BOOL:
				switch (strtolower($value)) {
					case "true":
						return true;
					case "false":
						return false;
					default:
						throw new ImportExportException("Unknown boolean state", $value);
				}
			case self::INT:
				return intval($value);
			case self::FLOAT:
				return floatval($value);
			case self::DATE:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_YMD, $value)) throw new ImportExportException("Invalid date format", $value);
				return $value;
			case self::TIME:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_TIME_HIS, $value)) throw new ImportExportException("Invalid time format", $value);
				return $value;
			case self::DATETIME:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_YMDHIS, $value)) throw new ImportExportException("Invalid datetime format", $value);
				return $value;
			default:
				throw new ImportExportException("Unknown value type", $type);
		}
	}

	//-------------------------------------------------------------------------

	/**
	 * Constructs a formatted [key]=[type]:"[value]" string for export.
	 * 
	 * @param string $attrib the variable key to use
	 * @param string $type the type of value encoding to use (SIMPLE, RAWURL, HEX etc.)
	 * @param mixed $value the actual value being stored
	 * @throws ImportExportException
	 * @return string
	 */
	public static function build_field($attrib, $type, $value) {
		if ($value === null) return "";
	
		switch ($type) {
			case self::SIMPLE:
				return "{$attrib}=" . self::SIMPLE . ":\"{$value}\"";
			case self::RAWURL:
				return "{$attrib}=" . self::RAWURL . ":\"" . rawurlencode($value) . "\"";
			case self::HEX:
				return "{$attrib}=" . self::HEX . ":\"" . Data\Data::hexchunk_encode($value) . "\"";
			case self::BOOL:
				return "{$attrib}=" . self::BOOL . ":\"" . ($value ? "true" : "false") . "\"";
			case self::INT:
				return "{$attrib}=" . self::INT . ":\"{$value}\"";
			case self::FLOAT:
				return "{$attrib}=" . self::FLOAT . ":\"{$value}\"";
			case self::DATE:
				return "{$attrib}=" . self::DATE . ":\"{$value}\"";
			case self::TIME:
				return "{$attrib}=" . self::TIME . ":\"{$value}\"";
			case self::DATETIME:
				return "{$attrib}=" . self::DATETIME . ":\"{$value}\"";
			default:
				throw new ImportExportException("Unknown value type", $type);
		}
	}

	/**
	 * Writes a suitable line string to stdout for the export.
	 * 
	 * The line is terminated with a carriage return and line break.
	 * 
	 * @param string $preface the line preface to use, which will be detected by the corresponding import
	 * @param array $fields an associative array of fields in the format key => array(type, value)
	 * @param string[] $attributes any attribute keywords for the line
	 * @return void
	 */
	public static function write_line($preface, array $fields = array(), array $attributes = array()) {
		$data = array();
		foreach ($fields as $attrib => $a) {
			list($type, $value) = $a;
			if ($value !== null) $data[] = self::build_field($attrib, $type, $value);
		}
		foreach ($attributes as $attrib) $data[] = $attrib;
		
		echo $preface;
		
		$data = trim(implode(" ", $data));
		if ($data !== "") {
			echo " ";
			echo $data;
		}
		
		echo "\r\n";
		self::$_EXISTING_GAP = false;
	}
	
	/**
	 * Writes a comment to stdout for the export.
	 * 
	 * The line is terminated with a carriage return and line break.
	 * 
	 * @param string $message the comment to write.
	 * @return void
	 */
	public static function comment($message) {
		echo "# {$message}\r\n";
		self::$_EXISTING_GAP = false;
	}

	private static $_EXISTING_GAP = false;
	/**
	 * Writes a vertical whitespace gap to stdout for the export.
	 * 
	 * @return void
	 */
	public static function gap() {
		if (self::$_EXISTING_GAP) return;
		echo "\r\n";
		self::$_EXISTING_GAP = true;
	}
	
	/**
	 * Writes a line to stdout for the export.
	 * 
	 * The line is terminated with a carriage return and line break.
	 * 
	 * @param string $message the comment to write.
	 * @return void
	 */
	public static function line() {
		echo "# ---------------------------------\r\n";
	}
	
	/**
	 * Writes an error message comment to stdout for the export.
	 * 
	 * The line is terminated with a carriage return and line break.
	 * 
	 * @param string $message the error to write.
	 * @return void
	 */
	public static function error($message) {
		echo "#ERROR: {$message}\r\n";
		self::$_EXISTING_GAP = false;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Exports the corresponding area data for the specified keyword.
	 * 
	 * Subclasses should override this method, but still call parent::export(keyword) to make use of the standard config export code.
	 * 
	 * @param string $keyword the keyword of the area data to be exported
	 * @return void
	 */
	public function export($keyword) {
		switch($keyword)  {
			case "config":
				$this->export_config();
				break;
			default:
		}
	}

	/**
	 * Attempts to build an automatic export data type list for managed models.
	 * 
	 * @internal
	 * @param Abstraction\Models\Models $model the Model to review
	 * @param string[] $fields an array of Model fields that should be exported
	 * @throws ImportExportException
	 * @return string[] an associative array of export types in the format fieldname => type
	 */
	private function build_export_field_types(Abstraction\Models\Models $model, array $fields) {
		$structure = $model->get_structure();
		
		$types = array();
		foreach ($fields as $field) {
			if (
				$structure[$field] instanceof Database\Type_Int
				|| $structure[$field] instanceof Database\Type_TinyInt
				|| $structure[$field] instanceof Database\Type_SmallInt
				|| $structure[$field] instanceof Database\Type_TinyIntEnum
			) {
				$types[$field] = self::INT;
				continue;
			}

			if (
				$structure[$field] instanceof Database\Type_Float
			) {
				$types[$field] = self::FLOAT;
				continue;
			}
				
			if (
				$structure[$field] instanceof Database\Type_IdName
				|| $structure[$field] instanceof Database\Type_HexColor
				|| $structure[$field] instanceof Database\Type_IdChar
				|| $structure[$field] instanceof Database\Type_Enum
			) {
				$types[$field] = self::SIMPLE;
				continue;
			}

			if (
				$structure[$field] instanceof Database\Type_String
			) {
				$types[$field] = self::RAWURL;
				continue;
			}

			if (
				$structure[$field] instanceof Database\Type_DateTime
			) {
				$types[$field] = self::DATETIME;
				continue;
			}

			if (
				$structure[$field] instanceof Database\Type_Date
			) {
				$types[$field] = self::DATE;
				continue;
			}
			
			if (
				$structure[$field] instanceof Database\Type_Text
				|| $structure[$field] instanceof Database\Type_Encrypted
			) {
				$types[$field] = self::HEX;
				continue;
			}

			if ($structure[$field] instanceof Database\Type_Bool) {
				$types[$field] = self::BOOL;
				continue;
			}
			
			throw new ImportExportException("Automatic export of managed models is not set up for fields of this type yet", $structure[$field]);
		}
		
		return $types;
	}

	/**
	 * Calculates the chain steps needed for an automatic export of a managed model
	 * 
	 * @param \Abstraction\Models\FirstClass $model the FirstClass (or subclass, e.g. SecondClass) model being exported
	 * @param mixed[] $object the current object being exported
	 * @param array $chain an array of objects back to either the Installed or Instance object
	 * @throws Adamantine\Exception
	 * @return array an associated array of chain 'steps' as fields
	 */
	private function get_export_chain_for_model(\Abstraction\Models\SecondClass $model, array $chain = array()) {
		// step downwards through the model chain back to Installed or Instance
		$model_chain = array();
		$step_names = array();
		while (true) {
			$step_names[] = $model->get_firstclass_field();
			$model = $model->get_firstclass();
			$model_chain[] = $model;
				
			if ($model instanceof Adamantine\Models\Installed || $model instanceof Adamantine\Models\Instance) break;
		}

		if (sizeof($model_chain) !== sizeof($chain)) {
			throw new Adamantine\Exception("Model chain and firstclass chain lengths are not equal: " . sizeof($model_chain) . " " . sizeof($chain));
		}
		
		$model = array_pop($model_chain);
		if ($model instanceof Adamantine\Models\Instance) throw new Adamantine\Exception("Managed exporting of Instance-rooted models is not supported yet!");

		$last = array_pop($chain);
		array_pop($step_names);

		$export_chain = array();
		
		// step upwards through the model chain back back to the top
		while (sizeof($model_chain) > 0) {
			$model = array_pop($model_chain);
			$step = array_pop($chain);
			$step_name = array_pop($step_names);
				
			if (null === ($check = $model->get_by_firstclass_and_id($last, $step[$model->get_id_field()]))) Adamantine\error("No such elements exists within the chain");
			
			$export_chain[strtoupper($step_name)] = array(self::SIMPLE, $check["name"]);
			
			$last = $step;
		}
		
		return $export_chain;
	}
	
	/**
	 * Attempts to perform an automatic export of a managed model.
	 *
	 * @see Models\ManagedSecondClass::get_importexportable_fields()
	 * @param Models\ManagedSecondClass $model the ManagedSecondClass model to export
	 * @param string $name the name of the model
	 * @param mixed[] $chain an array containing the corresponding firstclass parents for the export, back to either the Installed or Instance object
	 * @throws ImportExportException
	 * @return void
	 */
	protected function export_managedsecondclass(Models\Manageable $model, $name, array $chain, array $prefields = array()) {
		if (!($model instanceof \Abstraction\Models\SecondClass)) throw new ImportExportException("Trying to use export_managedsecondclass to export a non-secondclass");
		
		if (sizeof($chain) === 0) throw new ImportExportException("Chain must contain at least an Installed context to export on");
		if (array_key_exists("id", $chain)) {
			// assume the old style $installed has been supplied rather than a proper chain of array($installed)
			$chain = array($chain);
		}
		foreach ($chain as $c) if (!Data\Data::validate_id_object($c)) throw new ImportExportException("Supplied firstclass in chain is not valid");
				
		$types = $this->build_export_field_types($model, $model->get_importexportable_fields());
		
		$firstclass = $chain[0];
		foreach ($model->list_by_firstclass($firstclass) as $data) {
			$fields = $prefields; 
			$attributes = array();
			
			foreach ($this->get_export_chain_for_model($model, $chain) as $step_name => $step) $fields[$step_name] = $step;
		
			foreach ($types as $field => $type) {
				if ($type === self::BOOL) {
					if ($data[$field]) $attributes[] = strtoupper($field);
				} else $fields[strtoupper($field)] = array($type, $data[$field]);
			}
			
			self::write_line(strtoupper($this->get_area_name()) . " CREATE " . strtoupper($name), $fields, $attributes);
		}
	}

	/**
	 * Attempts to perform an automatic export of a managed model.
	 *
	 * @see Models\ManagedOrientatedOrdered::get_importexportable_fields()
	 * @param Models\ManagedOrientatedOrdered $model the ManagedOrientatedOrdered model to export
	 * @param string $name the name of the model
	 * @param mixed[] $chain an array containing the corresponding firstclass parents for the export, back to either the Installed or Instance object
	 * @throws ImportExportException
	 * @return void
	 */
	protected function export_managedorientatedordered(Models\Manageable $model, $name, array $chain, array $prefields = array()) {
		if (!($model instanceof \Abstraction\Models\OrientatedOrdered)) throw new ImportExportException("Trying to use export_managedorientatedordered to export a non-orientatedoredered");
		
		if (sizeof($chain) === 0) throw new ImportExportException("Chain must contain at least an Installed context to export on");
		if (array_key_exists("id", $chain)) {
			// assume the old style $installed has been supplied rather than a proper chain of array($installed)
			$chain = array($chain);
		}
		foreach ($chain as $c) if (!Data\Data::validate_id_object($c)) throw new ImportExportException("Supplied firstclass in chain is not valid");
		
		$types = $this->build_export_field_types($model, $model->get_importexportable_fields());

		$firstclass = $chain[0];
		foreach ($model->list_ordered_by_firstclass($firstclass) as $data) {
			$fields = $prefields; 
			$attributes = array();
			
			foreach ($this->get_export_chain_for_model($model, $chain) as $step_name => $step) $fields[$step_name] = $step;
		
			foreach ($types as $field => $type) {
				if ($type === self::BOOL) {
					if ($data[$field]) $attributes[] = strtoupper($field);
				} else $fields[strtoupper($field)] = array($type, $data[$field]);
			}
			
			self::write_line(strtoupper($this->get_area_name()) . " CREATE " . strtoupper($name), $fields, $attributes);
		}
	}

	/**
	 * Recursive method to export the tree
	 * 
	 * @internal
	 */
	private function _export_recursive_hierarchy(\Abstraction\Models\Hierarchy $model, array $installed, array $node, array $types, $name, array $prefields = array()) {
		$first = true; 
		$any = false;
		foreach ($model->list_children_ordered_by_firstclass($installed, $node) as $node) {
			if ($first) {
				$first = false; 
				$any = true;
				self::write_line(strtoupper($this->get_area_name()) . " BRANCH " . strtoupper($name) . " IN", $prefields);
			}
			
			$fields = $prefields; 
			$attributes = array();
			foreach ($types as $field => $type) {
				if ($type === self::BOOL) {
					if ($node[$field]) $attributes[] = strtoupper($field);
				} else $fields[strtoupper($field)] = array($type, $node[$field]);
			}
				
			self::write_line(strtoupper($this->get_area_name()) . " CREATE " . strtoupper($name), $fields, $attributes);
			
			if ($model->is_aliasing_supported()) {
				$fields = $prefields;
				$fields["ALIASES"] = array(self::SIMPLE, implode(",", $model->list_aliases_by_row($node)));
				self::write_line(strtoupper($this->get_area_name()) . " SET ALIASES FOR LAST " . strtoupper($name), $fields);
			}
			
			$this->_export_recursive_hierarchy($model, $installed, $node, $types, $name, $prefields);
		}
		
		if ($any) self::write_line(strtoupper($this->get_area_name()) . " BRANCH " . strtoupper($name) . " OUT", $prefields);
	}
	
	/**
	 * Attempts to perform an automatic export of a managed hierarchy.
	 *
	 * @see Models\ManagedHierarchy::get_importexportable_fields()
	 * @param Models\ManagedHierarchy $model the ManagedHierarchy model to export
	 * @param string $name the name of the model
	 * @param mixed[] $chain an array containing the corresponding firstclass parents for the export, back to either the Installed or Instance object
	 * @throws ImportExportException
	 * @return void
	 */
	protected function export_managedhierarchy(Models\Manageable $model, $name, array $chain, array $prefields = array()) {
		if (!($model instanceof \Abstraction\Models\Hierarchy)) throw new ImportExportException("Trying to use export_managedhierarchy to export a non-hierarchy");
		
		if (sizeof($chain) === 0) throw new ImportExportException("Chain must contain at least an Installed context to export on");
		if (array_key_exists("id", $chain)) {
			// assume the old style $installed has been supplied rather than a proper chain of array($installed)
			$chain = array($chain);
		}
		foreach ($chain as $c) if (!Data\Data::validate_id_object($c)) throw new ImportExportException("Supplied firstclass in chain is not valid");
		
		$types = $this->build_export_field_types($model, $model->get_importexportable_fields());
		
		$firstclass = $chain[0];
		$root = $model->get_root_by_firstclass($firstclass);
		foreach ($this->get_export_chain_for_model($model, $chain) as $step_name => $step) $prefields[$step_name] = $step;
		
		self::write_line(strtoupper($this->get_area_name()) . " BRANCH " . strtoupper($name) . " ROOT", $prefields);
		
		$this->_export_recursive_hierarchy($model, $firstclass, $root, $types, $name, $prefields);
	}
	
	protected function export_m2mlinktable(\Abstraction\Models\M2MLinkTable $model, $name, array $chain_a, array $chain_b) {
		$table_a = $model->get_table_a();
		$table_b = $model->get_table_b();
		
		if (!($table_a instanceof Adamantine\Models\Manageable)) throw new ImportExportException("M2MLinkTable can only be automatically exported if both the associated models are Manageable (A table failure)");
		if (!($table_b instanceof Adamantine\Models\Manageable)) throw new ImportExportException("M2MLinkTable can only be automatically exported if both the associated models are Manageable (B table failure)");
		
		$name_a = strtoupper($model->get_link_a());
		$name_b = strtoupper($model->get_link_b());

		if (sizeof($chain_a) === 0) throw new ImportExportException("Chain A must contain at least an Installed context to export on");
		foreach ($chain_a as $c) if (!Data\Data::validate_id_object($c)) throw new ImportExportException("Supplied firstclass in chain A is not valid");

		if (sizeof($chain_b) === 0) throw new ImportExportException("Chain B must contain at least an Installed context to export on");
		foreach ($chain_b as $c) if (!Data\Data::validate_id_object($c)) throw new ImportExportException("Supplied firstclass in chain B is not valid");
		
		$structure = $model->get_structure();
		unset($structure[$model->get_link_a()]);
		unset($structure[$model->get_link_b()]);
		
		$firstclass_a = $chain_a[0];
		foreach ($table_a->list_by_firstclass($firstclass_a) as $a) {
			$fields_a = $this->get_export_chain_for_model($table_a, $chain_a);
			
			foreach ($model->list_bs_by_a($a) as $b) {
				$fields = $fields_a;
				foreach ($this->get_export_chain_for_model($table_b, $chain_b) as $step_name => $step) {
					if (array_key_exists($step_name, $fields_a)) {
						list(, $compare1) = $fields_a[$step_name];
						list(, $compare2) = $step;
							
						if ($compare1 === $compare2) continue;
						throw new ImportExportException("The two M2MLinkTable model chains have a common ancestor model but not a common ancestor object within that model!");
					}
				
					$fields[$step_name] = $step;
				}
				
				$types = self::build_export_field_types($model, array_keys($structure));

				$fields[$name_a] = array(self::SIMPLE, $a["name"]);
				$fields[$name_b] = array(self::SIMPLE, $b["name"]);
				
				$data = $model->get_link_row($a, $b);
				foreach ($types as $field => $type) {
					if ($type === self::BOOL) {
						if ($data[$field]) $attributes[] = strtoupper($field);
					} else $fields[strtoupper($field)] = array($type, $data[$field]);
				}
				
				self::write_line(strtoupper($this->get_area_name()) . " LINK {$name}", $fields);
			}
		}
		
		return true;
	}
	
	/**
	 * Exports the configuration variable settings for this instance area in a standardised format.
	 * 
	 * @internal
	 * @throws ImportExportException
	 * @return void
	 */
	private function export_config() {
		$area = $this->system->get_area_by_name($this->get_area_name());
		$installed = $this->system->get_installed_by_area($area);
		
		$variables = $this->repository->get("_core", "Variable")->list_by_area($area);
		
		foreach ($variables as $variable) {
			if (null === ($value = $this->settings->get($installed, $variable))) continue;
			
			switch ($variable["type"]) {
				case Models\Variable::INTEGER:
					$value = array(self::INT, $value);
					break;
				case Models\Variable::UNSIGNED:
					$value = array(self::INT, $value);
					break;
				case Models\Variable::BOOLEAN:
					$value = array(self::BOOL, $value);
					break;
				case Models\Variable::STRING:
					$value = array(self::RAWURL, $value);
					break;
				case Models\Variable::SELECT:
					$value = array(self::SIMPLE, $value);
					break;
				default:
					throw new ImportExportException("Unable to convert variable type to export type");
			}
			
			self::write_line(strtoupper($area["name"]) . " SET CONFIG", array(
				"VARIABLE" => array(self::SIMPLE, $variable["name"]),
				"VALUE" => $value
			));
		}
	}
	

	/**
	 * Attempts to perform an automatic export of a StandardField model.
	 * NB only models with ListSets which are directly parented by installed can be exported automatically like this.
	 *
	 * @see Models\ManagedOrientatedOrdered::get_importexportable_fields()
	 * @param Models\StandardField $model the ManagedOrientatedOrdered model to export
	 * @param string $name the name of the model
	 * @param mixed[] $chain an array containing the corresponding firstclass parents for the export, back to either the Installed or Instance object
	 * @throws ImportExportException
	 * @return void
	 */
	protected function export_standardfield(Models\StandardField $model, $name, array $chain, array $prefields = array()) {
		if ($model->get_listset_model() !== null && $model->get_listset_model()->get_firstclass_field() !== "installed") throw new ImportExportException("Trying to use export_standardfield for a field who's ListSet is not directly parented by installed. This is not currently possible.");
		
		if (sizeof($chain) === 0) throw new ImportExportException("Chain must contain at least an Installed context to export on");
		if (array_key_exists("id", $chain)) {
			// assume the old style $installed has been supplied rather than a proper chain of array($installed)
			$chain = array($chain);
		}
		foreach ($chain as $c) if (!Data\Data::validate_id_object($c)) throw new ImportExportException("Supplied firstclass in chain is not valid");
		
		$this->export_managedorientatedordered($model, $name, $chain, $prefields);
		
		$installed = $chain[sizeof($chain) - 1]; // this is far from ideal to assume the last is installed, as it might be 'instance', but for now will have to do

		$lookup = array();
		if ($model->get_listset_model() !== null) $lookup = Data\Data::build_firstclass_lookup($model->get_listset_model()->list_by_firstclass($installed));
		
		$firstclass = $chain[0];
		foreach ($model->list_ordered_by_firstclass($firstclass) as $data) {
			if ($data["type"] !== Models\StandardField::LISTSET) continue;
			if ($data["listset"] === null) continue;
		
			$fields = array(); 
			$attributes = array();
				
			foreach ($this->get_export_chain_for_model($model, $chain) as $step_name => $step) $fields[$step_name] = $step;
			$fields[strtoupper($name)] = array(self::SIMPLE, $data["name"]);

			$fields["LISTSET"] = array(self::SIMPLE, $lookup[$data["listset"]]["name"]);
			if ($data["allow_multiple"]) $attributes[] = "MULTIPLE";
			
			self::write_line(strtoupper($this->get_area_name()) . " SET " . strtoupper($name) . " LISTSET", $fields, $attributes);
		}
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Evaluates the specified line for anything that can be imported by this area.
	 * 
	 * Subclasses should override this method, but still call parent::import(line) to make use of the standard config import code.
	 * 
	 * @param string $line the string to consider
	 * @return boolean true if an import was performed, false if nothing importable was found
	 */
	public function import($line) {
		if (preg_match("`^" . strtoupper($this->get_area_name()) . " SET CONFIG`iD", $line)) return $this->import_set_config($line);
	
		return false;
	}
	
	private $last_secondclass_import = null;

	/**
	 * @internal
	 * 
	 * return mixed[]
	 */
	protected function get_last_secondclass_import() {
		return $this->last_secondclass_import;
	}
	
	/**
	 * Extracts the chain of firstclass ancestor objects by interrogating the chain back to Installed or Instance.
	 * 
	 * @param Models\SecondClass $model the Managed... model being imported
	 * @param mixed[] $installed the root Installed context
	 * @param string $line the import source string
	 * @throws Adamantine\Exception
	 * @return array|boolean the ancestor chain, or FALSE if an error occurs.
	 */
	private function get_parent_firstclass_chain_by_steps(\Abstraction\Models\SecondClass $model, array $installed, $line) {
		// step downwards through the model chain back to Installed or Instance
		$model_chain = array();
		while (true) {
			$step_names[] = $model->get_firstclass_field();
			$model = $model->get_firstclass();
			$model_chain[] = $model;
		
			if ($model instanceof Adamantine\Models\Installed || $model instanceof Adamantine\Models\Instance) break;
		}
		
		$model = array_pop($model_chain);
		if ($model instanceof Adamantine\Models\Instance) throw new Adamantine\Exception("Managed importing of Instance-rooted models is not supported yet!");
		
		$last = $installed;
		array_pop($step_names);

		$chain = array($installed);
		
		// step upwards through the model chain back back to the top
		while (sizeof($model_chain) > 0) {
			$model = array_pop($model_chain);
			$step_name = strtoupper(array_pop($step_names));
		
			if (null === ($step = self::extract_field($step_name, $line))) return false;
			if (null === ($last = $model->get_by_name($last, $step))) return false;
			
			$chain[] = $last;
		}
		
		return $chain;
	}
	
	/**
	 * Attempts to perform an automatic import of a managed model from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedSecondClass::get_importexportable_fields()
	 * @param Models\ManagedSecondClass $model the ManagedSecondClass model to import into
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_managedsecondclass(Models\Manageable $model, array $installed, $line) {
		if (!($model instanceof \Abstraction\Models\SecondClass)) throw new ImportExportException("Trying to use import_managedsecondclass to import a non-secondclass");
		$structure = $model->get_structure();
		
		$data = array();
		foreach ($model->get_importexportable_fields() as $field) {
			if ($structure[$field] instanceof Database\Type_Bool) {
				$data[$field] = self::has_attribute(strtoupper($field), $line);
			} else {
				$value = self::extract_field(strtoupper($field), $line);
				if ($value === null && $structure[$field]->not_null) return false;
			
				try {
					if (!$structure[$field]->assert($value)) return false;
				} catch(Database\TypeMismatchException $e) {
					return false;
				}
				
				$data[$field] = $value;
			}
		}
		
		if (false === ($chain = $this->get_parent_firstclass_chain_by_steps($model, $installed, $line))) return false;
		$last = array_pop($chain);
		
		if (null === ($this->last_secondclass_import = $model->insert_for_firstclass($last, $data))) return false;
		if (method_exists($model, "post_create_processing")) $model->post_create_processing($this->repository, $this->last_secondclass_import);
		
		return true;
	}

	/**
	 * Attempts to perform an automatic import of a managed model from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedOrientatedOrdered::get_importexportable_fields()
	 * @param Models\ManagedOrientatedOrdered $model the ManagedOrientatedOrdered model to import into
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_managedorientatedordered(Models\Manageable $model, array $installed, $line) {
		if (!($model instanceof \Abstraction\Models\OrientatedOrdered)) throw new ImportExportException("Trying to use import_managedorientatedordered to import a non-orientatedordered");
		$structure = $model->get_structure();

		$data = array();
		foreach ($model->get_importexportable_fields() as $field) {
			if ($structure[$field] instanceof Database\Type_Bool) {
				$data[$field] = self::has_attribute(strtoupper($field), $line);
			} else {
				$value = self::extract_field(strtoupper($field), $line);
				if ($value === null && $structure[$field]->not_null) return false;
			
				try {
					if (!$structure[$field]->assert($value)) return false;
				} catch(Database\TypeMismatchException $e) {
					return false;
				}
				
				$data[$field] = $value;
			}
		}
		
		if (false === ($chain = $this->get_parent_firstclass_chain_by_steps($model, $installed, $line))) return false;
		$last = array_pop($chain);
				
		if (null === ($this->last_secondclass_import = $model->insert_for_firstclass($last, $data))) return false;
		if (method_exists($model, "post_create_processing")) $model->post_create_processing($this->repository, $this->last_secondclass_import);
		
		return true;
	}

	private $parent_node = array();
	private $parent_stack = array();
	private $last_node = array();
	
	/**
	 * Attempts to perform an automatic import of a managed hierarchy from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedHierarchy::get_importexportable_fields()
	 * @param Models\ManagedHierarchy $model the ManagedHierarchy model to import into
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_managedhierarchy(Models\Manageable $model, array $installed, $line) {
		if (!($model instanceof \Abstraction\Models\Hierarchy)) throw new ImportExportException("Trying to use import_managedhierarchy to import a non-hierarchy");
		$structure = $model->get_structure();
		
		$data = array();
		foreach ($model->get_importexportable_fields() as $field) {
			if ($structure[$field] instanceof Database\Type_Bool) {
				$data[$field] = self::has_attribute(strtoupper($field), $line);
			} else {
				$value = self::extract_field(strtoupper($field), $line);
				if ($value === null && $structure[$field]->not_null) return false;
			
				try {
					if (!$structure[$field]->assert($value)) return false;
				} catch(Database\TypeMismatchException $e) {
					return false;
				}
				
				$data[$field] = $value;
			}
		}
		
		if (false === ($chain = $this->get_parent_firstclass_chain_by_steps($model, $installed, $line))) return false;
		$index = $model->get_table() . "__" . implode(",", Data\Data::extract_ids_from_id_object_array($chain));
		$last = array_pop($chain);
		
		if (null === $this->parent_node[$index]) return false;
		if (null === ($this->last_secondclass_import = $this->last_node[$index] = $model->insert_for_firstclass_and_parent($last, $this->parent_node[$index], $data))) return false;
		if (method_exists($model, "post_create_processing")) $model->post_create_processing($this->repository, $this->last_secondclass_import);
		
		return true;
	}

	/**
	 * Attempts to perform an automatic import of a branch from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @param Models\ManagedHierarchy $model the ManagedHierarchy model context
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $direction either BRANCH_IN or BRANCH_OUT
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_managedhierarchy_branch(Models\Manageable $model, array $installed, $direction, $line) {
		if (!($model instanceof \Abstraction\Models\Hierarchy)) throw new ImportExportException("Trying to use import_managedhierarchy_branch to import a non-hierarchy");

		if (false === ($chain = $this->get_parent_firstclass_chain_by_steps($model, $installed, $line))) return false;
		$index = $model->get_table() . "__" . implode(",", Data\Data::extract_ids_from_id_object_array($chain));
		$last = array_pop($chain);
		
		switch (strtolower($direction)) {
			case self::BRANCH_ROOT:
				if (array_key_exists($index, $this->last_node)) return false;
				
				$this->last_node[$index] = $model->get_root_by_firstclass($last);
				$this->parent_node[$index] = null;
				$this->parent_stack[$index] = array();
				
				return true;
			case self::BRANCH_IN:
				if (!array_key_exists($index, $this->last_node)) return false;
				if ($this->last_node[$index] === null) return false;
				
				$this->parent_stack[$index][] = $this->parent_node[$index] = $this->last_node[$index];
				
				return true;
			case self::BRANCH_OUT:
				if (!array_key_exists($index, $this->last_node)) return false;
				if (sizeof($this->parent_stack[$index]) === 0) return false;
				
				$this->current_node[$index] = null;
				array_pop($this->parent_stack[$index]);
				
				if (sizeof($this->parent_stack[$index]) === 0) {
					$this->parent_node[$index] = null;
				} else {
					$this->parent_stack[$index][] = $this->parent_node[$index] = array_pop($this->parent_stack[$index]);
				}
				
				return true;
			default:
				return false;
		}
	}

	/**
	 * Attempts to perform an automatic import of a managed model row alias set from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @param Models\Manageable $model the model context, which must have alias support enabled
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_managed_alias(Models\Manageable $model, array $installed, $line) {
		if (!$model->is_aliasing_supported()) return false;

		if (false === ($chain = $this->get_parent_firstclass_chain_by_steps($model, $installed, $line))) return false;
		$index = $model->get_table() . "__" . implode(",", Data\Data::extract_ids_from_id_object_array($chain));
		$last = array_pop($chain);
		
		if ($this->last_node[$index] === null) return false;
	
		$aliases = self::extract_field("ALIASES", $line);
		if ($aliases === null || "" === trim($aliases)) $aliases = array();
		else $aliases = explode(",", $aliases);

		if (!$model->replace_aliases($this->last_node[$index], $aliases)) return false;
		
		return true;
	}

	protected function import_m2mlinktable(\Abstraction\Models\M2MLinkTable $model, array $installed, $line) {
		$table_a = $model->get_table_a();
		$table_b = $model->get_table_b();
		
		if (!($table_a instanceof Adamantine\Models\Manageable)) throw new ImportExportException("M2MLinkTable can only be automatically imported if both the associated models are Manageable (A table failure)");
		if (!($table_b instanceof Adamantine\Models\Manageable)) throw new ImportExportException("M2MLinkTable can only be automatically imported if both the associated models are Manageable (B table failure)");
		
		if (false === ($chain_a = $this->get_parent_firstclass_chain_by_steps($table_a, $installed, $line))) return false;
		$last_a = array_pop($chain_a);
		
		if (false === ($chain_b = $this->get_parent_firstclass_chain_by_steps($table_b, $installed, $line))) return false;
		$last_b = array_pop($chain_b);
		
		$name_a = strtoupper($model->get_link_a());
		$name_b = strtoupper($model->get_link_b());
		
		if (null === ($object_a = self::extract_field($name_a, $line))) return false;
		if (null === ($object_a = $table_a->get_by_name($last_a, $object_a))) return false;
		
		if (null === ($object_b = self::extract_field($name_b, $line))) return false;
		if (null === ($object_b = $table_b->get_by_name($last_b, $object_b))) return false;
		
		$structure = $model->get_structure();
		unset($structure[$model->get_link_a()]);
		unset($structure[$model->get_link_b()]);
		
		$data = array();
		foreach ($structure as $field => $type) {
			if ($type instanceof Database\Type_Bool) {
				$data[$field] = self::has_attribute(strtoupper($field), $line);
			} else {
				$value = self::extract_field(strtoupper($field), $line);
				if ($value === null && $type->not_null) return false;
					
				try {
					if (!$type->assert($value)) return false;
				} catch(Database\TypeMismatchException $e) {
					return false;
				}
		
				$data[$field] = $value;
			}
		}
		
		if (!$model->link($object_a, $object_b, $data)) return false;
		
		return true;
	}
	
	/**
	 * Attempts to import a standardised variable setting configuration value for the instance.
	 * 
	 * @param string $line the import line containing the setting value
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_set_config($line) {
		$area = $this->system->get_area_by_name($this->get_area_name());
		
		if (null === ($installed = $this->system->get_installed_by_area($area))) Adamantine\error("Sorry, no such area is installed");
		
		if (null === ($variable = self::extract_field("VARIABLE", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $variable)) return false;
		if (null === ($variable = $this->repository->get("_core", "Variable")->get_by_name($area, $variable))) Adamantine\error("Sorry, no such variable exists");
		
		if (null === ($value = self::extract_field("VALUE", $line))) return false;
		
		switch ($variable["type"]) {
			case Models\Variable::INTEGER:
				if (!Data\Data::validate_int($value)) return false;
				break;
			case Models\Variable::UNSIGNED:
				if (!Data\Data::validate_int($value, 0)) return false;
				break;
			case Models\Variable::STRING:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_TEXT_255, $value)) return false;
				break;
			case Models\Variable::BOOLEAN:
				$value = $value ? true : false;
				break;
			case Models\Variable::SELECT:
				if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_TEXT_255, $value)) return false;
				break;
			default: Adamantine\error("Unknown variable type", $variable["type"]);
		}
		
		$this->settings->set($installed, $variable, $value);

		// reload settings
		$instance = $this->repository->get("_core", "Instance")->get_by_id($installed["instance"]);
		$this->settings->initialize($instance);
		
		return true;
	}

	/**
	 * Attempts to perform an automatic import of a standard listset from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedOrientatedOrdered::get_importexportable_fields()
	 * @param Models\StandardListSet $model the StandardListSet model to import into
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_standardlistset(Models\StandardListSet $model, array $installed, $line) {
		return $this->import_managedorientatedordered($model, $installed, $line);
	}

	/**
	 * Attempts to perform an automatic import of a standard listset from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedOrientatedOrdered::get_importexportable_fields()
	 * @param Models\StandardListSet $model the StandardListSet model to import into
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_standardlistitem(Models\StandardListSet $model, array $installed, $line) {
		return $this->import_managedorientatedordered($model->get_listitem_model(), $installed, $line);
	}

	/**
	 * Attempts to perform an automatic import of a StandardField listset from an identified line.
	 * 
	 * Note, the line must already have been identified as suitable and corresponding to the model.
	 * 
	 * @see Models\ManagedOrientatedOrdered::get_importexportable_fields()
	 * @param Models\StandardField $model the StandardField model relating to the import
	 * @param string $name the name of the StandardField model which is being used to import on
	 * @param mixed[] $installed the corresponding installed instance area for the import
	 * @param string $line the import line containing the data
	 * @return boolean true if an import was performed, false if the line could not be imported or an error occurred
	 */
	protected function import_standardfield_listset(Models\StandardField $model, $name, array $installed, $line) {
		if ($model->get_listset_model() === null) throw new ImportExportException("Trying to use import_standardfield_listset for a model which doesn't support listsets");
		if ($model->get_listset_model()->get_firstclass_field() !== "installed") throw new ImportExportException("Trying to use import_standardfield_listset for a field who's ListSet is not directly parented by installed. This is not currently possible.");
		
		if (false === ($chain = $this->get_parent_firstclass_chain_by_steps($model, $installed, $line))) return false;
		$last = array_pop($chain);
		
		if (null === ($field = self::extract_field($name, $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $field)) return false;
		if (null === ($data = $model->get_by_name($last, $field))) return false;
		
		if (null === ($listset = self::extract_field("LISTSET", $line))) return false;
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $listset)) return false;
		if (null === ($listset = $model->get_listset_model()->get_by_name($installed, $listset))) return false;

		$data["listset"] = $listset["id"];
		$data["allow_multiple"] = self::has_attribute("MULTIPLE", $line);

		$model->update($data);
		
		return true;
	}
}
