<?php
/**
 * Adamantine extension of the Abstraction Session framework, providing user name, one time pad and higher-level user logging support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Framework;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";
require_once ABSTRACTION_ROOT_PATH . "framework/session.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/area.php";
require_once ADAMANTINE_ROOT_PATH . "models/instance.php";
require_once ADAMANTINE_ROOT_PATH . "models/user.php";
require_once ADAMANTINE_ROOT_PATH . "models/log.php";

use \Abstraction\Framework as Framework;
use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \Adamantine\Models as Models;

/**
 * @internal
 */
class SessionException extends Adamantine\Exception {}

/**
 * @internal
 */
class BadParameterException extends SessionException {
	function __construct($value) {
		parent::__construct("Bad parameter value", $value);
	}
}

/**
 * Adamantine version of the System framework, with named user and logging support.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Session extends Framework\Session {
	private $log;
	
	/**
	 * Constructs a new instance of the session management component.
	 * 
	 * @param string $name a name to use for the session in cookies etc.
	 * @param System $system the core framework System resource
	 * @param Framework\Access $access the access management resource
	 * @param Models\Area $area the Area model to associate with
	 * @param Models\Instance $instance the Instance model to associate with
	 * @param Models\User $user the User model to associate with
	 * @param Models\Log $log the Log model to use
	 */
	public function __construct($name, System $system, Framework\Access $access, Models\Area $area, Models\Instance $instance, Models\User $user, Models\Log $log) {
		parent::__construct($name, $system, $access, $area, $instance, $user);
		
		$this->log = $log;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * A convenience method for statically retreiving an installed based upon its area name and instance ID.
	 * 
	 * This is largely intended for the various AJAX and JSON 'live' systems which load data without initalising the full Adamantine framework.
	 * 
	 * @param string $area_name the name of the area to fetch the installed instance for
	 * @param int $xsi the ID of the current instance
	 * @param Models\Repository $repository the current repository
	 * @throws BadParameterException if the area name is invalid
	 * @throws SessionException for all other errors, including if the area or instance is not available or disabled etc.
	 * @return array the installed object
	 */
	public static function get_installed_by_area_name_and_xsi_for_uninitalized($area_name, &$xsi, Models\Repository $repository) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $area_name)) throw new BadParameterException("Invalid area name supplied");
		
		if (null === ($instances = $repository->get("_core", "Instance"))) throw new SessionException("Unable to locate Instance model");
		if (null === ($areas = $repository->get("_core", "Area")))  throw new SessionException("Unable to locate Area model");
		if (null === ($installeds = $repository->get("_core", "Installed"))) throw new SessionException("Unable to locate Installed model");
		
		if (!Data\Data::validate_id($xsi)) throw new SessionException("Bad instance ID sent");
		if (null === ($instance = $instances->get($xsi))) throw new SessionException("No such instance exists");
		
		if (null === ($area = $areas->get_by_name($area_name))) throw new SessionException("{$area_name} is not build into this system");
		
		$installed = null;
		foreach ($installeds->list_by_firstclass($instance) as $i) if ($i["area"] === $area["id"]) $installed = $i;
		
		if (null === $installed) throw new SessionException("{$area_name} is not installed into this instance");
		if ($installed["status"] === Models\Installed::DISABLED) throw new SessionException("{$area_name} is not enabled for this instance");
		
		return $installed;
	}
	
	/**
	 * A convenience method for statically retreiving an installed based upon its area name and instance ID.
	 * 
	 * This is largely intended for the various AJAX and JSON 'live' systems which load data without initalising the full Adamantine framework.
	 * 
	 * @param string $area_name the name of the area to fetch the installed instance for
	 * @param int $xsi the ID of the current instance
	 * @param Models\Repository $repository the current repository
	 * @throws BadParameterException if the area name is invalid
	 * @throws SessionException for all other errors, including if the area or instance is not available or disabled etc.
	 * @return array the installed object
	 */
	public static function get_installed_by_area_name_and_instance_name_for_uninitalized($area_name, $instance_name, Models\Repository $repository) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $area_name)) throw new BadParameterException("Invalid area name supplied");
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $instance_name)) throw new BadParameterException("Invalid instance name supplied");
		
		if (null === ($instances = $repository->get("_core", "Instance"))) throw new SessionException("Unable to locate Instance model");
		if (null === ($areas = $repository->get("_core", "Area")))  throw new SessionException("Unable to locate Area model");
		if (null === ($installeds = $repository->get("_core", "Installed"))) throw new SessionException("Unable to locate Installed model");
		
		if (null === ($instance = $instances->get_by_name($instance_name))) throw new SessionException("No such instance exists by that name");
		if (null === ($area = $areas->get_by_name($area_name))) throw new SessionException("{$area_name} is not build into this system");
		
		$installed = null;
		foreach ($installeds->list_by_firstclass($instance) as $i) if ($i["area"] === $area["id"]) $installed = $i;
		
		if (null === $installed) throw new SessionException("{$area_name} is not installed into this instance");
		if ($installed["status"] === Models\Installed::DISABLED) throw new SessionException("{$area_name} is not enabled for this instance");
		
		return $installed;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Attempts to log a user in using their username to identify their account.
	 * 
	 * @param string $name the unique username of the user
	 * @throws BadParameterException
	 * @return boolean
	 */
	function login_by_user_name($name) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $name)) throw new BadParameterException($name);
		
		if (null === ($user = $this->user->get_by_name($this->get_session_instance(), $name))) return false;
		
		return $this->login($user);
	}

	/**
	 * Attempts to log a user in using a pre-stored one time pad in their account.
	 * 
	 * Any existing one time pad stored in the user account is wiped regardless of whether the attempt succeeds or not.
	 * 
	 * @param mixed[] $user the user to attempt to log in
	 * @param string $md5 the one time pad, which must already be stored in the user account
	 * @throws BadParameterException
	 * @return boolean
	 */
	function login_by_one_time_pad(array $user, $md5) {
		if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_MD5, $md5)) throw new BadParameterException($md5);

		if (!$this->user->use_one_time_pad_md5($user, $md5)) return false;
		
		return $this->login($user);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the overall meaningful access that the currently loaded session user has to the area specified by name.
	 * 
	 * As well as explicit user access levels and inherited group access, aspects such as the instance and installed statuses (ENABLED, DISABLED, LOCKED etc.) are factored in to give an intelligent and meaningful result.
	 * 
	 * @see Framework\Access
	 * @param string $area the name of the area within the Area model
	 * @throws SessionException
	 * @return int|NULL
	 */
	public function get_access_by_name($area) {
		$match = null;
		foreach ($this->system->list_installed_areas() as $a) {
			if ($a["name"] === $area) {
				$match = $a; 
				break;
			}
		}
		if ($match === null) throw new SessionException("No such area with that name exists or is installed", $area);

		return $this->get_access($match);
	}

	/**
	 * Returns whether the currently loaded session user has access to the specified area at least to the specified level, using names and words rather than row arrays and numbers
	 * 
	 * Calculation of access is performed intelligently using get_access, so factors such as instance status are considered automatically.
	 * Access levels are defined within the Access model.
	 * 
	 * @see Framework\Access
	 * @param string $area the name of the area within the Area model
	 * @param string $level one of the follow string values: none, read, user, editor or full
	 * @throws SessionException
	 * @return boolean
	 */
	public function has_access_by_name($area, $level) {
		switch($level) {
			case "none";
				return false;
			case "read":
				$level = Framework\Access::READ;
				break;
			case "user":
				$level = Framework\Access::USER;
				break;
			case "editor":
				$level = Framework\Access::EDITOR;
				break;
			case "full":
				$level = Framework\Access::FULL;
				break;
			default:
				throw new SessionException("Bad access level", $level);
		}
		
		return $this->get_access_by_name($area) >= $level;
	}

	//-------------------------------------------------------------------------

	/**
	 * Returns a stored session variable.
	 * 
	 * This is exactly the same as accessing the $_SESSION superglobal, but uses a stricter structure of area names to avoid variable collisions.
	 * 
	 * @param string $area
	 * @param string $var
	 * @return mixed|NULL
	 */
	public function get_session_var_by_name($area, $var) {
		if (!isset($_SESSION["session_variables"])) return null;
		if (!isset($_SESSION["session_variables"][$area])) return null;
		if (!isset($_SESSION["session_variables"][$area][$var])) return null;
		
		return $_SESSION["session_variables"][$area][$var];
	}

	/**
	 * Sets a stored session variable.
	 * 
	 * This is exactly the same as accessing the $_SESSION superglobal, but uses a stricter structure of area names to avoid variable collisions.
	 * 
	 * @param string $area
	 * @param string $var
	 * @param mixed $variable
	 */
	public function set_session_var_by_name($area, $var, $variable) {
		if (!isset($_SESSION["session_variables"])) $_SESSION["session_variables"] = array();
		if (!isset($_SESSION["session_variables"][$area])) $_SESSION["session_variables"][$area] = array();
		
		$_SESSION["session_variables"][$area][$var] = $variable;
	}

	/**
	 * Unsets a stored session variable.
	 *
	 * This is exactly the same as accessing the $_SESSION superglobal, but uses a stricter structure of area names to avoid variable collisions.
	 *
	 * @param string $area
	 * @param string $var
	 */
	public function unset_session_var_by_name($area, $var) {
		if (!isset($_SESSION["session_variables"])) return;
		if (!isset($_SESSION["session_variables"][$area])) return;
		if (!isset($_SESSION["session_variables"][$area][$var])) return;
		
		unset($_SESSION["session_variables"][$area][$var]);
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Inserts an entry into the audit log about non-installed area activity that has been performed by the current session user.
	 * 
	 * @param string $action a brief description of the activity that has taken place
	 * @param string|NULL $data any further details for the audit log
	 * @return void
	 */
	public function log_core($action, $data = null) {
		$this->log->log_core($this->get_session_instance(), $this->get_active_user(), $action, $data);
	}

	/**
	 * Inserts an entry into the audit log about installed area activity that has been performed by the current session user.
	 * 
	 * @param string $area the name of the area within the Area model
	 * @param string $action a brief description of the activity that has taken place
	 * @param string|NULL $data any further details for the audit log
	 * @throws SessionException
	 * @return void
	 */
	public function log_by_name($area, $action, $data = null) {
		$installed = null;
		if ($area !== null) {
			$match = null;
			foreach ($this->system->list_installed_areas() as $a) {
				if ($a["name"] === $area) {
					$match = $a; 
					break;
				}
			}
			if ($match === null) throw new SessionException("No such area with that name exists or is installed to log", $area);
			$installed = $this->system->get_installed_by_area($match);
		}
		$this->log->log_area($this->get_session_instance(), $installed, $this->get_active_user(), $action, $data);
	}
}
