<?php
/**
 * Adamantine extension of the Abstraction System framework, providing named area support.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Framework;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "framework/system.php";

use \Abstraction\Framework as Framework;

/**
 * Adamantine version of the System framework, with named area support.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class System extends Framework\System {
	/**
	 * Returns an area from the installed matrix using its name alone.
	 * 
	 * Note, the area returned is from the cached matrix, not via a database lookup. This improves performance greatly, but means that this method should not be relied on if changes have been made directly to the Area model.
	 * 
	 * @param string $name the name of the area to locate
	 * @return mixed[]|NULL
	 */
	public function get_area_by_name($name) {
		foreach ($this->installed_matrix as $lookup) {
			unset($lookup[$this->installed->get_id_field()]);
			unset($lookup["area"]);
			unset($lookup["instance"]);
			$lookup[$this->area->get_id_field()] = $lookup["area_id"];
			if ($lookup["name"] === $name) return $lookup;
		}
		return null;
	}
}
