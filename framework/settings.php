<?php
/**
 * Adamantine Settings framework, providing storage and management of instance variables for installed areas.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Framework;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "data/data.php";

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ADAMANTINE_ROOT_PATH . "core.php";
require_once ADAMANTINE_ROOT_PATH . "models/installed.php";
require_once ADAMANTINE_ROOT_PATH . "models/variable.php";
require_once ADAMANTINE_ROOT_PATH . "models/setting.php";

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;
use \Adamantine\Models as Models;

/**
 * @internal
 */
class SettingsException extends Adamantine\Exception {}

/**
 * Adamantine Settings framework class, providing storage and management of instance variables for installed areas.
 * 
 * @api
 * @author Pete Morris
 * @version 1.2.0
 */
class Settings {
	private $system, $installed, $variable, $setting;
	
	private $loaded;

	/**
	 * Constructs a new instance of the settings management component.
	 * 
	 * @param System $system the core framework System resource
	 * @param Models\Installed $installed the Installed model to associate with
	 * @param Models\Variable $variable the Variable model to associate with
	 * @param Models\Setting $setting the Setting model to associate with
	 */
	public function __construct(System $system, Models\Installed $installed, Models\Variable $variable, Models\Setting $setting) {
		$this->system = $system;
		$this->installed = $installed;
		$this->variable = $variable;
		$this->setting = $setting;
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Initialises the framework by loading all the current setting values into the cache.
	 * 
	 * @param mixed[] $instance the instance to load the settings for
	 * @return void
	 */
	public function initialize($instance) {
		$this->loaded = array();
		
		foreach ($this->setting->list_all_by_instance($instance) as $setting) {
			if (!array_key_exists($setting["area"], $this->loaded)) $this->loaded[$setting["area"]] = array();
			$this->loaded[$setting["area"]][$setting["name"]] = $setting["value"];
		}
	}
	
	//-------------------------------------------------------------------------

	/**
	 * Returns the value of the specified setting directly from the Settings model.
	 * 
	 * @param mixed[] $installed the installed instance area the setting corresponds to
	 * @param mixed[] $variable the variable to fetch the value of
	 * @param boolean $use_default_for_none whether or not the Variable's default value should be returned if no setting has explicitly been defined
	 * @throws SettingsException
	 * @return mixed
	 */
	public function get(array $installed, array $variable, $use_default_for_none = true) {
		if (!Data\Data::validate_id_object($installed, $this->installed->get_id_field())) throw new SettingsException();
		if (!Data\Data::validate_id_object($variable, $this->variable->get_id_field())) throw new SettingsException();

		try {
			return $this->setting->get($installed, $variable, false);
		} catch (Models\NoSettingExistsYetException $e) {
			if ($use_default_for_none) return $variable["default"] === null ? null : json_decode($variable["default"]);
			throw new SettingsException("No setting currently exists for that variable, and default values are suppressed");
		}
	}

	/**
	 * Returns the value of the specified setting currently loaded using named lookups for the area and variable key.
	 * 
	 * Note, this method returns from the loaded cache rather than fetching from the Settings model database, which gives performance gains but may lead to data inaccuracy.
	 * 
	 * @param string $area the name of the area the variable corresponds to
	 * @param string $variable the name of the variable key
	 * @return mixed
	 */
	public function get_by_names($area, $variable) {
		return $this->loaded[$area][$variable];
	}

	/**
	 * Stores a new value in the Settings model corresponding to the current session instance for the specified installed area.
	 * 
	 * Note, the cache is not automatically updated, so you may need to called initialize again after using this method.
	 * 
	 * @param mixed[] $installed the installed instance area the setting corresponds to
	 * @param mixed[] $variable the variable to store the value for
	 * @param mixed $value the value to store
	 * @throws SettingsException
	 * @return true always returns true; failure will throw an exception
	 */
	public function set(array $installed, array $variable, $value) {
		if (!Data\Data::validate_id_object($installed, $this->installed->get_id_field())) throw new SettingsException();
		if (!Data\Data::validate_id_object($variable, $this->variable->get_id_field())) throw new SettingsException();
		
		return $this->setting->set($installed, $variable, $value);
	}

	/**
	 * Stores a new value in the Settings model corresponding to the current session instance for the specified installed area, using names to identify the area and variable.
	 * 
	 * Note, the cache is not automatically updated, so you may need to called initialize again after using this method.
	 * 
	 * @param string $area the name of the area the variable corresponds to
	 * @param string $variable the name of the variable key
	 * @param mixed $value the value to store
	 * @throws SettingsException
	 * @return true always returns true; failure will throw an exception
	 */
	public function set_by_names($area, $variable, $value) {
		$match = null;
		foreach ($this->system->list_installed_areas() as $a) {
			if ($a["name"] === $area) {
				$match = $a; 
				break;
			}
		}
		if ($match === null) throw new SettingsException("No such area with that name exists or is installed", $area);
		$area = $match;

		$installed = $this->system->get_installed_by_area($area);
		
		$match = null;
		foreach ($this->variable->list_by_area($area) as $v) {
			if ($v["name"] === $variable) {
				$match = $a; 
				break;
			}
		}
		if ($match === null) throw new SettingsException("No such variable with that name exists or is installed", $area);
		$variable = $match;
		
		return $this->set($installed, $variable, $value);
	}
}
