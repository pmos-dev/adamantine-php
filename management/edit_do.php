<?php
/**
 * Adamantine managed model functionality: save changes made to a model row.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Framework as Framework;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

if ($_MODEL instanceof Models\Hierarchy) Adamantine\error("This management approach does not work for hierarchy models. Please use the AJAX tree management instead.");
if ($_MODEL instanceof Models\SecondClass) {
	$id_chain = array();
	if (isset($_POST["chain"]) && "" !== $_POST["chain"]) {
		$id_chain = explode(",", $_POST["chain"]);
		foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
		unset($id);
	}
}

if (isset($_POST["id"]) && $_POST["id"] === "new") {
	$_UI->set_titles("Add new {$_POST["model"]}", $xsi["description"]);

	$data[$_MODEL->get_id_field()] = "new";
} else {
	$_UI->set_titles("Edit {$_POST["model"]}", $xsi["description"]);

	if (!Data\Data::validate_id($_POST["id"])) Adamantine\error("Invalid ID supplied");
	
	if ($_MODEL instanceof Models\SecondClass) {
		if (null === ($data = model_get_secondclass_data($_MODEL, $_POST["id"], $id_chain))) Adamantine\error("No such element exists");
	} elseif ($_MODEL instanceof Models\FirstClass) {
		if (null === ($data = $_MODEL->get_by_id($_POST["id"]))) Adamantine\error("No such element exists");
	} else throw new Adamantine\Exception("This model type is not supported");
}

foreach ($_MODEL->get_manageable_fields() as $field) {
	$type = $_MODEL_STRUCTURE[$field];
	$field_name = "field_" . Data\Data::hex_encode($field);

	$override = null;
	if (method_exists($_MODEL, "encrypted_field_type_override")) $override = $_MODEL->encrypted_field_type_override($field);
	
	if ($type instanceof Database\Type_Bool || $override === "bool") {
		if ($type->not_null) $_POST[$field_name] = Data\Data::checkbox_boolean($_POST[$field_name]);
		else {
			if (!isset($_POST[$field_name]) || "" === $_POST[$field_name]) $_POST[$field_name] = null;
			else $_POST[$field_name] = Data\Data::checkbox_boolean($_POST[$field_name]);
		}
	}
	if (($type instanceof Database\Type_DateTime || $override === "datetime") && Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_DMYHI, $_POST[$field_name])) $_POST[$field_name] = Data\Data::datetime_dmyhi_to_ymdhis($_POST[$field_name]);
	if (($type instanceof Database\Type_Date || $override === "date") && Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_DMY, $_POST[$field_name])) $_POST[$field_name] = Data\Data::date_dmy_to_ymd($_POST[$field_name]);
	
	if ((!$type->not_null) && (!isset($_POST[$field_name]) || "" === $_POST[$field_name])) $_POST[$field_name] = null;
	else {
		if ($type instanceof Database\Type_Int || $type instanceof Database\Type_SmallInt || $type instanceof Database\Type_TinyInt || $override === "int" || $override === "smallint" || $override === "tinyint") {
			if (preg_match(Data\Data::REGEX_PATTERN_INT, $_POST[$field_name])) $_POST[$field_name] = intval($_POST[$field_name]);
		}
		elseif ($type instanceof Database\Type_Float || $override === "float") {
			if (preg_match(Data\Data::REGEX_PATTERN_NUMERIC, $_POST[$field_name])) $_POST[$field_name] = floatval($_POST[$field_name]);
		}
		
		try {
			$type->assert($_POST[$field_name]);
		} catch(Database\TypeMismatchException $e) {
			model_error("Invalid data supplied for {$field}", "edit", "edit.php");
		}
	}

	$data[$field] = $_POST[$field_name];
}

foreach ($_MODEL->get_unique_keys() as $unique) {
	$field = null;

	if ($_MODEL instanceof Models\SecondClass) {
		$firstclass = $_MODEL->get_firstclass_field();
		
		$remainder = array();
		foreach ($unique->fields as $uf) if ($uf !== $firstclass) $remainder[] = $uf;
		
		$firstclass_object = model_get_secondclass_firstclass_data($_MODEL, $id_chain);
		
		$firstclass_structure = $_MODEL->get_firstclass()->get_structure();
		
		$conditions = array($_MODEL->get_firstclass_field() => new Database\Param($firstclass_object[$_MODEL->get_firstclass()->get_id_field()], $firstclass_structure[$_MODEL->get_firstclass()->get_id_field()]));
		$manageable_fields = $_MODEL->get_manageable_fields();
		foreach ($remainder as $uf) {
			if (!in_array($uf, $manageable_fields, true)) $conditions[$uf] = new Database\Param($_MODEL_STRUCTURE[$uf]->default, $_MODEL_STRUCTURE[$uf]);
			else $conditions[$uf] = new Database\Param($data[$uf], $_MODEL_STRUCTURE[$uf]);
		}
		
		$existing = $_DATABASE->select_rows_by_conditions($_DATABASE->view($_MODEL->get_table(), "SECONDCLASS_JOIN"), array(
			$_MODEL->get_id_field() => $_MODEL_STRUCTURE[$_MODEL->get_id_field()]
		), $conditions);
		if (sizeof($existing) > 0) {
			if ($data[$_MODEL->get_id_field()] === "new" || ($data[$_MODEL->get_id_field()] !== $existing[0][$_MODEL->get_id_field()])) model_error("Sorry, an existing {$_POST["model"]} with that {$field} is already used", "edit", "edit.php");
		}
	} elseif ($_MODEL instanceof Models\FirstClass) {
		$conditions = array();
		foreach ($unique->fields as $uf) $conditions[$uf] = new Database\Param($data[$uf], $_MODEL_STRUCTURE[$uf]);
		
		$existing = $_DATABASE->select_rows_by_conditions($_DATABASE->view($_MODEL->get_table(), "SECONDCLASS_JOIN"), array(
			$_MODEL->get_id_field() => $_MODEL_STRUCTURE[$_MODEL->get_id_field()]
		), $conditions);
		if (sizeof($existing) > 0) {
			if ($data[$_MODEL->get_id_field()] === "new" || ($data[$_MODEL->get_id_field()] !== $existing[0][$_MODEL->get_id_field()])) model_error("Sorry, an existing {$_POST["model"]} with that {$field} is already used", "edit", "edit.php");
		}
	}
}

if ($data[$_MODEL->get_id_field()] === "new") {
	// assign default values for any NOT NULL fields that are not explicitly set

	$insert_fields = array();
	foreach (array_keys($_MODEL_STRUCTURE) as $field) {
		if (($_MODEL instanceof Models\FirstClass) && $field === $_MODEL->get_id_field()) continue;
		if (($_MODEL instanceof Models\SecondClass) && $field === $_MODEL->get_firstclass_field()) continue;
		if (($_MODEL instanceof Models\OrientatedOrdered || $_MODEL instanceof Models\Ordered) && $field === "ordered") continue;
		if (!array_key_exists($field, $data) && $_MODEL_STRUCTURE[$field]->not_null) $data[$field] = $_MODEL_STRUCTURE[$field]->default;
	}

	unset($data[$_MODEL->get_id_field()]);
	
	if ($_MODEL instanceof Models\SecondClass) {
		$data = $_MODEL->insert_for_firstclass(model_get_secondclass_firstclass_data($_MODEL, $id_chain), $data);
	} elseif ($_MODEL instanceof Models\FirstClass) {
		$data = $_MODEL->insert($data);
	}
	
	if (method_exists($_MODEL, "post_create_processing")) $_MODEL->post_create_processing($_REPOSITORY, $data);
	
	if ($_MODEL instanceof Framework\Models\User) {
		// users need to be added to the everyone group automatically
		$everyone = $_GROUP->get_everyone($xsi);
		$_ACCESS->add_user_to_group($data, $everyone);
	}
	
	$identify = "-";
	if (array_key_exists("name", $data)) $identify = $data["name"];
	elseif (array_key_exists("description", $data)) $identify = $data["description"];
	
	$_SESSIONMANAGER->log_by_name($_POST["area"] === "_core" ? "admin" : $_POST["area"], "Create new {$_POST["model"]}", $identify);
} else {
	if ($_MODEL instanceof Models\SecondClass) {
		$_MODEL->update_for_firstclass(model_get_secondclass_firstclass_data($_MODEL, $id_chain), $data);
	} elseif ($_MODEL instanceof Models\FirstClass) {
		$_MODEL->update($data);
	}
	
	$identify = "-";
	if (array_key_exists("name", $data)) $identify = $data["name"];
	elseif (array_key_exists("description", $data)) $identify = $data["description"];
	
	$_SESSIONMANAGER->log_by_name($_POST["area"] === "_core" ? "admin" : $_POST["area"], "Edit {$_POST["model"]}", $identify);
}

$rebuild = array();
foreach (explode("&", $_URL_PARAMS) as $param) if (!preg_match("`^id=`i", $param)) $rebuild[] = $param;
$rebuild[] = "chain={$_POST["chain"]}";

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "management/list.php?" . implode("&", $rebuild));
