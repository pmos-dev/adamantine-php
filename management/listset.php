<?php
/**
 * Adamantine StandardField model functionality: change listset
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

if (!$_MODEL instanceof Adamantine\Models\StandardField) Adamantine\error("Attempting to change listset for a non-StandardField model.");
if ($_MODEL->get_listset_model() === null) Adamantine\error("Attempting to change listset for a StandardField model which doesn't support them.");

$_UI->set_titles("Change field listset", $xsi["description"]);

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");
	
if ($_MODEL instanceof Models\SecondClass) {
	$id_chain = array();
	if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
		$id_chain = explode(",", $_GET["chain"]);
		foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
		unset($id);
	}
	
	if (null === ($data = model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such element exists");
} elseif ($_MODEL instanceof Models\FirstClass) {
	if (null === ($data = $_MODEL->get_by_id($_GET["id"]))) Adamantine\error("No such element exists");
} else throw new Adamantine\Exception("This model type is not supported");

$_HTML->add_code("css", <<<CSS

form#listsets input#cancel {
	margin-left: 10px;
}

CSS
);

$_HTML->add_code("jquery", <<<JQUERY

$("input#cancel").click(function() {
	window.location = ADAMANTINE_ROOT_PATH + "management/view.php?{$_URL_PARAMS}&chain={$_GET["chain"]}";
});

JQUERY
);

$page = $_UI->get_content();

$page->add(new HTML\Header("{$_GET["model"]}: {$data["name"]}", HTML\Header::LEVEL_3));

if ($_MODEL->get_listset_model()->get_firstclass_field() !== "installed") \Adamantine\error("Attempting to use the StandardField listset handler for a Model which doesn't have installed as its direct parent. This is not currently possible; you will need to write your own handler.");

$installed = $_SYSTEM->get_installed_by_area($_AREA->get_by_name($_REQUEST["area"] === "_core" ? "admin" : $_REQUEST["area"]));

$listsets = $_MODEL->get_listset_model()->list_by_firstclass($installed);
if (sizeof($listsets) == 0) Adamantine\error("There are no listsets available at this time");

$page->add(new HTML\Header("Changing this field's list set will require all the existing list item data for this field to be wiped!", HTML\Header::LEVEL_4, null, "warning"));

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "management/listset_do.php", HTML\Form::SUBMIT_POST, null, "uniform simple autosize"));

$form->add_hidden("area", $_GET["area"]);
$form->add_hidden("model", $_GET["model"]);
$form->add_hidden("chain", $_GET["chain"]);
$form->add_hidden("id", $data["id"]);
$form->add_hidden("xsi", $xsi["id"]);

$form->add_row("List set", $select = new HTML\Form_Select("listset"), "listset");
foreach ($listsets as $listset) $select->add_option($listset["id"], $listset["name"], $data["listset"] === $listset["id"]);

$form->add_row("Allow multiple", new HTML\Form_CheckBox($data["allow_multiple"], "multiple"), "multiple");

$form->add_submit("Change list set");

$_HTML->complete();
