<?php
/**
 * Adamantine StandardField model functionality: change listset
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

if (!$_MODEL instanceof Adamantine\Models\StandardField) Adamantine\error("Attempting to change listset for a non-StandardField model.");
if ($_MODEL->get_listset_model() === null) Adamantine\error("Attempting to change listset for a StandardField model which doesn't support them.");

$_UI->set_titles("Change field listset", $xsi["description"]);

if (!Data\Data::validate_id($_POST["id"])) Adamantine\error("Invalid ID supplied");
	
if ($_MODEL instanceof Models\SecondClass) {
	$id_chain = array();
	if (isset($_POST["chain"]) && "" !== $_POST["chain"]) {
		$id_chain = explode(",", $_POST["chain"]);
		foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
		unset($id);
	}
	
	if (null === ($data = model_get_secondclass_data($_MODEL, $_POST["id"], $id_chain))) Adamantine\error("No such element exists");
} elseif ($_MODEL instanceof Models\FirstClass) {
	if (null === ($data = $_MODEL->get_by_id($_POST["id"]))) Adamantine\error("No such element exists");
} else throw new Adamantine\Exception("This model type is not supported");

$_HTML->add_code("css", <<<CSS

form#listsets input#cancel {
	margin-left: 10px;
}

CSS
);

$_HTML->add_code("jquery", <<<JQUERY

$("input#cancel").click(function() {
	window.location = ADAMANTINE_ROOT_PATH + "management/view.php?{$_URL_PARAMS}&chain={$_POST["chain"]}";
});

JQUERY
);

$page = $_UI->get_content();

$page->add(new HTML\Header("{$_POST["model"]}: {$data["name"]}", HTML\Header::LEVEL_3));

if ($_MODEL->get_listset_model()->get_firstclass_field() !== "installed") \Adamantine\error("Attempting to use the StandardField listset handler for a Model which doesn't have installed as its direct parent. This is not currently possible; you will need to write your own handler.");

$installed = $_SYSTEM->get_installed_by_area($_AREA->get_by_name($_REQUEST["area"] === "_core" ? "admin" : $_REQUEST["area"]));

if (!Data\Data::validate_id($_POST["listset"])) Adamantine\error("Invalid listset ID supplied");
if (null === ($listset = $_MODEL->get_listset_model()->get_by_firstclass_and_id($installed, $_POST["listset"]))) Adamantine\error("No such listset exists");

$_POST["multiple"] = Data\Data::checkbox_boolean($_POST["multiple"]);

if (!$_MODEL->pre_change_listset_wipe_data($data)) Adamantine\error("The pre-change call failed. ListSet change has been aborted.");

$data["listset"] = $listset["id"];
$data["allow_multiple"] = $_POST["multiple"];
$_MODEL->update($data);

$_SESSIONMANAGER->log_by_name($_POST["area"] === "_core" ? "admin" : $_POST["area"], "Change {$_POST["model"]} listset", "{$data["name"]}: {$listset["name"]} (" . ($_POST["multiple"] ? "multiple" : "singular") . ")");

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "management/view.php?model={$_POST["model"]}&area={$_POST["area"]}&id={$data["id"]}&chain={$_POST["chain"]}&xsi={$xsi["id"]}");
