<?php
namespace Adamantine\Management\AJAX;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

$_ERROR_RENDERER_OVERRIDE = $_JSON;

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

if (!($_MODEL instanceof Models\Hierarchy)) Adamantine\error("This management approach does not work for non-hierarchy models. Please use the regular management instead.");

if (!$_SESSIONMANAGER->has_access_by_name($_GET["area"], $_MODEL->get_access_required_to_manage())) Adamantine\error("You do not have permission to edit this model");

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");

$id_chain = array();
if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
	$id_chain = explode(",", $_GET["chain"]);
	foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
	unset($id);
}

if (null === ($node = \Adamantine\Management\model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such node exists");

if ($node["parent"] === null) Adamantine\error("You cannot move the root node");

if (!isset($_GET["direction"])) Adamantine\error("No direction supplied");
switch ($_GET["direction"]) {
	case "up":
		$_MODEL->move($node, "up");
		break;
	case "down":
		$_MODEL->move($node, "down");
		break;
	case "left":
		$_MODEL->move($node, "left");
		break;
	case "right":
		$_MODEL->move($node, "right");
		break;
	default:
		Adamantine\error("Bad direction");
}

$_SESSIONMANAGER->log_by_name($_GET["area"], "Move {$_GET["model"]}", $node["name"]);

$_JSON->success();
