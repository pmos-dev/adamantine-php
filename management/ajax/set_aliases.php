<?php
namespace Adamantine\Management\AJAX;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

$_ERROR_RENDERER_OVERRIDE = $_JSON;

use \Abstraction\Data as Data;
use \Adamantine as Adamantine;

if (!$_MODEL->is_aliasing_supported()) Adamantine\error("This model does not support aliases");

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");

$id_chain = array();
if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
	$id_chain = explode(",", $_GET["chain"]);
	foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
	unset($id);
}

if (null === ($row = \Adamantine\Management\model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such row exists");

$incoming = array();
foreach (explode(",", $_GET["aliases"]) as $alias) if ("" !== ($alias = trim($alias))) {
	if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $alias)) Adamantine\error("One or more of your aliases is invalid");
	$incoming[strtolower($alias)] = $alias;
}

try {
	$_MODEL->replace_aliases($row, array_values($incoming));
} catch(\Exception $e) {
	Adamantine\error("There was an error setting your aliases");
}

$_JSON->success();
