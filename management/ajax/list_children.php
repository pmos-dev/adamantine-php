<?php
namespace Adamantine\Management\AJAX;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

$_ERROR_RENDERER_OVERRIDE = $_JSON;

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

if (!($_MODEL instanceof Models\Hierarchy)) Adamantine\error("This management approach does not work for non-hierarchy models. Please use the regular management instead.");

if (!Data\Data::validate_id($_GET["parent"])) Adamantine\error("Invalid parent ID supplied");

$id_chain = array();
if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
	$id_chain = explode(",", $_GET["chain"]);
	foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
	unset($id);
}

if (null === ($parent = \Adamantine\Management\model_get_secondclass_data($_MODEL, $_GET["parent"], $id_chain))) Adamantine\error("No such node exists");

$firstclass = \Adamantine\Management\model_get_secondclass_firstclass_data($_MODEL, $id_chain);

$_JSON->success(Data\Data::extract_ids_from_id_object_array($_MODEL->list_children_ordered_by_firstclass($firstclass, $parent)));
