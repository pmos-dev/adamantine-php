<?php
namespace Adamantine\Management\AJAX;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

$_ERROR_RENDERER_OVERRIDE = $_JSON;

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

if (!($_MODEL instanceof Models\Hierarchy)) Adamantine\error("This management approach does not work for non-hierarchy models. Please use the regular management instead.");

if (!$_SESSIONMANAGER->has_access_by_name($_GET["area"], $_MODEL->get_access_required_to_manage())) Adamantine\error("You do not have permission to edit this model");

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");

$id_chain = array();
if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
	$id_chain = explode(",", $_GET["chain"]);
	foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
	unset($id);
}

if (null === ($node = \Adamantine\Management\model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such node exists");

if ($node["parent"] === null) Adamantine\error("You cannot edit the root node");

if (!isset($_GET["name"])) Adamantine\error("No code name supplied");
if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $_GET["name"])) Adamantine\error("Invalid name supplied");

$firstclass = \Adamantine\Management\model_get_secondclass_firstclass_data($_MODEL, $id_chain);

$existing = $_MODEL->get_by_name($firstclass, $_GET["name"]);
if ($existing !== null) if ($existing["id"] !== $node["id"]) Adamantine\error("An entry with that name already exists");

$node["name"] = $_GET["name"];

foreach ($_MODEL->get_manageable_fields() as $field) {
	if ($field === "name") continue;

	$type = $_MODEL_STRUCTURE[$field];
	$field_name = "field_" . Data\Data::hex_encode($field);

	if ($type instanceof Database\Type_Bool) $_GET[$field_name] = Data\Data::checkbox_boolean($_GET[$field_name]);
	if ($type instanceof Database\Type_DateTime && Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_DMYHI, $_GET[$field_name])) $_GET[$field_name] = Data\Data::datetime_dmyhi_to_ymdhis($_GET[$field_name]);
	if ($type instanceof Database\Type_Date && Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_DMY, $_GET[$field_name])) $_GET[$field_name] = Data\Data::date_dmy_to_ymd($_GET[$field_name]);
	
	if ((!$type->not_null) && (!isset($_GET[$field_name]) || "" === $_GET[$field_name])) $_GET[$field_name] = null;
	else {
		if ($type instanceof Database\Type_Int || $type instanceof Database\Type_SmallInt || $type instanceof Database\Type_TinyInt) {
			if (preg_match(Data\Data::REGEX_PATTERN_INT, $_GET[$field_name])) $_GET[$field_name] = intval($_GET[$field_name]);
		}

		try {
			$type->assert($_GET[$field_name]);
		} catch(Database\TypeMismatchException $e) {
			Adamantine\error("Invalid data supplied for {$field}");
		}
	}

	$node[$field] = $_GET[$field_name];
}

if (!$_MODEL->update_for_firstclass($firstclass, $node)) Adamantine\error("Unable to update entry");

$_SESSIONMANAGER->log_by_name($_GET["area"], "Edit {$_GET["model"]}", $node["name"]);

$_JSON->success($node["name"]);
