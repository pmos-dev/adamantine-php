<?php
namespace Adamantine\Management\AJAX;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

$_ERROR_RENDERER_OVERRIDE = $_JSON;

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

if (!($_MODEL instanceof Models\Hierarchy)) Adamantine\error("This management approach does not work for non-hierarchy models. Please use the regular management instead.");

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");

$id_chain = array();
if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
	$id_chain = explode(",", $_GET["chain"]);
	foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
	unset($id);
}

if (null === ($node = \Adamantine\Management\model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such node exists");

$firstclass = \Adamantine\Management\model_get_secondclass_firstclass_data($_MODEL, $id_chain);

$supplimentary = array();
foreach ($_MODEL->get_manageable_fields() as $field) if ($field !== "name") $supplimentary["field_" . Data\Data::hex_encode($field)] = $node[$field];

$_JSON->success(array("id" => $node["id"], "name" => $node["name"], "has_children" => sizeof($_MODEL->list_children_ordered_by_firstclass($firstclass, $node)) > 0, "supplimentary" => $supplimentary));
