<?php
namespace Adamantine\Management\AJAX;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

$_ERROR_RENDERER_OVERRIDE = $_JSON;

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

if (!($_MODEL instanceof Models\Hierarchy)) Adamantine\error("This management approach does not work for non-hierarchy models. Please use the regular management instead.");

if (!$_SESSIONMANAGER->has_access_by_name($_GET["area"], $_MODEL->get_access_required_to_manage())) Adamantine\error("You do not have permission to edit this model");

if (!Data\Data::validate_id($_GET["parent"])) Adamantine\error("Invalid parent ID supplied");

$id_chain = array();
if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
	$id_chain = explode(",", $_GET["chain"]);
	foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
	unset($id);
}

if (null === ($parent = \Adamantine\Management\model_get_secondclass_data($_MODEL, $_GET["parent"], $id_chain))) Adamantine\error("No such node exists");

if (!isset($_GET["name"])) Adamantine\error("No name supplied");
if (!Data\Data::validate_regex(Data\Data::REGEX_PATTERN_ID_NAME, $_GET["name"])) Adamantine\error("Invalid name supplied");

$firstclass = \Adamantine\Management\model_get_secondclass_firstclass_data($_MODEL, $id_chain);

if (null !== $_MODEL->get_by_name($firstclass, $_GET["name"])) Adamantine\error("An entry with that name already exists");
$data = array("name" => $_GET["name"]);

foreach ($_MODEL->get_manageable_fields() as $field) {
	if ($field === "name") continue;
	
	$type = $_MODEL_STRUCTURE[$field];
	$field_name = "field_" . Data\Data::hex_encode($field);
	
	if ($type instanceof Database\Type_Bool) $_GET[$field_name] = Data\Data::checkbox_boolean($_GET[$field_name]);
	if ($type instanceof Database\Type_DateTime && Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATETIME_DMYHI, $_GET[$field_name])) $_GET[$field_name] = Data\Data::datetime_dmyhi_to_ymdhis($_GET[$field_name]);
	if ($type instanceof Database\Type_Date && Data\Data::validate_regex(Data\Data::REGEX_PATTERN_DATE_DMY, $_GET[$field_name])) $_GET[$field_name] = Data\Data::date_dmy_to_ymd($_GET[$field_name]);

	if ((!$type->not_null) && (!isset($_GET[$field_name]) || "" === $_GET[$field_name])) $_GET[$field_name] = null;
	else {
		if ($type instanceof Database\Type_Int || $type instanceof Database\Type_SmallInt || $type instanceof Database\Type_TinyInt) {
			if (preg_match(Data\Data::REGEX_PATTERN_INT, $_GET[$field_name])) $_GET[$field_name] = intval($_GET[$field_name]);
		}

		try {
			$type->assert($_GET[$field_name]);
		} catch(Database\TypeMismatchException $e) {
			Adamantine\error("Invalid data supplied for {$field}");
		}
	}

	$data[$field] = $_GET[$field_name];
}

foreach ($_MODEL->get_unique_keys() as $unique) {
	$field = null;

	$firstclass_field = $_MODEL->get_firstclass_field();

	$remainder = array();
	foreach ($unique->fields as $uf) if ($uf !== $firstclass_field) $remainder[] = $uf;

	$firstclass_structure = $_MODEL->get_firstclass()->get_structure();

	$conditions = array($_MODEL->get_firstclass_field() => new Database\Param($firstclass[$_MODEL->get_firstclass()->get_id_field()], $firstclass_structure[$_MODEL->get_firstclass()->get_id_field()]));
	foreach ($remainder as $uf) $conditions[$uf] = new Database\Param($data[$uf], $_MODEL_STRUCTURE[$uf]);

	$existing = $_DATABASE->select_rows_by_conditions($_DATABASE->view($_MODEL->get_table(), "SECONDCLASS_JOIN"), array(
		$_MODEL->get_id_field() => $_MODEL_STRUCTURE[$_MODEL->get_id_field()]
	), $conditions);
	if (sizeof($existing) > 0) {
		if ($data[$_MODEL->get_id_field()] === "new" || ($data[$_MODEL->get_id_field()] !== $existing[0][$_MODEL->get_id_field()])) Adamantine\error("Sorry, an existing {$_GET["model"]} with that {$field} is already used");
	}
}

// assign default values for any NOT NULL fields that are not explicitly set

$insert_fields = array();
foreach (array_keys($_MODEL_STRUCTURE) as $field) {
	if ($field === $_MODEL->get_id_field()) continue;
	if ($field === $_MODEL->get_firstclass_field()) continue;
	if ($field === "ordered") continue;
	if (!array_key_exists($field, $data) && $_MODEL_STRUCTURE[$field]->not_null) $data[$field] = $_MODEL_STRUCTURE[$field]->default;
}

unset($data[$_MODEL->get_id_field()]);

if (null === ($node = $_MODEL->insert_for_firstclass_and_parent($firstclass, $parent, $data))) Adamantine\error("Unable to create new node");

$_SESSIONMANAGER->log_by_name($_GET["area"], "Add {$_GET["model"]}", $node["name"]);

$_JSON->success($node["id"]);
