<?php
namespace Adamantine\Management\AJAX;

define("APP_ROOT_PATH", "./../../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

$_ERROR_RENDERER_OVERRIDE = $_JSON;

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

if (!($_MODEL instanceof Models\Hierarchy)) Adamantine\error("This management approach does not work for non-hierarchy models. Please use the regular management instead.");

if (!$_SESSIONMANAGER->has_access_by_name($_GET["area"], $_MODEL->get_access_required_to_manage())) Adamantine\error("You do not have permission to edit this model");

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");

$id_chain = array();
if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
	$id_chain = explode(",", $_GET["chain"]);
	foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
	unset($id);
}

if (null === ($node = \Adamantine\Management\model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such node exists");

if ($node["parent"] === null) Adamantine\error("You cannot delete the root node");

$firstclass = \Adamantine\Management\model_get_secondclass_firstclass_data($_MODEL, $id_chain);

$children = $_MODEL->list_children_ordered_by_firstclass($firstclass, $node);
if (sizeof($children) > 0) Adamantine\error("For safety, you cannot delete nodes which still have children");

if (null === ($parent = $_MODEL->get_by_firstclass_and_id($firstclass, $node["parent"]))) Adamantine\error("There was an error looking up the parent node");

if (!$_MODEL->delete_for_firstclass_and_parent($firstclass, $parent, $node)) Adamantine\error("Unable to delete node");

$_SESSIONMANAGER->log_by_name($_GET["area"], "Delete {$_GET["model"]}", $node["name"]);

$_JSON->success();
