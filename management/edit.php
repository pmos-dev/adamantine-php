<?php
/**
 * Adamantine managed model functionality: edit a model row.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

if ($_MODEL instanceof Models\Hierarchy) Adamantine\error("This management approach does not work for hierarchy models. Please use the AJAX tree management instead.");
if (isset($_GET["id"]) && $_GET["id"] === "new") {
	$_UI->set_titles("Add new {$_GET["model"]}", $xsi["description"]);

	$data = array();
	foreach ($_MODEL_STRUCTURE as $name => $type) $data[$name] = $type->default;
	$data[$_MODEL->get_id_field()] = "new";
} else {
	$_UI->set_titles("Edit {$_GET["model"]}", $xsi["description"]);

	if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");
	
	if ($_MODEL instanceof Models\SecondClass) {
		$id_chain = array();
		if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
			$id_chain = explode(",", $_GET["chain"]);
			foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
			unset($id);
		}
		
		if (null === ($data = model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such element exists");
	} elseif ($_MODEL instanceof Models\FirstClass) {
		if (null === ($data = $_MODEL->get_by_id($_GET["id"]))) Adamantine\error("No such element exists");
	} else throw new Adamantine\Exception("This model type is not supported");
}

if (!$_MODEL->is_object_editable($data)) Adamantine\error("Sorry, you cannot edit this item");

$_HTML->add_code("css", <<<CSS

form#details input#cancel {
	margin-left: 10px;
}

CSS
);

$method = "list";
if ($data[$_MODEL->get_id_field()] !== "new") if (null !== $_MODEL->get_view_additional_file()) $method = "view";

$_HTML->add_code("jquery", <<<JQUERY

$("input#cancel").click(function() {
	window.location = ADAMANTINE_ROOT_PATH + "management/{$method}.php?{$_URL_PARAMS}&chain={$_GET["chain"]}";
});

JQUERY
);

$has_hexcolor = false;
foreach ($_MODEL_STRUCTURE as $name => $type) {
	if ($type instanceof Database\Type_HexColor) $has_hexcolor = true;
}

if ($has_hexcolor) {
	$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/jquery.colorpicker.css");
	$_HTML->add_include("js", ADAMANTINE_ROOT_PATH . "js/jquery.colorpicker.js");
	
	$_HTML->add_code("jquery", <<<JQUERY

$("input.hexcolor").colorpicker({
	"alpha": false,
	"colorFormat": "HEX"
});

$("#clearcolor").click(function() {
	$(this).prev("input.hexcolor").val("");
	return false;
});

JQUERY
	);
}

$has_datetime = $has_date = false;
foreach ($_MODEL_STRUCTURE as $name => $type) {
	if ($type instanceof Database\Type_DateTime) $has_datetime = true;
	if ($type instanceof Database\Type_Date) $has_date = true;
}

if ($has_datetime) {
	$_HTML->add_code("jquery", <<<JQUERY

$("input.datetime").datetimepicker({
	"dateFormat": "dd/mm/yy",
	"hourGrid": 4,
	"showOn": "button",
	"buttonImage": ADAMANTINE_ROOT_PATH + "css/icons/calendar_view_month.svg",
	"buttonImageOnly": true
});
			
JQUERY
	);
}

if ($has_date) {
	$_HTML->add_code("jquery", <<<JQUERY

$("input.date").datepicker({
	"dateFormat": "dd/mm/yy",
	"showOn": "button",
	"buttonImage": ADAMANTINE_ROOT_PATH + "css/icons/calendar_view_month.svg",
	"buttonImageOnly": true
});
			
JQUERY
	);
}

$page = $_UI->get_content();

$page->add(new HTML\Header("{$_GET["model"]} details", HTML\Header::LEVEL_3));

$page->add($form = new HTML\Form(ADAMANTINE_ROOT_PATH . "management/edit_do.php", "POST", "details", "uniform autosize"));
$form->add_hidden("area", $_GET["area"]);
$form->add_hidden("model", $_GET["model"]);
$form->add_hidden("id", $data["id"]);
$form->add_hidden("chain", $_GET["chain"]);
$form->add_hidden("xsi", $xsi["id"]);

foreach ($_MODEL->get_manageable_fields() as $field) {
	$type = $_MODEL_STRUCTURE[$field];
	$field_name = "field_" . Data\Data::hex_encode($field);
	
	$optional = $type->not_null ? "" : "optional";
	
	$override = null;
	if (method_exists($_MODEL, "encrypted_field_type_override")) $override = $_MODEL->encrypted_field_type_override($field);
	
	if ($type instanceof Database\Type_HexColor || $override === "hexcolor") {
		$element = new HTML\MultiElements(array(
			"#",
			new HTML\Form_Text($data[$field], $field_name, "validate {$optional} hexcolor"),
			new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/cross.svg", "Clear colour", "#", "clearcolor")
		));
	} elseif ($type instanceof Database\Type_IdName || $override === "idname") {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} idname");
	} elseif ($type instanceof Database\Type_Email || $override === "email") {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} email");
	} elseif ($type instanceof Database\Type_URL || $override === "url") {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} url");
	} elseif ($type instanceof Database\Type_IdChar || $override === "idchar") {
		$element = new HTML\Form_Select($field_name);
		if (!$type->not_null) $element->add_option("", "", $data[$field] === null);
		for ($i = 0; $i < 26; $i++) {
			$element->add_option(chr(65 + $i), chr(65 + $i), $data[$field] === chr(65 + $i));
		}
		for ($i = 0; $i < 10; $i++) {
			$element->add_option($i, $i, $data[$field] === "{$i}");
		}
	} elseif ($type instanceof Database\Type_Int || $override === "int") {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} int");
	} elseif ($type instanceof Database\Type_SmallInt || $override === "smallint") {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} smallint");
	} elseif ($type instanceof Database\Type_TinyInt || $override === "tinyint") {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} tinyint");
	} elseif ($type instanceof Database\Type_Float || $override === "float") {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} float");
	} elseif ($type instanceof Database\Type_Enum || $override === "enum") {
		$element = new HTML\Form_Select($field_name);
		if (!$type->not_null) $element->add_option("", "", $data[$field] === null);
		foreach ($type->get_options() as $option) $element->add_option($option, $option, $data[$field] === $option);
	} elseif ($type instanceof Database\Type_String || $override === "string") {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} string");
	} elseif ($type instanceof Database\Type_DateTime || $override === "datetime") {
		$element = new HTML\Form_Text(Data\Data::datetime_ymdhis_to_dmyhi($data[$field]), $field_name, "validate {$optional} datetime");
	} elseif ($type instanceof Database\Type_Date || $override === "date") {
		$element = new HTML\Form_Text(Data\Data::date_ymd_to_dmy($data[$field]), $field_name, "validate {$optional} date");
	} elseif ($type instanceof Database\Type_Encrypted && $override === null) {
		$element = new HTML\Form_Text($data[$field], $field_name, "validate {$optional} string");
	} elseif ($type instanceof Database\Type_Text || $override === "text") {
		$element = new HTML\Form_TextArea($data[$field], $field_name, "validate {$optional} text");
	} elseif ($type instanceof Database\Type_Bool || $override === "bool") {
		if ($type->not_null) $element = new HTML\Form_Checkbox($data[$field], $field_name);
		else {
			$element = new HTML\Form_Select($field_name);
			if (!$type->not_null) $element->add_option("", "", $data[$field] === null);
			$element->add_option("yes", "yes", $data[$field] === true);
			$element->add_option("no", "no", $data[$field] === false);
		}
	} elseif ($type instanceof Database\Type_Id) {
		// ignore any superflouous IDs
		continue;
	} else {
		print_r($type);
		die("Unknown type");
	}

	$description = $_MODEL->get_field_description($field);
	if (null !== ($suffix = $_MODEL->get_field_suffix($field))) $element = new HTML\MultiElements(array($element, new HTML\Span($suffix, null, "suffix")));
	
	$form->add_row($description, $element, $field_name);
}

$form->add_submit($data["id"] === "new" ? "Add" : "Save changes");
$form->add_button("Cancel", HTML\Form::BUTTON_OTHER, "cancel");

if ($_GET["id"] !== "new") {
	$page->add(HTML\LinkBar::singular(new HTML\Link("Delete this {$_GET["model"]}", ADAMANTINE_ROOT_PATH . "management/delete_do.php?{$_URL_PARAMS}&chain={$_GET["chain"]}", null, "command icon_delete")));
}

$_HTML->complete();
