<?php
/**
 * Adamantine managed model functionality: display hierarchy model as tree for current session instance.
 * 
 * NB, view_additional is not supported for this method at the moment, and neither is editing other than the name. This may need to be changed in the future.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

$_UI->set_titles("{$_GET["model"]} management", $xsi["description"]);

if (!($_MODEL instanceof \Abstraction\Models\Hierarchy)) Adamantine\error("This model cannot be managed as a list.");

$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/treeable.css");
$_HTML->add_include("js", ADAMANTINE_ROOT_PATH . "js/treeable.js");

$page = $_UI->get_content();

//-----------------------------------------------------------------------------
// Editing and add entry form

$has_datetime = $has_date = false;
foreach ($_MODEL_STRUCTURE as $name => $type) {
	if ($type instanceof Database\Type_DateTime) $has_datetime = true;
	if ($type instanceof Database\Type_Date) $has_date = true;
}

if ($has_datetime) {
	$_HTML->add_code("jquery", <<<JQUERY

$("input.datetime").datetimepicker({
	"dateFormat": "dd/mm/yy",
	"hourGrid": 4,
	"showOn": "button",
	"buttonImage": ADAMANTINE_ROOT_PATH + "css/icons/calendar_view_month.svg",
	"buttonImageOnly": true
});
			
JQUERY
	);
}

$editor = new HTML\Div($form = new HTML\Form(null, "GET", "editor", "validate uniform autosize simple"), null, "editor");
foreach ($_MODEL->get_manageable_fields() as $field) {
	$type = $_MODEL_STRUCTURE[$field];
	if ($field === "name") $field_name = "name";
	else $field_name = "field_" . Data\Data::hex_encode($field);
	
	$optional = $type->not_null ? "" : "optional";
	
	if ($type instanceof Database\Type_HexColor) {
		Adamantine\error("Not implemented yet");
	} elseif ($type instanceof Database\Type_IdName) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} idname");
	} elseif ($type instanceof Database\Type_Email) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} email");
	} elseif ($type instanceof Database\Type_URL) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} url");
	} elseif ($type instanceof Database\Type_IdChar) {
		Adamantine\error("Not implemented yet");
	} elseif ($type instanceof Database\Type_Int) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} int");
	} elseif ($type instanceof Database\Type_SmallInt) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} smallint");
	} elseif ($type instanceof Database\Type_TinyInt) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} tinyint");
	} elseif ($type instanceof Database\Type_Enum) {
		$element = new HTML\Form_Select($field_name);
		foreach ($type->get_options() as $option) $element->add_option($option, $option);
	} elseif ($type instanceof Database\Type_String) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} string");
	} elseif ($type instanceof Database\Type_DateTime) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} datetime");
	} elseif ($type instanceof Database\Type_Date) {
		$element = new HTML\Form_Text("", $field_name, "validate {$optional} date");
	} elseif ($type instanceof Database\Type_Encrypted) {
		Adamantine\error("Not implemented yet");
	} elseif ($type instanceof Database\Type_Text) {
		Adamantine\error("Not implemented yet");
	} elseif ($type instanceof Database\Type_Bool) {
		$element = new HTML\Form_Checkbox(false, $field_name);
	} elseif ($type instanceof Database\Type_Id) {
		// ignore any superflouous IDs
		continue;
	} else {
		print_r($type);
		die("Unknown type");
	}

	$description = $_MODEL->get_field_description($field);
	if (null !== ($suffix = $_MODEL->get_field_suffix($field))) $element = new HTML\MultiElements(array($element, new HTML\Span($suffix, null, "suffix")));
	
	$form->add_row($description, $element, $field_name);
}

//-----------------------------------------------------------------------------

$page->add(new HTML\Header("{$_GET["model"]} tree", HTML\Header::LEVEL_3));

$page->add($linkbar = new HTML\LinkBar(null, "structureall"));
$linkbar->add(new HTML\Link("Expand all", "#", null, "expandall command icon_expand"));
$linkbar->add(new HTML\Link("Collapse all", "#", null, "collapseall command icon_collapse"));

$page->add($list = new HTML\ItemList(HTML\ItemList::UNORDERED, null, "treeable"));
$page->add($editor);

$id_chain = array();
if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
	$id_chain = explode(",", $_GET["chain"]);
	foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
	unset($id);
}
$firstclass_object = model_get_secondclass_firstclass_data($_MODEL, $id_chain);
$root = $_MODEL->get_root_by_firstclass($firstclass_object);

$list->new_item(new HTML\MultiElements(array(
	new HTML\Form_Hidden($_GET["area"], null, "area"),
	new HTML\Form_Hidden($_GET["model"], null, "model"),
	new HTML\Form_Hidden($_GET["chain"], null, "chain"),
	new HTML\Form_Hidden($_MODEL->is_aliasing_supported() ? 1 : 0, null, "is_aliasing_supported"),
	new HTML\Form_Hidden($root["id"], null, "id")
)), null, "root");

$_HTML->complete();
