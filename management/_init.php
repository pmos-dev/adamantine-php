<?php
/**
 * Adamantine managed model functionality: (initialisation script)
 * 
 * This initialiser ensures that the supplied model is managable, installed, accessible, and that the current user has permission to manage it.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

if (!defined("APP_ROOT_PATH")) die();	// check the script isn't being called directly
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Framework as Framework;
use \Abstraction\Models as Models;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

$_UI->set_titles("Management", $xsi["description"]);
if (!Data\Data::validate_regex("`^[a-z0-9_]{1,32}$`iD", $_REQUEST["area"])) Adamantine\error("You did not supply a valid area");
if (!$_REPOSITORY->does_area_exist($_REQUEST["area"])) Adamantine\error("The supplied area does not exist, or maybe you do not have access. This can happen if you have been logged out.");

// does the user even have read access to the area in question?
if ($_REQUEST["area"] === "_core") {
	if (!$_SESSIONMANAGER->has_access_by_name("admin", "read")) Adamantine\error("You do not have permission to access this area");
} else {
	if (!$_SESSIONMANAGER->has_access_by_name($_REQUEST["area"], "read")) Adamantine\error("You do not have permission to access this area");
}

if (!Data\Data::validate_regex("`^[a-z0-9_]{1,32}$`iD", $_REQUEST["model"])) Adamantine\error("You did not supply a valid model");
if (!$_REPOSITORY->does_model_exist($_REQUEST["area"], $_REQUEST["model"])) Adamantine\error("The supplied model does not exist");

$_MODEL = $_REPOSITORY->get($_REQUEST["area"], $_REQUEST["model"]);

if (!($_MODEL instanceof Adamantine\Models\Manageable)) Adamantine\error("This model is not set up for management.");
if (!$_MODEL->is_model_manageable()) Adamantine\error("This model does not allow management.");

if ($_REQUEST["area"] === "_core") {
	if (!$_SESSIONMANAGER->has_access_by_name("admin", $_MODEL->get_access_required_to_manage())) Adamantine\error("You do not have permission to access this area");
} else {
	if (!$_SESSIONMANAGER->has_access_by_name($_REQUEST["area"], $_MODEL->get_access_required_to_manage())) Adamantine\error("You do not have permission to access this area");
}

$_MODEL_STRUCTURE = $_MODEL->get_structure();

$_URL_PARAMS = "model={$_REQUEST["model"]}&xsi={$xsi["id"]}";
if (isset($_REQUEST["area"])) $_URL_PARAMS .= "&area={$_REQUEST["area"]}";
if (isset($_REQUEST["id"])) $_URL_PARAMS .= "&id={$_REQUEST["id"]}";

function model_error($message, $back_message = null, $back_script = null) {
	global $_URL_PARAMS;

	if (isset($_REQUEST["chain"])) $_URL_PARAMS .= "&chain={$_REQUEST["chain"]}";
	
	$append = null;
	if ($back_message !== null) $append = new HTML\Link("Back to {$back_message} {$_REQUEST["model"]}", ADAMANTINE_ROOT_PATH . "management/{$back_script}?{$_URL_PARAMS}", null, "command icon_revert");
	Adamantine\error($message, $append);
}

function model_get_secondclass_firstclass_data(Models\SecondClass $secondclass, array $id_chain) {
	global $_SYSTEM, $_AREA, $xsi;
	
	$firstclass = $secondclass->get_firstclass();

	// step downwards through the model chain back to Installed or Instance
	$model_chain = array();
	while (true) {
		if ($firstclass instanceof Framework\Models\Installed || $firstclass instanceof Framework\Models\Instance) break;
		
		$model_chain[] = $firstclass;
		$firstclass = $firstclass->get_firstclass();
	}
	
	if (sizeof($model_chain) !== sizeof($id_chain)) {
		throw new Adamantine\Exception("Model chain and firstclass ID chain lengths are not equal: " . sizeof($model_chain) . " " . sizeof($id_chain));
	}
	
	if ($firstclass instanceof Framework\Models\Installed) {
		$last = $_SYSTEM->get_installed_by_area($_AREA->get_by_name($_REQUEST["area"] === "_core" ? "admin" : $_REQUEST["area"]));
	} else if ($firstclass instanceof Framework\Models\Instance) {
		$last = $xsi;
	}

	// step upwards through the model chain back back to the top
	for ($i = sizeof($model_chain); $i-- > 0;) {
		if (null === ($last = $model_chain[$i]->get_by_firstclass_and_id($last, $id_chain[$i]))) Adamantine\error("No such element exists within the chain");
	}
	
	return $last;
}

function model_get_secondclass_data(Models\SecondClass $secondclass, $id, array $id_chain) {
	return $secondclass->get_by_firstclass_and_id(model_get_secondclass_firstclass_data($secondclass, $id_chain), $id);
}

if (!array_key_exists("chain", $_REQUEST)) $_GET["chain"] = $_POST["chain"] = $_REQUEST["chain"] = "";
