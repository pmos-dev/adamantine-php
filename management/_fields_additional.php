<?php
/**
 * Adamantine standard model functionality: field additional model viewing.
 * 
 * @copyright 2015 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

use \Abstraction\Renderer\HTML as HTML;

if ($data["type"] === \Adamantine\Models\StandardField::LISTSET) {
	if ($_MODEL->get_listset_model()->get_firstclass_field() !== "installed") \Adamantine\error("Attempting to use the StandardField additional handler for a Model which doesn't have installed as its direct parent. This is not currently possible; you will need to write your own handler.");
	
	$installed = $_SYSTEM->get_installed_by_area($_AREA->get_by_name($_REQUEST["area"] === "_core" ? "admin" : $_REQUEST["area"]));
	
	$page->add(new HTML\Header("Associated list set", HTML\Header::LEVEL_3));
	
	if ($data["listset"] === null) {	
		$page->add(new HTML\Header("(none)", HTML\Header::LEVEL_4));
	} else {
		$listset = $_MODEL->get_listset_model()->get_by_firstclass_and_id($installed, $data["listset"]);
		$page->add(new HTML\Header($listset["name"] . " (" . ($data["allow_multiple"] ? "multiple" : "singular") . ")", HTML\Header::LEVEL_4));
	}
	
	$page->add($linkbar = new HTML\LinkBar());
	$linkbar->add(new HTML\Link("Change list set", ADAMANTINE_ROOT_PATH . "management/listset.php?model={$_GET["model"]}&area={$_GET["area"]}&id={$_GET["id"]}&chain={$_GET["chain"]}&xsi={$_GET["xsi"]}", null, "command icon_edit"));
}
