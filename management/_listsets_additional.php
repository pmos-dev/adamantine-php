<?php
/**
 * Adamantine standard model functionality: listset additional model viewing for listitems.
 * 
 * @copyright 2015 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

use \Abstraction\Renderer\HTML as HTML;

$page->add(new HTML\Header("List items", HTML\Header::LEVEL_3));

$page->add($list = new HTML\ItemList(HTML\ItemList::UNORDERED, null, "unilist"));

foreach ($_MODEL->get_listitem_model()->list_ordered_by_firstclass($data) as $item) {
	$list->new_item($item["name"]);
}

$page->add($linkbar = new HTML\LinkBar());
$linkbar->add(new HTML\Link("Edit items", ADAMANTINE_ROOT_PATH . "management/list.php?area={$_GET["area"]}&model=" . $_MODEL->get_listitem_model_repository_name() . "&chain={$data["id"]}" . ($_GET["chain"] === "" ? "" : ",{$_GET["chain"]}") . "&xsi={$xsi["id"]}", null, "command icon_edit"));
