<?php
/**
 * Adamantine managed model functionality: list model rows for the current session instance.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

$_UI->set_titles("{$_GET["model"]} management", $xsi["description"]);

if ($_MODEL instanceof Models\Hierarchy) Adamantine\error("This management approach does not work for hierarchy models. Please use the AJAX tree management instead.");

$page = $_UI->get_content();

$page->add($table = new HTML\Table($_GET["model"], "unitable autoscroll model " . ((($_MODEL instanceof Models\OrientatedOrdered) || ($_MODEL instanceof Models\Ordered)) ? "moveable" : "")));

$tabular_display_fields = $_MODEL->get_manageable_fields();
if (method_exists($_MODEL, "get_tabular_displayable_fields")) {
	$tabular_display_fields = $_MODEL->get_tabular_displayable_fields();
}

$has_hexcolor = false;
foreach ($tabular_display_fields as $field) {
	$type = $_MODEL_STRUCTURE[$field];
	$description = $_MODEL->get_field_description($field);

	if ($type instanceof Database\Type_Bool || $type instanceof Database\Type_IdChar) {
		$table->add_column($field, $description, "center");
	} else {
		$table->add_column($field, $description);
	}
	if ($type instanceof Database\Type_HexColor) $has_hexcolor = true;
}
if ($has_hexcolor) $_HTML->add_code("css", <<<CSS
	
div.colorblock {
	border: 1px solid black;
	width: 50px;
	height: 10px;
}
	
CSS
);

if (($_MODEL instanceof Models\OrientatedOrdered) || ($_MODEL instanceof Models\Ordered)) {
	$table->add_column("_movement", "", "moveable");

	$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/moveable.css");
	$_HTML->add_include("jquery", ADAMANTINE_ROOT_PATH . "js/moveable.js");
}

// this has to come after the moveable JS code, as moveable constructs dynamic table elements
$_HTML->add_include("css", ADAMANTINE_ROOT_PATH . "css/autoscroll.css");
$_HTML->add_include("js", ADAMANTINE_ROOT_PATH . "js/autoscroll.js");

$back_link = null;

if ($_MODEL instanceof Models\SecondClass) {
	$id_chain = array();
	if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
		$id_chain = explode(",", $_GET["chain"]);
		foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
		unset($id);
	}
	
	$firstclass_object = model_get_secondclass_firstclass_data($_MODEL, $id_chain);
	
	if ($_MODEL instanceof Models\OrientatedOrdered) $data = $_MODEL->list_ordered_by_firstclass($firstclass_object);
	else $data = $_MODEL->list_by_firstclass($firstclass_object);
	
	$firstclass_model = $_MODEL->get_firstclass();
	if ($firstclass_model instanceof Adamantine\Models\Manageable && $_MODEL->is_model_manageable()) {
		$id_chain = explode(",", $_GET["chain"]);
		array_shift($id_chain);
		$id_chain = implode(",", $id_chain);
		
		$name = $firstclass_model->get_repository_name();
		if (array_key_exists("name", $firstclass_object)) $name = $firstclass_object["name"];
		
		if (null !== $firstclass_model->get_view_additional_file() && $firstclass_model->is_object_viewable($firstclass_object)) $back_link = new HTML\Link("Back to {$name}", ADAMANTINE_ROOT_PATH . "management/view.php?area={$_GET["area"]}&model={$firstclass_model->get_repository_name()}&id={$firstclass_object["id"]}&chain={$id_chain}&xsi={$xsi["id"]}", null, "command icon_go");
	}
} elseif ($_MODEL instanceof Models\FirstClass) {
	if ($_MODEL instanceof Models\Ordered) $data = $_MODEL->list_ordered();
	else $data = $_MODEL->list();
} else throw new Adamantine\Exception("This model type is not supported");

foreach ($data as $element) {
	$row = $table->new_row();

	$first = true;
	foreach ($tabular_display_fields as $field) {
		$type = $_MODEL_STRUCTURE[$field];
		$value = $element[$field];
		
		if ($type instanceof Database\Type_HexColor) {
			if ($value === null) $value = "(none)";
			else {
				$div = new HTML\Div("", null, "colorblock");
				$div->add_param("style", "background:#{$value}");
				$value = $div;
			}
		} elseif ($type instanceof Database\Type_String) {
			$value = "{$value}";
		} elseif ($type instanceof Database\Type_DateTime) {
			$value = Data\Data::pretty_datetime($value);
		} elseif ($type instanceof Database\Type_Date) {
			$value = Data\Data::pretty_date($value);
		} elseif ($type instanceof Database\Type_Encrypted) {
			$value = "{$value}";
		} elseif ($type instanceof Database\Type_Text) {
			$value = "{$value}";
			if (strlen($value) > 64) $value = new HTML\MultiElements(array(substr($value, 0, 64), new HTML\Raw("&hellip;")));
		} elseif ($type instanceof Database\Type_Int || $type instanceof Database\Type_SmallInt || $type instanceof Database\Type_TinyInt || $type instanceof Database\Type_Float) {
			$value = "{$value}";
		} elseif ($type instanceof Database\Type_Enum) {
			$value = $value === null ? "-" : "{$value}";
		} elseif ($type instanceof Database\Type_Bool) {
			if ($value === null) $value = new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/bullet_white.svg", "Yes");
			else $value = $value ? new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/tick.svg", "Yes") : new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/cross.svg", "No");
		} else { 
			print_r($type); 
			die("Unknown type:"); 
		}
		
		if ($first) {
			$first = false;

			if (null !== ($path = $_MODEL->get_view_additional_file())) {
				if ($_MODEL->is_object_viewable($element)) $value = new HTML\Link($value, ADAMANTINE_ROOT_PATH . "management/view.php?{$_URL_PARAMS}&id={$element["id"]}&chain={$_GET["chain"]}", null, "command icon_view");
			} else {
				if ($_MODEL->is_object_editable($element)) $value = new HTML\Link($value, ADAMANTINE_ROOT_PATH . "management/edit.php?{$_URL_PARAMS}&id={$element["id"]}&chain={$_GET["chain"]}", null, "command icon_edit");
			}
		}
		$row->set_cell($field, $value);
	}
	
	if (($_MODEL instanceof Models\OrientatedOrdered) || ($_MODEL instanceof Models\Ordered)) {
		$row->set_cell("_movement", $movement = new HTML\MultiElements());

		$icons = array();
		$movement->add($icons[] = new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/bullet_arrow_top.svg", "Move item to top", ADAMANTINE_ROOT_PATH . "management/move_do.php?{$_URL_PARAMS}&direction=top&id={$element[$_MODEL->get_id_field()]}&chain={$_GET["chain"]}", null, "top"));
		$movement->add($icons[] = new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/bullet_arrow_up.svg", "Move item up", ADAMANTINE_ROOT_PATH . "management/move_do.php?{$_URL_PARAMS}&direction=up&id={$element[$_MODEL->get_id_field()]}&chain={$_GET["chain"]}", null, "up"));
		$movement->add($icons[] = new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/bullet_arrow_down.svg", "Move item down", ADAMANTINE_ROOT_PATH . "management/move_do.php?{$_URL_PARAMS}&direction=down&id={$element[$_MODEL->get_id_field()]}&chain={$_GET["chain"]}", null, "down"));
		$movement->add($icons[] = new HTML\LinkIcon(ADAMANTINE_ROOT_PATH . "css/icons/bullet_arrow_bottom.svg", "Move item to bottom", ADAMANTINE_ROOT_PATH . "management/move_do.php?{$_URL_PARAMS}&direction=bottom&id={$element[$_MODEL->get_id_field()]}&chain={$_GET["chain"]}", null, "bottom"));
		foreach ($icons as $icon) $icon->get_classes()->add("movementicon");
	}
}

$page->add($linkbar = new HTML\LinkBar());
$linkbar->add(new HTML\Link("Add new {$_GET["model"]}", ADAMANTINE_ROOT_PATH . "management/edit.php?{$_URL_PARAMS}&id=new&chain={$_GET["chain"]}", null, "command icon_add"));

if (method_exists($_MODEL, "custom_back_link")) {
	list($text, $url) = $_MODEL->custom_back_link($xsi, $_GET["chain"]);
	$back_link = new HTML\Link($text, $url, null, "command icon_go");
}

if ($back_link !== null) $linkbar->add($back_link);

$_HTML->complete();
