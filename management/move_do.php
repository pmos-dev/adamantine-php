<?php
/**
 * Adamantine managed model functionality: move a row's position within a ManagedOrientatedOrdered model.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

$_UI->set_titles("Move {$_GET["model"]}", $xsi["description"]);

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid element ID supplied");

if ($_MODEL instanceof Models\Hierarchy) Adamantine\error("This management approach does not work for hierarchy models. Please use the AJAX tree management instead.");
if ($_MODEL instanceof Models\OrientatedOrdered) {
	$id_chain = array();
	if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
		$id_chain = explode(",", $_GET["chain"]);
		foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
		unset($id);
	}
	
	if (null === ($object = model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such element exists");
} elseif ($_MODEL instanceof Models\Ordered) {
	if (null === ($object = $_MODEL->get_by_id($_GET["id"]))) Adamantine\error("No such element exists by that ID");
} else throw new Adamantine\Exception("This model type cannot be moved");

if (!isset($_GET["direction"])) Adamantine\error("No movement direction specified");
switch ($_GET["direction"]) {
	case "up":
	case "down":
	case "top":
	case "bottom":
		$_MODEL->move($object, $_GET["direction"]);
		break;
	default:
		error("Invalid movement direction");
}

$identify = "-";
if (array_key_exists("name", $object)) $identify = $object["name"];
elseif (array_key_exists("description", $object)) $identify = $object["description"];

$_SESSIONMANAGER->log_by_name($_GET["area"] === "_core" ? "admin" : $_GET["area"], "Move {$_GET["model"]}", $identify);

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "management/list.php?{$_URL_PARAMS}&chain={$_GET["chain"]}");
