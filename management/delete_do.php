<?php
/**
 * Adamantine managed model functionality: delete a row.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Models as Models;
use \Adamantine as Adamantine;

$_UI->set_titles("Delete {$_GET["model"]}", $xsi["description"]);

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");
	
if ($_MODEL instanceof Models\Hierarchy) Adamantine\error("This management approach does not work for hierarchy models. Please use the AJAX tree management instead.");
if ($_MODEL instanceof Models\SecondClass) {
	$id_chain = array();
	if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
		$id_chain = explode(",", $_GET["chain"]);
		foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
		unset($id);
	}
	
	if (null === ($data = model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such element exists");
} elseif ($_MODEL instanceof Models\FirstClass) {
	if (null === ($data = $_MODEL->get_by_id($_GET["id"]))) Adamantine\error("No such element exists");
} else throw new Adamantine\Exception("This model type is not supported");

$_UI->assert_confirmed("Deleting this {$_GET["model"]} will purge from the system any other data dependent on it! You may loose data you weren't intending to delete, and this operation cannot be undone. Continue with delete?");

if ($_MODEL instanceof Models\SecondClass) {
	$_MODEL->delete_for_firstclass(model_get_secondclass_firstclass_data($_MODEL, $id_chain), $data);
} elseif ($_MODEL instanceof Models\FirstClass) {
	$_MODEL->delete($data);
}

$identify = "-";
if (array_key_exists("name", $data)) $identify = $data["name"];
elseif (array_key_exists("description", $data)) $identify = $data["description"];

$_SESSIONMANAGER->log_by_name($_GET["area"] === "_core" ? "admin" : $_GET["area"], "Delete {$_GET["model"]}", $identify);

$_HTTP->redirect(ADAMANTINE_ROOT_PATH . "management/list.php?{$_URL_PARAMS}&chain={$_GET["chain"]}");
