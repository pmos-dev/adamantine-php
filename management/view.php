<?php
/**
 * Adamantine managed model functionality: view a model row.
 * 
 * Optional additional functionality can be declared via the get_view_additional_file() model method.
 * 
 * @see Adamantine\Models\Manageable::get_view_additional_file()
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Management;

define("APP_ROOT_PATH", "./../../");
require_once APP_ROOT_PATH . "adamantine/management/_init.php";

use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Models as Models;
use \Abstraction\Renderer\HTML as HTML;
use \Adamantine as Adamantine;

$_UI->set_titles("View {$_GET["model"]}", $xsi["description"]);

if (!Data\Data::validate_id($_GET["id"])) Adamantine\error("Invalid ID supplied");
	
if ($_MODEL instanceof Models\Hierarchy) Adamantine\error("This management approach does not work for hierarchy models. Please use the AJAX tree management instead.");
if ($_MODEL instanceof Models\SecondClass) {
	$id_chain = array();
	if (isset($_GET["chain"]) && "" !== $_GET["chain"]) {
		$id_chain = explode(",", $_GET["chain"]);
		foreach ($id_chain as &$id) if (!Data\Data::validate_id($id)) throw new Adamantine\Exception("Invalid ID supplied as part of the firstclass ID chain");
		unset($id);
	}
	
	if (null === ($data = model_get_secondclass_data($_MODEL, $_GET["id"], $id_chain))) Adamantine\error("No such element exists");
} elseif ($_MODEL instanceof Models\FirstClass) {
	if (null === ($data = $_MODEL->get_by_id($_GET["id"]))) Adamantine\error("No such element exists");
} else throw new Adamantine\Exception("This model type is not supported");

$has_hexcolor = false;
foreach ($_MODEL_STRUCTURE as $name => $type) {
	if ($type instanceof Database\Type_HexColor) $has_hexcolor = true;
}

if ($has_hexcolor) {
	$_HTML->add_code("css", <<<CSS
	
div.colorblock {
	border: 1px solid black;
	width: 50px;
	height: 10px;
}
	
CSS
	);
}

$page = $_UI->get_content();

$page->add(new HTML\Header("{$_GET["model"]} details", HTML\Header::LEVEL_3));

$page->add(new HTML\Div($layout = new HTML\Layout(), null, "unilayout model"));

foreach ($_MODEL->get_manageable_fields() as $field) {
	$type = $_MODEL_STRUCTURE[$field];
	
	$optional = $type->not_null ? "" : "optional";

	if ($data[$field] === null || $data[$field] === "") $element = new HTML\Raw("&nbsp;");
	elseif ($type instanceof Database\Type_HexColor) {
		$element = new HTML\Span("#{$data[$field]}", null, "string");
		$element->add_param("style", "background:#{$data[$field]}");
	} elseif (
		$type instanceof Database\Type_String ||
		$type instanceof Database\Type_Enum ||
		$type instanceof Database\Type_Encrypted ||
		$type instanceof Database\Type_Int || $type instanceof Database\Type_SmallInt || $type instanceof Database\Type_TinyInt || $type instanceof Database\Type_Float
	) {
		$element = new HTML\Span($data[$field], null, "string");
	} elseif ($type instanceof Database\Type_DateTime) {
		$element = new HTML\Span(Data\Data::pretty_datetime($data[$field]), null, "datetime");
	} elseif ($type instanceof Database\Type_Date) {
		$element = new HTML\Span(Data\Data::pretty_date($data[$field]), null, "date");
	} elseif ($type instanceof Database\Type_Text) {
		$element = new HTML\Paragraph($data[$field], null, "text");
	} elseif ($type instanceof Database\Type_Bool) {
		$element = $data[$field] ? new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/tick.svg", "Yes") : new HTML\Image(ADAMANTINE_ROOT_PATH . "css/icons/cross.svg", "No");
	} elseif ($type instanceof Database\Type_Id) {
		// ignore any superflouous IDs
		continue;
	} else {
		print_r($type);
		die("Unknown type");
	}
	
	$description = $_MODEL->get_field_description($field);
	if (null !== ($suffix = $_MODEL->get_field_suffix($field))) $element = new HTML\MultiElements(array($element, new HTML\Span($suffix, null, "suffix")));
	
	$layout->add_labelled_row($description, $element);
}

$page->add($linkbar = new HTML\LinkBar());
if ($_MODEL->is_object_editable($data)) $linkbar->add(new HTML\Link("Edit or delete this {$_GET["model"]}", ADAMANTINE_ROOT_PATH . "management/edit.php?{$_URL_PARAMS}&chain={$_GET["chain"]}", null, "command icon_edit"));

if ($_MODEL instanceof Models\Hierarchy) $linkbar->add(new HTML\Link("Back to {$_GET["model"]} tree", ADAMANTINE_ROOT_PATH . "management/tree.php?area={$_GET["area"]}&model={$_GET["model"]}&chain={$_GET["chain"]}&xsi={$xsi["id"]}", null, "command icon_list"));
else $linkbar->add(new HTML\Link("Back to {$_GET["model"]} list", ADAMANTINE_ROOT_PATH . "management/list.php?area={$_GET["area"]}&model={$_GET["model"]}&chain={$_GET["chain"]}&xsi={$xsi["id"]}", null, "command icon_list"));

if (null !== ($path = $_MODEL->get_view_additional_file())) {
	if (isset($installed)) unset($installed);	// this is never set explicitly for the context, so is just whatever the current floating value is. Better to explicitly remove it than leave a hanging value which might be assumed to be the 'current' installed area context.
	require_once $path;
}

$_HTML->complete();
