<?php
/**
 * Adamantine basic welcome page.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine;

define("APP_ROOT_PATH", "./../");
require_once APP_ROOT_PATH . "_init.php";

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Framework as Framework;

$_UI->set_titles("Welcome", $xsi["description"]);

$page = $_UI->get_content();

$page->add(new HTML\Header($_SESSIONMANAGER->is_guest() ? "Welcome. You are not currently logged in." : "Welcome {$xau["fullname"]}. You are logged in.", HTML\Header::LEVEL_3));

$blankpage = true;

$stack = $_UI->get_body();
foreach ($_SYSTEM->list_installed_areas() as $area) {
	if (!$_SESSIONMANAGER->has_access($area, Framework\Access::READ)) continue;

	$api = \Adamantine\Areas\Area_API::get_api_by_area_name($area["name"]);
	if (!($api instanceof \Adamantine\Areas\Welcomable)) continue;

	$installed = $_SYSTEM->get_installed_by_area($_SYSTEM->get_area_by_name($area["name"]));
	$block = $api->append_to_page($stack, $xsi, $installed, $xau);
	
	$blankpage = false;
	
	if ($block && !$_SESSIONMANAGER->is_guest() && !$_SESSIONMANAGER->is_root()) {
		$_UI->purge_functionality_menus();
		break;	// skip further welcomable appends. NB, this means that the block will only prevent further ones if the area is first.
	}
}

if ($blankpage) {
	$page->add(new HTML\Header("Not sure where to start? Try the menu bar above.", HTML\Header::LEVEL_4, null, "info"));
}

$_HTML->complete();
