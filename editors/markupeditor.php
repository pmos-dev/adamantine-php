<?php
/**
 * HTML element: MarkupEditor.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\Editors;

if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set.");
if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set.");
require_once ABSTRACTION_ROOT_PATH . "renderer/html/wrappingelement.php";

use \Abstraction\Renderer\HTML as HTML;
use \Abstraction\Renderer\HTMLJQuery as HTMLJQuery;

/**
 * Represents a markup editor within the HTML abstraction.
 *
 * @author Pete Morris
 * @version 1.2.0
 */
class Form_MarkupEditor extends HTML\Div {
	protected $html;
	protected $multielements;
	protected $toolbar;
	protected $editor;
	
	protected static function spacer() {
		return new HTML\Div("", null, "spacer");
	}
	
	protected static function icon($name, $class, $description) {
		$item = new HTML\Link($name, "#", null, "icon {$class}");
		$item->add_param("title", $description);
		
		return $item;
	}
	
	/**
	 * Constructs a new instance of this markup editor.
	 *
	 * The class 'markupeditor' is added to the component automatically.
	 * 
	 * @param HTMLJQuery\HTML $html a reference to the parent HTML component, which will need to have jQuery code added to invoke the tabs
	 * @param string $value optional child content to use as the 'value' of the textarea
	 * @param string|NULL $id an optional value for the id="..." parameter
	 * @param HTML\Classes|string|NULL $class an optional set of classes for this element
	 */
	public function __construct(HTMLJQuery\HTML $html, $value = "", $id = null, $class = null) {
		$this->html = $html;
		
		parent::__construct($this->multielements = new HTML\MultiElements(array(
			$this->toolbar = new HTML\ItemList(HTML\ItemList::UNORDERED, null, "toolbar"),
			$this->editor = new HTML\Form_TextArea($value, $id, "editor"),
			new HTML\Div("", null, "preview")
		)), null, $class);
		
		$this->get_classes()->add("markupeditor");
		$this->html->add_include(HTML\HTML::TYPE_CSS, ADAMANTINE_ROOT_PATH . "css/markupeditor.css");
		$this->html->add_include(HTML\HTML::TYPE_JAVASCRIPT, ADAMANTINE_ROOT_PATH . "js/jquery.selection.js");
		$this->html->add_include(HTML\HTML::TYPE_JAVASCRIPT, ADAMANTINE_ROOT_PATH . "js/markupeditor.js");
		
		$this->toolbar->new_item(self::icon("Bold", "bold", "Bold"));
		$this->toolbar->new_item(self::icon("Italic", "italic", "italic"));
		$this->toolbar->new_item(self::icon("H4", "h4", "Heading 4"));
		$this->toolbar->new_item(self::icon("Quote", "quote", "Quote"));
		
		$this->toolbar->new_item(self::spacer());

		$this->toolbar->new_item(self::icon("Bullet", "bullet", "Bullet list"));
		$this->toolbar->new_item(self::icon("Numbered", "numbered", "Numbered list"));
		$this->toolbar->new_item(self::icon("Table", "table", "Table"));
		
		$this->toolbar->new_item(self::spacer());

		$this->toolbar->new_item(self::icon("URL", "url", "URL link"));
		$this->toolbar->new_item(self::icon("Email", "email", "Email address"));
		
		$this->toolbar->new_item(self::spacer());

		$this->toolbar->new_item(self::icon("Preview", "preview", "Preview"));
	}
	
	/**
	 * Replaces the existing editor text with the specified new text.
	 *
	 * @param string $text the new text
	 */
	public function set_text($text) {
		$this->editor->set_text($text);
	}
}
