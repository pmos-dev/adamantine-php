<?php
/**
 * About page
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\About;

define("SKIP_SESSION_INITIALIZE", true);
define("APP_ROOT_PATH", "./../../");

require_once APP_ROOT_PATH . "adamantine/_init.php";

use \Abstraction\Renderer\HTML as HTML;

$_UI->set_titles("About the framework", "Adamantine");

$page = $_UI->get_content();

$page->add(new HTML\Header("Hardened development; robust reliability", HTML\Header::LEVEL_3));

$page->add(new HTML\Paragraph("Adamantine is a 'heavy-weight' framework! Why heavy-weight? Because there are already plenty of frameworks out there which are light as a feather. But sometimes you need a level of robustness which these don't provide. Enter Adamantine."));
$page->add(new HTML\Paragraph("Focusing on issues such as strong data typing, bi-directional input validation, native encryption support and abstracted coding, Adamantine provides a framework for development of web applications which need to put security and reliability at the top of the agenda over speed or scalability."));
$page->add(new HTML\Paragraph("Adamantine is NOT an MVC framework (there are plenty of those already), but does have support for models inbuilt. It also has common management functionality available for those models should you wish, which reduces the need to write duplicate add/edit/move/delete routines."));
$page->add(new HTML\Paragraph("Adamantine also makes use of an underlying abstraction framework, removing the need to write HTML and many SQL calls manually. This reduces the chance of one-off errors or mistakes creeping in."));

$page->add($linkbar = new HTML\LinkBar());

$page->add(new HTML\Hr());
$page->add(new HTML\Pre(<<<TEXT
Adamantine - PHP web application framework

Copyright 2013 Pete Morris

Licensed under the MIT and LGPL licenses:
http://www.opensource.org/licenses/mit-license.php
http://www.gnu.org/licenses/lgpl.html
TEXT
));

$_HTML->complete();
