<?php
/**
 * About page: cookies
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine\About;

define("SKIP_SESSION_INITIALIZE", true);
define("APP_ROOT_PATH", "./../../");

require_once APP_ROOT_PATH . "adamantine/_init.php";

use \Abstraction\Renderer\HTML as HTML;

$_UI->set_titles("Cookie usage", "Adamantine");

$page = $_UI->get_content();

$page->add(new HTML\Header("Sweet and delicious", HTML\Header::LEVEL_3));

$page->add(new HTML\Paragraph("This site is a web application which uses sessions and cookies. Session cookies are essential to web applications and allow features such as being able to log in."));
$page->add(new HTML\Paragraph("Without cookies this web application won't work. By using this web application, you agree to our use of cookies."));
$page->add($linkbar = new HTML\LinkBar());
$linkbar->add(new HTML\Link("Find out more at Wikipedia", "http://en.wikipedia.org/wiki/HTTP_cookie", null, "command icon_go"));

$_HTML->complete();
