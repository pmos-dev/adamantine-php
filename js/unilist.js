/**
 * Adamantine jQuery: universal list appearance
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */

$(document).ready(function() {

$("ul.unilist").not(".simple").find("li:odd").addClass("odd_row");

$("ul.unilist").each(function (i) {
	if ($(this).children("li").length > 0) return true;
	$(this).append("<li class=\"none\">(none)</li>");
});

});
