$(document).ready(function() {

$.fn.reloadNode = function(callback) {
	$(this).each(function(i) {
		$(this).removeClass("loaded").empty();
		
		$(this).loadNode(callback);
	});
};

$.fn.loadNode = function(callback, recurse) {
	$(this).each(function(i) {
		var id = $(this).data("id");
	
		var closure = $(this);
		$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/get_node.php", {
			"xsi": XSI,
			"area": $(this).closest("ul.treeable").data("area"),
			"model": $(this).closest("ul.treeable").data("model"),
			"chain": $(this).closest("ul.treeable").data("chain"),
			"id": id
		}, function(data) {
			if (data.outcome == "error") alert("Error: " + data.message);
			else if (data.outcome == "success") {
				$(closure).append("<img src=\"" + ADAMANTINE_ROOT_PATH + "css/icons/delete.svg\" title=\"Delete\" class=\"actions deletenode\" />");
				if ($(closure).closest("ul.treeable").data("is_aliasing_supported")) $(closure).append("<img src=\"" + ADAMANTINE_ROOT_PATH + "css/icons/text_list_bullets.svg\" title=\"Set aliases\" class=\"actions aliasnode\" />");
				$(closure).append("<img src=\"" + ADAMANTINE_ROOT_PATH + "css/icons/pencil.svg\" title=\"Edit\" class=\"actions editnode gap\" />");
				$(closure).append("<img src=\"" + ADAMANTINE_ROOT_PATH + "css/icons/bullet_arrow_down.svg\" title=\"Move down\" class=\"movement down\" />");
				$(closure).append("<img src=\"" + ADAMANTINE_ROOT_PATH + "css/icons/bullet_arrow_up.svg\" title=\"Move up\" class=\"movement up\" />");
				$(closure).append("<img src=\"" + ADAMANTINE_ROOT_PATH + "css/icons/bullet_arrow_right.svg\" title=\"Move right\" class=\"movement right\" />");
				$(closure).append("<img src=\"" + ADAMANTINE_ROOT_PATH + "css/icons/bullet_arrow_left.svg\" title=\"Move left\" class=\"movement left\" />");

				$(closure).append("<div class=\"structure expand\"></div><div class=\"name\"></div>");
				$(closure).children(".name").text(data.result.name);
				$(closure).data("supplimentary", data.result.supplimentary);
				
				if (typeof callback == 'function') callback.call(closure);
				if (recurse == true) $(closure).expandNode(recurse);
			}
		});
	});
};

$.fn.refactorNodes = function(callback) {
	$(this).each(function(i) {
		$(this).children("ul").children("li").removeClass("endnode").removeClass("addnode");
		$(this).children("ul").children("li:last").addClass("addnode").prev().addClass("endnode");
	});
};

$.fn.expandNode = function(recurse) {
	$(this).each(function(i) {
		$(this).children(".structure").removeClass("expand").addClass("collapse");

		if ($(this).hasClass("loaded")) {
			$(this).children("ul").show();
			if (typeof callback == 'function') callback.call(this);
			if (recurse) $(this).children("ul").children("li.node").expandNode(recurse);
			return;
		}
	
		$(this).addClass("loaded");
		$(this).append("<ul></ul>");
		
		var closure = $(this);
		$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/list_children.php", {
			"xsi": XSI,
			"area": $(this).closest("ul.treeable").data("area"),
			"model": $(this).closest("ul.treeable").data("model"),
			"chain": $(this).closest("ul.treeable").data("chain"),
			"parent": $(this).data("id")
		}, function(data) {
			if (data.outcome == "error") alert("Error: " + data.message);
			else if (data.outcome == "success") {
				for (var i = 0; i < data.result.length; i++) {
					$(closure).children("ul").append("<li></li>");
					$(closure).children("ul").children("li:last-child").addClass("node").data("id", data.result[i]);
				}

				//$(closure).children("ul").children("li:last").addClass("endnode");
				$(closure).children("ul").children("li").loadNode(null, recurse);
				
				$(closure).children("ul").append("<li><img src=\"" + ADAMANTINE_ROOT_PATH + "css/icons/add.svg\" class=\"addnode\" title=\"Add node here\" /></li>");
				$(closure).refactorNodes();
			}
		});
	});
};

$.fn.collapseNode = function(callback) {
	$(this).each(function(i) {
		$(this).children(".structure").removeClass("collapse").addClass("expand");
		$(this).children("ul").hide();
		$(this).children("ul").find("li").collapseNode();

		if (typeof callback == 'function') callback.call(this);
	});
};

$(document).on("click", ".structure.expand", function() {
	$(this).parent().expandNode();
});

$(document).on("click", ".structure.collapse", function() {
	$(this).parent().collapseNode();
});

function addTag() {
	return false;
}

$("div.editor").each(function(i) {
	var tree = $(this).prev("ul.treeable");
	
	$(this).data("preserveWidth", $(this).find("form").outerWidth());
	$(this).find("form").submit(function(event) {
		// to prevent the form from actually submitting
		event.preventDefault();
		return false;
	});
	
	var dialog = $("div.editor").dialog({
		autoOpen: false,
		modal: true,
		open: function() {
			$(this).find(".validate_error").removeClass("validate_error");
		},
		close: function() {
			$(this).find("form").get(0).reset();
		}
	});
	
	$(tree).data("editor", dialog);
});


$(document).on("click", ".addnode", function() {
	var closure = $(this).closest("li").parent().closest("li");
	var tree = $(this).closest("ul.treeable");
	var editor = $(tree).data("editor");
	
	$(editor).dialog("option", "width", $(editor).data("preserveWidth") + 30);
	$(editor).dialog("option", "title", "Add new node");
	$(editor).dialog("option", "buttons", {
		"Create": function() {
			var form = $(this).find("form");
			var data = $(form).serializeArray();
			
			$("form").submit();
			if (!$(form).data("validated")) return false;
			
			var params = {
				"xsi": XSI,
				"area": $(tree).data("area"),
				"model": $(tree).data("model"),
				"chain": $(tree).data("chain"),
				"parent": $(closure).data("id"),
			};
			
			$.each(data, function(i, obj) {
				params[obj.name] = obj.value;
			});

			$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/add_node.php", params, function(data2) {
				if (data2.outcome == "error") alert("Error: " + data2.message);
				else if (data2.outcome == "success") {
					// it's safer to just reload the node rather than trying to do it inline by DOM modification
					$(closure).reloadNode(function() {
						$(closure).expandNode();
					});
				}
				
				$(editor).dialog("close");
			});
		},
		"Cancel": function() {
			$(editor).dialog("close");
		}		
	});
	$(editor).dialog("open");
	return false;
});

$(document).on("click", ".editnode", function() {
	var closure = $(this).parent();
	var existing = $(closure).children(".name").text();
	var tree = $(this).closest("ul.treeable");
	var editor = $(tree).data("editor");
	
	var form = $(editor).find("form");
	$(form).find("#name").val(existing);
	
	$.each($(closure).data("supplimentary"), function(field, value) {
		$(editor).find("#" + field).val(value);
	});
	
	$(editor).dialog("option", "width", $(editor).data("preserveWidth") + 30);
	$(editor).dialog("option", "title", "Edit node");
	$(editor).dialog("option", "buttons", {
		"Save changes": function() {
			var form = $(this).find("form");
			var data = $(form).serializeArray();
			
			$("form").submit();
			if (!$(form).data("validated")) return false;
			
			var params = {
				"xsi": XSI,
				"area": $(tree).data("area"),
				"model": $(tree).data("model"),
				"chain": $(tree).data("chain"),
				"id": $(closure).data("id"),
			};
			
			$.each(data, function(i, obj) {
				params[obj.name] = obj.value;
			});

			$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/edit_node.php", params, function(data2) {
				if (data2.outcome == "error") alert("Error: " + data2.message);
				else if (data2.outcome == "success") {
					// it's safer to just reload the node rather than trying to do it inline by DOM modification
					$(closure).reloadNode(null);
				}
				
				$(editor).dialog("close");
			});
		},
		"Cancel": function() {
			$(editor).dialog("close");
		}		
	});
	$(editor).dialog("open");
	return false;
});

$(document).on("click", ".aliasnode", function() {
	if (!$(this).closest("ul.treeable").data("is_aliasing_supported")) return;

	var closure = $(this).parent();
	
	$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/list_aliases.php", {
		"xsi": XSI,
		"area": $(this).closest("ul.treeable").data("area"),
		"model": $(this).closest("ul.treeable").data("model"),
		"chain": $(this).closest("ul.treeable").data("chain"),
		"id": $(closure).data("id")
	}, function(data) {
		if (data.outcome == "error") alert("Error: " + data.message);
		else if (data.outcome == "success") {
			var aliases = prompt("Please enter the aliases for this node", data.result.join(", "));
			if (aliases == null) return false;
			
			if (aliases == data.result.join(", ")) return false;

			$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/set_aliases.php", {
				"xsi": XSI,
				"area": $(closure).closest("ul.treeable").data("area"),
				"model": $(closure).closest("ul.treeable").data("model"),
				"chain": $(closure).closest("ul.treeable").data("chain"),
				"id": $(closure).data("id"),
				"aliases": aliases
			}, function(data2) {
				if (data2.outcome == "error") alert("Error: " + data2.message);
				else if (data2.outcome == "success") alert("Aliases successfully set");
			});
		}
	});
	
	return false;
});

$(document).on("click", ".deletenode", function() {
	var closure = $(this).parent();
	
	if (!confirm("Confirm you want to delete this node. This will remove it from any associated entries that are currently using it!")) return false;

	$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/delete_node.php", {
		"xsi": XSI,
		"area": $(this).closest("ul.treeable").data("area"),
		"model": $(this).closest("ul.treeable").data("model"),
		"chain": $(this).closest("ul.treeable").data("chain"),
		"id": $(closure).data("id")
	}, function(data) {
		if (data.outcome == "error") alert("Error: " + data.message);
		else if (data.outcome == "success") {
			var parentnode = $(closure).parent().closest("li");
			$(closure).remove();
			$(parentnode).refactorNodes();
		}
	});
	
	return false;
});

$(document).on("click", ".movement.up", function() {
	var closure = $(this).closest("li");
	if ($(this).css("opacity") == 0) return false;

	$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/move_node.php", {
		"xsi": XSI,
		"area": $(this).closest("ul.treeable").data("area"),
		"model": $(this).closest("ul.treeable").data("model"),
		"chain": $(this).closest("ul.treeable").data("chain"),
		"id": $(closure).data("id"),
		"direction": "up"
	}, function(data) {
		if (data.outcome == "error") alert("Error: " + data.message);
		else if (data.outcome == "success") {
			$(closure).insertBefore($(closure).prev());
			$(closure).parent().closest("li").refactorNodes();
		}
	});

	return false;
});

$(document).on("click", ".movement.down", function() {
	var closure = $(this).closest("li");
	if ($(this).css("opacity") == 0) return false;

	$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/move_node.php", {
		"xsi": XSI,
		"area": $(this).closest("ul.treeable").data("area"),
		"model": $(this).closest("ul.treeable").data("model"),
		"chain": $(this).closest("ul.treeable").data("chain"),
		"id": $(closure).data("id"),
		"direction": "down"
	}, function(data) {
		if (data.outcome == "error") alert("Error: " + data.message);
		else if (data.outcome == "success") {
			$(closure).insertAfter($(closure).next());
			$(closure).parent().closest("li").refactorNodes();
		}
	});

	return false;
});

$(document).on("click", ".movement.left", function() {
	var closure = $(this).closest("li");
	if ($(this).css("opacity") == 0) return false;

	$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/move_node.php", {
		"xsi": XSI,
		"area": $(this).closest("ul.treeable").data("area"),
		"model": $(this).closest("ul.treeable").data("model"),
		"chain": $(this).closest("ul.treeable").data("chain"),
		"id": $(closure).data("id"),
		"direction": "left"
	}, function(data) {
		if (data.outcome == "error") alert("Error: " + data.message);
		else if (data.outcome == "success") {
			$(closure).insertAfter($(closure).parent().closest("li").parent().children("li.node:last"));
			$(closure).parent().closest("li").refactorNodes();
			$(closure).parent().closest("li").parent().closest("li").refactorNodes();
		}
	});

	return false;
});

$(document).on("click", ".movement.right", function() {
	var closure = $(this).closest("li");
	if ($(this).css("opacity") == 0) return false;

	$.getJSON(ADAMANTINE_ROOT_PATH + "management/ajax/move_node.php", {
		"xsi": XSI,
		"area": $(this).closest("ul.treeable").data("area"),
		"model": $(this).closest("ul.treeable").data("model"),
		"chain": $(this).closest("ul.treeable").data("chain"),
		"id": $(closure).data("id"),
		"direction": "right"
	}, function(data) {
		if (data.outcome == "error") alert("Error: " + data.message);
		else if (data.outcome == "success") {
			// it's safer to just reload the node rather than trying to do it inline by DOM modification
			var prev = $(closure).prev();
			$(closure).prev().reloadNode(function() {
				$(prev).expandNode();
			});
			$(closure).remove();
			$(prev).parent().closest("li").refactorNodes();
		}
	});

	return false;
});

$("ul.treeable").each(function(i) {
	var closure = $(this);
	
	$(this).data("area", $(this).children("li.root").children("input[type=hidden].area").val());
	$(this).data("model", $(this).children("li.root").children("input[type=hidden].model").val());
	$(this).data("chain", $(this).children("li.root").children("input[type=hidden].chain").val());
	$(this).data("is_aliasing_supported", $(this).children("li.root").children("input[type=hidden].is_aliasing_supported").val() == 1);
	
	$(this).children("li.root").data("id", $(this).children("li.root").children("input[type=hidden].id").val()).loadNode(function() {
		$(closure).children("li.root").expandNode();
	});
	
	$(this).prev("ul.structureall").find(".expandall").click(function() {
		$(closure).children("li.root").expandNode(true);
		return false;
	});

	$(this).prev("ul.structureall").find(".collapseall").click(function() {
		$(closure).children("li.root").collapseNode(function() {
			$(closure).children("li.root").expandNode();
		});
		return false;
	});

});

});
