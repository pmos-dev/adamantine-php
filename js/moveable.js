/**
 * Adamantine jQuery: move rows in tables
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */

$(document).ready(function() {

$("table.moveable > tbody > tr:first-child > td.moveable").prepend("<img src=\"" + ADAMANTINE_ROOT_PATH + "/css/icons/bullet_white.svg\" /><img src=\"" + ADAMANTINE_ROOT_PATH + "/css/icons/bullet_white.svg\" />");
$("table.moveable > tbody > tr:last-child > td.moveable").append("<img src=\"" + ADAMANTINE_ROOT_PATH + "/css/icons/bullet_white.svg\" /><img src=\"" + ADAMANTINE_ROOT_PATH + "/css/icons/bullet_white.svg\" />");

//add CSS 2 support
$("table.moveable > tbody > tr:last-child").addClass("lastchild");

});
