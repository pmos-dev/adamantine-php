/**
 * Adamantine jQuery: universal form appearance and functionality
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */

$(document).ready(function() {

$("form.uniform").children(".jqueryuitabs").find("label").closest("div").addClass("form_row");

$("form.uniform").find("div.form_row").each(function(i) {
	if ($(this).hasClass("optional") || $(this).children("input,textarea,select").hasClass("optional") || $(this).children("input[type=checkbox]").length > 0) $(this).addClass("optional");
});
	
$("form.uniform").not(".simple").find("div.form_row:odd").addClass("odd_row");
$("div.unilayout").not(".simple").children("div:odd").addClass("odd_row");

var MAX_LONG_LABEL_WIDTH = 200;

$("form.uniform").find("input.geo").attr("placeholder", "lat,long");
$("form.uniform").find("input.price").before("<span class=\"price\">&pound;</span>");

$("form.uniform.autosize").each(function(i) {
	if ($(this).find(".jqueryuitabs").length > 0) right_extra = 120;
	
	var max_label_width = 0;
	$(this).find("div.form_row").children("label").each(function(j) {
		max_label_width = Math.max(max_label_width, $(this).outerWidth());
		
		// wrap very long row labels
		if ($(this).outerWidth() >= (MAX_LONG_LABEL_WIDTH + 10)) {
			$(this).css({ "whiteSpace": "normal", "width": MAX_LONG_LABEL_WIDTH + 10 });
			$(this).closest("div.form_row").css({ "minHeight": $(this).outerHeight() });
		}
	}).css({ "width": Math.min(max_label_width, MAX_LONG_LABEL_WIDTH) + 10 });
});

});
