/**
 * Automatically handle date and time field types
 */

$(document).ready(function() {

$("input.date").datepicker({ 
	"dateFormat": "dd/mm/yy",
	"numberOfMonths": 3,
	"showOn": "button",
	"buttonImage": ADAMANTINE_ROOT_PATH + "css/icons/calendar_view_month.svg",
	"buttonImageOnly": true,
	"gotoCurrent": true
});

$("input.time").timepicker({
	"addSliderAccess": true,
	"sliderAccessArgs": { "touchonly": false },
	"showOn": "button",
	"buttonImage": ADAMANTINE_ROOT_PATH + "css/icons/clock.svg",
	"buttonImageOnly": true
});

$("input.datetime").datetimepicker({ 
	"dateFormat": "dd/mm/yy",
	"hourGrid": 4,
	"numberOfMonths": 3,
	"showOn": "button",
	"buttonImage": ADAMANTINE_ROOT_PATH + "css/icons/calendar_view_month.svg",
	"buttonImageOnly": true,
	"gotoCurrent": true
});

});
