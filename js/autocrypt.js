/**
 * Adamantine jQuery: form auto-encryption
 * 
 * @copyright 2015 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */

var autocryptCheckLoadInterval = null;

function autocryptCheckAllLibrariesLoaded() {
	if (
		(typeof Arcfour !== 'function') ||
		(typeof rng_seed_int !== 'function') ||
		(typeof parseBigInt !== 'function') ||
		(typeof BigInteger !== 'function') ||
		(typeof CryptoJS !== 'object') ||
		(typeof CryptoJS.RIPEMD160 !== 'function') ||
		(typeof CryptoJS.PBKDF2 !== 'function') ||
		(typeof CryptoJS.AES !== 'object') ||
		(typeof CryptoJS.AES.encrypt !== 'function') ||
		(typeof CryptoJS.pad.ZeroPadding !== 'object')
	) return false;

	$("form.autocrypt").removeClass("librariesPending");
	
	if (autocryptCheckLoadInterval !== null) window.clearInterval(autocryptCheckLoadInterval);
	autocryptCheckLoadInterval = null;
}

$(document).ready(function() {

autocryptCheckLoadInterval = window.setInterval(function() {
	autocryptCheckAllLibrariesLoaded();
}, 100);

$("form.autocrypt").addClass("librariesPending,keyPending").each(function(i) {
	if (!$(this).attr("action").startsWith("#")) {
		$(this).empty().append("Autocrypt form submit URL is not hash protected");
		return false;
	}

	var combined = $(this).attr("action").substring(1);

	var components = new RegExp("^([^,]+),([^,]+)$");
	var matches = null;
	if (null === (matches = components.exec(combined))) {
		$(this).empty().append("Autocrypt form does not contain combined submit and modulus URLs");
		return false;
	}

	var submiturl = matches[1];
	var modulusurl = matches[2];

	$(this).data({
		"modulus": null,
		"e": null
	});

	var closure = $(this);
	$.getJSON(modulusurl, function(result) {
		if (result === undefined || result.modulus === undefined || result.modulus === null || result.e === undefined || result.e === null) {
			alert("Invalid encryption key supplied");	
			return false;
		}

		$(closure).data("autocrypt_modulus", result.modulus);
		$(closure).data("autocrypt_e", result.e);
		$(closure).removeClass("keyPending");
	});

	var formmethod = $(this).attr("method");
	
	$(this).submit(function() {
		if (!$(this).data("validated")) return false;
		
		$(this).addClass("encrypting");

		window.setTimeout(function() {
			var submitdata = {};
			try {
				if ($(closure).is(".librariesPending")) {
					// this should not occur if the autocrypt.css stylesheet is loaded.
					throw "Not all JavaScript libraries have been loaded yet. Please wait a moment before submitting again.";
				}
		
				if ($(closure).data("autocrypt_modulus") === null) throw "Encryption modulus has not been loaded yet.";
				
				$(closure).find("input,select,button").each(function() {
					if ($(this).is("[name]")) return true;
					$(this).attr("name", $(this).attr("id"));
				});
		
				var datablock = $(closure).serialize().trim();
		
				var rsa = new RSAKey();
				rsa.setPublic($(closure).data("autocrypt_modulus"), $(closure).data("autocrypt_e"));
		
				var hash = CryptoJS.RIPEMD160(datablock).toString();
		
				var encryptedHash = rsa.encrypt(hash);
				if (!encryptedHash) throw "Error whilst encrypting the form data hash.";
		
				var passphraseHex = "";
				for (var i = 0; i < 96; i++) {
					var digit = Math.floor(Math.random() * 256);
					var hex = digit.toString(16);
		
					passphraseHex += ("00" + hex).slice(-2);
				}
		
				var salt = CryptoJS.lib.WordArray.random(128 / 8);
				var key128Bits = CryptoJS.PBKDF2(passphraseHex, salt, { keySize: 128 / 32, iterations: 1000 });
				var iv = CryptoJS.lib.WordArray.random(128 / 8);
		
				var encryptedKey = rsa.encrypt(key128Bits.toString(CryptoJS.enc.Hex));
				if (!encryptedKey) throw "Error whilst encrypting the passphrase key.";
		
				var encryptedIV = rsa.encrypt(iv.toString(CryptoJS.enc.Hex));
				if (!encryptedIV) throw "Error whilst encrypting the passphrase IV.";
		
				var ciphertext = CryptoJS.AES.encrypt(datablock, key128Bits, {
					"keySize": 128 / 8,
					"iv": iv,
					"mode": CryptoJS.mode.CBC,
					"padding": CryptoJS.pad.ZeroPadding
				}).ciphertext.toString(CryptoJS.enc.Hex);
		
				submitdata["hash"] = encryptedHash;
				submitdata["key"] = encryptedKey;
				submitdata["iv"] = encryptedIV;
				submitdata["ciphertext"] = ciphertext;
			} catch (exception) {
				alert(exception + " The form has not been submitted.");
				return;
			} finally {
				$(closure).removeClass("encrypting");
			}
	
			$(closure).addClass("submitting");
	
			$.ajax({
				"type": formmethod,
				"url": submiturl,
				"data": submitdata,
				"dataType": "json",
				"error": function(jqXHR, textStatus, errorThrown) {
					alert("Error submitting: " + textStatus + ", "+ errorThrown + ". The form has not been submitted.");
					$(closure).removeClass("submitting");
				},
				"success": function(result) {
					try {
						if (result.outcome === undefined || result.outcome === null) throw "Result did not return an outcome.";
						if (result.outcome === "error") throw "Error: " + result["message"];
						if (result.outcome !== "success") throw "Unknown outcome: " + results.outcome;
	
						$(closure).addClass("redirecting");
						window.location.href = submiturl + (result.passthru !== undefined ? ("?" + result.passthru) : "");
					} catch(exception) {
						alert(exception + " The form was submitted but may not have been processed.");
						return false;
					} finally {
						$(closure).removeClass("submitting");
					}
				}
			});
			
			return false;
		}, 100);
		
		return false;	// always prevent the form from submitting normally.
	});
});

});
