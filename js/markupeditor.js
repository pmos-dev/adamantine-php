/**
 * MarkupEditor
 */

$(document).ready(function() {

$("div.markupeditor > ul.toolbar > li a.icon").click(function() {
	return false;
});

$("body").on("click", ".ui-widget-content div.preview a", function() {
	// disable links in the preview window so as to not abandon the edit itself
	alert("Links have been disabled in preview to prevent you losing your edits!");
	return false;
});

$.fn.markupGetEditor = function() {
	return $(this).closest("div.markupeditor").find("textarea.editor");
};

$.fn.markupGetPreview = function() {
	return $(this).closest("div.markupeditor").find("div.preview");
};

$.fn.markupWrapSelection = function(tag, params) {
	var additional = "";
	if (params !== undefined) additional = params;
	
	var editor = $(this).markupGetEditor();
	$(editor).selection("insert", { text: "[" + tag + additional + "]", mode: "before" });
	$(editor).selection("insert", { text: "[/" + tag + "]", mode: "after" });
};

$.fn.markupListSelection = function(style) {
	var editor = $(this).markupGetEditor();
	var content = $(editor).selection("get");
	
	var items = content.trim().split("\n");
	if (items.length === 1 && items[0] === "") {
		$(editor).selection("replace", { text: style + " ", caret: "end" });
	} else {
		var replace = [];
		$.each(items, function(i, item) {
			replace.push(style + " " + item);
		});
		$(editor).selection("replace", { text: replace.join("\n"), caret: "end" });
	}
};

$("div.markupeditor > ul.toolbar > li a.icon.bold").click(function() {
	$(this).markupWrapSelection("b");
});
$("div.markupeditor > ul.toolbar > li a.icon.italic").click(function() {
	$(this).markupWrapSelection("i");
});
$("div.markupeditor > ul.toolbar > li a.icon.h4").click(function() {
	$(this).markupWrapSelection("h4");
});
$("div.markupeditor > ul.toolbar > li a.icon.quote").click(function() {
	$(this).markupWrapSelection("quote");
});

$("div.markupeditor > ul.toolbar > li a.icon.bullet").click(function() {
	$(this).markupListSelection("*");
});
$("div.markupeditor > ul.toolbar > li a.icon.numbered").click(function() {
	$(this).markupListSelection("#");
});

$("div.markupeditor > ul.toolbar > li a.icon.url").click(function() {
	var editor = $(this).markupGetEditor();
	var content = $(editor).selection("get");

	if (content.match(/^http(s?):\/\//i)) {
		$(editor).selection("replace", { text: "[url]" + content + "[/url]", caret: "keep" });
		return;
	}
	if (content.match(/^www./i)) {
		$(editor).selection("replace", { text: "[url]http://" + content + "[/url]", caret: "keep" });
		return;
	}
	
	url = prompt("Enter URL to link to");
	if (url === null) return false;
	if (url.match(/^www./i)) url = "http://" + url;
	
	if (content === "") $(editor).selection("replace", { text: "[url]" + url + "[/url]", caret: "keep" });
	else $(this).markupWrapSelection("url", "=" + url);
});

$("div.markupeditor > ul.toolbar > li a.icon.email").click(function() {
	var editor = $(this).markupGetEditor();
	var content = $(editor).selection("get");

	if (content.match(/^[-a-zA-Z0-9._%+']{1,64}@([-a-zA-Z0-9]{1,64}\.)*[-a-zA-Z]{2,12}$/)) {
		$(editor).selection("replace", { text: "[email]" + content + "[/email]", caret: "keep" });
		return;
	}
	
	email = prompt("Enter email address to link to");
	if (email === null) return false;
	
	if (content === "") $(editor).selection("replace", { text: "[email]" + email + "[/email]", caret: "keep" });
	else $(this).markupWrapSelection("email", "=" + email);
});

$("div.markupeditor > ul.toolbar > li a.icon.table").click(function() {
	var editor = $(this).markupGetEditor();
	$(editor).selection("insert", { text: "[table=column 1 | column 2 | column 3]\r\nrow 1 col 1 | row 1 col 2 | row 1 col 3\r\nrow 2 col 1 | row 2 col 2 | row 2 col 3\r\nrow 3 col 1 | row 3 col 2 | row 3 col 3\r\n[/table]", mode: "after" }); 
});
		
$("div.markupeditor > ul.toolbar > li a.icon.preview").click(function() {
	var editor = $(this).markupGetEditor();
	var content = $(editor).val();
	var preview = $(this).markupGetPreview();
	
	$.getJSON(ADAMANTINE_ROOT_PATH + "ajax/render_markup.php", {
		"markup": content
	}, function(data) {
		if (data.outcome == "error") alert("Error: " + data.message);
		else if (data.outcome == "success") {
			$(preview).empty().append("<div><div></div></div>").children("div").children("div").addClass("preview").append(data.result).parent().dialog({
				"title": "Markup preview",
				"modal": true,
				"width": 640,
				"height": 480
			});
		}
	});
});

});