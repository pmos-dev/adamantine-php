/**
 * Adamantine jQuery: table autoscrolling
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */

$(document).ready(function() {

$(window).on("load", function() {

$("body").append("<div id=\"scrollbarsize\" style=\"display:none;\"></div>");
$("#scrollbarsize").css({
	"position": "absolute", "top": 0, "left": 0, "overflow": "scroll"
}).append("<div style=\"width:100px;height:100px;background:white;\"></div>");
var SCROLLBAR_SIZE = $("#scrollbarsize").width() - 100;

$("table.autoscroll").wrap("<div></div>");
$("table.autoscroll").parent("div").addClass("autoscroll");

$("div.autoscroll").each(function(j) {
	$(this).css({ "width": $(this).children("table.autoscroll").outerWidth() + SCROLLBAR_SIZE });

	$(this).before("<table cellspacing=0 cellpadding=0></table>");
	var header_clone = $(this).prev("table");

	$(header_clone).addClass("unitable nobottomborder").append($(this).children("table.autoscroll").children("thead").clone());

	var body_table = $(this).children("table.autoscroll");
	$(body_table).children("thead").find("th").each(function(i) {
		$(header_clone).children("thead").find("th:eq(" + i + ")").css({ "width": $(this).width() });
		$(body_table).children("tbody").find("td:eq(" + i + ")").css({ "width": $(this).width() });
	});

	$(body_table).addClass("notopborder").children("thead").remove();
	
	var padding = $(body_table).children("tfoot.none").find("td").innerWidth() - $(body_table).children("tfoot.none").find("td").width();
	$(body_table).children("tfoot.none").find("td").css({ "width": $(header_clone).children("thead").width() - padding });
});

});

});
