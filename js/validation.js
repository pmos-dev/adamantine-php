/**
 * Adamantine jQuery: form input validation
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */

$(document).ready(function() {

$("form").on("change", "input[type=text].validate.time", function(e) {
	var pattern = "^([0-9]{1,2})([0-9]{2})$";
	var regex = new RegExp(pattern, "i");
	var array = regex.exec($(this).val());
	if (array != null) {
		if (array[1].length == 1) array[1] = "0" + "" + array[1];
		$(this).val(array[1] + ":" + array[2]);
	}
});

$.fn.validate_error = function() {
	$(this).addClass("validate_error");
	$(this).prev("label").addClass("validate_error");
	
	$(this).closest("form").data("validated", false);
	
	var search = $(this).parentsUntil($(this).closest(".jqueryuitabs", "div")).last();
	if (search.length == 0) return;
	
	var id = search.attr("id");
	$(this).closest(".jqueryuitabs").children("ul.ui-tabs-nav").find("a").each(function(i) {
		if ($(this).attr("href") == "#" + id) { $(this).find("span").addClass("validate_error"); return false; }
	});
};
	
$("form").submit(function() {

	$(this).find(".validate_error").removeClass("validate_error");
		
	$(this).data("validated", true).find(".validate").each(function(i) {
		var optional = $(this).hasClass("optional");

		if ($(this).hasClass("file")) {
			if (optional) return true;

			if ($(this).val() === undefined || $(this).val() === null || $(this).val() === "") $(this).validate_error();
			return true;
		}

		var tag = $(this)[0].tagName;

		if ($(this).attr("type") == "radio") {
			if (optional) return true;
			
			var name = $(this).attr("name");
			if ($("input[type=radio][name=" + name + "]:checked").length > 0) return true;
			$(this).validate_error(); return true;
		}
		
		switch (tag.toLowerCase()) {
			case "input":
			case "textarea":
				var val = $(this).val().trim();
				$(this).val(val);
				
				if (val == "" && optional) return true;
				
				if ($(this).hasClass("datetime") && !val.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}\s+[0-9]{2}:[0-9]{2}$/)) { $(this).validate_error(); return true; }
				if ($(this).hasClass("date") && !val.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) { $(this).validate_error(); return true; }
				if ($(this).hasClass("time") && !val.match(/^[0-9]{2}:[0-9]{2}$/)) { $(this).validate_error(); return true; }
	
				if ($(this).hasClass("string") && !val.match(/^.{1,255}$/)) { $(this).validate_error(); return true; }
				if ($(this).hasClass("text") && (val == "")) { $(this).validate_error(); return true; }

				if ($(this).hasClass("idname") && !val.match(/^[a-zA-Z0-9'#]([-a-zA-Z0-9'_#. ]{0,126}[-a-zA-Z0-9'_#.])?$/)) { $(this).validate_error(); return true; }
				if ($(this).hasClass("email") && !val.match(/^[-a-zA-Z0-9._%+']{1,64}@([-a-zA-Z0-9]{1,64}\.)*[-a-zA-Z]{2,12}$/)) { $(this).validate_error(); return true; }
				if ($(this).hasClass("phone") && !val.match(/^[+()0-9#*]?[- +()0-9#*]*[0-9#*]$/)) { $(this).validate_error(); return true; }
				if ($(this).hasClass("url") && !val.match(/^http(s)?:\/\/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62}){0,32}(:[0-9]+)?(\/[-a-zA-Z0-9\/._?&=!~*'():#\[\]@$+,;%]{0,940})?$/)) { $(this).validate_error(); return true; }
				if ($(this).hasClass("hexcolor") && !val.match(/^[-a-fA-F0-9]{6}$/)) { $(this).validate_error(); return true; }
				if ($(this).hasClass("letterdigit") && !val.match(/^[A-Z0-9]$/)) { $(this).validate_error(); return true; }

				if ($(this).hasClass("tinyint")) {
					if (!val.match(/^-?[0-9]{1,3}$/)) { $(this).validate_error(); return true; }
					if ($(this).hasClass("unsigned")) {
						if (val < 0) { $(this).validate_error(); return true; }
						if (val > 255) { $(this).validate_error(); return true; }
					} else {
						if (val < -128) { $(this).validate_error(); return true; }
						if (val > 127) { $(this).validate_error(); return true; }
					}
				}
				
				if ($(this).hasClass("smallint")) {
					if (!val.match(/^-?[0-9]{1,5}$/)) { $(this).validate_error(); return true; }
					if ($(this).hasClass("unsigned")) {
						if (val < 0) { $(this).validate_error(); return true; }
						if (val > 65535) { $(this).validate_error(); return true; }
					} else {
						if (val < -32768) { $(this).validate_error(); return true; }
						if (val > 32767) { $(this).validate_error(); return true; }
					}
				}
				
				if ($(this).hasClass("int")) {
					if (!val.match(/^-?[0-9]{1,8}$/)) { $(this).validate_error(); return true; }
					if ($(this).hasClass("unsigned") && val < 0) { $(this).validate_error(); return true; }
				}
				
				if ($(this).hasClass("float")) {
					if (!val.match(/^-?[0-9]{1,8}(\.[0-9]{1,8})?$/)) { $(this).validate_error(); return true; }
					if ($(this).hasClass("unsigned") && val < 0) { $(this).validate_error(); return true; }
				}
				
				if ($(this).hasClass("price")) {
					if (!val.match(/^[0-9]{1,8}(\.[0-9]{1,2})?$/)) { $(this).validate_error(); return true; }
				}
				
				if ($(this).hasClass("geo")) {
					if (!val.match(/^-?[0-9]{1,8}(\.[0-9]{1,8})?,-?[0-9]{1,8}(\.[0-9]{1,8})?$/)) { $(this).validate_error(); return true; }
				}
				
				if ($(this).hasClass("bigint") && !val.match(/^[0-9]+$/)) { $(this).validate_error(); return true; }

				if (val == "") { $(this).validate_error(); return true; }
				return true;
			case "select":
				if ($(this).val() == "" && optional) return true;
				if ($(this).val() == "") { $(this).validate_error(); return true; }
				return true;
		}
	});
	
	if (!$(this).data("validated")) {
		alert("Whoops, you have entered some invalid data");
		return false;
	}
	
});
	
});
