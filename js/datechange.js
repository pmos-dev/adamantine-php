$(document).ready(function() {

$("form.simpledatechange,form.rangedatechange").hide();

$(".simplechangedate").click(function() {
	$(this).closest(".linkbar").prev("form.simpledatechange").slideDown();
	$(this).closest(".linkbar").hide();
	return false;
});
$(".rangechangedate").click(function() {
	$(this).closest(".linkbar").prev("form.rangedatechange").slideDown();
	$(this).closest(".linkbar").hide();
	return false;
});

$("form.simpledatechange,form.rangedatechange").find("input.cancel").click(function() {
	$(this).closest("form").slideUp("fast");
	$(this).closest("form").next("ul.linkbar").show();
	return false;	
});

$("form.simpledatechange,form.rangedatechange").find("input.date").datepicker({ 
	"dateFormat": "dd/mm/yy",
	"showOn": "button",
	"buttonImage": ADAMANTINE_ROOT_PATH + "css/icons/calendar_view_month.svg",
	"buttonImageOnly": true,
	"gotoCurrent": true
});

});
