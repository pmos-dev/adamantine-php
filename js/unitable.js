/**
 * Adamantine jQuery: universal table appearance
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */

$(document).ready(function() {

$("table.unitable").not(".simple").children("tbody").children("tr:odd").addClass("odd_row");

$("table.unitable").each(function(i) {
	if ($(this).children("tbody").children("tr").length == 0) {
		$(this).append("<tfoot class=\"none\"><tr><td colspan=" + $(this).children("thead").find("th").length + ">(none)</td></tr></tfoot>");
	}
});

});

$.fn.refactor_table = function() {
	$(this).filter(".unitable").not(".simple").each(function(i) {
		$(this).find(".blank").remove();
		$(this).find("tbody > tr:first-child > td.moveable").prepend("<img class=\"blank\" src=\"" + ADAMANTINE_ROOT_PATH + "/css/icons/bullet_white.svg\" /><img class=\"blank\" src=\"" + ADAMANTINE_ROOT_PATH + "/css/icons/bullet_white.svg\" />");
		$(this).find("tbody > tr:last-child > td.moveable").append("<img class=\"blank\" src=\"" + ADAMANTINE_ROOT_PATH + "/css/icons/bullet_white.svg\" /><img class=\"blank\" src=\"" + ADAMANTINE_ROOT_PATH + "/css/icons/bullet_white.svg\" />");

		$(this).find("tbody").find("tr").removeClass("odd_row");
		$(this).find("tbody").find("tr:odd").addClass("odd_row");
	});
};
