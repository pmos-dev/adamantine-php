/**
 * Adamantine jQuery: core screen functionality
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */

$(document).ready(function() {

$("ul#menubar").find("ul").hide();
$("ul#menubar").find("li").click(function(event) {
	event.stopPropagation();
	var level = $(this).closest("ul");
	if ($(this).children("ul:visible").length > 0) {
		$(level).find("ul").slideUp("fast");
	} else {
		$(level).find("ul").hide();
		$(this).children("ul").slideDown("fast");
	}
});

$(document).click(function() {
	$("#menubar").find("ul").hide();
});

$("div#burger").click(function() {
	$("ul#menubar").slideToggle();
});

$("body").on("click", "a.confirm", function() {
	var message = "Confirm you want to do this?";
	if ($(this).data("confirm_message") != null) message = $(this).data("confirm_message");
	
	if (!confirm(message)) return false;
	return true;
});

$.fn.disableLink = function() {
	$(this).css({
		"opacity" : "0.5",
		"cursor": "default",
		"text-decoration": "none"
	}).off("click").on("click", function() {
		return false;
	});
};

});
