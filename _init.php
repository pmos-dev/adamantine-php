<?php
/**
 * Bootstrap for instantiating the Adamantine framework.
 * 
 * Requires the APP_ROOT_PATH global constant to have been previously defined.
 * Requires the file APP_ROOT_PATH . "config/config.php" to define further locations, such as ADAMANTINE_ROOT_PATH and ABSTRACTION_ROOT_PATH.
 * 
 * @copyright 2013 Pete Morris
 * @license MIT
 * @license LGPL
 * @author Pete Morris
 */
namespace Adamantine;

if (!defined("APP_ROOT_PATH")) die();	// check the script isn't being called directly

// Have to disable for PHP 8.1 upwards as the number of deprecation errors is huge
// error_reporting(E_ALL | E_NOTICE | E_STRICT);

require_once APP_ROOT_PATH . "config/config.php";

use \_APPLICATION_NAMESPACE_\Config as Config;

if (!defined("ADAMANTINE_ROOT_PATH")) die("ADAMANTINE_ROOT_PATH has not been set");
if (!defined("ABSTRACTION_ROOT_PATH")) die("ABSTRACTION_ROOT_PATH has not been set");

require_once ABSTRACTION_ROOT_PATH . "_init.php";

use \Abstraction as Abstraction;
use \Abstraction\Data as Data;
use \Abstraction\Database as Database;
use \Abstraction\Renderer as Renderer;

$_HTTP = new Renderer\HTTP\HTTP();
$_JSON = new Renderer\JSON\JSON();
$_REST = new Renderer\REST\REST();
$_HTML = new Renderer\HTMLJQuery\HTML();

require_once ADAMANTINE_ROOT_PATH . "core.php";

require_once ADAMANTINE_ROOT_PATH . "ui/ui.php";

require_once ADAMANTINE_ROOT_PATH . "editors/markupeditor.php";

$_UI_CLASSNAME = __NAMESPACE__ . "\UI\UI";
if (defined("CUSTOM_UI_PATH")) {
	require_once CUSTOM_UI_PATH;

	$_UI_CLASSNAME = "\_APPLICATION_NAMESPACE_\UI\UI";
}
$_UI = new $_UI_CLASSNAME($_HTML);

$_UI->add_includes(ADAMANTINE_ROOT_PATH . "js/", ADAMANTINE_ROOT_PATH . "css/");

function error($message, $append = null) {
	global $_ERROR_RENDERER_OVERRIDE, $_UI;

	if (isset($_ERROR_RENDERER_OVERRIDE)) $_ERROR_RENDERER_OVERRIDE->error($message, $append);
	else $_UI->error($message, $append);
}

try {
	$driver = "mysql";
	if (defined("_APPLICATION_NAMESPACE_\\Config\\DATABASE_DRIVER")) $driver = Config\DATABASE_DRIVER;
	$_PDO = new \PDO("{$driver}:dbname=" . Config\DATABASE_NAME . ";host=" . Config\DATABASE_HOST, Config\DATABASE_USERNAME, Config\DATABASE_PASSWORD);
} catch(\PDOException $e) {
	error("Unable to establish PDO connection to database: " . $e->getMessage());
}

$_DATABASE = new Database\Wrapper($_PDO);

if (!defined("SKIP_BUILT_TEST") || !SKIP_BUILT_TEST) try {
	$_DATABASE->query_params("
		SELECT *
		FROM " . $_DATABASE->delimit(Config\DATABASE_PREFIX . "installeds") . "
		LIMIT 1
	");
} catch(Database\Exception $e) {
	$_UI->set_titles("System error", Config\DEFAULT_SUBTITLE);
	error("System does not appear to have been built yet.");
}

require_once ADAMANTINE_ROOT_PATH . "models/repository.php";
$_REPOSITORY = new Models\Repository();

require_once ADAMANTINE_ROOT_PATH . "models/area.php";
require_once ADAMANTINE_ROOT_PATH . "models/instance.php";
require_once ADAMANTINE_ROOT_PATH . "models/installed.php";
require_once ADAMANTINE_ROOT_PATH . "models/user.php";
require_once ADAMANTINE_ROOT_PATH . "models/group.php";
require_once ADAMANTINE_ROOT_PATH . "models/user_group.php";
require_once ADAMANTINE_ROOT_PATH . "models/user_installed.php";
require_once ADAMANTINE_ROOT_PATH . "models/group_installed.php";
require_once ADAMANTINE_ROOT_PATH . "models/variable.php";
require_once ADAMANTINE_ROOT_PATH . "models/setting.php";

$_AREA_CLASSNAME = __NAMESPACE__ . "\Models\Area";
$_INSTANCE_CLASSNAME = __NAMESPACE__ . "\Models\Instance";
$_INSTALLED_CLASSNAME = __NAMESPACE__ . "\Models\Installed";
$_USER_CLASSNAME = __NAMESPACE__ . "\Models\User";
$_GROUP_CLASSNAME = __NAMESPACE__ . "\Models\Group";
$_USER_GROUP_CLASSNAME = __NAMESPACE__ . "\Models\User_Group";
$_USER_INSTALLED_CLASSNAME = __NAMESPACE__ . "\Models\User_Installed";
$_GROUP_INSTALLED_CLASSNAME = __NAMESPACE__ . "\Models\Group_Installed";
$_VARIABLE_CLASSNAME = __NAMESPACE__ . "\Models\Variable";
$_SETTING_CLASSNAME = __NAMESPACE__ . "\Models\Setting";

if (defined("CUSTOM_MODEL_PATH")) {
	foreach (explode(",", "Area,Instance,Installed,User,Group,User_Group,User_Installed,Group_Installed,Variable,Setting") as $model) {
		if (file_exists(CUSTOM_MODEL_PATH . strtolower($model) . ".php")) {
			require_once CUSTOM_MODEL_PATH . strtolower($model) . ".php";
			$variable = "_" . strtoupper($model) . "_CLASSNAME";
			$$variable = "\_APPLICATION_NAMESPACE_\Models\\{$model}";
		}
	}
}

$_REPOSITORY->add_area("_core");

$_REPOSITORY->add("_core", "Area", $_AREA = new $_AREA_CLASSNAME($_DATABASE));
$_REPOSITORY->add("_core", "Instance", $_INSTANCE = new $_INSTANCE_CLASSNAME($_DATABASE));
$_REPOSITORY->add("_core", "Installed", $_INSTALLED = new $_INSTALLED_CLASSNAME($_DATABASE, $_AREA, $_INSTANCE));
$_REPOSITORY->add("_core", "User", $_USER = new $_USER_CLASSNAME($_DATABASE, $_INSTANCE));
$_REPOSITORY->add("_core", "Group", $_GROUP = new $_GROUP_CLASSNAME($_DATABASE, $_INSTANCE));
$_REPOSITORY->add("_core", "User_Group", $_USER_GROUP = new $_USER_GROUP_CLASSNAME($_DATABASE, $_USER, $_GROUP));
$_REPOSITORY->add("_core", "User_Installed", $_USER_INSTALLED = new $_USER_INSTALLED_CLASSNAME($_DATABASE, $_USER, $_INSTALLED));
$_REPOSITORY->add("_core", "Group_Installed", $_GROUP_INSTALLED = new $_GROUP_INSTALLED_CLASSNAME($_DATABASE, $_GROUP, $_INSTALLED));
$_REPOSITORY->add("_core", "Variable", $_VARIABLE = new $_VARIABLE_CLASSNAME($_DATABASE, $_AREA));
$_REPOSITORY->add("_core", "Setting", $_SETTING = new $_SETTING_CLASSNAME($_DATABASE, $_INSTANCE, $_AREA, $_INSTALLED, $_VARIABLE));

require_once ADAMANTINE_ROOT_PATH . "framework/system.php";

$_SYSTEM = new Framework\System($_DATABASE, $_AREA, $_INSTANCE, $_INSTALLED);
$_ACCESS = new Abstraction\Framework\Access($_DATABASE, $_SYSTEM, $_INSTANCE, $_INSTALLED, $_USER, $_GROUP, $_USER_GROUP, $_USER_INSTALLED, $_GROUP_INSTALLED);

require_once ADAMANTINE_ROOT_PATH . "framework/settings.php";

$_SETTINGS = new Framework\Settings($_SYSTEM, $_INSTALLED, $_VARIABLE, $_SETTING);

require_once ADAMANTINE_ROOT_PATH . "models/log.php";

$_REPOSITORY->add("_core", "Log", $_LOG = new Models\Log($_DATABASE, $_AREA, $_INSTANCE, $_INSTALLED, $_USER));

require_once ADAMANTINE_ROOT_PATH . "framework/session.php";

$session_name = "adamantine";
if (defined("Config\SESSION_NAME")) $session_name = Config\SESSION_NAME;
$_SESSIONMANAGER = new Framework\Session($session_name, $_SYSTEM, $_ACCESS, $_AREA, $_INSTANCE, $_USER, $_LOG);

if (!defined("SKIP_SESSION_INITIALIZE") || !SKIP_SESSION_INITIALIZE) {
	if (!isset($_REQUEST["xsi"])) {
		$instance = null;
		if (defined("ALLOW_DEFAULT_INSTANCE") && ALLOW_DEFAULT_INSTANCE) {
			$instance = $_INSTANCE->get_by_name(Config\DEFAULT_INSTANCE_NAME);
		}
		
		if (null === $instance) error("No instance ID supplied in the url or POST variables. Stopping for safety");
		$_REQUEST["xsi"] = $instance["id"];
	}
	
	if (!Data\Data::validate_id($_REQUEST["xsi"])) error("Bad instance ID sent");
	if (null === ($xsi = $_INSTANCE->get($_REQUEST["xsi"]))) error("No such instance exists");
		
	$_SESSIONMANAGER->initialize($xsi);

	if ($xsi["status"] === Models\Instance::DISABLED) {
		$_UI->set_titles("(Instance disabled)", $xsi["description"]);
		error("This instance is currently disabled. Please try again later");
	}

	$_SETTINGS->initialize($xsi);
	
	$xau = $_SESSIONMANAGER->get_active_user();
	$_UI->set_user($xau);

	if ($xau["status"] === Models\User::DISABLED) {
		$_SESSIONMANAGER->logoff();
		error("Your account is currently disabled.");
	}
	
	$app_root_path = APP_ROOT_PATH;
	$adamantine_root_path = ADAMANTINE_ROOT_PATH;
	
	$_HTML->add_code("js", <<<JAVASCRIPT
var XSI = {$xsi["id"]};
var APP_ROOT_PATH = "{$app_root_path}";
var ADAMANTINE_ROOT_PATH = "{$adamantine_root_path}";

JAVASCRIPT
	);

	foreach ($_INSTALLED->list_ordered_by_firstclass($xsi) as $installed) {
		$area = $_AREA->get_by_id($installed["area"]);
		if ($_SESSIONMANAGER->has_access($area, Abstraction\Framework\Access::READ)) {
			require_once APP_ROOT_PATH . "areas/{$area["name"]}/_load.php";
		}
	}
	
	$system_menu = array(
		"Welcome page" => APP_ROOT_PATH . "index.php?xsi={$xsi["id"]}"
	);
	
	if ($_SESSIONMANAGER->is_guest()) $system_menu["Login"] = APP_ROOT_PATH . "login.php?xsi={$xsi["id"]}";
	else $system_menu["Logoff"] = APP_ROOT_PATH . "logoff_do.php?xsi={$xsi["id"]}";
	
	$_UI->set_system_menu($system_menu);
		
	if (!defined("SKIP_AREA_MENUS") || !SKIP_AREA_MENUS) {
		foreach ($_INSTALLED->list_ordered_by_firstclass($xsi) as $installed) {
			$area = $_AREA->get_by_id($installed["area"]);
			if ($_SESSIONMANAGER->has_access($area, Abstraction\Framework\Access::READ)) {
				require_once APP_ROOT_PATH . "areas/{$area["name"]}/_menus.php";
			}
		}
	}
}

// this is good practice to prevent subsequent code from accidentally picking up left-over values if it forgets to explicitly re-create the variable.
unset($installed);
unset($area);
